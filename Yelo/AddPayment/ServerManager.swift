//
//  ServerManager.swift
//  Liquid
//
//  Created by Gurwinder Singh on 12/10/19.
//  Copyright © 2019 Zoptal Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import CFNetwork
import Alamofire
import SwiftyJSON
import MobileCoreServices

typealias ServerSuccessCallBack = (_ json:JSON, _ statusCode:Int)->Void
typealias SocketComplitionBlock = (_ json:Any)->Void
typealias ServerFailureCallBack=(_ error:Error?)->Void
typealias ServerProgressCallBack = (_ progress:Double?) -> Void
typealias ServerNetworkConnectionCallBck = (_ reachable:Bool) -> Void

class ServerManager: NSObject {
    
    override init() {
        super.init()
    }
    //MARK: - Singlton -
    class var shared:ServerManager{
        struct  Singlton {
            static let instance = ServerManager()
        }
        return Singlton.instance
    }
    
    //MARK:- documentsDirectoryURL -
    lazy var documentsDirectoryURL : URL = {
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        return documents
    }()
    //MARK: - Background Manager -
    lazy var backgroundManager: Alamofire.SessionManager = {
        let bundleIdentifier = Bundle.main.bundleIdentifier
        let configure  = URLSessionConfiguration.background(withIdentifier: bundleIdentifier! + ".background")
        let session = Alamofire.SessionManager(configuration: URLSessionConfiguration.background(withIdentifier: bundleIdentifier! + ".background"))
        session.startRequestsImmediately = true
        return session
    }()
    var backgroundCompletionHandler: (() -> Void)? {
        get {
            return backgroundManager.backgroundCompletionHandler
        } set {
            backgroundManager.backgroundCompletionHandler = newValue
        }
    }
    
    //MARK: - Session Manager -
    private lazy var sessionManager: Alamofire.SessionManager = {
        let configure  = URLSessionConfiguration.default
        configure.timeoutIntervalForRequest = 30
        return Alamofire.SessionManager(configuration: configure)
    }()
    
    //MARK: - Check Network Connetion -
    func CheckNetwork() -> Bool {
        let is_net = Reachability.isConnectedToNetwork()
        if(is_net==true){
            return true;
        } else{
            return false;
        }
    }
    
    func checkNetworkConnetion(OnConnetion Completion:@escaping ServerNetworkConnectionCallBck){
        let manager = NetworkReachabilityManager(host: "www.apple.com")
        manager?.listener = { status in
            DispatchQueue.main.async {
                //print("status \(status)")
                switch status{
                case .reachable(.ethernetOrWiFi),.reachable(.wwan):
                    Completion(true)
                    break
                case .notReachable,.unknown:
                    manager?.stopListening()
                    Completion(false)
                    break
                }
            }
        }
        manager?.startListening()
    }
    
    //MARK: - Header -
    var appHeader:[String:String] = {
          if let authToken = UserDefaults.standard.object(forKey: kAuthToken) as? String {
              return ["AUTHENTICATIONTOKEN" : authToken]
          }else{
              return ["AUTHENTICATIONTOKEN" : ""]
          }
      }()

    //MARK:- Post -
    func Post(request url: String!,params: [String:Any]!,successHandler:ServerSuccessCallBack?,failureHandler:ServerFailureCallBack?){
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.result.value != nil {
                    do {
                        let json = try JSON(data: response.data!)
                        if (successHandler != nil){
                            if ((json.null) == nil){
                                if json["code"].intValue == 201 && json["result"].boolValue == false && json["message"].stringValue == "Wrong access token" {
      
                                    self.logout()
                                }else{
                                    successHandler!(json, (response.response?.statusCode)!)
                                }
                            }
                        }
                    } catch {
                        print("error")
                    }
                }
                break
            case .failure(_):
                if (failureHandler != nil){
                    failureHandler!(response.result.error!)
                }
                break
            }
        }
    }
    
    //MARK:- Put -
    func Put(request url: String!,params: [String:Any]!,successHandler:ServerSuccessCallBack?,failureHandler:ServerFailureCallBack?){
        Alamofire.request(url, method: .put, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.result.value != nil {
                    do {
                        let json = try JSON(data: response.data!)
                        if (successHandler != nil){
                            if ((json.null) == nil){
                                if json["code"].intValue == 201 && json["result"].boolValue == false && json["message"].stringValue == "Wrong access token" {
      
                                    self.logout()
                                }else{
                                    successHandler!(json, (response.response?.statusCode)!)
                                }
                            }
                        }
                    } catch {
                        print("error")
                    }
                }
                break
            case .failure(_):
                if (failureHandler != nil){
                    failureHandler!(response.result.error!)
                }
                break
            }
        }
    }

    //MARK:- Delete -
    func Delete(request url: String!,params: [String:Any]!,appHeader:[String:String]? = nil,successHandler:ServerSuccessCallBack?,failureHandler:ServerFailureCallBack?){
        Alamofire.request(url, method: .delete, parameters: params, encoding: URLEncoding.default, headers: appHeader).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    do {
                        let json = try JSON(data: response.data!)
                        if (successHandler != nil) {
                            if ((json.null) == nil) {
                                successHandler!(json, (response.response?.statusCode)!)
                            }
                        }
                    } catch {
                        print("error")
                    }
                }
                break
            case .failure(_):
                if (failureHandler != nil){
                    failureHandler!(response.result.error!)
                }
                break
            }
        }
    }

    //MARK:- Get -
    func get(url:String!,successHandler:ServerSuccessCallBack?, failureHandler:ServerFailureCallBack?) {
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    do {
                        let json = try JSON(data: response.data!)
                        if (successHandler != nil){
                            if ((json.null) == nil){
                                successHandler!(json, (response.response?.statusCode)!)
                            }
                        }
                    } catch {
                        print("error")
                    }
                }
                break
            case .failure(_):
                if (failureHandler != nil){
                    failureHandler!(response.result.error!)
                }
                break
            }
        }
    }

    //MARK:- UPLOAD MULTIPLE -
    func httpUpload(api:String! ,params:[String:Any]?, multipartObject:[MultipartData]?,successHandler:ServerSuccessCallBack?,failureHandler:ServerFailureCallBack?,progressHandler:ServerProgressCallBack?){
        if (multipartObject != nil) {
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                if let mediaList  = multipartObject{
                    for object in mediaList{
                        multipartFormData.append(object.media, withName: object.mediaUploadKey, fileName: object.fileName, mimeType: object.mimType)
                    }
                }
                if (params != nil){
                    for (key, value) in params! {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                    }
                }
            }, to: api,headers:nil, encodingCompletion: { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (Progress) in
                        if (progressHandler != nil){
                            progressHandler!(Progress.fractionCompleted)
                        }
                    })
                    upload.responseString(completionHandler: { (response) in
                        do {
                            let json = try JSON(data: response.data!)
                            successHandler!(json, (response.response?.statusCode)!)
                        } catch {
                            print("error")
                        }
                    })
                case .failure(let encodingError):
                    if (failureHandler != nil){
                        failureHandler!(encodingError as NSError )
                    }
                }
            })
        }else{
            self.Post(request: api, params: params, successHandler: successHandler, failureHandler: failureHandler)
        }
    }
    
    //MARK: - UPLOAD ONE SINGLE IMAGE -
    func httpUploadImage(apiUrl:String,parameter:[String:Any]?, appHeader:[String:String]?, image:UIImage ,withKey: String, successHandler:@escaping ServerSuccessCallBack,progressHandler:@escaping ServerProgressCallBack ,failureHandlers:@escaping ServerFailureCallBack) {
        let imageData = image.jpeg(.medium)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if (parameter != nil){
                for (key, value) in parameter! {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            multipartFormData.append(imageData!, withName: withKey, fileName: "\(Int64(NSDate().timeIntervalSince1970  * 1000))_image.jpg", mimeType: "image/jpeg")
            }
        }, to: apiUrl,headers:nil , encodingCompletion: { (result) in
            switch result {
            case .success(let upload, _,  _):
                upload.uploadProgress(closure:{ (progress) in
                    progressHandler(progress.fractionCompleted)
                })
                upload.responseJSON { response in
                    if response.result.value != nil {
                        do {
                            let json = try JSON(data: response.data!)
                            if ((json.null) == nil){
                                successHandler(json, (response.response?.statusCode)!)
                            }
                        } catch {
                            print("error")
                        }
                    }
                }
            case .failure( _): break
            }
        })
    }
    
    //MARK:- Download -
    func httpDownload(request api:String! ,enableBackGroundTask isBackground:Bool,successHandler:@escaping (_ result:Any)->Void?,failureHandler:ServerFailureCallBack?,progressHandler:ServerProgressCallBack?){
        let fileUrl  = URL(string: api)
        let request = URLRequest(url: fileUrl!)
        let destination: DownloadRequest.DownloadFileDestination = { filePath,response in
            let directory : NSURL = (self.documentsDirectoryURL as NSURL)
            let fileURL = directory.appendingPathComponent(response.suggestedFilename!)!
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        if  isBackground{
            backgroundManager.download(request, to: destination).response(completionHandler: { (response:DefaultDownloadResponse) in
                if response.error != nil{
                    if (failureHandler != nil){
                        failureHandler!(response.error! as NSError )
                    }
                }else{
                    if (response.destinationURL != nil){
                        successHandler(response.destinationURL!)
                    }
                }
            }).downloadProgress { (progress) in
                if (progressHandler != nil){
                    progressHandler!(progress.fractionCompleted)
                }
            }
        }else{
            Alamofire.download(request, to: destination).responseJSON { (response:DownloadResponse<Any>) in
                switch(response.result){
                case .success(_):
                    if response.result.value != nil{
                      //  print("response\(response)")
                        successHandler(response.destinationURL!)
                    }
                    break
                case .failure(_):
                    if (failureHandler != nil){
                        failureHandler!(response.result.error! )
                    }
                    break
                }
                }.downloadProgress { (progress) in
                    if (progressHandler != nil){
                        progressHandler!(progress.fractionCompleted)
                    }
            }
        }
    }

  
    //MARK: - Logout -
        func logout() {
            UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
    //        AppDelegate.sharedDelegate.setUpLogin()
        }
}

//MARK:- AppUtility -
class AppUtility:NSObject{
    class func setGradientColor(_ colors:[Any],view:UIView){
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = colors
        gradient.locations = [0.0, 0.25, 0.75, 1.0]
        view.layer.addSublayer(gradient)
    }
    //MARK:- mimeTypeForPath -
    class func mimeType(forPath filePath:URL)->String{
        var  mimeType:String;
        let fileExtension:CFString = filePath.pathExtension as CFString
        let UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, nil);
        let str = UTTypeCopyPreferredTagWithClass(UTI!.takeUnretainedValue(), kUTTagClassMIMEType);
        if (str == nil) {
            mimeType = "application/octet-stream";
        } else {
            mimeType = str!.takeUnretainedValue() as String
        }
        return mimeType
    }
    //MARK:- filename -
    class func filename(Prefix:String , fileExtension:String)-> String{
        let dateformatter=DateFormatter()
        dateformatter.dateFormat="MddyyHHmmss"
        let dateInStringFormated=dateformatter.string(from: Date() )
        return "\(Prefix)_\(dateInStringFormated).\(fileExtension)"
    }
    
    //MARK:-get JsonObjectDict -
    class func getJsonObjectDict(responseObject:Any)->[String:Any]{
        var anyObj :[String:Any]!
        do{
            anyObj = try (JSONSerialization.jsonObject(with: (responseObject as! NSData) as Data, options: []) as? [String:Any])
            return anyObj!
        } catch  {
        }
        return anyObj!
    }
    
    //MARK:- getJsonObjectArray -
    class func JSONArray(responseObject:Any)->[Any]{
        var anyObj :[Any] = [Any]()
        do{
            anyObj = try JSONSerialization.jsonObject(with: (responseObject as! NSData) as Data, options: []) as! [Any]
            return anyObj
            
        } catch  {
        }
        return anyObj
    }
    
    //MARK:- JSONStringify -
    class func JSONStringify(value: AnyObject, prettyPrinted: Bool = false) -> String{
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        if JSONSerialization.isValidJSONObject(value){
            if let data = try? JSONSerialization.data(withJSONObject: value, options: options){
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue){
                    return string as String
                }
            }
        }
        return ""
    }
    
    //MARK:- getJsonData -
    class func JSONData(responseObject:Any)->Data{
        var data :Data!
        do{
            data = try JSONSerialization.data(withJSONObject: responseObject, options: []) as Data
            return data!
            
        } catch  {
        }
        return data!
    }
  
}

//MARK: - Multipart Object Class -
class MultipartData: NSObject {
    var media:Data!
    var mediaUploadKey:String!
    var fileName:String!
    var mimType:String!
    
    override init(){ }
    
    init(medaiObject object:Any!,mediaKey uploadKey: String!,isPNG: Bool = true) {
        if (object != nil) , (uploadKey != nil) {
            if  object is UIImage {
                if let imageObject = object as? UIImage {
                    self.media =  imageObject.jpegData(compressionQuality: 0.3)
                    self.mimType = "image/png"
                    self.fileName = AppUtility.filename(Prefix: "image_drycleaner_\(UserDefaults.SFSDefault(valueForKey: "user_id"))", fileExtension: "jpeg")
                }
            }else{
                if  let filepath = object as? String{
                    let url = NSURL.fileURL(withPath: filepath)
                    self.fileName = url.lastPathComponent
                    self.media = try! Data(contentsOf: url) //NSData(contentsOf: url)
                    self.mimType = AppUtility.mimeType(forPath: url )
                }
            }
            self.mediaUploadKey = uploadKey
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        
        if let media = aDecoder.decodeObject(forKey: "mediaData") as? Data {
            self.media = media
        }
        
        if let mediaUploadKey = aDecoder.decodeObject(forKey: "mediaUploadKey") as? String {
            self.mediaUploadKey = mediaUploadKey
        }
        
        if let fileName = aDecoder.decodeObject(forKey: "fileName") as? String {
            self.fileName = fileName
        }
        
        if let mimType = aDecoder.decodeObject(forKey: "mimType") as? String {
            self.mimType = mimType
        }
    }
    open func encodeWithCoder(_ aCoder: NSCoder){
        
        if let media = self.media{
            aCoder.encode(media, forKey: "media")
        }
        
        if let mediaUploadKey = self.mediaUploadKey {
            aCoder.encode(mediaUploadKey, forKey: "mediaUploadKey")
        }
        
        if let fileName = self.fileName {
            aCoder.encode(fileName, forKey: "fileName")
        }
        
        if let mimType = self.mimType {
            aCoder.encode(mimType, forKey: "mimType")
        }
    }
}

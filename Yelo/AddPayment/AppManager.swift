//
//  AppManager.swift
//  Liquid
//
//  Created by Gurwinder Singh on 12/10/19.
//  Copyright © 2019 Zoptal Solutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

class AppManager: NSObject {
    //MARK: App Manager Shared Instance
    class var shareInstance:AppManager {
        struct Singleton {
            static let instance = AppManager()
        }
        return Singleton.instance
    }
    //MARK: - Left View to textfield -
    func setLeftViewOnTextField(textField: UITextField) -> Void {
        let paddingView = UIView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: 10.0, height: 20.0))
        textField.leftView = paddingView
        textField.leftViewMode = UITextField.ViewMode.always
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.smGreyBorderColor().cgColor
        textField.layer.cornerRadius = 5.0
    }
    
    func setLeftViewOnTextView(textView: UITextView) -> Void {
        textView.contentInset = .init(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.smGreyBorderColor().cgColor
        textView.layer.cornerRadius = 5.0
    }
    
    //MARK: - Shake textfield/textview -
    func shakeTextField(textField: UITextField, withText:String, currentText:String) -> Void {
        textField.text = ""
        textField.attributedPlaceholder = NSAttributedString(string: currentText,
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        let isSecured = textField.isSecureTextEntry
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.10
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint:CGPoint.init(x: textField.center.x - 10, y: textField.center.y) )
        animation.toValue = NSValue(cgPoint: CGPoint.init(x: textField.center.x + 10, y: textField.center.y) )
        textField.layer.add(animation, forKey: "position")
        if isSecured {
            textField.isSecureTextEntry = false
        }
        textField.attributedPlaceholder = NSAttributedString(string: withText,
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
            textField.attributedPlaceholder = NSAttributedString(string: currentText,
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            //  textField.placeholder = placeholder
            if isSecured {
                textField.isSecureTextEntry = true
            }
        }
    }
    
    
    func shakeTextView(textView: UITextView
        , withText:String, currentText:String) -> Void {
        textView.text = currentText
        textView.textColor =  UIColor.darkText
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.10
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint:CGPoint.init(x: textView.center.x - 10, y: textView.center.y) )
        animation.toValue = NSValue(cgPoint: CGPoint.init(x: textView.center.x + 10, y: textView.center.y) )
        textView.layer.add(animation, forKey: "position")
        textView.text = withText
        textView.textColor =  UIColor.red
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
            textView.text = currentText
            textView.textColor = UIColor.darkText
            //  textField.placeholder = placeholder
        }
        
    }
    
    func shakeButton(button: UIButton){
        let shake:CABasicAnimation = CABasicAnimation(keyPath: "position")
        shake.duration = 0.10
        shake.repeatCount = 2
        shake.autoreverses = true
        let from_point:CGPoint = CGPoint.init(x: button.center.x - 10, y: button.center.y )
        let from_value:NSValue = NSValue(cgPoint: from_point)
        let to_point:CGPoint = CGPoint.init(x: button.center.x + 10, y: button.center.y )
        let to_value:NSValue = NSValue(cgPoint: to_point)
        shake.fromValue = from_value
        shake.toValue = to_value
        button.layer.add(shake, forKey: "position")
    }
    
    //MARK: - Login/Sign Up Validations -
    func isValidEmail(testStr:String) -> Bool {
       // print("validate emilId: \(testStr)")
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    //  Phone Number validation
    func validatePhoneNumber(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    //  WhiteSpace Validation
    func containsWhiteSpace(textField: UITextField) -> Bool {
        if !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            // string contains non-whitespace characters
            return false
        }
        return true
    }
    
    class func checkMaxLength(textField: UITextField!, maxLength: Int , range : NSRange) -> Bool {
        if (textField.text?.count)! >= maxLength && range.length == 0 {
            return false
        }else {
            return true
        }
    }
    
    //MARK:-  GET USER DETAILS -
    class func isLogin() -> Bool {
        return UserDefaults.SFSDefault(boolForKey: kIsLogin) ? true:false
    }

    class func getUserID() -> String {
       return UserDefaults.standard.object(forKey: kUserID) as! String
    }
    class func getUserToken() -> String {
        return UserDefaults.standard.object(forKey: kAccessToken) as! String
    }
    class func getReceiptData() -> String {
//   0     self.receiptValidation()
        let receiptPath = Bundle.main.appStoreReceiptURL?.path
        if FileManager.default.fileExists(atPath: receiptPath!){
            var receiptData:NSData?
            do{
                receiptData = try NSData(contentsOf: Bundle.main.appStoreReceiptURL!, options: NSData.ReadingOptions.alwaysMapped)
            } catch {
                //print("ERROR: " + error.localizedDescription)
            }
            let base64encodedReceipt = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn)
            return base64encodedReceipt!
        }
        return ""
    }
    class func receiptValidation() {
        let SUBSCRIPTION_SECRET = "604ada29339f494284534f4860457fbe"
        let receiptPath = Bundle.main.appStoreReceiptURL?.path
        if FileManager.default.fileExists(atPath: receiptPath!){
            var receiptData:NSData?
            do{
                receiptData = try NSData(contentsOf: Bundle.main.appStoreReceiptURL!, options: NSData.ReadingOptions.alwaysMapped)
            }
            catch{
                print("ERROR: " + error.localizedDescription)
            }
            //let receiptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let base64encodedReceipt = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn)
            
            print(base64encodedReceipt!)
            
            let requestDictionary = ["receipt-data":base64encodedReceipt!,"password":SUBSCRIPTION_SECRET]
            
            guard JSONSerialization.isValidJSONObject(requestDictionary) else {  print("requestDictionary is not valid JSON");  return }
            do {
                let requestData = try JSONSerialization.data(withJSONObject: requestDictionary)
                let validationURLString = "https://sandbox.itunes.apple.com/verifyReceipt"  // this works but as noted above it's best to use your own trusted server
                guard let validationURL = URL(string: validationURLString) else { print("the validation url could not be created, unlikely error"); return }
                let session = URLSession(configuration: URLSessionConfiguration.default)
                var request = URLRequest(url: validationURL)
                request.httpMethod = "POST"
                request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
                let task = session.uploadTask(with: request, from: requestData) { (data, response, error) in
                    if let data = data , error == nil {
                        do {
                            let appReceiptJSON = try JSONSerialization.jsonObject(with: data)
                            print("success. here is the json representation of the app receipt: \(appReceiptJSON)")
                            // if you are using your server this will be a json representation of whatever your server provided
                        } catch let error as NSError {
                            print("json serialization failed with error: \(error)")
                        }
                    } else {
                        print("the upload task returned an error: \(error ?? "" as! Error)")
                    }
                }
                task.resume()
            } catch let error as NSError {
                print("json serialization failed with error: \(error)")
            }
        }
    }
    class func getDeviceDimention() -> String {
        switch UIDevice.current.modelName {
        case "iPhone 4" ,
             "iPhone 4s",
             "iPhone 5" ,
             "iPhone 5c",
             "iPhone 5s",
             "iPhone 6" ,
             "iPhone 6s",
             "iPhone 7" ,
             "iPhone SE",
             "iPhone 8" ,
             "iPhone XR"
            : return "xhdpi"
        default
            : return "xxxhdpi"
        }
    }
    class func getAppVersion() -> String {
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        return appVersion ?? "0.1"
    }
    
    class func getDeviceToken() -> String {
        if let deviceToken = UserDefaults.standard.value(forKey: kDeviceToken) as? String {
            if deviceToken.count > 0 {
                return deviceToken
            }
            else{
                return "12S12S1FSD21F2132DS1F"
            }
        }else{
            return "12S12S1FSD21F2132DS1F"
        }
    }
    
    
    class func showAlertLogout(viewControler : UIViewController, title:String? = kAlertTitle , message:String?, completion: (() -> Swift.Void)? = nil){
           _ =  UIAlertController.showAlertInViewController(viewController: viewControler, withTitle: title, message: message, cancelButtonTitle: "OK", destructiveButtonTitle: nil, otherButtonTitles: nil) { (alert, acrtion, btnIndex) in
               if (completion != nil){
                   completion!()
               }
           }
    }
    
    class func showAlert(viewControler : UIViewController , message : String) {
           _ = UIAlertController.showAlertInViewController(viewController: viewControler, withTitle: kAlertTitle, message: message, cancelButtonTitle: "OK", destructiveButtonTitle: nil, otherButtonTitles: nil) { (controller, action , buttonIndex) in
           }
    }
    class func dayNameFromID(dayID : Int) -> String {
        switch dayID {
        case 1:
            return "Sunday"
        case 2:
            return "Monday"
        case 3:
            return "Tuesday"
        case 4:
            return "Wednesday"
        case 5:
            return "Thursday"
        case 6:
            return "Friday"
        case 7:
            return "Saturday"
        default:
            return ""
        }
    }
        
    class func getTimeZone() -> String {
        let timeZone = TimeZone.current
        let name = timeZone.identifier
        if name.count > 0 {
            return name
        }else{
            return ""
        }
    }
  
    
    func resizeImage(image : UIImage , targetSize : CGSize) -> UIImage{
        let originalSize:CGSize =  image.size
        
        let widthRatio :CGFloat = targetSize.width/originalSize.width
        let heightRatio :CGFloat = targetSize.height/originalSize.height
        
        var newSize : CGSize!
        if widthRatio > heightRatio {
            newSize =  CGSize(width : originalSize.width*heightRatio ,  height : originalSize.height * heightRatio)
        }
        else{
            newSize = CGSize(width : originalSize.width * widthRatio , height : originalSize.height*widthRatio)
        }
        
        // preparing rect for new image
        let rect:CGRect =  CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.main.scale)
        image .draw(in: rect)
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
        
    func countryName(from countryCode: String) -> String {
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            // Country name was found
            return name
        } else {
            // Country name cannot be found
            return countryCode
        }
    }
    
    //MARK:- Time Methods -
    
    class func currentTime() -> String {
        //Convert time in UTC to Local TimeZone
        let outputTimeZone = NSTimeZone.local
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.locale = Locale.init(identifier: "en_US_POSIX")
        outputDateFormatter.timeZone = outputTimeZone
        outputDateFormatter.dateFormat = "hh:mm a dd MMMM, yyyy"
        //outputDateFormatter.dateFormat = dateFormat
        let currentData = Date()
        let outputString = outputDateFormatter.string(from: currentData)
        return outputString
    }
    
    class func convertTimeStampLocalTimeZone(timeStamp : Double) -> String {
        if timeStamp > 0 {
            let timeInterval: TimeInterval = TimeInterval.init(timeStamp)
            let date = NSDate(timeIntervalSince1970: timeInterval)
            //Convert time in UTC to Local TimeZone
            let outputTimeZone = NSTimeZone.local
            let outputDateFormatter = DateFormatter()
            outputDateFormatter.locale = Locale.init(identifier: "en_US_POSIX")
            outputDateFormatter.timeZone = outputTimeZone
            outputDateFormatter.dateFormat = "hh:mm a dd MMMM, yyyy"
            //outputDateFormatter.dateFormat = dateFormat
            let outputString = outputDateFormatter.string(from: date as Date)
            return outputString
        }else{
            return AppManager.currentTime()
        }
        
        
    }
    
    
    
    class func getFormatedStringTimeFromDate(currentdate : Date )->String{
        let dateformatter = DateFormatter()
        dateformatter.locale = Locale.init(identifier: "en_US_POSIX")
        dateformatter.dateStyle = DateFormatter.Style.medium
        dateformatter.dateFormat="HH:mm"
        let dateInStringFormated=dateformatter.string(from: currentdate)
        return dateInStringFormated
    }
     
       class func convertTimeStampLocalTimeZone333(timeStamp : String) -> String {
             
        let timeInterval: TimeInterval = TimeInterval.init(timeStamp)!
                   let date = NSDate(timeIntervalSince1970: timeInterval)
                   //Convert time in UTC to Local TimeZone
                   let outputTimeZone = NSTimeZone.local
                   let outputDateFormatter = DateFormatter()
                   outputDateFormatter.locale = Locale.init(identifier: "en_US_POSIX")
                   outputDateFormatter.timeZone = outputTimeZone
               //outputDateFormatter.dateFormat = "hh:mm a dd MMMM, yyyy"
                   outputDateFormatter.dateFormat = "E, d MMM"
                   //outputDateFormatter.dateFormat = dateFormat
                   let outputString = outputDateFormatter.string(from: date as Date)
                   return outputString
               
               
               
           }
    class func getFormatedStringFromDate(currentdate : Date )->String{
        let dateformatter = DateFormatter()
        dateformatter.locale = Locale.init(identifier: "en_US_POSIX")
        dateformatter.dateStyle = DateFormatter.Style.medium
        dateformatter.dateFormat="yyyy-MM-dd"
        let dateInStringFormated=dateformatter.string(from: currentdate)
        return dateInStringFormated
    }
    
    class func getFormatedDateFromString(currentdate :String )-> Date {
        let dateformatter = DateFormatter()
        dateformatter.locale = Locale.init(identifier: "en_US_POSIX")
        dateformatter.dateStyle = DateFormatter.Style.medium
        dateformatter.dateFormat = "yyyy-MM-dd"
        let date = dateformatter.date(from: currentdate)
        if date == nil {
            return Date()
        }
        return date!
    }
    
    class func getCurrentDateString()-> String {
        let dateformatter = DateFormatter()
        dateformatter.locale = Locale.init(identifier: "en_US_POSIX")
        dateformatter.dateStyle = DateFormatter.Style.medium
        dateformatter.dateFormat = "yyyy-MM-dd"
        let date = dateformatter.string(from: Date())
        return date
    }
    /////////////////////////////////////////////////////
    class func getDateFromString(currentdate: String )->String {
        let componentArrayString : [String] = currentdate.components(separatedBy: ",")
        var str = componentArrayString[1]
        str.remove(at: str.startIndex)
        //print(str)
        let dateformatter = DateFormatter()
        dateformatter.locale = Locale.init(identifier: "en_US_POSIX")
        dateformatter.dateStyle = DateFormatter.Style.medium
        dateformatter.dateFormat="dd MMM yyyy"
        let date = dateformatter.date(from: str)
        let dateformatter1 = DateFormatter()
        dateformatter1.locale = Locale.init(identifier: "en_US_POSIX")
        dateformatter1.dateStyle = DateFormatter.Style.medium
        dateformatter1.dateFormat="yyyy-MM-dd"
        let dateInStringFormated=dateformatter1.string(from: date!)
        return dateInStringFormated
    }
    
    class func getDateInMonthFormat(currentdate: String )->String{
        let dateformatter = DateFormatter()
        dateformatter.locale = Locale.init(identifier: "en_US_POSIX")
        dateformatter.dateFormat="yyyy-MM-dd"
        let date = dateformatter.date(from: currentdate)
        let dateformatter1 = DateFormatter()
        dateformatter1.locale = Locale.init(identifier: "en_US_POSIX")
        dateformatter1.dateFormat="dd MMM yyyy"
        if date != nil {
            let dateInStringFormated=dateformatter1.string(from: date!)
            return dateInStringFormated
        }else{
            return currentdate
        }
    }
    
    
    
    class func calculateTimeAgo(startTimeStamp : Double ) -> String {
        let startDate = Date.init(timeIntervalSince1970: startTimeStamp)
        let endDate = Date()
        let outputTimeZone = NSTimeZone.local
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.locale = Locale.init(identifier: "en_US_POSIX")
        outputDateFormatter.timeZone = outputTimeZone
        outputDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let strDate1 =  outputDateFormatter.string(from: startDate as Date)
        let strDate2 =  outputDateFormatter.string(from: endDate)
        
        let startLocalDate = outputDateFormatter.date(from: strDate1)
        let endLocalDate = outputDateFormatter.date(from: strDate2)
        
        if startLocalDate != nil && endLocalDate != nil {
            
            var components = DateComponents()
            
            let calendar = NSCalendar.current
            let unitFlags = Set<Calendar.Component>([.day, .month, .year, .hour,.minute, .second ,.weekOfYear])
            components = calendar.dateComponents(unitFlags, from: startLocalDate! ,  to: endLocalDate!)
            let days = components.day!
            let hour = components.hour!
            let minutes = components.minute!
            let year = components.year!
            // let month = components.month!
            let second = components.second!
            let week = components.weekOfYear!
            
            if(year > 0){
                return "\(year)y ago"
            }
            else if(week > 0){
                return "\(week)w ago"
            }
            else if(days > 0){
                return "\(days)d ago"
            }
                
            else if(hour > 0){
                return "\(hour)h ago"
            }
                
            else if(minutes > 0){
                return "\(minutes)m ago"
            }
            else if(second > 0){
                return "\(second)s ago"
                
            }
            else{
                return "Just Now"
            }
        }else{
            return ""
        }
        
    }
    
    //MARK:- UIImageView Methods -
    class func calculateHeight(_ width: CGFloat , _ height: CGFloat , _ scaleWidth: CGFloat) -> CGFloat {
        let newHeight : CGFloat = (( height  / width ) * scaleWidth)
        return newHeight
    }

}

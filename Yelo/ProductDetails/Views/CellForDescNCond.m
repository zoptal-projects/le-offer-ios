//
//  CellForDescNCond.m

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForDescNCond.h"

@implementation CellForDescNCond

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.labelForText.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range)
    {
        NSString *urlstr = [@"https://"    stringByAppendingString:string];
        NSURL *url = [NSURL URLWithString:urlstr];
        if (![[UIApplication sharedApplication] openURL:url]) {
            [Helper showAlertWithTitle:nil Message:NSLocalizedString(invalidUrl, invalidUrl) viewController:self.productVC];
        }
    };

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

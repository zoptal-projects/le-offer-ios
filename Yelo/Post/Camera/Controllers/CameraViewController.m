#import "CameraViewController.h"
#import "TWPhotoPickerController.h"
#import <CoreMedia/CoreMedia.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "WebServiceConstants.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ProgressIndicator.h"
#import <SCRecorder/SCImageView.h>
#import "MultipleImagesCell.h"
#import "LibraryPhotosViewController.h"
#import "Cellable-Swift.h"

#define kVideoPreset AVCaptureSessionPresetHigh
#define mCoverImageInCollectionView    @"cover_image"
#define mCoverImageForMultipleImages   @"cover_image1"
#define mCollectionViewStoryboardID    @"collectionViewCellInCamera"
#define mFlashOnIcon                   @"flash_on"
#define mFlashOffIcon                  @"flash_off"



@interface CameraViewController ()<UIImagePickerControllerDelegate,UIGestureRecognizerDelegate,CLUploaderDelegate,SCRecorderDelegate,SCPlayerDelegate,SCVideoOverlay,SCRecorderToolsViewDelegate,UIAlertViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegate,LibraryScreenDelegate,WebServiceHandlerDelegate>
{
    BOOL cancelCamera;
    CLCloudinary *cloudinary;
    
    //screcorder
    SCRecordSession *_recordSession;
    SCRecordSession *sessionTosend;
    NSTimer *recordTimer;
    int duration;
    UIImage *thumbimg;
    NSMutableArray *arrayOfCapturedImages;
    
    NSString *statusChecking;
}

@end


@implementation CameraViewController
@synthesize PreviewLayer;
UIImage *image;
NSURL *outputURL;
NSString *videoPathUrl;
UIImage *videoThumbNailImage;
NSString *thumbnailImageforvideoPath;



/*-------------------------------------*/
#pragma mark
#pragma mark -ViewController LifeCycle
/*-------------------------------------*/

- (void)viewDidLoad
{
     // asdfasdfasdf
    [super viewDidLoad];
    self.photoButtonOutlet.selected = YES;
    statusChecking = @"0";
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}
-(void)connectcheckstatus
{
    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
    [HomePI showPIOnView:self.view withMessage:@"Stripe connectivity checking..."];
    
    NSDictionary *requestDict = @{mauthToken : [Helper userToken] };
    [WebServiceHandler getStripePost:requestDict andDelegate:self];
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    [self afterSuccess];
}
-(void)afterSuccess
{
    arrayOfCapturedImages = [NSMutableArray new];
    if(self.arrayOfChoosenImages!= nil)
        arrayOfCapturedImages = self.arrayOfChoosenImages;
    self.navigationController.navigationBarHidden = YES;
    [self checkCameraPermissionsStatus];
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if(_dismiss)
        [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.collectionViewForCapturedImages reloadData];
    
    if(arrayOfCapturedImages.count == 5 )
    {
        self.labelToShowAlert.hidden = NO;
        self.photoButtonOutlet.hidden = YES ;
    }
    else
    {
        self.labelToShowAlert.hidden = YES;
        self.photoButtonOutlet.hidden = NO;
    }
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    @try {
        [self.focusView.recorder removeObserver:self forKeyPath:@"isAdjustingFocus"];
        [_recorder unprepare];
        [_recorder stopRunning];
    }
    @catch (NSException *exception) {
    }
}

-(void)makingPreViewViewForCapture {
    CGRect jj = self.frameForCaptureImage.frame;
    jj.size.width= self.view.frame.size.width;
    jj.size.height= self.view.frame.size.height;
   self.frameForCaptureImage.frame = jj;
    
    UIView *previewView = self.frameForCaptureImage;
    _recorder.previewView = previewView;
    [previewView bringSubviewToFront:self.libraryButtonOutlet];
    [previewView bringSubviewToFront:self.photoButtonOutlet];
    
    if (![_recorder startRunning]) {
        NSLog(@"Something wrong there: %@", _recorder.error);
    }
}
-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:YES];
     self.navigationController.navigationBarHidden = NO;
    
    //if user clicks on next button after recording video then progressindicator will be hide.
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    
    //unhiding navbar of next screen.
    NSString *key = [[NSUserDefaults standardUserDefaults]valueForKey:kController];
    if ([key isEqualToString:@"Lib"]) {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:kController];
    }
    else
    _recorder.flashMode = SCFlashModeOff;
}


-(BOOL)prefersStatusBarHidden
{
    return YES ;
}





/*-------------------------------------*/
#pragma mark
#pragma mark -ButtonAction
/*-------------------------------------*/


- (IBAction)cancelButtonAction:(id)sender {
    if(_sellProduct){
        [self.tabBarController setSelectedIndex:0];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    if(_cameraAgain )
        [self.navigationController popViewControllerAnimated:YES];
    
    else {
        NSString *controllerType = [[NSUserDefaults standardUserDefaults]valueForKey:@"CameraControllerType"];
        if ([controllerType isEqualToString:@"directChaT"]) {
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"CameraControllerType"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            [self.tabBarController setSelectedIndex:0];
            
            return;
        }
    }
    [self dismissViewControllerAnimated:NO completion:nil];

}





- (IBAction)nextButtonAction:(id)sender {
    if (self.counter > 5 ) {
        ProgressIndicator *loginPI = [ProgressIndicator sharedInstance];
        [loginPI showPIOnView:self.view withMessage:@"processing.."];
        
        [_recorder pause:^{
            [self saveAndShowSession:_recorder.session];
        }];
    }
}

-(void)hideProgressIndicator {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
}

- (IBAction)takePhoto:(id)sender {

    NSString *checking = [[NSUserDefaults standardUserDefaults]objectForKey:mAccountId];
    NSLog(@"checking=%@",checking);
    if (checking.length != 0)
    {
         [self checkingVal];
    }
    else
    {
         [self connectcheckstatus];
    }
    

}
-(void)checkingVal
{
    self.photoButtonOutlet.enabled  = NO;
           NSInteger orientation = [[UIDevice currentDevice] orientation];
           [_recorder capturePhoto:^(NSError *error, UIImage *image) {
               if (image != nil) {
                   if (_recorder.device == AVCaptureDevicePositionBack)
                   {
                       
                       [self gotoImageEditor:image andDeviceOrientation:orientation];
                       
                   }
                   else
                   {
                       UIImage *flippedImage = [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationLeftMirrored];
                       [self gotoImageEditor:flippedImage andDeviceOrientation:orientation];
                   }}
               else {
               }
           }];
       double delayInSeconds = 1.0;
       dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
       dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
           self.photoButtonOutlet.enabled = YES;
       });
       
       if(arrayOfCapturedImages.count >=4 )
       {
           self.labelToShowAlert.hidden = NO;
           self.photoButtonOutlet.hidden = YES ;
       }
}

/*---------------------------------*/
#pragma mark - Video Capturing
/*---------------------------------*/

-(void)captureForVideo {
    
    self.viewWhenCameraPermissonDenied.hidden = YES;
    
    
    //it is creating screcorder to record the video.
    
    _recorder = [SCRecorder recorder];
    _recorder.captureSessionPreset = [SCRecorderTools bestCaptureSessionPresetCompatibleWithAllDevices];
    _recorder.device = AVCaptureDevicePositionBack;
    //maximum record session time is 60 secs and after 60 secs video will not record and saveAndShowSession
    _recorder.maxRecordDuration = CMTimeMake(60, 1);
    _recorder.delegate = self;
    _recorder.mirrorOnFrontCamera =  NO;
    _recorder.autoSetVideoOrientation = NO;
    // _recorder.photoConfiguration
    _recorder.videoConfiguration.sizeAsSquare = YES;
    _recorder.audioConfiguration.enabled = NO;
    _recorder.flashMode = SCFlashModeOff;
    
    if (!_recorder) {
         [Helper showAlertWithTitle:@"Error" Message:@"Camera not available" viewController:self];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        });
        return;
    }
    
    if (_recorder.device == AVCaptureDevicePositionFront) {
        [self.flashButtonOutlet setHidden:YES];
    }
    else
        [self.flashButtonOutlet setHidden:NO];
    _recorder.initializeSessionLazily = NO;
    
    NSError *error;
    if (![_recorder prepare:&error]) {
        NSLog(@"Prepare error: %@", error.localizedDescription);
    }
    
    
    _recorder.SCImageView.scaleAndResizeCIImageAutomatically = NO;
    _recorder.SCImageView.frame =CGRectMake(0, 0, 200, 200); //self.frameForCaptureImage.frame;
}


-(void)checkCameraPermissionsStatus {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    switch (status) {
        case AVAuthorizationStatusAuthorized:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                // authorized
                [self captureForVideo];
                [self makingPreViewViewForCapture];
            });
        }
            break;
        case AVAuthorizationStatusDenied:
        { // denied
            dispatch_async(dispatch_get_main_queue(), ^{
                self.viewWhenCameraPermissonDenied.hidden = NO;
                [self.view bringSubviewToFront:self.viewWhenCameraPermissonDenied];
            });
        }
            break;
        case AVAuthorizationStatusRestricted:
            break;
        case AVAuthorizationStatusNotDetermined:
        { // not determined
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(granted){ // Access has been granted ..do something
                        [self captureForVideo];
                        [self makingPreViewViewForCapture];
                    } else {
                        // Access denied ..do something
                        self.viewWhenCameraPermissonDenied.hidden = NO;
                        [self.view bringSubviewToFront:self.viewWhenCameraPermissonDenied];
                    }
                });
            }];
        }
            break;
        default:
            break;
    }
}

- (void) prepareCamera {
    //it will create new record session if any recorded video is not there.
    if (_recorder.session == nil) {
        SCRecordSession *session = [SCRecordSession recordSession];
        session.fileType = AVFileTypeQuickTimeMovie;
        _recorder.session = session;
    }
}

/*----------------------------------------*/
#pragma mark
#pragma mark screcorder delegate methods
/*----------------------------------------*/
- (void)recorderDidStartFocus:(SCRecorder *)recorder {
    [self.focusView showFocusAnimation];
}

- (void)recorderDidEndFocus:(SCRecorder *)recorder {
    [self.focusView hideFocusAnimation];
}

- (void)recorderWillStartFocus:(SCRecorder *)recorder {
    [self.focusView showFocusAnimation];
}

//this method will call after recording and after 60 secs (it depends on requirement).

- (void)recorder:(SCRecorder *)recorder didCompleteSession:(SCRecordSession *)recordSession {
    NSLog(@"didCompleteSession:");
    [self saveAndShowSession:recordSession];
}

- (void)saveAndShowSession:(SCRecordSession *)recordSession {
    [[SCRecordSessionManager sharedInstance] saveRecordSession:recordSession];
    _recordSession = recordSession;
    
    // Merge all the segments into one file using an AVAssetExportSession
    [recordSession mergeSegmentsUsingPreset:AVAssetExportPresetHighestQuality completionHandler:^(NSURL *url, NSError *error) {
        if (error == nil) {
            // Easily save to camera roll
            [url saveToCameraRollWithCompletion:^(NSString *path, NSError *saveError) {
                videoPathUrl = path;
                thumbimg = [self gettingThumbnailImage:path];
//                [self performSegueWithIdentifier:@"videoPLayersegue" sender:nil];
            }];
        }
        else {
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
            ;
        }
    }];
}

- (void)recorder:(SCRecorder *)recorder didInitializeAudioInSession:(SCRecordSession *)recordSession error:(NSError *)error {
    if (error == nil) {
        NSLog(@"Initialized audio in record session");
    } else {
        NSLog(@"Failed to initialize audio in record session: %@", error.localizedDescription);
    }
}

- (void)recorder:(SCRecorder *)recorder didInitializeVideoInSession:(SCRecordSession *)recordSession error:(NSError *)error {
    if (error == nil) {
        NSLog(@"Initialized video in record session");
    } else {
        NSLog(@"Failed to initialize video in record session: %@", error.localizedDescription);
    }
}

- (void)recorder:(SCRecorder *)recorder didBeginSegmentInSession:(SCRecordSession *)recordSession error:(NSError *)error {
    NSLog(@"Began record segment: %@", error);
}

- (void)recorder:(SCRecorder *)recorder didCompleteSegment:(SCRecordSessionSegment *)segment inSession:(SCRecordSession *)recordSession error:(NSError *)error {
    NSLog(@"Completed record segment at %@: %@ (frameRate: %f)", segment.url, error, segment.frameRate);
}


/*---------------------------------------------------*/
#pragma mark
#pragma mark - converting video to thumbnail image
/*---------------------------------------------------*/

-(UIImage *)gettingThumbnailImage :(NSString *)url {
    NSURL *videoURl = [NSURL fileURLWithPath:url];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURl options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    
    UIImage *img = [[UIImage alloc] initWithCGImage:imgRef];
    return img;
}


/*--------------------------------------------------------*/
#pragma mark
#pragma mark - Bottom Library,Video,Photo Button Actions.
/*--------------------------------------------------------*/


- (IBAction)libraryButtonAction:(id)sender {
 
    
    NSString *checking = [[NSUserDefaults standardUserDefaults]objectForKey:mAccountId];
      NSLog(@"checking=%@",checking);
      if (checking.length != 0)
      {
          [self getingfunc];
        
      }
     else
     {
          [self connectcheckstatus];
     }
    
    
//    if ([statusChecking isEqualToString:@"0"])
//    {
//       [self connectcheckstatus];
//    }
//    else if ([statusChecking isEqualToString:@"1"])
//    {
//
//    }
   
}
-(void)getingfunc
{
    // image picker from library.
       LibraryPhotosViewController *photoPicker = [self.storyboard instantiateViewControllerWithIdentifier:@"LibPhotosStoryboardID"];
       photoPicker.arrayOfCameraImages = arrayOfCapturedImages;
       photoPicker.addAgain = self.cameraAgain;
       photoPicker.cameraFromTab = !self.sellProduct ;
       photoPicker.libDelegate = self ;
       [UIView beginAnimations:@"View Flip" context:nil];
       [UIView setAnimationDuration: 0.50];
       [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
       [UIView setAnimationTransition: UIViewAnimationTransitionFlipFromRight forView: self.navigationController.view cache: NO];
       [self.navigationController pushViewController: photoPicker animated: YES];
       [UIView commitAnimations];
}

- (IBAction)photoButtonAction:(id)sender {
    
    self.libraryButtonOutlet.selected = NO;
    self.photoButtonOutlet.selected = YES;
}



/*-----------------------------*/
#pragma mark -
#pragma mark - ImageFilters
/*-----------------------------*/

-(void)gotoImageEditor:(UIImage *)data andDeviceOrientation:(NSInteger)orientation {
    
    UIImage *img = data;
    
    switch (orientation) {
        case 3:
            img = [UIImage imageWithCGImage:[data CGImage]scale:[data scale] orientation:UIImageOrientationUp];
            break;
        case 4:
            img = [UIImage imageWithCGImage:[data CGImage]scale:[data scale] orientation:UIImageOrientationDown];
            break;
        default:
            break;
    }
    
    NSString *imageName = [NSString stringWithFormat:@"%@%@.png",@"Image",[self getCurrentTime]];
    NSData *imgData1 = UIImageJPEGRepresentation(img, 0.6f);
    NSLog(@"0.7 size: %lu", (unsigned long)imgData1.length);
    //to get the image path.
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* imagePath = [documentsDirectory stringByAppendingPathComponent:imageName];
    [imgData1 writeToFile:imagePath atomically:NO];
    NSString *typeOfImage = @"storingImagePath";
    NSMutableDictionary  *dictForImagePath = [[NSMutableDictionary alloc] init];
    [dictForImagePath setValue:typeOfImage forKey:@"imageType"];
    [dictForImagePath setValue:imagePath forKey:@"imageValue"];
    [arrayOfCapturedImages addObject:dictForImagePath];
    [self.collectionViewForCapturedImages reloadData];
    
}

-(NSString*)getCurrentTime
{
    NSDate *currentDateTime = [NSDate date];
    
    // Instantiate a NSDateFormatter
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the dateFormatter format
    
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    
    // Get the date time in NSString
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    
    return dateInStringFormated;
    
}

- (void)dealloc {
    _recorder.previewView = nil;
}


- (void) turnTorchOn: (bool) on {
    
    // check if flashlight available
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash]){
            [device lockForConfiguration:nil];
            if (on) {
                //[device setTorchMode:AVCaptureTorchModeOn];
                [device setFlashMode:AVCaptureFlashModeOn];
                //torchIsOn = YES; //define as a variable/property if you need to know status
            } else {
                //  [device setTorchMode:AVCaptureTorchModeOff];
                [device setFlashMode:AVCaptureFlashModeOff];
                //torchIsOn = NO;
            }
            [device unlockForConfiguration];
        }
    }
}


- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

- (IBAction)enableCameraAccessButtonAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

/*-------------------------------------*/
#pragma mark
#pragma mark -Collectionview Datasource
/*-------------------------------------*/



-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(arrayOfCapturedImages.count>4)
        return 5;
    return arrayOfCapturedImages.count +1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    MultipleImagesCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:mCollectionViewStoryboardID forIndexPath:indexPath];
    
        if( indexPath.row == arrayOfCapturedImages.count)
        {
            if(arrayOfCapturedImages.count>0)
            {
                cell.imageViewContainer.image = [UIImage imageNamed:mCoverImageForMultipleImages];
            }
            else
            {
            cell.imageViewContainer.image = [UIImage imageNamed:mCoverImageInCollectionView];
            }
            cell.removeButton.hidden = YES ;
            return cell;
        }
    
        if([arrayOfCapturedImages[indexPath.row][@"imageType"] isEqualToString:@"cloudinaryUrl"])
        {
            [cell.imageViewContainer sd_setImageWithURL:arrayOfCapturedImages[indexPath.row][@"imageValue"]];
        }
        else{
    
        NSString *filePath = flStrForObj(arrayOfCapturedImages[indexPath.row][@"imageValue"]);
        NSData *imgData = [NSData dataWithContentsOfFile:filePath];
        cell.imageViewContainer.image  = [[UIImage alloc] initWithData:imgData];
         }
        [cell layoutIfNeeded];
        cell.removeButton.hidden = NO ;
        cell.removeButton.tag = indexPath.row ;
        [cell.removeButton addTarget:self action:@selector(removeImage:) forControlEvents:UIControlEventTouchUpInside];
        cell.imageViewContainer.layer.cornerRadius = 5;
        cell.imageViewContainer.clipsToBounds = YES;
        return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (arrayOfCapturedImages.count >0) {
        if (indexPath.row != arrayOfCapturedImages.count) {
            }
    }
}

/*-------------------------------------*/
#pragma mark
#pragma mark -Collectionview Delegate
/*-------------------------------------*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
        CGSize cellSize;
        cellSize.width = 65;
        cellSize.height = 70;
        return cellSize;
}

- (IBAction)doneButtonAction:(id)sender {
    
    if(_cameraAgain)
    {
    [self.cameraDelegate getMaintainedDataOfImages:arrayOfCapturedImages];
    [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        if (arrayOfCapturedImages.count >0) {
            PostListingsViewController *postScreen = [self.storyboard instantiateViewControllerWithIdentifier:mPostScreenStoryboardID];
            postScreen.arrayOfImagePaths = arrayOfCapturedImages;
            postScreen.cameraFromTabBar = !self.sellProduct;
            postScreen.callBackForCamera = ^(NSArray *arrayOfImagesFromCamera)
            {
            self.arrayOfChoosenImages = (NSMutableArray *)arrayOfImagesFromCamera ;
            };
            [self.navigationController pushViewController:postScreen animated:YES];
        }
        else {
            [Helper showAlertWithTitle:nil Message:NSLocalizedString(needAtleastOnePhoto, needAtleastOnePhoto) viewController:self];
        }
}
}

- (IBAction)flashButtonAction:(id)sender {
    if (self.flashButtonOutlet.selected) {
        [self turnTorchOn:NO];
        self.flashButtonOutlet.selected = NO;
    }
    else {
        [self turnTorchOn:YES];
        self.flashButtonOutlet.selected = YES;
    }

}

-(void)getMaintainedDataOfImagesFromLibrary:(NSMutableArray *)arrayOfmaintainedLibImages
{
    self.arrayOfChoosenImages = arrayOfmaintainedLibImages ;
}

/*-------------------------------------*/
#pragma mark
#pragma mark - RemoveImage Selector Method.
/*------------------------------------*/

-(void)removeImage:(id) sender
{
    UIButton *btn=(UIButton *)sender;
    NSInteger tag=btn.tag%1000;
    [arrayOfCapturedImages removeObjectAtIndex:tag];
    [self.collectionViewForCapturedImages reloadData];
    
    if(arrayOfCapturedImages.count < 5)
    {
        self.labelToShowAlert.hidden = YES;
        self.photoButtonOutlet.hidden = NO ;
    }
}

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (error) {
        
        return;
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    NSLog(@"response=%@",response);
    switch (requestType) {
        case RequestTypeGetStripePost:
        {
            
            //success response(200 is for success code).
            switch ([responseDict[@"code"] integerValue]) {
                case 200:
                {
                    //success response(200 is for success code).
                    switch ([responseDict[@"code"] integerValue])
                    {
                        case 200: {
                            
                             if ([responseDict[@"status"] integerValue] == 1 || [responseDict[@"message"] isEqualToString:@"User has already stripe account id."])
                             {
                                 [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[responseDict[@"data"] valueForKey:@"accountId"]] forKey:mAccountId];
                        
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                statusChecking = @"1";
                                 [self checkingVal];
                             }
                            else
                            {
                                statusChecking = @"0";
                                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(alertTitle, alertTitle) message:responseDict[@"message"] preferredStyle:UIAlertControllerStyleAlert];
                                                      
                                                        UIAlertAction *actionForOk = [UIAlertAction actionWithTitle:NSLocalizedString(alertOk, alertOk) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
                                                            
                                                         SetupStripeConnect *addNewAddressVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:mStripeConnectStoryboardID];
                                                         [ self.navigationController pushViewController :addNewAddressVC animated:YES];
                                                            
                                                        }];
                                                         [alertController addAction:actionForOk];
                                                      [self presentViewController:alertController animated:YES completion:nil];
                            }

                       
                        }
                            break;
                        default:
                            
                            [Helper showAlertWithTitle:NSLocalizedString(alertTitle, alertTitle) Message:responseDict[@"message"] viewController:self];
                            break;
                    }
                }
                    break;
                default:
                    
                    [Helper showAlertWithTitle:NSLocalizedString(alertTitle, alertTitle) Message:responseDict[@"message"] viewController:self];
                    break;
            }
        }
            break;
            
     
        case RequestTypeEditPost:{
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                [[NSNotificationCenter defaultCenter] postNotificationName:mUpdatePostDataNotification object:responseDict];
                ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                [pi hideProgressIndicator];
                [self dismissViewControllerAnimated:YES completion:nil];
                
            }
                break;
                
            default:
                
                [Helper showAlertWithTitle:NSLocalizedString(alertTitle, alertTitle) Message:responseDict[@"message"] viewController:self];
                break;
                
        }
    }
            break;
        case RequestTypeCloudinaryCredintials:  {
        
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                // success response.
                [[NSUserDefaults standardUserDefaults] setObject:responseDict forKey:cloudinartyDetails];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
                break;
                
            default:
                break;
        }
    }
            
            
        default:
            break;
    }
    
    
}
@end














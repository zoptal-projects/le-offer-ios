//
//  PriceTableViewCell.h
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostListingsViewController.h"

@interface PriceTableViewCell : UITableViewCell

@property (nonatomic,strong) Listings *listing ;
@property (nonatomic, strong)PostListingsViewController *refrenceVC ;
@property (weak, nonatomic) IBOutlet UILabel *priceTitle;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UILabel *currency;

@property (nonatomic)BOOL *isPriceTextFieldEnable ;

-(void)setPrice :(NSString *)price  currency :(NSString *)currency;

- (IBAction)currencyButtonAction:(id)sender;

- (IBAction)priceTextFieldValueChangedAction:(id)sender;


@end

//
//  AddressTableViewCell.m
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "AddressTableViewCell.h"
#import "PinAddressController.h"

@interface AddressTableViewCell()<GetCurrentLocationDelegate>
{
     GetCurrentLocation *getLocation;
}
@end

@implementation AddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    if(!self.isEditPost)
    {
    getLocation = [GetCurrentLocation sharedInstance];
    [getLocation getLocation];
    getLocation.delegate = self;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setAddress
{
       self.addressLabel.text = self.listing.address ;
    
     //  self.addressLabel.text = @"Mohali Location" ;
}

- (IBAction)removeAddressAction:(id)sender {
    
    self.addressLabel.text = NSLocalizedString(addLocationTitle, addLocationTitle);
    self.listing.address =  NSLocalizedString(addLocationTitle, addLocationTitle);
    self.removeAddressButton.hidden = YES;

}

- (IBAction)addAddressButtonAction:(id)sender {
    
//    self.listing.lattitude = 30.7046;
//    self.listing.longitude =  76.7179;
//    self.listing.address = [NSString stringWithFormat:@"%s, %s","Mohali" , "India"] ;
//    self.addressLabel.text = @"Mohali Location" ;
    
    PinAddressController *PinVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:mPinAddressStoryboardID];
    PinVC.navigationItem.title = NSLocalizedString(mNavTitleForPinAddress, mNavTitleForPinAddress);
    PinVC.lat = self.listing.lattitude;
    PinVC.longittude = self.listing.longitude;
    PinVC.callBackForLocation=^(NSDictionary *locDict)
    {
        self.listing.cityName = locDict[@"city"];
        self.listing.countrySName = locDict[@"countryShortName"];
        NSString *location = locDict[@"address"];

        if(location.length == 0){
            self.addressLabel.text = NSLocalizedString(addLocationTitle, addLocationTitle);
         self.removeAddressButton.hidden = YES;
        }
        else
        {
            NSString *countryName = [[NSLocale systemLocale] displayNameForKey:NSLocaleCountryCode value:self.listing.countrySName];
            self.listing.address = [NSString stringWithFormat:@"%@, %@",self.listing.cityName , countryName] ;
            self.addressLabel.text = self.listing.address ;
        }
        self.removeAddressButton.hidden = NO;
        self.listing.lattitude =  [locDict[@"lat"] doubleValue];
        self.listing.longitude = [locDict[@"long"] doubleValue];

    };
    [self.refrenceVC.navigationController pushViewController:PinVC animated:YES];
}

#pragma mark
#pragma mark - Location Delegate -

- (void)updatedLocation:(double)latitude and:(double)longitude
{
   self.listing.lattitude = latitude;
   self.listing.longitude = longitude;
    
//    self.listing.lattitude = 30.7046;
 ///  self.listing.longitude = 76.7179;
    
}

- (void)updatedAddress:(NSString *)currentAddress
{
    NSString * city = [getLocation.currentCity capitalizedString];
    NSString *countryName = [[NSLocale systemLocale] displayNameForKey:NSLocaleCountryCode value:getLocation.countryShortCode];
        self.listing.address = [NSString stringWithFormat:@"%@, %@",city , countryName];
        self.listing.cityName = getLocation.currentCity;
        self.listing.countrySName = getLocation.countryShortCode;
    
//
//    NSString * city = @"Mohali";
//    NSString *countryName = @"India";
//          self.listing.address = [NSString stringWithFormat:@"%@, %@",city , countryName];
//    self.listing.cityName =@"Mohali";
//    self.listing.countrySName = @"India";
}



@end

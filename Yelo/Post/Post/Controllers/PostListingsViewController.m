//
//  PostListingsViewController.m
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "PostListingsViewController.h"
#import "AddImagesTableViewCell.h"
#import "PostDescriptionTableViewCell.h"
#import "CategoryTableViewCell.h"
#import "PriceTableViewCell.h"
#import "AddressTableViewCell.h"
#import "ShareTableViewCell.h"
#import "PostingScreenModel.h"
#import "SuccessFullyPostedViewController.h"
#import "NegotiableTableViewCell.h"
#import "ExchangeListTableViewCell.h"

@import Firebase;
@interface PostListingsViewController ()<UITableViewDataSource, UITableViewDelegate, CloudinaryModelDelegate, WebServiceHandlerDelegate, successfullyPostedDelegate>
{
    UITextField *activeTextField;
    BOOL isPriceTextFieldEnable, showContestCell;
  
}
@end

@implementation PostListingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(!self.arrayOfImagePaths)
    {
        self.arrayOfImagePaths = [NSMutableArray new];
    }
    
     [self setNavigationBar];
    self.tapGestureOutlet.enabled = NO ;
    
    self.setCurrency = [NSString stringWithFormat:@" %@  %@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode],[[NSLocale currentLocale] objectForKey: NSLocaleCurrencySymbol]];
    
    self.listings = [[Listings alloc]init];
    self.listings.refrenceVC = self ;
    self.listings.currency = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
    self.listings.arrayOfImagePaths = self.arrayOfImagePaths;
    if(self.editingPost)
    {
        [self updateFieldsForPostedProduct];
    }
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self   name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)setNavigationBar
{
    self.navigationController.navigationBar.translucent = NO;
    [self createNavLeftButton];
    if(self.editingPost){
        self.navigationItem.title = NSLocalizedString(navTitleForEditPost, navTitleForEditPost);
        [self.postButtonOutlet setTitle:NSLocalizedString(updateButtonTitle, updateButtonTitle) forState:UIControlStateNormal];;
        
    }
    else
        self.navigationItem.title = NSLocalizedString(navTitleForPostProduct, navTitleForPostProduct);
}


- (void)createNavLeftButton {
    UIButton *navLeft = [[UIButton alloc]init];
    if(_editingPost)
    {
        navLeft = [CommonMethods createNavButtonsWithselectedState:mCloseButton normalState:mCloseButton];
    }
    else
    {
        navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
        [navLeft rotateButton];
    }
    
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,25,20)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}

- (void)navLeftButtonAction
{
    
    if(self.editingPost)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        if(self.callBackForCamera)
        {
            self.callBackForCamera(self.arrayOfImagePaths ) ;
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)postButtonAction:(id)sender {
    if([self.listings isListingDone])
    {
     [self createViewModelForListing];
    }
}

/**
 This method will create a model object of posting details, cloudinary details and upload edited post.
 */
-(void)createViewModelForListing
{
    if(self.editingPost)
    {
        ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
        [HomePI showPIOnView:self.view withMessage:@"Updating..."];
    }
    else
    {
        ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
        [HomePI showPIOnView:self.view withMessage:@"Posting..."];
        
    }
    
    PostingScreenModel *post = [[PostingScreenModel alloc]initWithListings:self.listings];
    
    post.editPost = self.editingPost;
    
    NSLog(@"post=%@",post);
    [PostingScreenModel sharedInstance].modelObj = post;
    post.modelObj = post;
    UploadToCloudinary *cloudinaryObj = [[UploadToCloudinary alloc]initWithArrayOfImagePaths:self.listings.arrayOfImagePaths];
    cloudinaryObj.delegate = self;
    [UploadToCloudinary sharedInstance].cloudObj = cloudinaryObj;
    [cloudinaryObj uploadingImageToCloudinaryWithArrayOfPaths];
    
}


#pragma mark - UITableView Delegate-


static CategoryTableViewCell * extracted(UITableView * _Nonnull tableView) {
    CategoryTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"categoryCell"];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(_editingPost)
    {
     return 9 ;
    }
    return 12;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
       
        case 0:
        {
            AddImagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addImagesCell"];
            cell.arrayOfImagePaths = self.arrayOfImagePaths ;
            cell.listing = self.listings ;// Model refrence
            cell.referenceVC = self;
            cell.titleTextField.text = self.listings.titleOfPost;

            return cell;
        }
            break;
        case 1:
        {   PostDescriptionTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"descriptionCell"];
            cell.listing = self.listings ;
            cell.refrenceVC = self ;
            cell.textViewOutletForDescrption.text = self.listings.descriptionForPost ;
            return cell;
        }
            break;
        case 2:
        {
            CategoryTableViewCell * cell = extracted(tableView);
            cell.referenceVC = self;
            cell.listing = self.listings;
            [cell setTitle:NSLocalizedString(navTitleForProductCategory, navTitleForProductCategory) andValue:self.listings.category];
            return cell;
        }
            break;
        case 3:
        {
            CategoryTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"categoryCell"];
            cell.referenceVC = self;
            cell.listing = self.listings;
             [cell setTitle:NSLocalizedString(@"Conditions", @"Conditions") andValue:self.listings.condition];
            return cell;
        }
            
            break;
            
        case 4:
           {
                PriceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"priceCell"];
                cell.refrenceVC = self ;
                cell.listing = self.listings ;
                activeTextField= cell.priceTextField ;
                cell.isPriceTextFieldEnable = &isPriceTextFieldEnable;
                [cell setPrice:self.listings.price currency:self.setCurrency];
                return cell ;
          }
            break ;
            
        case 5:
        {
            NegotiableTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"negotiableCell"];
            cell.listing = self.listings ;
            cell.refrenceVC = self;
            [cell setStateForNegtiableSwitch:self.listings.negotiable];
            return cell ;
            
        }
            break;
        case 6:
        {
            NegotiableTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"negotiableCell"];
            cell.listing = self.listings ;
            cell.refrenceVC = self;
            [cell setStateForWillingToExchange:self.listings.willingToExchange];
            return cell ;
            
        }
            
            
        case 7:
        {
            ExchangeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"exchangeCell"];
            cell.listing = self.listings ;
            cell.referenceVC = self ;
            [cell.swapListCollectionView reloadData];
            return cell;
        }
        case 8:
        {
            AddressTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"addressCell"];
            cell.isEditPost = self.editingPost ;
            cell.listing = self.listings ;
            cell.refrenceVC = self ;
            [cell setAddress];
            return cell;
        }
            
            break ;
            
        case 9:
        {
            ShareTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"shareCell"];
            cell.listing = self.listings ;
            [cell setMediaTitle:NSLocalizedString(@"Facebook", @"Facebook") mediaIcon_Off:@"share_to_facebook_icon" andMediaIcon_On:@"share_facebook_icon_on"];
            cell.switchOutlet.tag = 0;
            return cell;
        }
            
            break ;
        case 10:
        {
            ShareTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"shareCell"];
            cell.listing = self.listings ;
            [cell setMediaTitle:NSLocalizedString(@"Twitter", @"Twitter") mediaIcon_Off:@"share_to_twitter_icon" andMediaIcon_On:@"share_twitter_icon_on"];
             cell.switchOutlet.tag = 1;
            return cell;
        }
            
            break ;
            
        case 11:
        {
            ShareTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"shareCell"];
            cell.listing = self.listings ;
            [cell setMediaTitle:NSLocalizedString(@"Instagram", @"Instagram") mediaIcon_Off:@"share_instagram_icon_off" andMediaIcon_On:@"share_instagram_icon_on"];
             cell.switchOutlet.tag = 2;
            return cell;
        }
            
            break ;
        default:
        {
            UITableViewCell *cell = [[UITableViewCell alloc]init];
            return cell;
        }
            
    }
}




-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
       
        case 0:  return 180;
        case 7:
        {
            if(self.listings.willingToExchange)
            {
                return 50;
            }
            else
            {
                return 0;
            }
        }
        case 1:
        case 8:  return 80;
        default:
        {
            return 50 ;
        }
            break;
    }
}




- (IBAction)tapGestureAction:(id)sender {
    
    [self.view endEditing:YES];
}



#pragma mark
#pragma mark - Move View

/**
 This method will move view by evaluating keyboard height.
 
 @param n post by Notificationcentre.
 */

- (void)keyboardWillShow:(NSNotification *)n
{
//    tapToDismissKeyboard.enabled = YES;
    if(isPriceTextFieldEnable){
        NSDictionary* info = [n userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0,0 /*kbSize.height*/, 0.0);
        self.tableViewForListings.contentInset = contentInsets;
        self.tableViewForListings.scrollIndicatorInsets = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, activeTextField.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0,-(activeTextField.frame.origin.y - (kbSize.height +50)));
            [self.tableViewForListings setContentOffset:scrollPoint animated:YES];
        }
    }
    
}

- (void)keyboardWillHide:(NSNotification *)n
{
   // tapToDismissKeyboard.enabled = NO;
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableViewForListings.contentInset = contentInsets;
    self.tableViewForListings.scrollIndicatorInsets = contentInsets;
}


/*----------------------------------------------------- */
#pragma mark - Share To SocialMedia
/*----------------------------------------------------*/
/**
 *  This method will let the media link generate in main thread
 *
 *  @param postDetails media path
 */

- (void) postToSocialMedia:(NSDictionary *)postDetails
{
    ProductDetails *product = [[ProductDetails alloc]initWithDictionary:postDetails];
    if (self.listings.isFacebookShare){
        dispatch_async(dispatch_get_main_queue(), ^{
            [Helper makeFBPostWithParams:product];
        });
    }
    if (self.listings.isInstagramShare) {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"instagram:app"]])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"instagramShare" object:product.mainUrl];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isInstagramSharing"];
        }
    }
}




#pragma mark -
#pragma mark - Cloudinary Delegates
/**
 Get dictionary from coudinary Model.
 
 @param requestDic Dictionary.
 */
-(void)getDictionaryFromCloudinaryModelClass:(NSDictionary *)requestDic
{
    
    if(self.editingPost)
    {
        [WebServiceHandler editPost:requestDic andDelegate:self];
    }
    else
    {
        [WebServiceHandler postImageOrVideo:requestDic andDelegate:self];
    }
    
}


-(void)cloudinaryError:(NSString *)errorResult andCheck:(BOOL)isError
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    NSString *message;
    
    message = NSLocalizedString(unableToPostYourListings, unableToPostYourListings) ;
    
    
    UIAlertController *controller  = [UIAlertController alertControllerWithTitle:NSLocalizedString(sorry, sorry) message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(alertOk, alertOk) style:UIAlertActionStyleDefault handler:
                             ^(UIAlertAction *action)
                             {
                                 [WebServiceHandler getCloudinaryCredintials:@{@"":@""} andDelegate:self];
                                 
                             }];
    
    [controller addAction:action];
    [self presentViewController:controller animated:YES completion:nil];
}


/**
 This method is delegate method get call on success of web service call.
 
 @param requestType RequestType.
 @param response    response comes from web Service.
 @param error       error if any.
 */

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [self.activityIndicator stopAnimating];
    self.postButtonOutlet.titleLabel.hidden = NO;
    [self.postButtonOutlet setTitle:NSLocalizedString(postButtonTitle, postButtonTitle) forState:UIControlStateNormal];
    
    if (error) {
        
        return;
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    switch (requestType) {
        case RequestTypePost:
        {
            //success response(200 is for success code).
            switch ([responseDict[@"code"] integerValue]) {
                case 200:
                {
                    //success response(200 is for success code).
                    switch ([responseDict[@"code"] integerValue]) {
                        case 200: {
                            [self postToSocialMedia:response[@"data"][0]];
                            [[ProgressIndicator sharedInstance] hideProgressIndicator];
                            [[NSNotificationCenter defaultCenter] postNotificationName:mSellingPostNotifiName object:response[@"data"]];
                            [[NSNotificationCenter defaultCenter] postNotificationName:mAddNewPost object:response[@"data"]];
                            
                            [self CheckSocialSharing:[[ProductDetails alloc]initWithDictionary:response[@"data"][0]]];
                        }
                            break;
                        default:
                            
                            [Helper showAlertWithTitle:NSLocalizedString(alertTitle, alertTitle) Message:responseDict[@"message"] viewController:self];
                            break;
                    }
                }
                    break;
                default:
                    
                    [Helper showAlertWithTitle:NSLocalizedString(alertTitle, alertTitle) Message:responseDict[@"message"] viewController:self];
                    break;
            }
        }
            break;
            
     
        case RequestTypeEditPost:{
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                [[NSNotificationCenter defaultCenter] postNotificationName:mUpdatePostDataNotification object:responseDict];
                ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                [pi hideProgressIndicator];
                if(self.popDelegate)
                {
                    [self.popDelegate popToRootViewController:YES];
                }
                [self dismissViewControllerAnimated:YES completion:nil];
                
            }
                break;
                
            default:
                
                [Helper showAlertWithTitle:NSLocalizedString(alertTitle, alertTitle) Message:responseDict[@"message"] viewController:self];
                break;
                
        }
    }
            break;
        case RequestTypeCloudinaryCredintials:  {
        
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                // success response.
                [[NSUserDefaults standardUserDefaults] setObject:responseDict forKey:cloudinartyDetails];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
                break;
                
            default:
                break;
        }
    }
            
            
        default:
            break;
    }
    
    
}

#pragma mark
#pragma mark - PostedSuccessfully Delegate

-(void)postingListedSuccessfully
{
    [self dismissViewControllerAnimated:NO completion:nil];
    if(self.cameraFromTabBar){
        [self.tabBarController setSelectedIndex:0];
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:Nil];
    }
    
    
}


/**
 Get details of post and update the respective fields accordingly.
 */
-(void)updateFieldsForPostedProduct
{
    self.listings.titleOfPost = self.product.productName;
    self.listings.descriptionForPost = self.product.productDescription;
    self.listings.category = self.product.category;
    self.listings.subCategory = self.product.subcategory;
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    for(Filter *filter in self.product.propertyArray)
    {
        
        [dictionary setObject:filter.values forKey:filter.fieldName];
    }
    
    self.listings.filter = dictionary;
    self.listings.currency  = self.product.currency;
    self.listings.condition =  [self.product.condition capitalizedString];
    self.listings.price =  self.product.price ;
    self.listings.address = self.product.place ;
    self.listings.lattitude = self.product.latitude ;
    self.listings.longitude = self.product.longitude;
    self.listings.cityName = self.product.city;
    self.listings.countrySName = self.product.countrySname;
    self.listings.negotiable = self.product.negotiable ;
    self.listings.postId = self.product.postId;
    [self addImageUrlsToUpdate];
    
    [self.tableViewForListings reloadData];
}

-(void)addImageUrlsToUpdate
{
    NSArray *arrayOfUrlNames = [[NSArray alloc]initWithObjects:self.product.mainUrl,self.product.imageUrl1,self.product.imageUrl2,self.product.imageUrl3,self.product.imageUrl4, nil];
    NSArray *arrayOfPublicId = [[NSArray alloc]initWithObjects:self.product.cloudinaryPublicId,self.product.cloudinaryPublicId1,self.product.cloudinaryPublicId2,self.product.cloudinaryPublicId3,self.product.cloudinaryPublicId4, nil];
    NSInteger imageCount = self.product.imageCount;
    
    for(int j=0;j<imageCount;j++)
    {   NSString *url = [NSString stringWithFormat:@"%@",[arrayOfUrlNames objectAtIndex:j]];
        NSString *publicId = [NSString stringWithFormat:@"%@",[arrayOfPublicId objectAtIndex:j]];
        NSString *typeOfImage = @"cloudinaryUrl";
        NSMutableDictionary  *dictForImageUrl = [[NSMutableDictionary alloc] init];
        [dictForImageUrl setValue:typeOfImage forKey:@"imageType"];
        [dictForImageUrl setValue:url forKey:@"imageValue"];
        [dictForImageUrl setValue:publicId forKey:@"publicId"];
        [self.arrayOfImagePaths addObject:dictForImageUrl];
    }
    
    self.listings.arrayOfImagePaths = self.arrayOfImagePaths;
}


#pragma mark - Share -


-(void)CheckSocialSharing :(ProductDetails *)product
{
    
    if(self.listings.isFacebookShare || self.listings.isTwitterShare)
    {
       // [self genrateDeepLinkingComponent:product];
    }
    else
    {
        SuccessFullyPostedViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PostedSuccessfullyStoryboardId"];
        newVC.product = product;
        newVC.postingDelegate = self ;
        newVC.isTwitterSharing = self.listings.isTwitterShare ;
        newVC.postId = product.postId;
        [self presentViewController:newVC animated:NO completion:nil];
    }
}


//-(void)genrateDeepLinkingComponent :(ProductDetails *)product
//{
//    NSString *url =  [SHARE_LINK stringByAppendingString:[NSString stringWithFormat:@"%@",product.postId]];
//
//    NSString *originalLink = [SHARE_LINK  stringByAppendingString:product.postId] ;
//    NSURL *link = [NSURL URLWithString:originalLink];
//    components =
//    [FIRDynamicLinkComponents componentsWithLink:link
//                                          domain:mDomainForDeepLinking];
//
//    FIRDynamicLinkSocialMetaTagParameters *socialParams = [FIRDynamicLinkSocialMetaTagParameters parameters];
//    socialParams.title = product.productName ;
//    socialParams.descriptionText = product.productDescription ;
//    socialParams.imageURL = [NSURL URLWithString:product.mainUrl] ;
//    components.socialMetaTagParameters = socialParams;
//
//    NSString *bundleId = mIosBundleId;
//
//    FIRDynamicLinkIOSParameters *iosParams = [FIRDynamicLinkIOSParameters parametersWithBundleID:bundleId];
//    iosParams.fallbackURL = [NSURL URLWithString: url] ;
//    components.iOSParameters = iosParams;
//
//
//    FIRDynamicLinkAndroidParameters *androidParam = [FIRDynamicLinkAndroidParameters parametersWithPackageName:mAndroidBundleId];
//    androidParam.fallbackURL = [NSURL URLWithString: url] ;
//    components.androidParameters = androidParam;
//
//    FIRDynamicLinkNavigationInfoParameters *navigationInfoParameters = [FIRDynamicLinkNavigationInfoParameters parameters];
//    navigationInfoParameters.forcedRedirectEnabled = 0;
//    components.navigationInfoParameters = navigationInfoParameters;
//
//    SuccessFullyPostedViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PostedSuccessfullyStoryboardId"];
//    newVC.product = product;
//    newVC.postingDelegate = self ;
//    newVC.isTwitterSharing = self.listings.isTwitterShare ;
//    newVC.isFacebookSharing = self.listings.isFacebookShare;
//    newVC.postId = product.postId;
//    newVC.components = components;
//    [self presentViewController:newVC animated:NO completion:nil];
//
//}


@end

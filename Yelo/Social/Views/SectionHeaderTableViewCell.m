//
//  SectionHeaderTableViewCell.m
//  Yelo
//
//  Created by Rahul Sharma on 28/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SectionHeaderTableViewCell.h"

@implementation SectionHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}
-(void)setSectionHeader :(ProductDetails *)product
{
    [self.userProfileImageOutlet sd_setImageWithURL:[NSURL URLWithString:product.memberProfilePicUrl] placeholderImage:[UIImage imageNamed:@"defaultpp"]];
    self.userNameLabel.text = product.memberFullName;
    NSString *timeStamp = [Helper convertEpochToNormalTimeInshort:product.postedOn];
    self.timeStampLabel.text = timeStamp;
}

@end

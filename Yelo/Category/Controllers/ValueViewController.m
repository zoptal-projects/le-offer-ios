//
//  ValueViewController.m
//  CollegeStax
//
//  Created by 3Embed on 18/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "ValueViewController.h"
#import "SubCategoryTableViewCell.h"

@interface ValueViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    NSArray *values;
    NSString *textBoxValue;
}

@end

@implementation ValueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.backButtonOulet setTitle:self.filter.fieldName forState:UIControlStateNormal];
    values = [self.filter.values componentsSeparatedByString:@","];
    [self setUIonTheBasisOfType:self.filter.type];
    
    if(self.previousValue.length)
    {
       self.textBoxTextField.text = self.previousValue;
       textBoxValue = self.previousValue;
       self.doneButtonOutlet.enabled = YES;
    }
    else
    {
        self.previousValue = @"";
    }
    
    //  KeyboardWillShowNotification.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShown:)name:UIKeyboardWillShowNotification object:nil];
    
    //when keyboard appears this will  notifiy.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return values.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SubCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ValueSubcategoryCell" forIndexPath:indexPath];
    [cell setValuesForFilter:values[indexPath.row] previousSelection:self.previousValue];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.valueCallBack(values[indexPath.row],0);
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - Textfield view data source

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case 0:
            textBoxValue = textField.text;
            break;
        default:
            break;
    }
    
}



-(void)setUIonTheBasisOfType:(NSUInteger )type
{
//    * 1 : textbox
//    * 2 : checkbox
//    * 3 : slider
//    * 4 : radio button
//    * 5 : range
//    * 6 : drop down
//    * 7 : date
    switch (type) {
        case 1:
            self.listTableView.hidden = YES;
            self.textBoxView.hidden = NO;
            self.textBoxTextField.keyboardType = UIKeyboardTypeDefault;
            self.textBoxTextField.text = self.previousValue;
            [self.textBoxTextField becomeFirstResponder];
            self.tapGestureOutlet.enabled = YES;
            self.doneButtonOutlet.hidden = NO;
            break;
            
        case 3:
        case 5:
        case 7:
            self.listTableView.hidden = YES;
            self.textBoxView.hidden = NO;
            self.textBoxTextField.keyboardType = UIKeyboardTypeNumberPad;
            self.textBoxTextField.text = self.previousValue;;
            [self.textBoxTextField becomeFirstResponder];
            self.tapGestureOutlet.enabled = YES;
             self.doneButtonOutlet.hidden = NO;
            break;
        case 2:
        case 4:
        case 6:
            self.listTableView.hidden = NO;
            self.textBoxView.hidden = YES;
            self.doneButtonOutlet.hidden = YES;
            break;
        default:
            break;
    }

}

- (IBAction)tapGestureAction:(id)sender {
    
    [self.view endEditing:YES];
}

- (IBAction)DoneButtonAction:(id)sender {
    
    switch (self.filter.type) {
        case 1:
          self.valueCallBack(textBoxValue,0);
            break;
        case 3:
        case 5:
        case 7:
           self.valueCallBack(nil,[textBoxValue integerValue]);
            break;
        default:
            break;
    }
    
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backButtonAction:(id)sender {
    
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)textFieldEditingChangedAction:(id)sender {
    
    UITextField *textField = (UITextField *)sender;
    if(textField.tag == 0)
    {
        if(self.textBoxTextField.text.length > 0)
        {
           
            self.doneButtonOutlet.enabled = YES;
            self.doneButtonOutlet.backgroundColor = mBaseColor;
        }
        else
        {
           
           self.doneButtonOutlet.enabled = NO;
        }
    
      textBoxValue = textField.text;
    }

}


- (void)keyboardWillHidden:(NSNotification *)notification
{
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.doneButtonBottomConstraint.constant = 20;
                         [self.view layoutIfNeeded];
                     }];
}
- (void)keyboardWillShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    int height = MIN(keyboardSize.height,keyboardSize.width) + 20;
    self.doneButtonBottomConstraint.constant = height;
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.doneButtonBottomConstraint.constant = height;
                         [self.view layoutIfNeeded];
                     }];
}


@end

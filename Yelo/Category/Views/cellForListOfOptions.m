//
//  cellForListOfOptions.m

//
//  Created by Rahul Sharma on 14/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "cellForListOfOptions.h"


@implementation cellForListOfOptions

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setCategoriesList :(Category *)category previousSelection :(NSString *)previousSelection{
    self.conditionLabel.hidden = YES;
    self.labelForList.hidden = NO;
    self.categoryImage.hidden = NO;
    
    self.labelForList.text = category.name;
    [self.categoryImage sd_setImageWithURL:[NSURL URLWithString:category.activeimage] placeholderImage:[UIImage imageNamed:@"itemProdDefault.png"]];
    
    if([previousSelection isEqualToString:self.labelForList.text])
        self.imageViewForSelection.hidden = NO;
    else
        self.imageViewForSelection.hidden = YES;
}
-(void)setCondition :(NSArray *)dataArray IndexPath :(NSIndexPath *)indexpath andPreviousSelection:(NSString *)previousSelection
{
    self.labelForList.hidden = YES;
    self.categoryImage.hidden = YES;
    self.conditionLabel.hidden = NO ;
    self.conditionLabel.text = dataArray[indexpath.row];
    self.conditionLabel.text = [self.conditionLabel.text capitalizedString];
    
    if([previousSelection isEqualToString:self.labelForList.text])
        self.imageViewForSelection.hidden = NO;
    else
        self.imageViewForSelection.hidden = YES;
}

@end


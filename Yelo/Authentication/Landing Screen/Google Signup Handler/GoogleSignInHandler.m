//
//  GoogleSignInHandler.m

//
//  Created by Rahul Sharma on 23/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

//#import "GoogleSignInHandler.h"
//
//@implementation GoogleSignInHandler
//
//
//- (void)signIn:(GIDSignIn *)signIn
//didSignInForUser:(GIDGoogleUser *)user
//     withError:(NSError *)error {
//    // Perform any operations on signed in user here.
//
//    // ...
//}
//
//
//- (void)signIn:(GIDSignIn *)signIn
//didDisconnectWithUser:(GIDGoogleUser *)user
//     withError:(NSError *)error {
//    // Perform any operations when the user disconnects from app here.
//    // ...
//}
//@end

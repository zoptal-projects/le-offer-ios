//
//  LandingPageController.h

//
//  Created by Rahul Sharma on 12/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GIDSignInButton;

@interface LandingPageViewController : UIViewController 
{
     GetCurrentLocation *getLocation; // Location Handler Model Object
}


#pragma mark -
#pragma mark - NON IB Properties.

/**
 To store current location lattitude and Longitude.
 */
@property (nonatomic)double  currentLat, currentLong;

@property (strong, nonatomic) IBOutlet UIStackView *appleStackView;
@property (strong, nonatomic) IBOutlet UIView *appleButtonView;

/**
 Bool Value to recognize google and facebook Login.
 */
@property BOOL googleSignin,fbSignIn ;


/**
 Dictionary object to store facebbokLogin user details fetched by facebook handler.
 */
@property NSDictionary *fbLoginDetails ;



/**
 Strings to hold temporary values, if need to pass from one VC to another VC.
 */
@property NSString *faceBookUniqueIdOfUser, *googlePlusId , *googlePlusName;
@property NSString *faceBookUserEmailId, *googlePlusEmailId;
@property NSString *profilepicurlFb,*googlePlusProfileUrl;
@property NSString *fullNameForFb ,*fbloggedUserAccessToken, *googlePlusUserAccessToken;


#pragma mark -
#pragma mark - UIButton Actions -

/**
 Facebook Login Button Action.

 @param sender continue with facebook UIButton object.
 */
- (IBAction)continueFacbookButtonAction:(id)sender;


/**
  Login Button Action

 @param sender login Button Outlet.
 */
- (IBAction)loginButtonAction:(id)sender;


/**
 SignUp Button Action.

 @param sender SignUp button outlet.
 */
- (IBAction)signupButtonAction:(id)sender;



/**
 Close Button to Dismiss the Landing Screen.

 @param sender UIButton Outlet.
 */
- (IBAction)closeButtonAction:(id)sender;


/**
 Show Terms And Conditions Button ACtion.

 @param sender UIButton Outlet.
 */
- (IBAction)termsButtonACtion:(id)sender;


/**
 Redirect to Privacy Policy of application.

 @param sender UIButton Outlet.
 */
- (IBAction)privacyButtonAction:(id)sender;


@end

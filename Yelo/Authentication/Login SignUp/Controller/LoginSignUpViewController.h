
//  LoginSignUpViewController.h
//  Created by Ajay Thakur on 16/07/17.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "FBLoginHandler.h"
#import "EMCCountryPickerController.h"



@interface LoginSignUpViewController : UIViewController <UITextFieldDelegate,FBLoginHandlerDelegate,CLUploaderDelegate,UIImagePickerControllerDelegate,WebServiceHandlerDelegate,UIActionSheetDelegate,EMCCountryDelegate , GetCurrentLocationDelegate> {
    BOOL userNameLength,signUpByPhoneNumber,updateFbImageOnlyFirstTime, keyboardShown, SignUpFinshed;
    CLCloudinary *cloudinary;
    NSString *profilePicUrl, *deviceId, *ThumbnailimagePath ,*previousSelection;
    NSDictionary *cloundinaryCreditinals;
    UILabel *labelForErrorMessg;
    UITextField *activeField;
    GetCurrentLocation *getLocation;
}

#pragma mark -
#pragma mark - Non IB Properties -
// Current Lat , Long values holder.
@property (nonatomic)double  currentLat, currentLong;

// Strings value to hold the google/Facebook properties.
@property NSString *faceBookUniqueIdOfUser,*fullNameForFb,*faceBookUserEmailId,*faceBookUniqueIdOfUserToRegister,*fbloggedUserAccessToken, *currentCity , *currentCountryShortName ,*currentAddress;

@property NSDictionary *fbLoginDetails, *googlePlusLoginDetails, *fbResponseDetails;
@property BOOL signUpWithFacebook,signupFlag, signUpWithGooglePlus,signUpWithApple;
@property(strong,nonatomic) NSString *googlePlusName,*googlePlusProfileUrl,*googlePlusEmail, *googlePlusAccessToken , *googlePlusId;

@property(strong, nonatomic)NSString *faceBookEmailIdOfUserToRegister, *SignUpType, *codeForSignUpType, *userEnteredEmail, *emailForRegistration;

#pragma mark -
@property (weak, nonatomic) IBOutlet UIButton *backButtonOutLet;
#pragma mark - UIView Outlets -

@property (strong, nonatomic) IBOutlet UIView *viewForLoginSignup;
@property (strong, nonatomic) IBOutlet UIView *loginViewOutlet;
@property (strong, nonatomic) IBOutlet UIView *signupViewOutlet;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityViewOutlet;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *createActivityIndicatorOutlet;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

#pragma mark -
#pragma mark - UILabel Outlets -
@property (strong, nonatomic) IBOutlet UILabel *countryCodeLabelOutlet;

#pragma mark -
#pragma mark - UITextField Outlets -
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *loginPasswordTextfield;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *numberTextField;
@property (strong, nonatomic) IBOutlet UITextField *userNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *signupPasswordTextfield;

#pragma mark -
#pragma mark - UIButton Outlets -

@property (strong, nonatomic) IBOutlet UIButton *loginSelectOutlet;
@property (strong, nonatomic) IBOutlet UIButton *signupSelectOutlet;
@property (weak, nonatomic) IBOutlet UIButton *createButton;
@property (strong, nonatomic) IBOutlet UIButton *agreeButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *loginbutton;


#pragma mark -
#pragma mark - UIImageView Outlets -
@property (strong, nonatomic) IBOutlet UIImageView *editImageOnProfileView;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UIImageView *userNameCheckImage;
@property (strong, nonatomic) IBOutlet UIImageView *imageForEmailCheck;
@property (strong, nonatomic) IBOutlet UIImageView *imageForNumberCheck;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewForFlag;


#pragma mark -
#pragma mark - UIButton Actions -

- (IBAction)LogInWithFbButtonAction:(id)sender;
- (IBAction)logInButtonAction:(id)sender;
- (IBAction)getHelpSignInButtonAction:(id)sender;
- (IBAction)textFieldDidChange:(id)sender;
- (IBAction)loginOptionButtonAction:(id)sender;
- (IBAction)signupOptionButtonAction:(id)sender;
- (IBAction)backButtonAction:(id)sender;
- (IBAction)privacyPolicyButtonAction:(id)sender;
- (IBAction)termsConditionsButtonAction:(id)sender;
- (IBAction)createButtonAction:(id)sender;
- (IBAction)countryCodeButton:(id)sender;
- (IBAction)editProfileImageButton:(id)sender;
- (IBAction)agreeButtonAction:(id)sender;
- (IBAction)textFieldValueIsChanged:(id)sender;

@end


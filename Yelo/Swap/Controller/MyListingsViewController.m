//
//  MyListingsViewController.m
//  Tac Traderz
//
//  Created by 3Embed on 09/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "MyListingsViewController.h"
#import "RFQuiltLayout.h"
#import "UserProfileCollectionViewCell.h"
#import "Cellable-Swift.h"

@interface MyListingsViewController ()<UICollectionViewDataSource, UICollectionViewDelegate,RFQuiltLayoutDelegate,WebServiceHandlerDelegate>
{
    NSMutableArray *sellingPostData;
    NSIndexPath *selectedindex;
    ProductDetails *swapProduct;
}
@end

@implementation MyListingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    sellingPostData = [NSMutableArray new];
    
    self.swapButtonOutlet.backgroundColor = mLoginButtonDisableBackgroundColor;
    [self RFQuiltLayoutIntialization];
    
    NSDictionary *requestDict = @{
                                  mauthToken :[Helper userToken],
                                  moffset : @"0",
                                  mlimit :  @"50",
                                  @"sold" : @"0"
                                  };
    
    ProgressIndicator *progress = [ProgressIndicator sharedInstance];
    [progress showPIOnView:self.view withMessage:@"Loading.."];
    
    [WebServiceHandler getUserPosts:requestDict andDelegate:self];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - RFQuiltLayout Intialization

/**
 This method will create an object for RFQuiltLayout.
 */
-(void)RFQuiltLayoutIntialization
{
    RFQuiltLayout* layout = [[RFQuiltLayout alloc]init];
    layout.direction = UICollectionViewScrollDirectionVertical;
    if(self.view.frame.size.width == 375)
        layout.blockPixels = CGSizeMake( 37,31);
    else if(self.view.frame.size.width == 414)
    {
        layout.blockPixels = CGSizeMake( 102,31);
    }
    
    else
        layout.blockPixels = CGSizeMake( 79,31);
    self.collectionViewOutlet.collectionViewLayout = layout;
    layout.delegate=self;

}

/*-----------------------------------------------------*/
#pragma mark -
#pragma mark - collectionView Delegates and DataSource.
/*-----------------------------------------------------*/

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   return sellingPostData.count ;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UserProfileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myListingsCell" forIndexPath:indexPath];
    [cell setProducts:sellingPostData[indexPath.row]];
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    swapProduct = sellingPostData[indexPath.row];
    UserProfileCollectionViewCell *selectedCell = (UserProfileCollectionViewCell *)[collectionView cellForItemAtIndexPath:selectedindex];
    selectedCell.tickMarkImage.hidden = YES;
    
     UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
     cell.tickMarkImage.hidden = NO;
     selectedindex = indexPath;

    self.swapButtonOutlet.enabled = YES;
    self.swapButtonOutlet.backgroundColor = mBaseColor;
}
            

#pragma mark –
#pragma mark – RFQuiltLayoutDelegate

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [Helper blockSizeOfProduct:sellingPostData[indexPath.row] view:self.view forHome:YES];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetsForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return UIEdgeInsetsMake (5,5,0,0);
}

#pragma mark –
#pragma mark – WebServiceDelegate

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
    }

        if(requestType ==  RequestTypeGetPosts)
        {
            [[ProgressIndicator sharedInstance]hideProgressIndicator];
            switch ([response[@"code"] integerValue]) {
                case 200: {
                    
                self.collectionViewOutlet.backgroundView = nil;
                [self handlingResponseForRequestPosts:response];
                        
                    }
                break;
                case 204:{
                    self.collectionViewOutlet.backgroundView = self.emptyBackgroundView ;
                }
                    break;
                default :
                    break;
            }
        }
}

-(void)handlingResponseForRequestPosts :(NSDictionary *)response
{
      [sellingPostData removeAllObjects];
      sellingPostData = [ProductDetails arrayOfProducts:response[@"data"]];
    [self.collectionViewOutlet reloadData];
}

// Redirect user to chat and create swap offer.
- (IBAction)swapButtonAction:(id)sender {
    
   NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
  IndividualChatViewModel *individualCVMObj= [[IndividualChatViewModel alloc] initWithCouchbase:[Couchbase sharedInstance]];
        
        NSString *receiverID = self.product.memberMqttId;
        NSString *secretID = self.product.postId;
        NSString *toDocID = [individualCVMObj fetchIndividualChatDocWithRecieverID:receiverID andSecretID:secretID];
        
        if (toDocID.length>0) {
            data[@"offerStatus"] = @"3";
        NSDictionary *messageObject = [self createObjectForMessageWithDocID:toDocID withPrice:self.product.price andType:@"17" andProduct:self.product];
            data[@"sendchat"] = messageObject;
        }else {
            data[@"offerStatus"] = @"1";
            NSDictionary *messageObject = [self createObjectForMessageWithDocID:@" " withPrice:self.product.price andType:@"17" andProduct:self.product];
            data[@"sendchat"] = messageObject;
        }
        
        data[@"token"] = [Helper userToken];
        data[@"postId"] = self.product.postId;
        data[@"price"] =  self.product.price;
        data[@"type"] = @"0";
        data[@"membername"] = self.product.membername ;
        data[@"swapPostId"] = swapProduct.postId;
        data[@"swapStatus"] = @"1";
    
        
        ProgressIndicator *progress = [ProgressIndicator sharedInstance];
        [progress showPIOnView:self.view withMessage:@"Loading.."];
        
        // API call for initiating swap offer

            API *apiObj = [[API alloc] init];
            NSDictionary *params = @{@"receiverID":receiverID,
                                     @"secretID":secretID};
            
            [apiObj sendNotificationWithText:@" " andTitle:@"You got an swap Offer" toTopic:receiverID andData:params];
   [apiObj initiatedSwapOfferWithWithdata:data completionBlock:^(NSInteger code, NSDictionary<NSString *,id> * _Nullable response){
        if(code == 200)
        {
            if (response) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [self goToChatControllerWithRecieverID:receiverID andSecretID:secretID andProductbj:self.product];
                    [[ProgressIndicator sharedInstance] hideProgressIndicator];
                });
            }
        }
        else if (code == 400)
        {
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
            [Helper showAlertWithTitle:nil Message:@"You have already sent swap offer with same product." viewController:self];
        }

         
            }];
}
    
-(void)goToChatControllerWithRecieverID:(NSString *)receiverID andSecretID : (NSString *)secretID andProductbj : (ProductDetails *)productObj {
        
        NSMutableDictionary *chatObj = [NSMutableDictionary new];
        chatObj[@"receiverID"] = receiverID;
        chatObj[@"secretID"] = secretID;
        
        // redirecting it to the next controller with the chat object data.
        // check for empty object.
        UIViewController *tabController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
        if ([tabController isKindOfClass:[UITabBarController class]]) {
            UITabBarController *tbControler = (UITabBarController *)tabController;
            dispatch_async(dispatch_get_main_queue(), ^{
                [tbControler setSelectedIndex:3];
                [self.navigationController dismissViewControllerAnimated:false completion:nil];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"OfferInitiated" object:nil userInfo:@{@"chatObj":chatObj,@"productObj":productObj}];
                });
            });
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tabBarController setSelectedIndex:3];
            [self.navigationController dismissViewControllerAnimated:false completion:nil];
            
        });
    }
    
    -(NSDictionary *)createObjectForMessageWithDocID : (NSString *) docID withPrice:(NSString *)price andType:(NSString *)type andProduct :(ProductDetails *)product {
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        NSData *ploadData = [price dataUsingEncoding:NSUTF8StringEncoding];
        
        // Get NSString from NSData object in Base64
        NSString *base64Encoded = [ploadData base64EncodedStringWithOptions:0];
        if ([price isEqualToString:@"0"]){
            base64Encoded = @"";
        }
        UInt64 timeStamp = ([[NSDate date] timeIntervalSince1970] * 1000);
        
        data[@"name"] = [Helper userName];
        data[@"from"] = [Helper getMQTTID];
        data[@"to"] = product.memberMqttId ; ; // have to check about the other persons mqtt id
        data[@"payload"] = base64Encoded; //
        data[@"type"] = type;
        data[@"offerType"] = @"5"; // offertype 1: make offer 3:counter offer 2: accepted
        data[@"id"] = [NSString stringWithFormat:@"%llu",timeStamp];
        data[@"secretId"] = [NSString stringWithFormat:@"%@",product.postId];
        data[@"thumbnail"] = @"";
        data[@"userImage"] = product.profilePicUrl;
        data[@"toDocId"] = [NSString stringWithFormat:@"%@",docID];
        data[@"dataSize"] = [NSNumber numberWithInt:1];
        data[@"isSold"] = @"0";
        data[@"productImage"] = product.thumbnailImageUrl ;
        data[@"productId"] = [NSString stringWithFormat:@"%@",product.postId];
        data[@"productName"] = product.productName;
        data[@"productPrice"] = price;
        // Exchange
        data[@"isRejected"] = @"0";
        data[@"swapType"] = @"1";
        data[@"isSwap"] = @"1";
        data[@"productUrl"] = self.product.thumbnailImageUrl;
        data[@"swapProductUrl"] = swapProduct.thumbnailImageUrl;
        data[@"swapProductName"] = swapProduct.productName;
        data[@"swapProductId"] = swapProduct.postId;
        data[@"swapTextMessage"] = [NSString stringWithFormat:@"I would like to offer %@ in place of your %@",swapProduct.productName,self.product.productName];
    
        return data;
  }


- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end

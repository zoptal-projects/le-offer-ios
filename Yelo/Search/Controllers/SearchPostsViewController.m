
//
//  SearchPostsViewController.m

//
//  Created by Rahul Sharma on 17/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SearchPostsViewController.h"
#import "TopTableViewCell.h"
#import "PeopleTableViewCell.h"
#import "ProfileViewController.h"
#import "ProductDetailsViewController.h"


@interface SearchPostsViewController () <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UISearchBarDelegate,WebServiceHandlerDelegate>
{
    CGPoint lastScrollContentOffset;
    NSMutableArray *postData,*peopleData;
    int postsRequestPagingIndex;
    int peopleRequestPagingIndex;
    NSString *stringToSearchPosts, *stringToSearchPeople ;
}

@end

@implementation SearchPostsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    postData =[[NSMutableArray alloc] init] , peopleData = [[NSMutableArray alloc] init] ;
    self.postButtonOutlet.selected = YES;
    peopleRequestPagingIndex = 0;
    postsRequestPagingIndex = 0 ;
    // Do any additional setup after loading the view.
    self.navigationItem.title = NSLocalizedString(navTitleForSearch, navTitleForSearch) ;
    self.searchBarOutlet.placeholder = NSLocalizedString(searchPosts, searchPosts);
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mCloseButton normalState:mCloseButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,25,40)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navBack) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
    self.searchBarOutlet.text = @"";
    [_searchBarOutlet becomeFirstResponder];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
   
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [self.view endEditing:YES];
    
}


-(void)navBack
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*---------------------------------------------------------*/
#pragma mark - searchbar delegates
/*---------------------------------------------------------*/

#pragma UIsearchbardelegate


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{

    if (self.postButtonOutlet.selected)
    {
        self.searchBarOutlet.placeholder = NSLocalizedString(searchPosts, searchPosts);
        
    }
    else {
        self.searchBarOutlet.placeholder = NSLocalizedString(searchPeople, searchPeople);
        
    }
    return YES;
}



- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}
- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar {
    
    [self performSegueWithIdentifier:@"addContactToDiscoverPeopleSegue" sender:nil];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSString *encodedString =  [searchText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    
    if(self.peopleButtonOutlet.selected){
    if (![searchText isEqualToString:@""]) {
        [self requestForPeople:encodedString];
         stringToSearchPeople = encodedString ;
    }
    else {
        [peopleData removeAllObjects];
        [self.peopleTableView reloadData];
    }
    }
    else
    {
        
    if(![searchText isEqualToString:@""]) {
        [self requestForPosts:encodedString];
        stringToSearchPosts = encodedString ;
    }
    else {
        [postData removeAllObjects];
        [self.postTableView reloadData];
    }
    }
}


-(void)requestForPeople:(NSString *)searchText {
    
    if([[Helper userToken]isEqualToString:mGuestToken])
    {
        NSDictionary *requestDict = @{
                                      mMember :searchText,
                                      moffset:flStrForObj([NSNumber numberWithInteger:peopleRequestPagingIndex *40]),
                                      mlimit:@"40"
                                      };
        [WebServiceHandler getSearchPeopleForGuestUser:requestDict andDelegate:self];
        
    }
    else{
    NSDictionary *requestDict = @{
                                  @"userNameToSearch" :searchText,
                                  mauthToken :flStrForObj([Helper userToken]),
                                  moffset:flStrForObj([NSNumber numberWithInteger:peopleRequestPagingIndex *40]),
                                  mlimit:@"40"
                                  };
    [WebServiceHandler getSearchPeople:requestDict andDelegate:self];
    }
}

-(void)requestForPosts:(NSString *)searchText {
    NSDictionary *requestDict = @{
                                  mauthToken :flStrForObj([Helper userToken]),
                                  mProductName:searchText,
                                 moffset:flStrForObj([NSNumber numberWithInteger:postsRequestPagingIndex *40]),
                                  mlimit:@"20"
                                  
                                  };
    [WebServiceHandler getSearchForPosts:requestDict andDelegate:self];
}



/*------------------------------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*--------------------------------------------------------*/
- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    //handling response.
    if (error) {
      return;
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
        if (requestType == RequestTypeGetSearchForPosts ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                [self handlingPostsResponseData:responseDict];
            }
                break;
            default: {
                //[self errorAlert:responseDict[@"message"]];
            }
        }
    }
    
    if (requestType == RequestTypeGetSearchPeople) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                [self hadnlingPeopleApiResponse:responseDict];
            }
                break;
            default: {
               // [self errorAlert:responseDict[@"message"]];
            }
        }
        
    }
    
}

- (void)errorAlert:(NSString *)message {
    //alert for failure response.
     [Helper showAlertWithTitle:NSLocalizedString(alertMessage, alertMessage) Message:message viewController:self];
}



-(void)hadnlingPeopleApiResponse:(NSDictionary *)peopleDic {
    if (peopleDic) {
        
        if (peopleRequestPagingIndex == 0)
        {
            [peopleData removeAllObjects];
        }
        [peopleData addObjectsFromArray:peopleDic[@"data"]];
        [self.peopleTableView reloadData];
    }
    else {
        peopleData = nil;
        [self.peopleTableView reloadData];
    }
}


-(void)handlingPostsResponseData:(NSDictionary *)postsData {
    if (postsData) {
        
        if (postsRequestPagingIndex == 0)
        {
            [postData removeAllObjects];
        }
        [postData addObjectsFromArray: postsData[@"data"]];
        [self.postTableView reloadData];
    }
    else {
        postData = nil;
        [self.postTableView reloadData];
    }
}



/*--------------------------------------------------*/
#pragma mark - Scrollview Delegate
/*-------------------------------------------------*/
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    lastScrollContentOffset = scrollView.contentOffset ;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.mainScrollViewOutlet]) {
        CGPoint offset = scrollView.contentOffset;
        self.movingDividerLeadingConstraint.constant = scrollView.contentOffset.x/2;
        if(offset.x == 0 ) {
            
            self.postButtonOutlet.selected = YES;
            self.peopleButtonOutlet.selected = NO;
            self.searchBarOutlet.placeholder = NSLocalizedString(searchPosts, searchPosts);
            self.searchBarOutlet.text = @"";

        }
        else if( offset.x <=CGRectGetWidth(self.view.frame)) {

            self.postButtonOutlet.selected = NO;
            self.peopleButtonOutlet.selected = YES;
            self.searchBarOutlet.placeholder = NSLocalizedString(searchPeople, searchPeople);
            self.searchBarOutlet.text = @"";
        }
        scrollView.contentOffset = offset;
    }
}

/*-------------------------------------------------------------------------*/
#pragma mark - tableview Delegate and data source.
/*------------------------------------------------------------------------*/

// top tableview  (posts)         -       10(tag)
//peopleTableView      -       20(tag)

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 10)
        return postData.count;
    else if (tableView.tag == 20)
        return peopleData.count;
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 10 ) {
        TopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:mTopTableViewCellID  forIndexPath:indexPath];
        [cell setValuesInCellWithUsername:flStrForObj(postData[indexPath.row][@"productName"]) fullName:flStrForObj(postData[indexPath.row][@"category"]) andProfileImage:flStrForObj( postData[indexPath.row][@"thumbnailImageUrl"])];
        return cell;
    }
    else  if (tableView.tag == 20 )
    {
        PeopleTableViewCell *peopleCell = [tableView dequeueReusableCellWithIdentifier:mPeopleTableViewCellID forIndexPath:indexPath];
        [peopleCell setValuesInCellWithUsername:flStrForObj(peopleData[indexPath.row][@"username"]) fullName:flStrForObj(peopleData[indexPath.row][@"fullName"]) andProfileImage:flStrForObj(peopleData[indexPath.row][@"profilePicUrl"])];
        
        return peopleCell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 10) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        ProductDetailsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
        newView.hidesBottomBarWhenPushed = YES;
        newView.postId = flStrForObj(postData[indexPath.row][@"postId"]);
        NSArray *data = [NSArray new];
        data = postData[indexPath.row];
        newView.movetoRowNumber = indexPath.item;
        newView.noAnimation = YES;
        newView.hidesBottomBarWhenPushed = YES ;
        [self.navigationController pushViewController:newView animated:YES];
        }
    else
    {
        ProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
        newView.memberName = flStrForObj(peopleData[indexPath.row][@"username"]);
        newView.isMemberProfile = YES;
        newView.productDetails = YES;
        [self.navigationController pushViewController:newView animated:YES];

    }
  
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 10 )
    {
        if(postData.count >20) {
            if (indexPath.row == [postData count] - 1 ) {
                ++postsRequestPagingIndex;
                [self requestForPosts:stringToSearchPosts];
            }
        }
    }
    else {
        if(peopleData.count >20) {
            if (indexPath.row == [peopleData count] - 1 ) {
                ++peopleRequestPagingIndex;
                [self requestForPeople:stringToSearchPeople];
            }
        }
    }
}
- (IBAction)postsButtonAction:(id)sender
{
    self.movingDividerLeadingConstraint.constant = self.mainScrollViewOutlet.contentOffset.x/2;
    CGRect frame = self.mainScrollViewOutlet.bounds;
    frame.origin.x = 0 * CGRectGetWidth(self.view.frame);
    [self.mainScrollViewOutlet scrollRectToVisible:frame animated:YES];
    self.postButtonOutlet.selected = YES;
    self.peopleButtonOutlet.selected = NO;
    
    _postButtonOutlet.backgroundColor = [UIColor colorWithRed:163.0/255.0f green:200.0/255.0f blue:63.0/255.0f alpha:1.0f];
    [_postButtonOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _peopleButtonOutlet.backgroundColor = [UIColor whiteColor];
    [_peopleButtonOutlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

- (IBAction)peopleButtonAction:(id)sender {
    
    CGRect frame = self.mainScrollViewOutlet.bounds;
    frame.origin.x = 1 * CGRectGetWidth(self.view.frame);
    [self.mainScrollViewOutlet scrollRectToVisible:frame animated:YES];
    self.movingDividerLeadingConstraint.constant = self.mainScrollViewOutlet.contentOffset.x/2;
    self.postButtonOutlet.selected = NO;
    self.peopleButtonOutlet.selected = YES;
   
    _postButtonOutlet.backgroundColor = [UIColor whiteColor];
    [_postButtonOutlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    _peopleButtonOutlet.backgroundColor = [UIColor colorWithRed:163.0/255.0f green:200.0/255.0f blue:63.0/255.0f alpha:1.0f];
   
}


@end

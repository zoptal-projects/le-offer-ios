//
//  NativeAdsCellCollectionViewCell.h
//  Tac Traderz
//
//  Created by 3Embed on 11/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NativeAdsCellCollectionViewCell : UICollectionViewCell

@property(strong,nonatomic)UIViewController *vcRefrence;
@property (strong, nonatomic) IBOutlet UILabel *adStatusLabel;

@property (strong, nonatomic) IBOutlet UIImageView *adIconImageView;

@property (strong, nonatomic) IBOutlet UILabel *adTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *adBodyLabel;
@property (strong, nonatomic) IBOutlet UIButton *adCallToActionButton;
@property (strong, nonatomic) IBOutlet UILabel *adSocialContextLabel;
@property (strong, nonatomic) IBOutlet UILabel *sponsoredLabel;

@property (strong, nonatomic) IBOutlet UIView *adUIView;

@end

//
//  DiscoverPeopleViewController.m

//
//  Created by Rahul Sharma on 2/25/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "PGDiscoverPeopleViewController.h"
#import "PGDiscoverPeopleTableViewCell.h"
#import "DiscoverTableViewPostedImagesCell.h"
#import "postedImagesCollectionViewCell.h"
#import "ConnectToFaceBookViewController.h"
#import "TopTableViewCell.h"
#import "FontDetailsClass.h"
#import "WebServiceConstants.h"
#import "WebServiceHandler.h"
#import "TinderGenericUtility.h"
#import "UIImageView+WebCache.h"
#import "UIImage+GIF.h"
#import "ProfileViewController.h"
#import "Helper.h"

@interface PGDiscoverPeopleViewController () <WebServiceHandlerDelegate>{
    NSArray *title;
    NSString *titleForFb;
    NSString *subtitleForFb;
    NSString *imageForFb;
    NSString *titleForContacts;
    NSString *subTitleForContacts;
    NSString *imageForContcts;
    
    
    postedImagesCollectionViewCell *collectionViewCell;
    
    NSMutableArray *arrayOfFollowingStaus;
    NSMutableArray *respDeatils;
    NSIndexPath *rowAt;
    BOOL chnageColorOfSubTitle;
    UIActivityIndicatorView *avForCollectionView;
    NSInteger offsetForMorePosts;
    
    UIRefreshControl *refreshControl;
    BOOL neccessaryToRemoveOldPostsData;
}
@property bool classIsAppearing;
@end

@implementation PGDiscoverPeopleViewController

#pragma mark
#pragma mark - viewcontroller

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    [self createNavLeftButton];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self requestForDiscoverPeopleApi:0];
    arrayOfFollowingStaus = [[NSMutableArray alloc] init];
    respDeatils = [[NSMutableArray alloc] init];
    offsetForMorePosts = 0;
    [self addingRefreshControl];
    neccessaryToRemoveOldPostsData = NO;
    [self addingActivityIndicatorToCollectionViewBackGround];
    
    [self updateFollowStatus];
    [self creatingNotificationForUpdatingTitleFb];
    [self creatingNotificationForUpdatingTitleContacts];
   
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    _classIsAppearing = NO;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    _classIsAppearing = YES;
    [self navBarCustomization];
}

-(void)navBarCustomization {
    //navigationbar title
    
    self.navigationController.navigationBar.translucent = NO ;
    self.navigationController.navigationBar.shadowImage = nil ;
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = NSLocalizedString(navTitleForDiscoverPeople, navTitleForDiscoverPeople);
}


-(void)creatingNotificationForUpdatingTitleContacts {
    NSInteger numberOfContactsInPicogram = [[[NSUserDefaults standardUserDefaults]
                                             stringForKey:numberOfContactsFoundInPicogram] integerValue];
    
    NSString *numberofContscs = [NSString stringWithFormat:@"%ld",(long)numberOfContactsInPicogram];
    
    if (numberOfContactsInPicogram > 0) {
        titleForContacts = NSLocalizedString(connectedContacts, connectedContacts);
        subTitleForContacts = [numberofContscs stringByAppendingString:NSLocalizedString(titleForContacts, titleForContacts)];
        imageForContcts = @"discovery_people_contact_icon";
    }
    else {
        titleForContacts = NSLocalizedString(connectToContacts, connectToContacts);
        subTitleForContacts = NSLocalizedString(toFollowYourFriends, toFollowYourFriends);
        imageForContcts = @"discover_people_contacts_icon_off";
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTitleForContcts:) name:@"updateContactSectionTitle" object:nil];
}



-(void)creatingNotificationForUpdatingTitleFb {
    NSInteger numberOfFaceBookFriendsInPicogram = [[[NSUserDefaults standardUserDefaults]
                                                    stringForKey:numberOfFbFriendFoundInPicogram] integerValue];
    NSString *numberofFbFriends = [NSString stringWithFormat:@"%ld",numberOfFaceBookFriendsInPicogram];
    if (numberOfFaceBookFriendsInPicogram > 0) {
        titleForFb = NSLocalizedString(connectedFacebook, connectedFacebook);
        subtitleForFb = [numberofFbFriends stringByAppendingString:NSLocalizedString(titleForFriends, titleForFriends)];
        imageForFb = @"discover_people_facebook_icon_whenavailable";
    }
    else {
        titleForFb = NSLocalizedString(connectToFacebook, connectToFacebook);
        subtitleForFb = NSLocalizedString(toFollowYourFriends, toFollowYourFriends);
        imageForFb = @"discover_people_facebook_icon_off";
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTitleForFb:) name:@"updateFaceBookSectionTitle" object:nil];
}

-(void)updateTitleForContcts:(NSNotification *)noti {
    
    NSString *numberOfContcts = flStrForObj(noti.object[@"numberOfContactsSynced"][@"numberOfContacts"]);
    
    NSInteger numberOfContactsInPicogram = [numberOfContcts integerValue];
    
    titleForContacts = NSLocalizedString(connectedContacts, connectedContacts);
    imageForContcts = @"discovery_people_contact_icon";
    
    if (numberOfContactsInPicogram > 0) {
        
        if(numberOfContactsInPicogram == 1)
        {
            subTitleForContacts = [numberOfContcts stringByAppendingString:NSLocalizedString(titleForContact, titleForContact)];
        }
        else
        {
            subTitleForContacts = [numberOfContcts stringByAppendingString:NSLocalizedString(titleForContacts, titleForContacts)];
        }
        
        
        
    }
    else {
        titleForContacts = NSLocalizedString(connectToContacts, connectToContacts) ;
        subTitleForContacts = NSLocalizedString(toFollowYourFriends, toFollowYourFriends);
        imageForContcts = @"discover_people_contacts_icon_off";
    }
    
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:1 inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.discoverTableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}

-(void)updateTitleForFb:(NSNotification *)noti {
    
    NSString *numberOfContcts = flStrForObj(noti.object[@"numberOfContactsSynced"][@"numberOfContacts"]);
    
    NSInteger numberOfFaceBookFriendsInPicogram = [numberOfContcts integerValue];
    
    if (numberOfFaceBookFriendsInPicogram > 0) {
        titleForFb = NSLocalizedString(connectedFacebook, connectedFacebook);
        imageForFb = @"discover_people_facebook_icon_whenavailable";
        
        if(numberOfFaceBookFriendsInPicogram == 1)
        {
            subtitleForFb = [numberOfContcts stringByAppendingString:NSLocalizedString(titleForFriend, tititleForFriendtle)];
        }
        else
        {
            subtitleForFb = [numberOfContcts stringByAppendingString:NSLocalizedString(titleForFriends, titleForFriends)];
        }
    }
    else {
        titleForFb = NSLocalizedString(connectToFacebook, connectToFacebook);
        subtitleForFb = NSLocalizedString(toFollowYourFriends, toFollowYourFriends);
        imageForFb = @"discover_people_facebook_icon_off";
    }
    
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:0 inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.discoverTableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}



-(void)updateFollowStatus {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollwoStatus:) name:@"updatedFollowStatus" object:nil];
}

-(void)updateFollwoStatus:(NSNotification *)noti {
    //check the postId and Its Index In array.
    
    if (!_classIsAppearing) {
        NSString *userName = flStrForObj(noti.object[@"newUpdatedFollowData"][@"userName"]);
        NSString *foolowStatusRespectToUser = noti.object[@"newUpdatedFollowData"][@"newFollowStatus"];
        
        if ([foolowStatusRespectToUser isEqualToString:@"2"]) {
            foolowStatusRespectToUser = @"0";
        }
        else if ([foolowStatusRespectToUser isEqualToString:@"0"]) {
            foolowStatusRespectToUser = @"2";
        }
        
        for (int i=0; i <respDeatils.count;i++) {
            if ([flStrForObj(respDeatils[i][@"postedByUserName"]) isEqualToString:userName]) {
                arrayOfFollowingStaus[i] = foolowStatusRespectToUser;
                NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:i inSection:1];
                NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
                [self.discoverTableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
                break;
            }
        }
    }
}

-(void)sendNewFollowStatusThroughNotification:(NSString *)userName andNewStatus:(NSString *)newFollowStatus {
    
    
    
    NSDictionary *newFollowDict = @{@"newFollowStatus"     :flStrForObj(newFollowStatus),
                                    @"userName"            :flStrForObj(userName),
                                    };
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedFollowStatus" object:[NSDictionary dictionaryWithObject:newFollowDict forKey:@"newUpdatedFollowData"]];
}




-(void )requestForDiscoverPeopleApi:(NSInteger)numb{
    //passing parameters(faceBookids list and token)
    NSDictionary *requestDict = @{
                                  mauthToken      :[Helper userToken],
                                  moffset:flStrForObj([NSNumber numberWithInteger:numb*10]),
                                  mlimit:flStrForObj([NSNumber numberWithInteger:10]),
                                  };
    //requesting the service and passing parametrs.
    [WebServiceHandler discoverPeople:requestDict andDelegate:self];
}




/*---------------------------------------------------------*/
#pragma mark - Pull To refresh
/*---------------------------------------------------------*/
-(void)addingRefreshControl {
    refreshControl = [[UIRefreshControl alloc]init];
    [self.discoverTableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
}

-(void)refreshData:(id)sender {
    neccessaryToRemoveOldPostsData = YES;
    [self requestForDiscoverPeopleApi:0];
}

#pragma mark
#pragma mark - table view delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return  [title count];
    }
    else {
        return  respDeatils.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 ) {
        return 50;
    }
    else {
       NSString *memberPrivateAccountState = flStrForObj(respDeatils[indexPath.row][@"privateProfile"]);
        

        
        NSMutableArray   *postDetails = [NSMutableArray new];
        [postDetails addObjectsFromArray:respDeatils[indexPath.row][@"postData"]];
        NSString *mainUrlFirst;
        if (postDetails.count >0) {
             mainUrlFirst = flStrForObj(postDetails[0][@"thumbnailImageUrl"]);
        }
        
        if (mainUrlFirst.length >2) {
           
        }
        else {
           [postDetails removeAllObjects];
        }
      
        if ([memberPrivateAccountState isEqualToString:@"1"]) {
            return 90;
        }
        else {
            if(postDetails.count ==0 ) {
                return 90;
                // 60 for first section and 50 for 2nd section(no need to show rectangular images just we need to show no posts available message.)
            }
            else {
                return 60 + self.view.frame.size.width/3;
            }
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    rowAt =indexPath;
    if (indexPath.section == 0) {
        static NSString *simpleTableIdentifier = @"Cell";
        PGDiscoverPeopleTableViewCell *cell;
        if (cell == nil) {
            cell = (PGDiscoverPeopleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        }
        
        if (indexPath.row == 0) {
            cell.TitleLabelOutlet.text = titleForFb ;
            cell.subTitleLabelOutlet.text =subtitleForFb;
            cell.imageViewOutlet.image =[UIImage imageNamed:imageForFb];
            
            if ([subtitleForFb isEqualToString:NSLocalizedString(toFollowYourFriends, toFollowYourFriends)]) {
                cell.subTitleLabelOutlet.textColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1.0];
            }
            else {
                cell.subTitleLabelOutlet.textColor = mBaseColor2;
            }
        }
        else {
            cell.TitleLabelOutlet.text = titleForContacts;
            cell.subTitleLabelOutlet.text = subTitleForContacts;
            cell.imageViewOutlet.image =[UIImage imageNamed:imageForContcts];
            
            
            if ([subTitleForContacts isEqualToString:NSLocalizedString(toFollowYourFriends, toFollowYourFriends)]) {
                cell.subTitleLabelOutlet.textColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1.0];
            }
            else {
                cell.subTitleLabelOutlet.textColor = mBaseColor2;
            }
        }
        return cell;
    }
    else {
        static NSString *simpleTableIdentifier = @"PostedImagecell";
        DiscoverTableViewPostedImagesCell *PostedImagecell;
        if (PostedImagecell == nil) {
            PostedImagecell = (DiscoverTableViewPostedImagesCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        }
        
        PostedImagecell.userNameLabelOutlet.text = flStrForObj(respDeatils[indexPath.row][@"postedByUserName"]);
        PostedImagecell.labelUnderUserNameOutelt.text = flStrForObj(respDeatils[indexPath.row][@"postedByUserFullName"]);
        
        [PostedImagecell layoutIfNeeded];
        PostedImagecell.followButtonOutlet .layer.cornerRadius = 2;
        PostedImagecell.followButtonOutlet .layer.borderWidth = 1;
        
        
        [self updateFollowButtonTitle:indexPath.row and:PostedImagecell.followButtonOutlet];
        
        if(([arrayOfFollowingStaus[indexPath.row]  isEqualToString:@"0"])) {
            PostedImagecell.followButtonTrailingConstraint.constant = 5;
            PostedImagecell.widthConstraintFollowButtonOutlet.constant = 60;
        }
        else {
            PostedImagecell.followButtonTrailingConstraint.constant = - 40;
            PostedImagecell.widthConstraintFollowButtonOutlet.constant = 105;
        }
        
        
        PostedImagecell.hideButtonOutlet.tag = 63000 + indexPath.row;
        [PostedImagecell.hideButtonOutlet addTarget:self action:@selector(hideButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [PostedImagecell.profileImageViewOutlet sd_setImageWithURL:[NSURL URLWithString:flStrForObj(respDeatils[indexPath.row][@"profilePicUrl"]]) placeholderImage:[UIImage imageNamed:@"itemUserDefault"]];
         
         NSMutableArray   *postDetails = [NSMutableArray new];
         [postDetails addObjectsFromArray:respDeatils[indexPath.row][@"postData"]];
         
         NSString *mainUrlFirst;
         if (postDetails.count >0) {
             mainUrlFirst = flStrForObj(postDetails[0][@"thumbnailImageUrl"]);
         }
         
         if (mainUrlFirst.length >2) {
             
         }
         else {
             [postDetails removeAllObjects];
         }
         
         NSString *memberPrivateAccountState = flStrForObj(respDeatils[indexPath.row][@"privateProfile"]);
         
         
         if ([memberPrivateAccountState isEqualToString:@"1"]) {
             //private account no need to show posts.
             PostedImagecell.viewWhenNoPostsAvailable.hidden = NO;
             PostedImagecell.messageWhenNoPostsAvailableLabelOutlet.text = @"This account is private. Follow to see photos.";
            
         }
         else {
             PostedImagecell.messageWhenNoPostsAvailableLabelOutlet.text = @"No Posts Available";
             
             if(postDetails.count == 0 ) {
                 //show view for no photos or videos message.
                 PostedImagecell.viewWhenNoPostsAvailable.hidden = NO;
                
                 // 60 for first section and 30 for 2nd section(no need to show rectangular images just we need to show no posts available message.)
             }
             else {
                 PostedImagecell.viewWhenNoPostsAvailable.hidden = YES;
                 
             }
         }
         
         
         if(postDetails.count == 1) {
             if(flStrForObj(respDeatils[indexPath.row][@"postData"][0][@"thumbnailImageUrl"])) {
                 [PostedImagecell.postedImage1  sd_setImageWithURL:[NSURL URLWithString:flStrForObj(respDeatils[indexPath.row][@"postData"][0][@"thumbnailImageUrl"])]];
             }
             PostedImagecell.postedImage2.image = nil;
             PostedImagecell.postedImage3.image = nil;
         }
         else if (postDetails.count  == 2) {
             if(flStrForObj(respDeatils[indexPath.row][@"postData"][0][@"thumbnailImageUrl"])) {
                 [PostedImagecell.postedImage1  sd_setImageWithURL:[NSURL URLWithString:flStrForObj(respDeatils[indexPath.row][@"postData"][0][@"thumbnailImageUrl"])]];
             }
             if(flStrForObj(respDeatils[indexPath.row][@"postData"][1][@"thumbnailImageUrl"])) {
                 [PostedImagecell.postedImage2  sd_setImageWithURL:[NSURL URLWithString:flStrForObj(respDeatils[indexPath.row][@"postData"][1][@"thumbnailImageUrl"])]];
             }
             PostedImagecell.postedImage3.image = nil;
         }
         else if (postDetails.count  > 2) {
             if(flStrForObj(respDeatils[indexPath.row][@"postData"][0][@"thumbnailImageUrl"])) {
                 [PostedImagecell.postedImage1  sd_setImageWithURL:[NSURL URLWithString:flStrForObj(respDeatils[indexPath.row][@"postData"][0][@"thumbnailImageUrl"])]];
             }
             if(flStrForObj(respDeatils[indexPath.row][@"postData"][1][@"thumbnailImageUrl"])) {
                 [PostedImagecell.postedImage2  sd_setImageWithURL:[NSURL URLWithString:flStrForObj(respDeatils[indexPath.row][@"postData"][1][@"thumbnailImageUrl"])]];
             }
             if(flStrForObj(respDeatils[indexPath.row][@"postData"][2][@"thumbnailImageUrl"])) {
                 [PostedImagecell.postedImage3  sd_setImageWithURL:[NSURL URLWithString:flStrForObj(respDeatils[indexPath.row][@"postData"][2][@"thumbnailImageUrl"])]];
             }
         }
         
         PostedImagecell.buttonForPostedImage1.tag = 1500 + indexPath.row;
         PostedImagecell.buttonForPostedImage2.tag = 2000 + indexPath.row;
         PostedImagecell.buttonForPostedImage3.tag = 2500 + indexPath.row;
         [PostedImagecell.buttonForPostedImage1 addTarget:self
                                                   action:@selector(buttonActionFor1stImage:)
                                         forControlEvents:UIControlEventTouchUpInside];
         [PostedImagecell.buttonForPostedImage2 addTarget:self
                                                   action:@selector(buttonActionFor2ndImage:)
                                         forControlEvents:UIControlEventTouchUpInside];
         [PostedImagecell.buttonForPostedImage3 addTarget:self
                                                   action:@selector(buttonActionFor3rdImage:)
                                         forControlEvents:UIControlEventTouchUpInside];
         [PostedImagecell layoutIfNeeded];
         PostedImagecell.profileImageViewOutlet.layer.cornerRadius = PostedImagecell.profileImageViewOutlet.frame.size.height/2;
         PostedImagecell.profileImageViewOutlet.clipsToBounds = YES;
         PostedImagecell.hideButtonOutlet.layer.borderWidth = 1;
         
         PostedImagecell.hideButtonOutlet.layer.borderColor = [UIColor colorWithRed:0.6781 green:0.6781 blue:0.6781 alpha:1.0].CGColor;
         return PostedImagecell;
         }
}
         
-(void)buttonActionFor3rdImage:(id)sender {
    
}
         
-(void)buttonActionFor2ndImage:(id)sender {
    
}
         
-(void)buttonActionFor1stImage:(id)sender {
    
}
         
-(void)updateFollowButtonTitle :(NSInteger )row and:(id)sender {
             
             
    UIButton *reeceivedButton = (UIButton *)sender;
       if(([arrayOfFollowingStaus[row]  isEqualToString:@"1"])) {
        [reeceivedButton makeButtonAsFollowing];
    }
    else if(([arrayOfFollowingStaus[row]  isEqualToString:@"0"])) {
        [reeceivedButton makeButtonAsFollow];
    }
    reeceivedButton.tag = 1000 + row;
    [reeceivedButton addTarget:self
                        action:@selector(FollowButtonAction:)
              forControlEvents:UIControlEventTouchUpInside];
}
         
         
         

         
-(void)hideButtonAction:(id)sender {
    NSIndexPath *indexPath = [self.discoverTableView indexPathForCell:(UITableViewCell *)[[[[sender superview] superview] superview] superview]];
    NSUInteger row = [indexPath row];
    
    // requesting hide particular user from suggestions.
    NSDictionary *requestDict = @{mmembername     : flStrForObj(respDeatils[row][@"postedByUserName"]),
                                  mauthToken            :[Helper userToken],
                                  };
    [WebServiceHandler hideFromDiscovery:requestDict andDelegate:self];
    
    [respDeatils removeObjectAtIndex:row];
    [arrayOfFollowingStaus removeObjectAtIndex:row];
    [_discoverTableView beginUpdates];
    [_discoverTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]  withRowAnimation:UITableViewRowAnimationTop];
    [_discoverTableView endUpdates];
}
         

         
-(void)FollowButtonAction:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;

    NSIndexPath *selectedCellForLike = [_discoverTableView indexPathForCell:(UITableViewCell *)[[[[sender superview] superview] superview] superview ]];

    DiscoverTableViewPostedImagesCell *selectedCell = [self.discoverTableView cellForRowAtIndexPath:selectedCellForLike];
    
        if ([selectedButton.titleLabel.text containsString:NSLocalizedString(followingButtonTitle, followingButtonTitle)]) {
            arrayOfFollowingStaus[selectedCellForLike.row] = @"0";
            
            [UIView animateWithDuration:0.1 animations:^ {
               [selectedButton makeButtonAsFollow];;
                selectedCell.followButtonTrailingConstraint.constant = 5;
                selectedCell.widthConstraintFollowButtonOutlet.constant = 60;
              
            }];
            
            [self sendNewFollowStatusThroughNotification:flStrForObj(respDeatils[selectedButton.tag%1000][@"postedByUserName"]) andNewStatus:@"2"];
            
            //passing parameters.    muserNameToUnFollow
            NSDictionary *requestDict = @{muserNameToUnFollow     : respDeatils[selectedCellForLike.row][@"postedByUserName"],
                                          mauthToken            :[Helper userToken],
                                          };
            //requesting the service and passing parametrs.
            [WebServiceHandler unFollow:requestDict andDelegate:self];
        }
        else {
            arrayOfFollowingStaus[selectedCellForLike.row] = @"1";
            
            [UIView animateWithDuration:0.1 animations:^ {
               [selectedButton makeButtonAsFollowing];
                selectedCell.followButtonTrailingConstraint.constant = -40;
                selectedCell.widthConstraintFollowButtonOutlet.constant = 105;
             
            }];
            
            [self sendNewFollowStatusThroughNotification:flStrForObj(respDeatils[selectedButton.tag%1000][@"postedByUserName"]) andNewStatus:@"1"];
            
            //passing parameters.
            NSDictionary *requestDict = @{muserNameTofollow     :respDeatils[selectedCellForLike.row][@"postedByUserName"],
                                          mauthToken            :flStrForObj([Helper userToken]),
                                          };
            //requesting the service and passing parametrs.
            [WebServiceHandler follow:requestDict andDelegate:self];
        }
}
         
         
#pragma mark
#pragma mark - navigation bar next button
         
- (void)createNavRightButton {
    UIButton *navDoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [navDoneButton setTitle:NSLocalizedString(doneButtonTitle, doneButtonTitle) forState:UIControlStateNormal];
     
    [navDoneButton setTitleColor:mNavigationBarColor forState:UIControlStateNormal];
     
    [navDoneButton setTitleColor:mNavigationBarColor  forState:UIControlStateHighlighted];
     
    navDoneButton.titleLabel.font = [UIFont fontWithName:RobotoRegular size:13];
    [navDoneButton setFrame:CGRectMake(0,0,50,30)];
    [navDoneButton addTarget:self action:@selector(DoneButtonAction:)
            forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navDoneButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
         
- (void)DoneButtonAction:(UIButton *)sender {
    // [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)createNavLeftButton {
    self.navigationController.navigationItem.hidesBackButton =  YES;
    UIButton  *navCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton rotateButton];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]  forState:UIControlStateNormal];
     
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName] forState:UIControlStateSelected];
     
    [navCancelButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
     
     
    [navCancelButton setFrame:CGRectMake(0,0,40,40)];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace  target:nil action:nil];
    negativeSpacer.width = -14;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
         
         
-(void)backButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}
         
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
             
    if (indexPath.section == 1) {
        ProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
        newView.memberName = flStrForObj(respDeatils[indexPath.row][@"postedByUserName"]);
        newView.isMemberProfile = YES;
        newView.productDetails = YES;
        [self.navigationController pushViewController:newView animated:YES];
    }
    else {
        if (indexPath.section == 0 && indexPath.row == 0) {
            ConnectToFaceBookViewController *postsVc = [self.storyboard instantiateViewControllerWithIdentifier:@"connectToFaceBookFriendsStoryBoardId"];
            postsVc.syncingContactsOf = @"faceBook";
            [self.navigationController pushViewController:postsVc animated:YES];
        }
        if (indexPath.section == 0 && indexPath.row == 1) {
            ConnectToFaceBookViewController *postsVc = [self.storyboard instantiateViewControllerWithIdentifier:@"connectToFaceBookFriendsStoryBoardId"];
            postsVc.syncingContactsOf = @"phoneBook";
            [self.navigationController pushViewController:postsVc animated:YES];
        }
    }
}
         
#pragma mark
#pragma mark - collection view delegates
         
        /**
         *  declaring numberOfSectionsInCollectionView
         *  @param collectionView declaring numberOfSectionsInCollectionView in collection view.
         *  @return number of sctions in collection view here it is 1.
         */
         
         - (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
             return 1;
         }
         
        /**
         *  declaring numberOfItemsInSection
         *  @param collectionView declaring numberOfItemsInSection in collection view.
         *  @param section    here only one section.
         *  @return number of items in collection view here it is 100.
         */
         
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
             NSMutableArray   *postedData = respDeatils[rowAt.row][@"postData"];
             return postedData.count;
}
         
        /**
         *  implementation of collection view cell
         *  @param collectionView collectionView has only image view
         *  @return implemented cell will return.
         */
         
         - (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
             static NSString *reuseIdentifier = @"discoverPeopleCollectionViewCell";
             collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
             //  collectionViewCell.postedImageViewOutlet.image = [UIImage imageNamed:@"instagr.am3_.jpg"];
             [ collectionViewCell.postedImageViewOutlet  sd_setImageWithURL:[NSURL URLWithString:flStrForObj(respDeatils[rowAt.row][@"postData"][indexPath.item][@"thumbnailImageUrl"])]  placeholderImage:[UIImage imageNamed:@""]];
             return collectionViewCell;
         }
         
         -(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
             
         }
         
         - (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
             return CGSizeMake(CGRectGetWidth(self.view.frame)/3,CGRectGetWidth(self.view.frame)/3);
         }
         
         /*-------------------------------------------------------*/
#pragma mark - Webservice Handler
#pragma mark - WebServiceDelegate
         /*------------------------------------------------------*/
         
- (void) didFinishLoadingRequest:(RequestType )requestType withResponse:(id)response error:(NSError*)error {
             // handling response.
       [avForCollectionView stopAnimating];
             if (error) {
                 [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
                 self.discoverTableView.backgroundView = nil;
                 [avForCollectionView stopAnimating];
                 [self showingMessageForCollectionViewBackground:[error localizedDescription]];
                 return;
             }
             
             NSDictionary *responseDict = (NSDictionary*)response;
    if (requestType == RequestTypeDiscoverPeople ) {
        
        [refreshControl endRefreshing];
        
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                //successs response.
                NSArray *newData = responseDict[@"discoverData"];
                
                if(neccessaryToRemoveOldPostsData) {
                    neccessaryToRemoveOldPostsData = NO;
                    [respDeatils removeAllObjects];
                    [arrayOfFollowingStaus removeAllObjects];
                    [respDeatils addObjectsFromArray:newData];
                    
                }
                else {
                    [respDeatils addObjectsFromArray:newData];
                }
                title = [NSArray arrayWithObjects:NSLocalizedString(connectToFacebook, connectToFacebook),NSLocalizedString(connectToContacts, connectToContacts), nil];
                if(respDeatils.count) {
                    for(int i = 0; i< newData.count;i++) {
                        NSString *followingstatus = flStrForObj(respDeatils[i][@"followsFlag"]);
                        [arrayOfFollowingStaus addObject:followingstatus];
                    }
                    [self.discoverTableView reloadData];
                }
                else {
                    [self.discoverTableView reloadData];
                    [self backGrounViewWithImageAndTitle:[NSString stringWithFormat:@"%@ %@" ,NSLocalizedString(noneOfFriendUsingApp, noneOfFriendUsingApp),APP_NAME]];
                }
            }
                break;
            case 91222: {
                [avForCollectionView stopAnimating];
                UIView *noDataAvailableMessageView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                [noDataAvailableMessageView setCenter:self.view.center];
                UILabel *message = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 -100, self.view.frame.size.height/2 -20, 200, 40)];
                message.textAlignment = NSTextAlignmentCenter;
                message.text = responseDict[@"message"];
                [noDataAvailableMessageView addSubview:message];
                self.discoverTableView.backgroundColor = [UIColor clearColor];
                self.discoverTableView.backgroundView = noDataAvailableMessageView;
            }
                break;
            default:
                break;
        }
    }
         }
         
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    //NSInteger result = maximumOffset - currentOffset;
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 10.0) {
        [self requestForMoreData:++offsetForMorePosts];
    }
}
         
         
-(void)requestForMoreData:(NSInteger )pageNumber {
    if(respDeatils.count %10 == 0) {
        pageNumber++;
        [self requestForDiscoverPeopleApi:pageNumber];
    }
}
         
       
-(UIView *)showingMessageForCollectionViewBackground:(NSString *)textmessage {
    UIView *noDataAvailableMessageView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [noDataAvailableMessageView setCenter:self.view.center];
    UILabel *message = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 -100, self.view.frame.size.height/2 -20, 200,100)];
    message.numberOfLines =0;
    message.textAlignment = NSTextAlignmentCenter;
    message.text = textmessage;
    [noDataAvailableMessageView addSubview:message];
    self.discoverTableView.backgroundColor = [UIColor whiteColor];
    self.discoverTableView.backgroundView = noDataAvailableMessageView;
    return noDataAvailableMessageView;
}
         
-(void)addingActivityIndicatorToCollectionViewBackGround {
    avForCollectionView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    avForCollectionView.frame =CGRectMake(self.view.frame.size.width/2 -12.5, self.view.frame.size.height/2 -12.5, 25,25);
    avForCollectionView.tag  = 1;
    [self.discoverTableView addSubview:avForCollectionView];
    [avForCollectionView startAnimating];
}
         
-(void)backGrounViewWithImageAndTitle:(NSString *)mesage{
             
    UIView *viewWhenNoPosts = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    UIImageView *image =[[UIImageView alloc] init];
    image.image = [UIImage imageNamed:@"ip5"];
    image.frame =  CGRectMake(self.view.frame.size.width/2 - 45, self.view.frame.size.height/2 - 45, 90, 90);
    [viewWhenNoPosts addSubview:image];
    
    UILabel *labelForNoPostsMessage = [[UILabel alloc] init];
    labelForNoPostsMessage.text = mesage;
    labelForNoPostsMessage.numberOfLines =0;
    labelForNoPostsMessage.textAlignment = NSTextAlignmentCenter;
    labelForNoPostsMessage.frame = CGRectMake(0, CGRectGetMaxY(image.frame) + 10, self.view.frame.size.width, 60);
    [labelForNoPostsMessage setFont:[UIFont fontWithName:RobotoRegular size:15]];
    labelForNoPostsMessage.textAlignment = NSTextAlignmentCenter;
    [viewWhenNoPosts addSubview:labelForNoPostsMessage];
    self.discoverTableView.backgroundColor = [UIColor whiteColor];
    self.discoverTableView.backgroundView = viewWhenNoPosts;
}
@end

//
//  StarTableViewCell.m

//
//  Created by Rahul Sharma on 27/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "RateUserTableViewCell.h"

@implementation RateUserTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)starActionButton:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    if (selectedButton.tag==1) {
        self.starOne.selected =YES;
        self.starTwo.selected =NO;
        self.starThree.selected =NO;
        self.starFour.selected =NO;
        self.starFive.selected =NO;
        self.ratingValue = 1;
         _yesLabel.textColor = [UIColor colorWithRed:153.0/255.0f green:153.0/255.0f blue:153.0/255.0f alpha:1.0f];
        _notLabel.textColor = mBaseOTPButton;
    }else  if (selectedButton.tag==2) {
        self.starOne.selected =YES;
        self.starTwo.selected =YES;
        self.starThree.selected =NO;
        self.starFour.selected =NO;
        self.starFive.selected =NO;
        self.ratingValue = 2;
        _yesLabel.textColor = [UIColor colorWithRed:153.0/255.0f green:153.0/255.0f blue:153.0/255.0f alpha:1.0f];
        _notLabel.textColor = [UIColor colorWithRed:153.0/255.0f green:153.0/255.0f blue:153.0/255.0f alpha:1.0f];
    }else  if (selectedButton.tag==3) {
        self.starOne.selected =YES;
        self.starTwo.selected =YES;
        self.starThree.selected =YES;
        self.starFour.selected =NO;
        self.starFive.selected =NO;
        self.ratingValue = 3;
        _yesLabel.textColor = [UIColor colorWithRed:153.0/255.0f green:153.0/255.0f blue:153.0/255.0f alpha:1.0f];
        _notLabel.textColor = [UIColor colorWithRed:153.0/255.0f green:153.0/255.0f blue:153.0/255.0f alpha:1.0f];
    }else  if (selectedButton.tag==4) {
        self.starOne.selected =YES;
        self.starTwo.selected =YES;
        self.starThree.selected =YES;
        self.starFour.selected =YES;
        self.starFive.selected =NO;
        self.ratingValue = 4;
        _yesLabel.textColor = [UIColor colorWithRed:153.0/255.0f green:153.0/255.0f blue:153.0/255.0f alpha:1.0f];
        _notLabel.textColor = [UIColor colorWithRed:153.0/255.0f green:153.0/255.0f blue:153.0/255.0f alpha:1.0f];
    }else  if (selectedButton.tag==5) {
        self.starOne.selected =YES;
        self.starTwo.selected =YES;
        self.starThree.selected =YES;
        self.starFour.selected =YES;
        self.starFive.selected =YES;
        self.ratingValue = 5;
        _notLabel.textColor = [UIColor colorWithRed:153.0/255.0f green:153.0/255.0f blue:153.0/255.0f alpha:1.0f];
        _yesLabel.textColor = mBaseOTPButton;
    }
    
}
@end

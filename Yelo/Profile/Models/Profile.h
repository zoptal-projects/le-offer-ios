//
//  Profile.h
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProfileDetails.h"

@interface Profile : NSObject

@property(nonatomic,retain)NSMutableArray *sellingPostData;
@property(nonatomic,retain)NSMutableArray *soldPostData;

@property(nonatomic,retain)NSMutableArray *likesPostData;
@property(nonatomic,retain)NSMutableArray *exchangePostData;

@property(nonatomic)NSUInteger selectedButtonIndex;
@property(nonatomic)BOOL *isMemberProfile;
@property(nonatomic,copy)NSString *memberName;
@property(nonatomic,retain)ProfileDetails *profileDetails;

@property(nonatomic)CGFloat profileCellHeight;
@property(nonatomic,strong)UITableViewCell *sectionHeaderCell;

@end

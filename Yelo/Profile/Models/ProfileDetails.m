//
//  ProfileDetails.m
//  Tac Traderz
//
//  Created by 3Embed on 27/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "ProfileDetails.h"

@implementation ProfileDetails

-(instancetype)initWithDictionary:(NSDictionary *)response
{
    self = [super init];
    self.averageRating = flStrForObj(response[@"avgRating"]);
    self.bio = flStrForObj(response[@"bio"]);
    self.email = flStrForObj(response[@"email"]);
    self.facebookVerified = flStrForObj(response[@"facebookVerified"]);
    self.emailVerified = flStrForObj(response[@"emailVerified"]);
    self.followers = flStrForObj(response[@"followers"]);
    self.following = flStrForObj(response[@"following"]);
    self.fullName = flStrForObj(response[@"fullName"]);
    self.googleVerified = flStrForObj(response[@"googleVerified"]);
      self.accountId = flStrForObj(response[@"accountId"]);
    self.is_strip_connected = flStrForObj(response[@"is_strip_connected"]);
    self.paypalUrl = flStrForObj(response[@"paypalUrl"]);
    self.phoneNumber = flStrForObj(response[@"phoneNumber"]);
    self.posts = flStrForObj(response[@"posts"]);
  self.profilePicUrl = flStrForObj(response[@"profilePicUrl"]);
    if(!self.profilePicUrl.length)
    {
        self.profilePicUrl = @"defaultUrl";
    }

    self.ratedByCount = flStrForObj(response[@"ratedByCount"]);
    if(!self.ratedByCount.length)
    {
        _ratedByCount = @"0";
    }
    
    self.username = flStrForObj(response[@"username"]);
    self.website = flStrForObj(response[@"website"]);
    self.followStatus = [flStrForObj(response[@"followStatus"])boolValue];
    if([self.username isEqualToString:[Helper userName]])
    {
        self.isOwnProfile = YES;
    }
    else
    {
         self.isOwnProfile = NO;
    }
    
    return self;
}


@end

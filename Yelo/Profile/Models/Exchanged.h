//
//  Exchanged.h
//  Tac Traderz
//
//  Created by 3Embed on 31/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Exchanged : NSObject


@property(nonatomic,copy)NSString *acceptedTime;
@property(nonatomic,copy)NSString *mainUrl;
@property(nonatomic,copy)NSString *productName;
@property(nonatomic,copy)NSString *postId;

@property(nonatomic,copy)NSString *swapMainUrl;
@property(nonatomic,copy)NSString *swapPostId;
@property(nonatomic,copy)NSString *swapProductName;
@property(nonatomic,copy)NSString *thumbnailImageUrl;
@property(nonatomic,copy)NSString *swapThumbnailImageUrl;

- (instancetype)initWithDictionary:(NSDictionary *)response;
+(NSMutableArray *) arrayOfExchangedPosts :(NSDictionary *)responseData ;

@end




//(lldb) po response
//{
//    code = 200;
//    data =     (
//                {
//                    acceptedOn = 1527767044692;
//                    city = bengaluru;
//                    condition = "Used(normal wear)";
//                    countrySname = in;
//                    currency = INR;
//                    description = Tst;
//                    imageUrl1 = "<null>";
//                    imageUrl2 = "<null>";
//                    imageUrl3 = "<null>";
//                    imageUrl4 = "<null>";
//                    latitude = "13.0286263";
//                    longitude = "77.5893672";
//                    mainUrl = "https://res.cloudinary.com/tactraderz-com/image/upload/fl_progressive:steep/v1527766073/iqpdptp2nyjic0ulsf17.jpg";
//                    negotiable = 0;
//                    place = "19, 1st Main Road, RBI Colony, Hebbal, Bengaluru, Karnataka 560024, India";
//                    postId = 1527766074364;
//                    price = 599;
//                    priceInUSD = "9.295468364510603";
//                    productName = gggg;
//                    productsTagged = "<null>";
//                    swapImageUrl1 = "<null>";
//                    swapImageUrl2 = "<null>";
//                    swapImageUrl3 = "<null>";
//                    swapImageUrl4 = "<null>";
//                    swapMainUrl = "https://res.cloudinary.com/tactraderz-com/image/upload/v1526365530/ghqsubncufybopexn4og.jpg";
//                    swapPostId = 1526365531724;
//                    swapProductName = Iphone;
//                    swapThumbnailImageUrl = "https://res.cloudinary.com/tactraderz-com/image/upload/q_60,w_150,h_150,c_thumb/v1526365530/ghqsubncufybopexn4og.jpg";
//                    thumbnailImageUrl = "https://res.cloudinary.com/tactraderz-com/image/upload/w_150,h_150/v1527766073/iqpdptp2nyjic0ulsf17.jpg";
//                }
//                );
//    message = success;
//}

//
//  ProfileDetails.h
//  Tac Traderz
//
//  Created by 3Embed on 27/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProfileDetails : NSObject

@property(nonatomic,copy)NSString *averageRating;
@property(nonatomic,copy)NSString *bio;
@property(nonatomic,copy)NSString *email;
@property(nonatomic,copy)NSString *emailVerified;
@property(nonatomic,copy)NSString *facebookVerified;
@property(nonatomic,copy)NSString *followers;
@property(nonatomic,copy)NSString *following;
@property(nonatomic,copy)NSString *fullName;
@property(nonatomic,copy)NSString *googleVerified;
@property(nonatomic,copy)NSString *accountId;
@property(nonatomic,copy)NSString *paypalUrl;
@property(nonatomic,copy)NSString *phoneNumber;
@property(nonatomic,copy)NSString *posts;
@property(nonatomic,copy)NSString *profilePicUrl;
@property(nonatomic,copy)NSString *ratedByCount;
@property(nonatomic,copy)NSString *username;
@property(nonatomic,copy)NSString *website;
@property(nonatomic,copy)NSString *is_strip_connected;
@property(nonatomic)BOOL isOwnProfile, followStatus;

-(instancetype)initWithDictionary:(NSDictionary *)response;
@end



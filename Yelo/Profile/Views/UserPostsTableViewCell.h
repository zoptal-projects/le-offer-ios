//
//  UserPostsTableViewCell.h
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileViewController.h"
#import "RFQuiltLayout.h"
#import "UserProfileCollectionViewCell.h"
#import "ProfileHeadersTableViewCell.h"

@interface UserPostsTableViewCell : UITableViewCell <UICollectionViewDelegate, UICollectionViewDataSource, RFQuiltLayoutDelegate, UITableViewDataSource, UITableViewDelegate, WebServiceHandlerDelegate >
{
    UserProfileCollectionViewCell *collectionViewCell;
}

@property (nonatomic,strong)ProfileViewController *profileVC;

@property (nonatomic,strong)Profile *profileObj;
@property (weak, nonatomic) IBOutlet UICollectionView *sectionCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (weak, nonatomic) IBOutlet UICollectionView *sellingCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *soldCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *likesCollectionView;
@property (weak, nonatomic) IBOutlet UIScrollView *slidingScrollView;
@property (weak, nonatomic) IBOutlet UITableView *exchangeTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *soldViewLeadingConstraint;


@end

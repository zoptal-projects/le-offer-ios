//
//  UserPostsTableViewCell.m
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "UserPostsTableViewCell.h"
#import "SectionCollectionViewCell.h"
#import "EditProductViewController.h"
#import "ProductDetailsViewController.h"
#import "ExchangePostsTableViewCell.h"
#import "FrameSize.h"



@implementation UserPostsTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePostFromNotification:) name:mDeletePostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(maintainFavoritesFromNotification:)name:mfavoritePostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(maintainSellingDataFromNotification:)name:mSellingPostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeSellingToSoldNotification:) name:mSellingToSoldNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updatePostDetailsOnEditing:) name:mUpdatePostDataNotification object:nil];
    [self RFQuiltLayoutIntialization];
   }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



#pragma mark - RFQuiltLayout Intialization

/**
 This method will create an object for RFQuiltLayout.
 */
-(void)RFQuiltLayoutIntialization
{
    RFQuiltLayout* layout = [[RFQuiltLayout alloc]init];
    layout.direction = UICollectionViewScrollDirectionVertical;
    if([[FrameSize sharedInstance] frame].size.width == 375){
        layout.blockPixels = CGSizeMake( 37,31);
    }
    else if([[FrameSize sharedInstance] frame].size.width == 414)
    {
        layout.blockPixels = CGSizeMake( 102,31);
    }
    else
    {
        layout.blockPixels = CGSizeMake( 79,31);
    }
    self.sellingCollectionView.collectionViewLayout = layout;
    layout.delegate = self;
    
    
    RFQuiltLayout* soldLayout = [[RFQuiltLayout alloc]init];
    soldLayout.direction = UICollectionViewScrollDirectionVertical;
    if([[FrameSize sharedInstance] frame].size.width == 375){
        soldLayout.blockPixels = CGSizeMake( 37,31);
    }
    else if([[FrameSize sharedInstance] frame].size.width == 414)
    {
        soldLayout.blockPixels = CGSizeMake( 102,31);
    }
    else{
        soldLayout.blockPixels = CGSizeMake( 79,31);
    }
    self.soldCollectionView.collectionViewLayout = soldLayout;
    soldLayout.delegate=self;
    
    RFQuiltLayout* favLayout = [[RFQuiltLayout alloc]init];
    favLayout.direction = UICollectionViewScrollDirectionVertical;
    if([[FrameSize sharedInstance] frame].size.width == 375)
    {
        favLayout.blockPixels = CGSizeMake( 37,31);
    }
    else if([[FrameSize sharedInstance] frame].size.width == 414)
    {
        favLayout.blockPixels = CGSizeMake( 102,31);
    }
    else{
        favLayout.blockPixels = CGSizeMake( 79,31);
    }
    self.likesCollectionView.collectionViewLayout = favLayout;
    favLayout.delegate=self;
    
}


/**
 *  declaring numberOfSectionsInCollectionView
 *
 *  @param collectionView declaring numberOfSectionsInCollectionView in collection view.
 *
 *  @return number of sctions in collection view here it is 1.
 */

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

/**
 *  declaring numberOfItemsInSection
 *  @param collectionView declaring numberOfItemsInSection in collection view.
 *  @param section    here only one section.
 *  @return number of items in collection view here it is 100.
 */

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    switch (collectionView.tag) {
        case 0:
        {
            return self.profileObj.sellingPostData.count ;
        }
            break;
        case 1:
        {
            return self.profileObj.soldPostData.count ;
        }
        break;
        default:
        {
            return self.profileObj.likesPostData.count ;
        }
            break;
    }
}

/**
 *  implementation of collection view cell
 *  @param collectionView collectionView has only image view
 *  @return implemented cell will return.
 */

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.item == 0) {
        //[self calculateCollectionViewHeightForTag:collectionView.tag];
    }
    switch (collectionView.tag)
    {
        case 0:
        {
            collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"sellingCellIdentifier" forIndexPath:indexPath];
            [collectionViewCell setProducts:self.profileObj.sellingPostData[indexPath.row]];
            return collectionViewCell;
        }
            break;
        case 1:
        {
            collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"soldCellIdentifier" forIndexPath:indexPath];
            [collectionViewCell setProducts:self.profileObj.soldPostData[indexPath.row]];
            return collectionViewCell;
            
        }
          
        break;
        default:
        {
            collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"favCellIdentifier" forIndexPath:indexPath];
            [collectionViewCell setProducts:self.profileObj.likesPostData[indexPath.row]];
            return collectionViewCell;
            
        }
            break;
    }
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (collectionView.tag) {
        case 0:
        {
            if(!self.profileVC.isMemberProfile)
            {
                
                EditProductViewController *editVC = [self.profileVC.storyboard instantiateViewControllerWithIdentifier:mEditItemStoryBoardID];
                editVC.product = self.profileObj.sellingPostData[indexPath.row] ;
                editVC.hidesBottomBarWhenPushed = YES;
                editVC.showInsights = YES ;
                [self.profileVC.navigationController pushViewController:editVC animated:YES];
                
            }
            else
            {
                
                    UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[self.sellingCollectionView cellForItemAtIndexPath:indexPath];
                    ProductDetailsViewController *newView = [self.profileVC.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
                    ProductDetails *product = self.profileObj.sellingPostData[indexPath.row];
                    newView.imageFromHome  = cell.postedImagesOutlet.image ;
                    newView.postId = product.postId ;
                    newView.movetoRowNumber =indexPath.item;
                    newView.dataFromHomeScreen = YES;
                    newView.hidesBottomBarWhenPushed = YES ;
                    newView.product = self.profileObj.sellingPostData[indexPath.row];
                    newView.indexPath = indexPath ;
                    [self.profileVC.navigationController pushViewController:newView animated:YES];
                
            }
        }
            break;
        case 1:
        {
            if(!self.profileVC.isMemberProfile)
            {
                
                UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[self.soldCollectionView cellForItemAtIndexPath:indexPath];
                ProductDetailsViewController *newView = [self.profileVC.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
                ProductDetails *product = self.profileObj.soldPostData[indexPath.row];
                newView.imageFromHome  = cell.postedImagesOutlet.image ;
                newView.postId = product.postId ;
                newView.movetoRowNumber =indexPath.item;
                newView.dataFromHomeScreen = YES;
                newView.hidesBottomBarWhenPushed = YES ;
                newView.product = self.profileObj.soldPostData[indexPath.row];
                newView.indexPath = indexPath ;
                [self.profileVC.navigationController pushViewController:newView animated:YES];
                
                
//                UIAlertController *alertController  = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(sellProductAgain, sellProductAgain) preferredStyle:UIAlertControllerStyleAlert];
//                UIAlertAction *yes = [UIAlertAction actionWithTitle:NSLocalizedString(alertYes, alertYes) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                    ProductDetails *product = self.profileObj.soldPostData[indexPath.row];
//                    NSDictionary *requestDic = @{ mpostid : product.postId ,
//                                                  mtype : @"0",
//                                                  mauthToken : [Helper userToken]
//                                                  };
//                    [ WebServiceHandler markAsSelling:requestDic andDelegate:self];
//
//                }];
//                UIAlertAction *no = [UIAlertAction actionWithTitle:NSLocalizedString(alertNo, alertNo) style:UIAlertActionStyleDefault handler:nil];
//                [alertController addAction:yes];
//                [alertController addAction:no];
//                [self.profileVC presentViewController:alertController animated:YES completion:nil];
            }
        }
            break;
        default:
        {
            {
                if (![[Helper userToken] isEqualToString:mGuestToken])
                {
                    UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[self.likesCollectionView cellForItemAtIndexPath:indexPath];
                    ProductDetailsViewController *newView = [self.profileVC.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
                    ProductDetails *product = self.profileObj.likesPostData[indexPath.row];
                    newView.imageFromHome  = cell.postedImagesOutlet.image ;
                    newView.hidesBottomBarWhenPushed = YES;
                    newView.postId = product.postId ;
                    newView.movetoRowNumber =indexPath.item;
                    newView.dataFromHomeScreen = YES;
                    newView.product = self.profileObj.likesPostData[indexPath.row];
                    [self.profileVC.navigationController pushViewController:newView animated:YES];
                }
            }
        }
            break;
    }
    
}

#pragma mark –
#pragma mark – RFQuiltLayoutDelegate

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (collectionView.tag)
    {
        case 0:
        {
            return [Helper blockSizeOfProduct:self.profileObj.sellingPostData[indexPath.row] view:self.profileVC.view forHome:YES];
        }
            break;
        case 1:
        {
            return [Helper blockSizeOfProduct:self.profileObj.soldPostData[indexPath.row] view:self.profileVC.view forHome:YES];
            
        }
            break;
        default:
        {
            return [Helper blockSizeOfProduct:self.profileObj.likesPostData[indexPath.row] view:self.profileVC.view forHome:YES];
        
            break;
    }
    
  }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetsForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return UIEdgeInsetsMake (5,5,0,0);
}


#pragma mark - UIScrollViewDelegate-
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    BOOL checkingMemberProfile = false;
    ProfileHeadersTableViewCell *cell =(ProfileHeadersTableViewCell *) self.profileObj.sectionHeaderCell;
    
    if (self.profileVC.isMemberProfile && ![[Helper userName]isEqualToString:self.profileVC.memberName]) {
        self.slidingScrollView.contentSize =  CGSizeMake(self.frame.size.width *2,self.slidingScrollView.contentSize.height ) ;
        checkingMemberProfile = YES;
    }
    if(scrollView == self.slidingScrollView)
    {
        CGPoint offset = self.slidingScrollView.contentOffset;
        if(self.profileVC.isMemberProfile && ![[Helper userName]isEqualToString:self.profileVC.memberName])
        {
            if([[Helper userToken] isEqualToString:mGuestToken])
            {
                [self.profileVC presentLoginForGuestUsers];
            }
        }
       
        if(offset.x ==0 )
        {
            [cell sellingButtonAction:cell.sellingButton];
        }
        
        else if(offset.x == CGRectGetWidth([[FrameSize sharedInstance] frame]))
        {
            if(checkingMemberProfile)
            {
             [cell soldButtonAction:cell.memberSoldButton];
            }else
            {
                [cell exchangesButtonAction:cell.exchangesButton];
            }
        }
        
        else if(offset.x == CGRectGetWidth([[FrameSize sharedInstance] frame])*2)
        {
              [cell soldButtonAction:cell.soldButton];
        }
//        else if (offset.x == CGRectGetWidth([[FrameSize sharedInstance] frame])*3)
//        {
//         //   [cell purchasedBtnAction:cell.purchaseButton];
//        }
        else if (offset.x == CGRectGetWidth([[FrameSize sharedInstance] frame])*3)
        {
            [cell likesButtonAction:cell.likesButton];
        }
    }
    
}


#pragma mark - TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  //  return self.profileObj.exchangePostData.count;
    return self.profileObj.exchangePostData.count;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     ExchangePostsTableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:@"exchangePostsCell"];
    [cell setExchangedPostsFor:self.profileObj.exchangePostData[indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 320;
}



/*------------------------------------*/
#pragma mark
#pragma mark - Notification Methods
/*------------------------------------*/

// selling Data.

-(void)deletePostFromNotification:(NSNotification *)noti {
    
    NSString *updatepostId = flStrForObj(noti.object[@"data"][@"postId"]);
    for (int i=0; i < self.profileObj.sellingPostData.count;i++) {
        
        ProductDetails *product = self.profileObj.sellingPostData[i];
        if ([product.postId isEqualToString:updatepostId])
        {
            NSInteger numberOfPosts = [self.profileObj.profileDetails.posts integerValue];
             numberOfPosts--;
            if (numberOfPosts >=0) {
                self.profileObj.profileDetails.posts = [NSString stringWithFormat:@"%ld",(long)numberOfPosts ];
            }
            else {
                self.profileObj.profileDetails.posts = 0;
            }
            [self.sellingCollectionView performBatchUpdates:^{
                [self.profileObj.sellingPostData removeObjectAtIndex:i];
                NSIndexPath *indexPath =[NSIndexPath indexPathForRow:i inSection:0];
                [self.sellingCollectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
            } completion:^(BOOL finished) {
                
            [self.profileVC.tableViewOutlet reloadData];
            }];
            break ;
        }
    }
}

/**
 Add/Remove products to/from favourites on like/unlike
 
 @param noti notification object which have the response of added/removed product.
 */
-(void)maintainFavoritesFromNotification:(NSNotification *)noti
{
    ProductDetails *newFavProduct =  noti.object[@"postdetails"];
    NSString *needtoaddAsFavourite = flStrForObj(noti.object[@"notificationForLike"]);
    if ([needtoaddAsFavourite isEqualToString:@"1"]) {
        //add post to favourites
        self.likesCollectionView.backgroundView = nil;
        if(!self.profileObj.likesPostData) {
            self.profileObj.likesPostData  = [[NSMutableArray alloc] init];
        }
        [self.profileObj.likesPostData insertObject:newFavProduct atIndex:0];
        [self.likesCollectionView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
            ProfileHeadersTableViewCell *cell = (ProfileHeadersTableViewCell *)self.profileObj.sectionHeaderCell;
            [cell calculateCollectionViewHeightForTag:2];
            [self.profileVC.tableViewOutlet reloadData];
        });
    }
    else
    {
        //if  post is exist then remove from favourites
        for (int i=0; i <self.profileObj.likesPostData.count;i++) {
            ProductDetails *existingProduct = self.profileObj.likesPostData[i];
            if ([existingProduct.postId isEqualToString:newFavProduct.postId])
            {
                [self.likesCollectionView  performBatchUpdates:^{
                    [self.profileObj.likesPostData removeObjectAtIndex:i];
                    NSIndexPath *indexPath =[NSIndexPath indexPathForRow:i inSection:0];
                    [self.likesCollectionView  deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
                } completion:^(BOOL finished) {
                }];
                
                
                break;
            }
        }
        
        if(!self.profileObj.likesPostData.count){
            ProfileHeadersTableViewCell *cell = (ProfileHeadersTableViewCell *)self.profileObj.sectionHeaderCell;
            [cell showingBackgroundMessageFor:self.likesCollectionView type:2];
        }
    }
    
}


/**
 Add product to Sold data and remove from selling on uploading any product.
 
 @param noti notification object which have the details of sold product.
 */
-(void)changeSellingToSoldNotification:(NSNotification *)noti
{
    ProductDetails *soldProduct = [[ProductDetails alloc]initWithDictionary: noti.object[@"data"]];
    for (int i=0; i < self.profileObj.sellingPostData.count;i++) {
        ProductDetails *product = self.profileObj.sellingPostData[i];
        if ([product.postId isEqualToString:soldProduct.postId])
        {
            [self.profileObj.sellingPostData removeObjectAtIndex:i];
            
            [self.sellingCollectionView reloadData];
            
            [self.profileObj.soldPostData insertObject:soldProduct atIndex:0];
            
            if(!self.profileObj.sellingPostData.count)
            {   [self.profileObj.sellingPostData removeAllObjects];
                ProfileHeadersTableViewCell *cell = (ProfileHeadersTableViewCell *)self.profileObj.sectionHeaderCell;
            [cell showingBackgroundMessageFor:self.sellingCollectionView type:0];
            }
            break ;
        }
    }
    
    
    
}



/**
 Add selling product on uploading any new product.
 
 @param noti notification object whaich have the response of added product.
 */
-(void)maintainSellingDataFromNotification:(NSNotification *)noti
{
    ProductDetails *newSellingProduct = [[ProductDetails alloc]initWithDictionary: noti.object[0]];
    
    if(!self.profileObj.sellingPostData) {
        self.profileObj.sellingPostData  = [[NSMutableArray alloc] init];
    }
    [self.profileObj.sellingPostData insertObject:newSellingProduct atIndex:0];
    
    ProfileHeadersTableViewCell *cell = (ProfileHeadersTableViewCell *)self.profileObj.sectionHeaderCell;
    
      [self.sellingCollectionView reloadData];
      if(cell.sellingButton.selected)
       {
        if(!(self.profileObj.sellingPostData.count > 0)){
    [cell showingBackgroundMessageFor:self.sellingCollectionView type:0];
        }
        else
        {
        self.sellingCollectionView.backgroundView = nil;
            
        }
        [cell calculateCollectionViewHeightForTag:0];
        dispatch_async(dispatch_get_main_queue(), ^{
        [self.profileVC.tableViewOutlet reloadData];
        });
     }
}



/**
 Selling sold product again.
 */
-(void)sellingAgainPost :(NSDictionary *)postDetails
{
    ProductDetails *sellProduct = [[ProductDetails alloc]initWithDictionary:postDetails[@"data"]];
    
    for (int i=0; i < self.profileObj.soldPostData.count;i++) {
        ProductDetails *soldProduct = self.profileObj.soldPostData[i];
        if ([soldProduct.postId isEqualToString:sellProduct.postId])
        {
            [self.soldCollectionView performBatchUpdates:^{
                [self.profileObj.soldPostData removeObjectAtIndex:i];
                NSIndexPath *indexPath =[NSIndexPath indexPathForRow:i inSection:0];
                [self.soldCollectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
            } completion:^(BOOL finished) {
            }];
            [self.profileObj.sellingPostData insertObject:sellProduct atIndex:0];
            
            break ;
        }
    }
    
    if(!self.profileObj.soldPostData.count)
    {
        
        ProfileHeadersTableViewCell *cell = (ProfileHeadersTableViewCell *)self.profileObj.sectionHeaderCell;
        
        [cell showingBackgroundMessageFor:self.soldCollectionView type:1];
    }
    
}



/**
 Update Post Data on editing Notification Method.
 
 @param noti notification object.
 */
-(void)updatePostDetailsOnEditing:(NSNotification *)noti
{
    ProductDetails *updateProduct = [[ProductDetails alloc]initWithDictionary:noti.object[@"data"][0]];
    for (int i=0; i < self.profileObj.sellingPostData.count;i++) {
        ProductDetails *sellingProduct = self.profileObj.sellingPostData[i];
        if ([sellingProduct.postId isEqualToString:updateProduct.postId])
        {
            [self.profileObj.sellingPostData replaceObjectAtIndex:i withObject:updateProduct];
            [self.sellingCollectionView performBatchUpdates:^{
                NSIndexPath *indexPath =[NSIndexPath indexPathForRow:i inSection:0];
                [self.sellingCollectionView reloadItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
            } completion:^(BOOL finished) {
            }];
            break ;
        }
    }
    
}



#pragma mark - Webservice Delegate -

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self.profileVC];
    }
    
    switch (requestType) {
            // Case User Marks his sold post back to selling.
        case RequestToMarkAsSelling:
        {
            switch ([response[@"code"] integerValue]) {
                case 200: {
                    
                    [[NSNotificationCenter defaultCenter]postNotificationName:mSellingAgainNotification object:[NSDictionary dictionaryWithObject:response[@"data"] forKey:@"postDetails"]];
                    
                    [self sellingAgainPost:response];
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    // Case user Selling Post/Sold Posts details request.
    
    
}





//
///*------------------------------*/
//#pragma mark- CollectionView DataSource Method
///*------------------------------*/
//
//-(NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//
//    return 4;
//}
//
//-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    switch (indexPath.row) {
//        case 0:{
//            SectionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"postSectionCell" forIndexPath:indexPath];
//            cell.postsArray = self.profileObj.sellingPostData;
//            cell.postsCollectionView.hidden = NO;
//            cell.soldPostCollectionView.hidden = YES;
//            cell.likesCollectionView.hidden = YES;
//            [cell.postsCollectionView reloadData];
//
//            return cell;
//        }
//
//            break;
//        case 2:
//        {
//            SectionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"postSectionCell" forIndexPath:indexPath];
//            cell.postsArray = self.profileObj.soldPostData;
//            cell.postsCollectionView.hidden = NO;
//            cell.soldPostCollectionView.hidden = YES;
//            cell.likesCollectionView.hidden = YES;
//            [cell.postsCollectionView reloadData];
//            return cell;
//        }
//            break;
//        case 3:
//        {
//            SectionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"postSectionCell" forIndexPath:indexPath];
////            cell.postsArray = self.profileObj.likesPostData;
////            cell.postsCollectionView.hidden = YES;
////            cell.soldPostCollectionView.hidden = YES;
////            cell.likesCollectionView.hidden = NO;
////            [cell.likesCollectionView reloadData];
//            cell.postsArray = self.profileObj.likesPostData;
//            cell.postsCollectionView.hidden = NO;
//            cell.soldPostCollectionView.hidden = YES;
//            cell.likesCollectionView.hidden = YES;
//            [cell.postsCollectionView reloadData];
//            return cell;
//        }
//        default:
//        {
//            SectionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"postSectionCell" forIndexPath:indexPath];
//            return cell;
//        }
//
//            break;
//    }
//
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    CGFloat height = 320;
//
//    switch (indexPath.row) {
//        case 10:
//        {
//            SectionCollectionViewCell *cell = (SectionCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
//            if(cell)
//            {
//                height = cell.postsCollectionView.contentSize.height;
//                self.heightConstraint.constant = height;
//           //     [self.profileVC.tableViewOutlet reloadData];
//            }
//        }
//            break;
//        case 12:
//        {
//            SectionCollectionViewCell *cell = (SectionCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
//            if(cell)
//            {
//                height = cell.soldPostCollectionView.contentSize.height;
//                self.heightConstraint.constant = height;
//           //     [self.profileVC.tableViewOutlet reloadData];
//            }
//        }
//            break;
//        case 13:
//        {
//            SectionCollectionViewCell *cell = (SectionCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
//            if(cell)
//            {
//                height = cell.likesCollectionView.contentSize.height;
//                self.heightConstraint.constant = height;
//        //        [self.profileVC.tableViewOutlet reloadData];
//            }
//        }
//            break;
//
//        default:
//        {
//            SectionCollectionViewCell *cell = (SectionCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
//            if(cell)
//            {
//                height = cell.postsCollectionView.contentSize.height;
//                self.heightConstraint.constant = height;
////                [self.profileVC.tableViewOutlet reloadData];
//            }
//        }
//            break;
//    }
//
//
//    return CGSizeMake(320, height);
//}
//
//-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsZero ;
//}
//
//
//

@end

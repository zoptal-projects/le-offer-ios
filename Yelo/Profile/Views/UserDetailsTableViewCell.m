//
//  UserDetailsTableViewCell.m
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "UserDetailsTableViewCell.h"
#import "LikeViewController.h"
#import "UpDatingEmailViewController.h"
#import "KILabel.h"

@implementation UserDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contactButtonOutlet.layer.borderColor = mOurThemeBaseColour.CGColor ;
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(savePayPalLink:) name:mSavePayPalNotify object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateProfilePicUrl:) name:@"updateProfilePic" object:nil];
    [self handlingWebUrlFromBio];
    [self tapGestureForWebsiteUrl];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)savePayPalLink:(NSNotification *)noti
{
    [self PaypalLinkIsSaved:noti.object];
}


- (IBAction)followersButtonAction:(id)sender {
    if([[Helper userToken] isEqualToString:mGuestToken]){
        [self.profileVC presentLoginForGuestUsers];
    }
    else{
        if (self.profileVC.memberName) {
            //Opening LikeViewController With navigation title as FOLLOWERS.
            LikeViewController *newView = [self.profileVC.storyboard instantiateViewControllerWithIdentifier:@"likeStoryBoardId"];
            newView.navigationTitle = NSLocalizedString(navTitleForFollowers, navTitleForFollowers);
            newView.getdetailsDetailsOfUserName = self.profileVC.memberName;
            [self.profileVC.navigationController pushViewController:newView animated:YES];
        }
        else {
            //Opening LikeViewController With navigation title as FOLLOWERS.
            LikeViewController *newView = [self.profileVC.storyboard instantiateViewControllerWithIdentifier:@"likeStoryBoardId"];
            newView.navigationTitle = NSLocalizedString(navTitleForFollowers, navTitleForFollowers);
            [self.profileVC.navigationController pushViewController:newView animated:YES];
        }
    }
}

- (IBAction)followingButtonAction:(id)sender {
    if([[Helper userToken] isEqualToString:mGuestToken]){
        [self.profileVC presentLoginForGuestUsers];
    }
    else{
        if (self.profileVC.memberName) {
            //Opening LikeViewController With navigation title as FOLLOWING.
            LikeViewController *newView = [self.profileVC.storyboard instantiateViewControllerWithIdentifier:@"likeStoryBoardId"];
            newView.navigationTitle = NSLocalizedString(navTitleForFollowing, navTitleForFollowing);
            newView.getdetailsDetailsOfUserName = self.profileVC.memberName;
            [self.profileVC.navigationController pushViewController:newView animated:YES];    }
        else {
            //Opening LikeViewController With navigation title as FOLLOWING.
            LikeViewController *newView = [self.profileVC.storyboard instantiateViewControllerWithIdentifier:@"likeStoryBoardId"];
            newView.navigationTitle = NSLocalizedString(navTitleForFollowing, navTitleForFollowing);
            [self.profileVC.navigationController pushViewController:newView animated:YES];
        }
    }
}

-(void)setProfileDetails :(ProfileDetails *)profile
{
    
    if ([profile.profilePicUrl isEqualToString:@"defaultUrl"]) {
        self.profileVC.profilePic.image = [UIImage imageNamed:@"defaultpp"];
        self.profileVC.headerProfileImage.image = [UIImage imageNamed:@"profile_default bg"];
        self.profileVC.headerProfileImage.alpha = 1;
    }
    else  {
        [self.profileVC.profilePic setImageWithURL:[NSURL URLWithString:profile.profilePicUrl] placeholderImage:[UIImage imageNamed:@"defaultpp"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.profileVC.headerProfileImage sd_setImageWithURL:[NSURL URLWithString:profile.profilePicUrl]];
        
        self.profileVC.headerProfileImage.alpha = 0.5;
    }
    self.profileName.text = profile.fullName;
    
    self.postsCountLabel.text = profile.posts;
    self.followersCountLabel.text = profile.followers;
    self.followingCountLabel.text = profile.following;
    
    if (profile.isOwnProfile)     {
            self.contactView.hidden = YES;
            self.topConstraintRatingView.constant = -25;
        }
        else {
            self.contactView.hidden = NO;
            self.topConstraintRatingView.constant = 5;
            if (profile.followStatus) {
                [self.followButtonOutlet makeButtonAsFollowing];
            }
            else
            {
                [self.followButtonOutlet makeButtonAsFollow];
            }
        }
    self.labelForWebsite.text = profile.website;
    self.bioLabel.text = profile.bio ;

    [[NSUserDefaults standardUserDefaults]setObject:profile.paypalUrl forKey:payPalLink];
    
    self.averageRatingCount.text = [NSString stringWithFormat:@"(%@)",flStrForObj(profile.ratedByCount)];
    
    [self setVerificationStatusforProfile:profile];
    [self showUerRating:[profile.averageRating integerValue]];

    
    if(self.isAPIcall){
     self.isAPIcall = NO ;
     self.verifyView.hidden = NO ;
    }
    
    [self updateConstraints];
    [self updateConstraintsIfNeeded];
    
}

#pragma mark - Contact Button Action

- (IBAction)contactButtonAction:(id)sender {
    
    if([[Helper userToken] isEqualToString:mGuestToken])
    {
        [self.profileVC presentLoginForGuestUsers];
    }
    else{
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(actionsheetTitleForCall, actionsheetTitleForCall) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [self callNumber:self.profileObj.profileDetails.phoneNumber];
            
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(actionsheetEmailOptionTitle, actionsheetEmailOptionTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            // [self showMailPanel];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(alertCancel, alertCancel) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // OK button tapped.
            
            [self.profileVC dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        
        // Present action sheet.
        [self.profileVC presentViewController:actionSheet animated:YES completion:nil];
    }
    
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}



/**
 Method to call on registered contact of User.
 */
-(void)callNumber :(NSString *)num
{
    
    NSString *callThisNumber = [@"tel://" stringByAppendingString:num];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callThisNumber]];
}


/**
 This method get invoked on choosing email option in contact.
 */
-(void)showMailPanel{
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;        // Required to invoke mailComposeController when send
        [mailCont setSubject:NSLocalizedString(emailSubject, emailSubject)];
        [mailCont setToRecipients:[NSArray arrayWithObject:flStrForObj(self.profileObj.profileDetails.email)]];
        [mailCont setMessageBody:@" " isHTML:NO];
        mailCont.navigationBar.tintColor = [UIColor blackColor];
        [mailCont.navigationBar setBackgroundColor:[UIColor whiteColor]];
        mailCont.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:mOurThemeBaseColour};
        [self.profileVC presentViewController:mailCont animated:YES completion:^{
        }];
    }
}


#pragma mark - follow Button Action

- (IBAction)followButtonAction:(id)sender {
    
    if([[Helper userToken] isEqualToString:@"guestUser"])
    {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
       [self.profileVC presentViewController:navigationController animated:YES completion:nil];
    }
    else
    {
        
        CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        [ani setDuration:0.1];
        [ani setRepeatCount:1];
        [ani setFromValue:[NSNumber numberWithFloat:1.0]];
        [ani setToValue:[NSNumber numberWithFloat:0.9]];
        [ani setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[self.followButtonOutlet layer] addAnimation:ani forKey:@"zoom"];
        
        //actions for when the account is public.
        if ([self.followButtonOutlet.titleLabel.text isEqualToString:NSLocalizedString(followingButtonTitle, followingButtonTitle)]) {
            [Helper showUnFollowAlert:self.profileVC.profilePic.image and:self.profileName.text viewControllerReference:self.profileVC onComplition:^(BOOL isUnfollow)
             {
                 if(isUnfollow)
                 {
                     [self unfollowAction];
                 }
             }];
        }
        else {
            
            [self increaseTheFollowersCount:self.profileObj.profileDetails.followers];
            [self sendNewFollowStatusThroughNotification:flStrForObj(self.profileVC.memberName) andNewStatus:@"1"];
            
            [self.followButtonOutlet makeButtonAsFollowing];
            
            //passing parameters.
            NSDictionary *requestDict = @{muserNameTofollow     :self.profileVC.memberName,
                                          mauthToken            :flStrForObj([Helper userToken]),
                                          mdeviceToken          :[Helper deviceToken]
                                          };
            //requesting the service and passing parametrs.
            [WebServiceHandler follow:requestDict andDelegate:self];
        }
    }
}


/*-----------------------------------------------------*/
#pragma mark -
#pragma mark - CustomActionSheet
/*----------------------------------------------------*/

-(void)unfollowAction {
    
    if ([self.followButtonOutlet.titleLabel.text isEqualToString:NSLocalizedString(followingButtonTitle, followingButtonTitle)]) {
        [self decreaseFollowersCount:self.profileObj.profileDetails.followers];
    }
    [self sendNewFollowStatusThroughNotification:self.profileVC.memberName andNewStatus:@"2"];
    
    [self.followButtonOutlet makeButtonAsFollow];
    //passing parameters.
    NSDictionary *requestDict = @{muserNameToUnFollow     : self.profileVC.memberName ,
                                  mauthToken            :flStrForObj([Helper userToken]),
                                  };
    //requesting the service and passing parametrs.
    [WebServiceHandler unFollow:requestDict andDelegate:self];
}


/*-----------------------------------------------*/
#pragma mark -
#pragma mark - Increase Follow Count
/*-----------------------------------------------*/

/**
 This method will increase Follow count on following.
 
 @param followersCount Follower count in string format.
 */
-(void)increaseTheFollowersCount:(NSString *)followersCount {
    NSInteger numberOfFollowers = [followersCount integerValue];
    numberOfFollowers ++;
    if (numberOfFollowers < 0 || numberOfFollowers ==0) {
        self.followersCountLabel.text = @"0";
    }
    else {
        self.followersCountLabel.text = flStrForInt(numberOfFollowers);
    }
     self.profileObj.profileDetails.followers = self.followersCountLabel.text;
}

/*-----------------------------------------------*/
#pragma mark -
#pragma mark - Decrease Follow Count
/*-----------------------------------------------*/

/**
 This method will decrease Follow count on unfollowing.
 
 @param followersCount Follower count in string format.
 */

-(void)decreaseFollowersCount:(NSString *)followersCount {
    NSInteger numberOfFollowers = [followersCount integerValue];
    
    numberOfFollowers --;
    
    if (numberOfFollowers < 0 || numberOfFollowers ==0) {
        self.followersCountLabel.text = @"0";
        //        self.followersButtonOutlet.enabled = NO;
    }
    else {
        self.followersCountLabel.text = flStrForInt(numberOfFollowers);
        //        self.followersButtonOutlet.enabled = YES;
    }
     self.profileObj.profileDetails.followers = self.followersCountLabel.text;
}


-(void)sendNewFollowStatusThroughNotification:(NSString *)userName andNewStatus:(NSString *)newFollowStatus {
    
    
    
    NSDictionary *newFollowDict = @{@"newFollowStatus"     :flStrForObj(newFollowStatus),
                                    @"userName"            :flStrForObj(userName),
                                    };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedFollowStatus" object:[NSDictionary dictionaryWithObject:newFollowDict forKey:@"newUpdatedFollowData"]];
}



-(void)getProfileDetails
{
    if (self.profileVC.isMemberProfile && self.profileVC.memberName.length) {
        NSDictionary *requestDict = @{
                                      mauthToken :[Helper userToken],
                                      mmemberName : self.profileVC.memberName
                                      };
        if([[Helper userToken]isEqualToString:mGuestToken])
        {
            [WebServiceHandler getProfileDetailsOfMemberForGuest:requestDict andDelegate:self];
        }
        else
            
            [WebServiceHandler getProfileDetailsOfMember :requestDict andDelegate:self];
    }
    
    else
    {
        
        NSDictionary *requestDict = @{
                                      mauthToken :[Helper userToken],
                                      };
        
        [WebServiceHandler getProfileDetailsOfUser :requestDict andDelegate:self];
    }
    
}


#pragma mark - Webservice Delegate -

-(void)internetIsNotAvailable:(RequestType)requsetType {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
   
    [self.profileVC.refreshControl endRefreshing];
}

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [self.profileVC.refreshControl endRefreshing];
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self.profileVC];
    }
    
    switch (requestType) {
            
            // Case to handle Profile details of User.
        case RequestTypemakeUserProfileDetails:
        {
           // [self updateDetailsOfUser:response];
        }
            break;
            
            // Case to handle Profile details of Member.
        case RequestTypeProfileDetails:
        {
            NSLog(@"Data=%@",response[@"data"][0]);
            self.profileObj.profileDetails = [[ProfileDetails alloc]initWithDictionary:response[@"data"][0]];
            self.isAPIcall = YES;
            [self.profileVC.tableViewOutlet reloadData];
        }
            break;
            // Case when user verify Facebook stag.
        case RequestToVerifyWithFacebook:
        {
            switch ([response[@"code"] integerValue])
            {
                case 200: {
                    self.verifyFacebookImage.image = [UIImage imageNamed:@"facebook verified"];
                    
                    self.verifyFacebookButtonOutlet.enabled = NO ;
                   self.profileObj.profileDetails.facebookVerified = @"1";
                }
                    break;
                case 409 :{
                    [Helper showAlertWithTitle:nil Message:NSLocalizedString(facebookAccountAlreadyLinked, facebookAccountAlreadyLinked) viewController:self.profileVC];
                    break;
                default:
                    break;
                }
            }
        }
            break;
        default:
            break;
    }
}




-(void)setVerificationStatusforProfile:(ProfileDetails *)profile
{
    // Facebook Verification Status
          if(![profile.facebookVerified boolValue])
            {
                if(profile.isOwnProfile){self.verifyFacebookButtonOutlet.enabled = YES; }
                
                else {self.verifyFacebookButtonOutlet.enabled = NO;
                    
                }
                    self.verifyFacebookImage.image = [UIImage imageNamed:@"facebook unverified"];
            }
            else
            {   self.verifyFacebookButtonOutlet.enabled = NO;
                self.verifyFacebookImage.image = [UIImage imageNamed:@"facebook verified"];
            }

    // GoogleVerification Status
    
            if(![profile.googleVerified boolValue])
            {
                if(profile.isOwnProfile){self.verifyGoogleButtonOutlet.enabled = YES; }
                else {self.verifyGoogleButtonOutlet.enabled = NO; }
                self.googleVerifyImage.image = [UIImage imageNamed:@"g+ unverified"];
            }
            else
            {
                self.verifyGoogleButtonOutlet.enabled = NO;
                self.googleVerifyImage.image = [UIImage imageNamed:@"g+ verified"];
            }
    
   // EMAIL Status

            if(![profile.emailVerified boolValue])
            {
                if(profile.isOwnProfile){self.verifyEmailButtonOutlet.enabled = YES; }
                else {self.verifyEmailButtonOutlet.enabled = NO; }
                self.verifyEmailImage.image = [UIImage imageNamed:@"email unverified"];
            }
            else
            {
                self.verifyEmailButtonOutlet.enabled = NO;
                self.verifyEmailImage.image = [UIImage imageNamed:@"email verified"];
            }
   
    // paypal Status

            if(profile.paypalUrl.length < 2)
            {
                if(profile.isOwnProfile)
                {
                    self.paypalButtonOutlet.enabled = YES;
                }
                
                else
                {
                    self.paypalButtonOutlet.enabled = NO;
                }
                
                [self.paypalButtonOutlet setImage:[UIImage imageNamed:@"paypal unverified"] forState:UIControlStateNormal];
                self.paypalButtonOutlet.layer.borderWidth = 0.5;
                self.paypalButtonOutlet.layer.borderColor = [UIColor lightGrayColor].CGColor;
                [self.paypalButtonOutlet setTitle:@"Paypal.Me" forState:UIControlStateNormal];
                
            }
            else
            {
                if(profile.isOwnProfile)
                {
                    self.paypalButtonOutlet.enabled = YES;
                }
                else
                {   self.paypalButtonOutlet.hidden = YES ;
                    self.paypalButtonOutlet.enabled = NO;
                    self.payapalImage.hidden = NO;
                    self.payapalImage.image = [UIImage imageNamed:@"paypal verified"];
                }
                
                [self.paypalButtonOutlet setImage:[UIImage imageNamed:@"paypal verified"] forState:UIControlStateNormal];
                self.paypalButtonOutlet.layer.borderWidth = 0;
                self.paypalButtonOutlet.layer.borderColor = [UIColor clearColor].CGColor;
                [self.paypalButtonOutlet setTitle:@"" forState:UIControlStateNormal];
                
            }

}


/**
 Star Rating For user on the basis of average ratings.
 
 @param rating integer value.
 */
-(void)showUerRating:(NSInteger)rating
{
    switch (rating) {
        case 1:
            self.starButton1.selected = YES;
            break;
        case 2:
            self.starButton1.selected = YES;
            self.starButton2.selected = YES;
            break;
        case 3:
            self.starButton1.selected = YES;
            self.starButton2.selected = YES;
            self.starButton3.selected = YES;
            break;
        case 4:
            self.starButton1.selected = YES;
            self.starButton2.selected = YES;
            self.starButton3.selected = YES;
            self.starButton4.selected = YES;
            break;
        case 5:
            self.starButton1.selected = YES;
            self.starButton2.selected = YES;
            self.starButton3.selected = YES;
            self.starButton4.selected = YES;
            self.starButton5.selected = YES;
            break;
        default:
            break;
    }
}

                        


- (IBAction)verifyFacebookButtonAction:(id)sender {
    
    FBLoginHandler *handler = [FBLoginHandler sharedInstance];
    [handler loginWithFacebook:self.profileVC];
    [handler setDelegate:self];
    
}

- (IBAction)verifyGoogleButtonAction:(id)sender {
//    GIDSignIn *signin = [GIDSignIn sharedInstance];
//    [signin signOut];
//    signin.shouldFetchBasicProfile = true;
//    signin.delegate = self.profileVC;
//    signin.uiDelegate = self.profileVC;
//    [signin signIn];
    
}
- (IBAction)verifyEmailButtonAction:(id)sender {
    
    UpDatingEmailViewController *updateEmailVC = [self.profileVC.storyboard instantiateViewControllerWithIdentifier:@"verifyEmail"];
    updateEmailVC.verifyEmail = YES;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:updateEmailVC];
    [self.profileVC.navigationController presentViewController:navigationController animated:YES completion:nil];
}

- (IBAction)verifyMobileButtonAction:(id)sender {
    
    
    
}

- (IBAction)paypalButtonAction:(id)sender {
    PaypalViewController *newView = [self.profileVC.storyboard instantiateViewControllerWithIdentifier:mpaypalView];
    newView.paypalCallback = self;
    newView.paypalLink = self.profileObj.profileDetails.paypalUrl;
    newView.hidesBottomBarWhenPushed = YES;
    [self.profileVC.navigationController pushViewController:newView animated:YES];
}



/*--------------------------------------------*/
#pragma mark
#pragma mark - facebook handler -
/*--------------------------------------------*/

/**
 Facebook handler get call on success of facebook service.
 
 @param userInfo user information in dictionary.
 */
- (void)didFacebookUserLoginWithDetails:(NSDictionary*)userInfo {
    
   NSString *fbloggedUserAccessToken = [FBSDKAccessToken currentAccessToken].tokenString;
    
    
    NSDictionary *requestDict = @{mFacebookAccessToken :fbloggedUserAccessToken ,
                                  mauthToken  :flStrForObj([Helper userToken]),
                                  };
    [WebServiceHandler verifyWithFacebook:requestDict andDelegate:self];
}


/**
 If facebook handler get unsuccessful rsponse.
 
 @param error errorDescription
 */
- (void)didFailWithError:(NSError *)error {
    [Helper showAlertWithTitle:nil Message:NSLocalizedString(facebookVerificationFailed, facebookVerificationFailed) viewController:self.profileVC];
}





#pragma mark -
#pragma PayPal callback delegate -

/**
 Delegate Method from Paypal controller.
 Update Status of Paypal Button on the basis of paypal link saved/unlink.
 
 @param paypalLink payPal link saved/unlink from paypal controller.
 */
-(void)PaypalLinkIsSaved:(NSString *)paypalLink
{
    if(paypalLink.length)
    {
        [self.paypalButtonOutlet setImage:[UIImage imageNamed:@"paypal verified"] forState:UIControlStateNormal];
        self.paypalButtonOutlet.layer.borderWidth = 0;
        self.paypalButtonOutlet.layer.borderColor = [UIColor clearColor].CGColor;
        [self.paypalButtonOutlet setTitle:@"" forState:UIControlStateNormal];
        self.profileObj.profileDetails.paypalUrl = paypalLink ;
    }
    else
    {
        [self.paypalButtonOutlet setImage:[UIImage imageNamed:@"paypal unverified"] forState:UIControlStateNormal];
        self.paypalButtonOutlet.layer.borderWidth = 0.5;
        self.paypalButtonOutlet.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [self.paypalButtonOutlet setTitle:@"Paypal.Me" forState:UIControlStateNormal];
        self.profileObj.profileDetails.paypalUrl = @"";
    }
    [[NSUserDefaults standardUserDefaults]setObject:self.profileObj.profileDetails.paypalUrl forKey:payPalLink ];
}



/*------------------------------------*/
#pragma mark
#pragma mark - Update Profile Pic
/*------------------------------------*/

-(void)updateProfilePicUrl:(NSNotification *)noti {
    
    UserDetails *user = noti.object ;
    ProfileDetails *profile = self.profileObj.profileDetails;
    
    profile.website = user.website ;
    profile.bio = user.bio ;
    profile.fullName = user.fullName ;
    profile.username = user.username ;
    profile.profilePicUrl = user.profilePicUrl ;
    
    [self.profileVC.tableViewOutlet reloadData];
  }


// Bio And Website Links Clickaeble.

-(void)tapGestureForWebsiteUrl {
    UITapGestureRecognizer *labelrecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(websiteLinkTapGestureAction:)];
    labelrecog.numberOfTapsRequired = 1;
    [self.labelForWebsite addGestureRecognizer:labelrecog];
    self.labelForWebsite.userInteractionEnabled = YES;
}

- (IBAction)websiteLinkTapGestureAction:(id)sender
{
    [self openLinkInSafari:self.labelForWebsite.text];
}



-(void)handlingWebUrlFromBio{
    self.bioLabel.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range)
    {
        [self openLinkInSafari:string];
    };
}

-(void)openLinkInSafari :(NSString *)link
{
    UIAlertController *alertController  =[UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(alertMessageToOpenSafari, alertMessageToOpenSafari) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(alertYes, alertYes) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSString *urlstr = [@"http://"    stringByAppendingString:link];
        NSURL *url = [NSURL URLWithString:urlstr];
        if (![[UIApplication sharedApplication] openURL:url]) {
            [Helper showAlertWithTitle:nil Message:NSLocalizedString(invalidUrl, invalidUrl) viewController:self.profileVC];
        }
    }];
    UIAlertAction *noButton = [UIAlertAction actionWithTitle:NSLocalizedString(alertNo, alertNo) style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:yesButton];
    [alertController addAction:noButton];
    [self.profileVC presentViewController:alertController animated:YES completion:nil];
}

@end

//
//  MessageViewModal.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 23/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

class MessageViewModal : NSObject {
    
    
    /// Message Object instance
    var message : Message
    
    
    /// Initializing with the message object.
    ///
    /// - Parameter message: message object.
    init(withMessageObject message : Message) {
        self.message = message
    }
    
    /// Couchbase Object for performing the couchbase tasks.
    let couchbaseObj = Couchbase.sharedInstance
    
    
    /// current id of the self user.
    var selfID :String? {
        return self.getUserID()
    }
    
    /// Used for updating unsent message Document which will
    ///
    /// - Parameter Message: Current message going to be update on the doc.
    func updateUnsentMessageDoc(withMessageData data:Data, withChannelName channelName : String,  andMessageID messageID: UInt16) {
        let params = ["messageData" : data.base64EncodedString(),
                      "channelName" : channelName,
                      "messageID":messageID] as [String : Any]
        self.updateUnsentMessageDoc(withData: params)
    }
    
    
    /// Used for fetching the users mqtt ID
    ///
    /// - Returns: optional value of the mqtt id.
    fileprivate func getUserID() -> String? {
        guard let mqttID = Helper.getMQTTID() else { return nil }
        if mqttID == "mqttId" {
            return nil
        }
        return mqttID
    }
    
    
    /// After reciving the message this will update it for by default message.
    ///
    /// - Parameter messageData: message data in the dictionary format.
    func updateLocalDBForStatus(withMessage messageData : [String : Any]) {
        var messageObj = messageData
        messageObj["deliveryStatus"] = "1"
        messageObj["isSelf"] = true
        let name = NSNotification.Name(rawValue: "MessageNotification" + selfID!)
        NotificationCenter.default.post(name: name, object: self, userInfo: ["message": messageObj, "status":"1"])
    }
    
    
    /// For sending the unsent message to the MQTT server after connecting with the internet.
    ///
    /// - Parameter message: message data in the message format.
    fileprivate func updateUnsentMessageDoc(withData message :[String :Any]) {
        let userDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
        guard let userData = userDocVMObject.getUserData() else { return }
        if let unsentMessageDocID = userData["unsentMessagesDocument"] as? String {
            guard let unsentDocData = couchbaseObj.getData(fromDocID: unsentMessageDocID) else { return }
            var unsentDocumentData = unsentDocData
            guard let unsentMessagesArray = unsentDocumentData["unsentMessagesArray"] as? [Any] else { return }
            var unsentMsgsArray = unsentMessagesArray
            unsentMsgsArray.append(message as Any)
            unsentDocumentData["unsentMessagesArray"] = unsentMsgsArray
            couchbaseObj.updateData(data: unsentDocumentData, toDocID: unsentMessageDocID)
        }
    }
    
    
    /// For getting formated message sending as an accepted offer.
    ///
    /// - Parameters:
    ///   - productID: Current product ID
    ///   - price: price which is going to be accepted.
    /// - Returns: A dictionary object with all the data.
    func getMessageDictionaryForAcceptedOffer(withProductID productID : String, andPrice price : String) -> [String:Any] {
        var params = [String : Any]()
        params["name"] = Helper.userName()
        params["from"] = self.message.messageFromID
        params["to"] = self.message.messageToID // have to check about the other persons mqtt id
        params["payload"] = self.message.messagePayload //
        params["type"] = "\(self.message.messageType?.rawValue ?? 15)"
        params["offerType"] = "4" // offertype 1: make offer 3:counter offer 2: accepted 4: Thank you for accepting
        params["id"] = "\(UInt64(floor(Date().timeIntervalSince1970 * 1000)))"
        params["secretId"] = productID
        params["thumbnail"] = ""
        params["userImage"] = Helper.userProfileImageUrl()
        params["toDocId"] = self.message.messageDocId
        params["dataSize"] = 1
        params["isSold"] = 0
        params["isSelf"] = self.message.isSelfMessage
        params["productImage"] = ""
        params["productId"] = productID
        params["productName"] = ""
        params["timestamp"] = "\(UInt64(floor(Date().timeIntervalSince1970 * 1000)))"
        params["productPrice"] = price
        return params
    }
    
    
    
    func getMessageDictionaryForAcceptedSwapOffer(withProductID productID : String, andSwapType swapType : String) -> [String:Any] {
        var params = [String : Any]()
        params["name"] = Helper.userName()
        params["from"] = self.message.messageFromID
        params["to"] = self.message.messageToID // have to check about the other persons mqtt id
        params["payload"] = self.message.messagePayload //
        params["type"] = "17"
        params["swapType"] = swapType // 2: Accepted 3:Resjected 4: Thank you for accepting
        params["id"] = "\(UInt64(floor(Date().timeIntervalSince1970 * 1000)))"
        params["secretId"] = productID
        params["thumbnail"] = ""
        params["userImage"] = Helper.userProfileImageUrl()
        params["toDocId"] = self.message.messageDocId
        params["dataSize"] = 1
        params["isSold"] = 0
        params["isSelf"] = self.message.isSelfMessage
        params["productImage"] = ""
        params["productId"] = productID
        params["productName"] = ""
        params["timestamp"] = "\(UInt64(floor(Date().timeIntervalSince1970 * 1000)))"
        params["productPrice"] = ""
        
        if let isswap = self.message.isSwap {
            if(swapType == "3"){
                params["isRejected"] = "1"
            }
            else
            {
                params["isRejected"] = "0"
            }
            params["isSwap"] = isswap
            params["swapType"] = swapType
            params["productUrl"] = message.productUrl
            params["swapProductName"] = message.swapProductName
            params["swapProductUrl"] = message.swapProductUrl
            params["swapProductId"] = message.swapProductId
            params["swapTextMessage"] = message.swapTextMessage
            
        }
        
        
        
        return params
    }
    
    /// By using this method it will return the accepted message with creating a different object and returning it by adding more properties.
    ///
    /// - Returns: Message object with the new properties.
    func getThankYouMessageObject() -> Message {
        let messsageObj = self.message
        messsageObj.offerType = "4"
        messsageObj.messageType = MessageTypes.order
        messsageObj.isSelfMessage = false
        let timeStamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))
        messsageObj.timeStamp = "\(timeStamp)"
        return messsageObj
    }
    
    func getAcceptedMessageObject() -> Message {
        let messsageObj = self.message
        messsageObj.offerType = "4"
        messsageObj.messageType = MessageTypes.order
        messsageObj.isSelfMessage = true
        let timeStamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))
        messsageObj.timeStamp = "\(timeStamp)"
        return messsageObj
    }
    
    func getSwapOfferAcceptedMessageObject() -> Message {
        let messsageObj = self.message
        messsageObj.swapType = "2"
        messsageObj.messageType = MessageTypes.swap
        messsageObj.isSelfMessage = true
        let timeStamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))
        messsageObj.timeStamp = "\(timeStamp)"
        return messsageObj
    }
    
    func getSwapOfferRejectedMessageObject() -> Message {
        let messsageObj = self.message
        messsageObj.swapType = "3"
        messsageObj.messageType = MessageTypes.swap
        messsageObj.isSelfMessage = true
        let timeStamp = UInt64(floor(Date().timeIntervalSince1970 * 1000))
        messsageObj.timeStamp = "\(timeStamp)"
        return messsageObj
    }
    
    
    
}

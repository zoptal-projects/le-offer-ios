//
//  ReceivedLocationCell.swift
//  Tac Traderz
//
//  Created by Sachin Nautiyal on 13/11/2017.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


protocol ReceivedLocationCellDelegate {
    
    func tapOnReceivedLocationDelegate(messageObj: Message)
}


/// class for the outlets of the ReceivedLocationCell.
class ReceivedLocationCell: JSQMessagesCollectionViewCell {
    
    @IBOutlet weak var locationImageOutlet: UIImageView!
    var locationDelegate : ReceivedLocationCellDelegate? = nil
    
    /// model object of message. see didSet for more details.
    var msgObj  : Message!{
        didSet {
            self.setValues(withMsgObj : msgObj)
        }
    }
    
    /// used fo setting values for location inside the cell.
    ///
    /// - Parameter msgObj: Message Object.
    func setValues(withMsgObj msgObj : Message) {
        var msgStr = ""
        if let msg = msgObj.messagePayload?.fromBase64() {
            msgStr = msg
        } else {
            msgStr = msgObj.messagePayload!
        }
        self.locationImageOutlet.kf.indicatorType = .activity
        self.locationImageOutlet.kf.indicator?.startAnimatingView()
        DispatchQueue.global().async {
            if let latStr = msgStr.slice(from: "(", to: ","), let longStr = msgStr.slice(from: ",", to: ")") {
                let str = "http://maps.googleapis.com/maps/api/staticmap?markers=color:red|\(latStr),\(longStr)&zoom=15&size=180x220&sensor=true&key=\(mGoogleAPIKey)"
                   print(str)
                if let utfStr = str.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                    if let mapUrl = URL(string: utfStr) {
                        do {
                            let image = try UIImage(data: Data(contentsOf: mapUrl))
                            DispatchQueue.main.async {
                                self.locationImageOutlet.image = image
                                self.locationImageOutlet.kf.indicator?.stopAnimatingView()
                            }
                        } catch let error {
                            print(error)
                        }
                    }
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    @IBAction func tapOnReceivedLocationCell(_ sender: Any) {
        self.locationDelegate?.tapOnReceivedLocationDelegate(messageObj: msgObj)
    }
    
}

/// ReceivedLocationMediaItem inherites the JSQMessageMediaData properties
class ReceivedLocationMediaItem : NSObject, JSQMessageMediaData {
    
    /// Used for the instance of ReceivedLocationCell
    let locationCell = ReceivedLocationCell(frame:CGRect(x: 0, y: 0, width: 180, height: 220))
    
    func mediaView() -> UIView? {
        return locationCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return locationCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 220)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}

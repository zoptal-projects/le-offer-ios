
//
//  SwapOfferView.swift
//  Tac Traderz
//
//  Created by 3Embed on 08/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

protocol swapOfferButtonPressedDelegate {
    func swapOfferAcceptButtonPressed(withMessageDetails :Message)
     func swapOfferRejectButtonPressed(withMessageDetails :Message)
}

class SwapOfferCollectionViewCell: JSQMessagesCollectionViewCell {
 
    var swapOfferCellDelegate : swapOfferButtonPressedDelegate? = nil

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var acceptRejectView: UIView!
    
    
    @IBOutlet weak var productNameAndImagesView: UIView!
    
    @IBOutlet weak var userProductImageView: UIImageView!
    
    @IBOutlet weak var swapProductImageView: UIImageView!
    
    @IBOutlet weak var swapOfferDescription: UILabel!
    @IBOutlet weak var referenceVC: ChatViewController!

    var swapPostId:String?
    var userPostId:String?
    var chatVMObj : ChatViewModel!
    var msgObj  : Message! {
        didSet {
            if(self.mainView != nil)
            {
            self.setSwapOfferSentViewCorners()
            //self.setSwapOfferReceivedViewCorners()
            }

            if let userProductImage = msgObj.productUrl{
                if let imaegUrl = URL(string : userProductImage) {
                self.userProductImageView?.kf.setImage(with: imaegUrl, placeholder: #imageLiteral(resourceName: "defaultpp"), options: [.transition(ImageTransition.fade(1))])
                }
            }
            
            if let swapProductImage = msgObj.swapProductUrl{
                if let imaegUrl = URL(string : swapProductImage) {
                    self.swapProductImageView?.kf.setImage(with: imaegUrl, placeholder: #imageLiteral(resourceName: "defaultpp"), options: [.transition(ImageTransition.fade(1))])
                }
            }
            if let swapText = msgObj.swapTextMessage{
            swapOfferDescription?.text = swapText
            }
            swapPostId = msgObj.swapProductId;
    
            self.bringSubviewToFront(self.productNameAndImagesView)
        }
    }
    
    func setSwapOfferSentViewCorners() {
    
        self.mainView.roundCorners(corners: [.topLeft,.bottomRight, .bottomLeft], radius: 10)
        if(self.productNameAndImagesView != nil)
        {
            self.productNameAndImagesView.roundCorners(corners:[.topLeft], radius: 10)
        }
        if(self.acceptRejectView != nil)
        {
            self.acceptRejectView.roundCorners(corners:[.bottomLeft, .bottomRight], radius: 10)
        }
    }
    
    func setSwapOfferReceivedViewCorners()
    {
        self.mainView.roundCorners(corners: [.topRight,.bottomRight, .bottomLeft], radius: 10)
        if(self.productNameAndImagesView != nil)
        {
      self.productNameAndImagesView.roundCorners(corners:[.topRight], radius: 10)
        }
        if(self.acceptRejectView != nil)
        {
       self.acceptRejectView.roundCorners(corners:[.bottomLeft, .bottomRight], radius: 10)
        }
    }
    
    @IBAction func showSwapProductDetailsButtonAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC :ProductDetailsViewController = storyboard.instantiateViewController(withIdentifier:mInstaTableVcStoryBoardId) as! ProductDetailsViewController
        newVC.hidesBottomBarWhenPushed = true;
        newVC.postId = swapPostId
        newVC.noAnimation = true
    self.referenceVC.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func showUserProductDetailsButtonAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC :ProductDetailsViewController = storyboard.instantiateViewController(withIdentifier:mInstaTableVcStoryBoardId) as! ProductDetailsViewController
        newVC.hidesBottomBarWhenPushed = true;
        newVC.postId = userPostId;
        newVC.noAnimation = true
        self.referenceVC.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    
    
    @IBAction func AcceptSwapOfferButtonAction(_ sender: Any) {
        swapOfferCellDelegate?.swapOfferAcceptButtonPressed(withMessageDetails: self.msgObj)
    }
    
    
    
    @IBAction func RejectSwapOfferButtonAction(_ sender: Any) {
       swapOfferCellDelegate?.swapOfferRejectButtonPressed(withMessageDetails: self.msgObj)
    }
    
    
    
}

class SwapOfferSentMediaItem : NSObject, JSQMessageMediaData {
    
    let swapOfferCell = OfferCollectionViewCell(frame:CGRect(x: 0, y: 0, width: 180, height: 180))
    
    func mediaView() -> UIView? {
        return swapOfferCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return swapOfferCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 180)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}

class SwapOfferRecievedMediaItem : NSObject, JSQMessageMediaData {
    
    var SwapOfferReceivedCell = OfferCollectionViewCell(frame:CGRect(x: 0, y: 0, width: 180, height: 180))
    
    func mediaPlaceholderView() -> UIView {
        return SwapOfferReceivedCell
    }
    
    func mediaView() -> UIView? {
        return SwapOfferReceivedCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 180)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
    
    
    
}

class SwapOfferAcceptedMediaItem : NSObject, JSQMessageMediaData {
    
    var SwapOfferAcceptedMediaCell = OfferCollectionViewCell(frame:CGRect(x: 0, y: 0, width: 180, height: 180))
    
    func mediaPlaceholderView() -> UIView {
        return SwapOfferAcceptedMediaCell
    }
    
    func mediaView() -> UIView? {
        return SwapOfferAcceptedMediaCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 180)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}





//extension UIView {
//    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
//        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//        self.layer.mask = mask
//    }
//}


//
//  CounterOfferPriceCollectionViewCell.swift
//  Tac Traderz
//
//  Created by Rahul Sharma on 27/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


protocol OfferButtonPressedDelegate {
    func counterOfferButtonPressed(withMessageDetails :Message)
    func acceptButtonPressed(withMessageDetails :Message)
}

class CounterOfferPriceCollectionViewCell: JSQMessagesCollectionViewCell {
    
    @IBOutlet weak var counterOfferSentView: UIView!
    
    @IBOutlet weak var counterOfferWholeView: UIView!
    @IBOutlet weak var counterOfferBottomView: UIView!
    @IBOutlet weak var mainViewOutlet: UIView!
    @IBOutlet weak var mainTitleLabelOutlet: UILabel!
    @IBOutlet weak var priceOutlet: UILabel!
    var offerCellDelegate : OfferButtonPressedDelegate? = nil
    var chatVMObj : ChatViewModel!
    var msgObj  : Message! {
        didSet {
            if(self.counterOfferSentView != nil)
            {
                self.counterOfferSentView.roundCorners(corners:[.topRight], radius: 10)
            }
            
            if(self.counterOfferBottomView != nil)
            {
                self.counterOfferBottomView.roundCorners(corners:[.bottomRight, .bottomLeft], radius: 10)
            }
            
            if(self.counterOfferWholeView != nil)
            {
                self.counterOfferWholeView.roundCorners(corners: [.topRight,.bottomRight, .bottomLeft], radius: 10)
            }
            
            
            if let currency = chatVMObj.currency, let msg = msgObj.messagePayload {
                self.priceOutlet.text = currency+" "+msg
            } else {
                self.priceOutlet.text = msgObj.messagePayload
            }
//            if msgObj.isSelfMessage {
//                self.mainViewOutlet.backgroundColor = AppConstants.chatColor.chatBaseColor
//            } else {
//                self.mainViewOutlet.backgroundColor = AppConstants.chatColor.chatBaseColor
//            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func acceptButtonAction(_ sender: Any) {
        //print(self.msgObj as Any)
        print(self.msgObj.messageToID as Any)
        print(self.msgObj.uniquemessageId as Any)
        offerCellDelegate?.acceptButtonPressed(withMessageDetails: self.msgObj)
    }
    
    @IBAction func counterButtonAction(_ sender: Any) {
        print(self.msgObj.messageToID as Any)
        print(self.msgObj.uniquemessageId as Any)
        offerCellDelegate?.counterOfferButtonPressed(withMessageDetails: self.msgObj)
    }
}

class CounterOfferCellMediaItem : NSObject, JSQMessageMediaData {
    
    let CounterOfferCell = CounterOfferPriceCollectionViewCell(frame:CGRect(x: 0, y: 0, width: 180, height: 150))
    
    func mediaView() -> UIView? {
        return CounterOfferCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return CounterOfferCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 150)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}

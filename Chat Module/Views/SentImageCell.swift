//
//  SentLocationCell.swift
//  Tac Traderz
//
//  Created by Sachin Nautiyal on 13/11/2017.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

import Kingfisher

protocol SentImageCellDelegate {
    
    func tapOnSentImageCellDelegate(messageObj: Message)
}

// Used for the outlets of the SentLocationCell
class SentImageCell: JSQMessagesCollectionViewCell {
    
    var imageDelegate : SentImageCellDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var imageOutlet: UIImageView!
    
    /// model object of message. see didSet for more details.
    var msgObj  : Message!{
        didSet {
            self.setValues(withMsgObj : msgObj)
        }
    }
    
    /// used fo setting values for location inside the cell.
    ///
    /// - Parameter msgObj: Message Object.
    func setValues(withMsgObj msgObj : Message) {
        var imageURL = ""
        if let imgurl = msgObj.imageUrl
        {
           print(imgurl)
            imageURL = imgurl
        } else {
            var msgStr = ""
            if let msg = msgObj.messagePayload?.fromBase64() {
                msgStr = msg
            } else {
                msgStr = msgObj.messagePayload!
            }
            imageURL = msgStr
        }
        
//        if (self.imageOutlet.image == nil)
//        {
//            DispatchQueue.global().async {
//                if let utfStr = imageURL.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)  {
//                    if let mapUrl = URL(string: utfStr) {
//                        do {
//                            let image = try UIImage(data: Data(contentsOf: mapUrl))
//                            DispatchQueue.main.async {
//                                self.imageOutlet.image = image
//                                self.imageOutlet.kf.indicator?.stopAnimatingView()
//                            }
//                        } catch let error {
//                            print(error)
//                        }
//                    }
//                }
//            }
//        } else {
            if let imageUrl = URL(string :imageURL)
            {
                self.imageOutlet.kf.indicatorType = .activity
                self.imageOutlet.kf.indicator?.startAnimatingView()
                print(imageUrl)
                self.imageOutlet.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "itemProdDefault"), options:[.transition(ImageTransition.fade(1))] , progressBlock: nil, completionHandler: { (image, error, cache, url) in
                    DispatchQueue.main.async {
                        self.imageOutlet.image = image
                        self.imageOutlet.kf.indicator?.stopAnimatingView()
                    }
                })
            } else {
                self.imageOutlet.kf.indicatorType = .activity
                self.imageOutlet.kf.indicator?.startAnimatingView()
                self.imageOutlet.image = #imageLiteral(resourceName: "itemProdDefault")
            }
        }
   // }
    
    @IBAction func imageTappedAction(_ sender: Any) {
        self.imageDelegate?.tapOnSentImageCellDelegate(messageObj: msgObj)
    }
}

/// SentLocationMediaItem inherites the JSQMessageMediaData properties
class SentImageMediaItem : NSObject, JSQMessageMediaData {
    
    /// Used for the instance of SentLocationCell
    let imageCell = SentImageCell(frame:CGRect(x: 0, y: 0, width: 180, height: 220))
    
    func mediaView() -> UIView? {
        return imageCell
    }
    
    func mediaPlaceholderView() -> UIView {
        return imageCell
    }
    
    func mediaViewDisplaySize() -> CGSize {
        return CGSize(width: 180, height: 220)
    }
    
    func mediaHash() -> UInt {
        return UInt(60000 + arc4random_uniform(1000))
    }
}



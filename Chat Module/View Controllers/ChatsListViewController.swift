//
//  ChatsListViewController.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 22/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol GetCurrentIndexDelgate {
    func currentIndex(index : Int)
}

class ChatsListViewController: UIViewController {
    
    struct  Constants {
        static let cellIdentifier = "ChatListTableViewCell"
        static let chatSegueIdentifier = "segueToChatViewController"
    }
    
    var pageIndex:Int!
    var getCurrentIndexDelgate:GetCurrentIndexDelgate?
    var chatListViewModel: ChatListViewModel!
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var noChatMsgAvailableView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getCurrentIndexDelgate?.currentIndex(index: pageIndex)
        self.tableViewOutlet.tag = self.pageIndex
//        self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].title = NSLocalizedString("Chat", comment: "Chat")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.chatListViewModel.getNumberOfRows() == 0 {
            self.tableViewOutlet.isHidden = true
            self.noChatMsgAvailableView.isHidden = false
        } else {
            self.tableViewOutlet.isHidden = false
            self.noChatMsgAvailableView.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadTableView() {
        self.tableViewOutlet.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.chatSegueIdentifier {
            let controller = segue.destination as! ChatViewController
            if let rowIndex = sender as? Int {
                let chatObj = self.chatListViewModel.chats[rowIndex]
                
                let chatVMObject = ChatViewModel(withChatData: chatObj)
                controller.chatViewModelObj = chatVMObject
            }
            else if let chatObj = sender as? Chat {
                let chatVMObject = ChatViewModel(withChatData: chatObj)
                controller.chatViewModelObj = chatVMObject
            }
        }
    }
    
    
}

extension ChatsListViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.chatListViewModel.setUpTableViewCell(indexPath: indexPath, tableView: tableView)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatListViewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: Constants.chatSegueIdentifier, sender: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableViewOutlet.setEditing(editing, animated: animated)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            //1 delete locally
            tableView.beginUpdates()
            let pgIndicator = ProgressIndicator.sharedInstance()
            pgIndicator?.showMessage("Löschen..", on: self.view)
            self.chatListViewModel.deleteRow(fromIndexPath: indexPath, success: { response in
                print("success")
                self.chatListViewModel.deleteChat(fromIndexPath: indexPath)
                tableView.deleteRows(at: [indexPath], with:.none)
                tableView.endUpdates()
                if self.chatListViewModel.getNumberOfRows() == 0 {
                    self.tableViewOutlet.isHidden = true
                    self.noChatMsgAvailableView.isHidden = false
                } else {
                    self.tableViewOutlet.isHidden = false
                    self.noChatMsgAvailableView.isHidden = true
                }
                pgIndicator?.hide()
            }, failure: { error in
                print("error")
                pgIndicator?.hide()
                self.chatListViewModel.deleteChat(fromIndexPath: indexPath)
                tableView.deleteRows(at: [indexPath], with:.none)
                tableView.endUpdates()
            })
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

//
//  SavedAddressVC.swift
//  Cellable
//
//  Created by Anish Mac Mini on 06/06/20.
//  Copyright © 2020 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import iProgressHUD
class SavedAddressVC: UIViewController,iProgressHUDDelegete
{
   
    @IBOutlet weak var addAddressBtnOut: UIBarButtonItem!
    let api = API()
    @IBOutlet weak var address1lbl: UILabel!
    @IBOutlet weak var address2lbl: UILabel!
    @IBOutlet weak var landmarkLblOut: UILabel!
    @IBOutlet weak var countryOut: UILabel!
    @IBOutlet weak var statelbl: UILabel!
    var addNavigationBtn : UIButton!
    
    var  userInfo : NSDictionary = [:]
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
    }
    override func viewWillAppear(_ animated: Bool)
        {
        self.title = "Saved Address"
        super.viewWillAppear(true)
            
        let iprogress: iProgressHUD = iProgressHUD()
        iprogress.delegete = self
        iprogress.iprogressStyle = .horizontal
        iprogress.indicatorStyle = .orbit
        iprogress.isShowModal = false
        iprogress.boxSize = 50
        iprogress.attachProgress(toViews: view)
        view.showProgress()
            
        self.fetchAllCard()
        address1lbl.isHidden = true
        address2lbl.isHidden = true
        landmarkLblOut.isHidden = true
        countryOut.isHidden = true
        statelbl.isHidden = true
            
        addNavigationBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        addNavigationBtn.setImage(UIImage(named: "plusImage"), for: .normal)
        addNavigationBtn.addTarget(self, action: #selector(addCardBtnAction), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: addNavigationBtn)
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        }
    
        func fetchAllCard()
        {
        var parameters = [String : Any]()
        parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
        print(parameters)
        self.api.GetAddress(withdata: parameters, success: { (data) in
        //loader.hide()
        self.view.dismissProgress()
        self.userInfo = data["data"] as! NSDictionary
        print(self.userInfo)
        self.address1lbl.isHidden = false
        self.address2lbl.isHidden = false
        self.landmarkLblOut.isHidden = false
        self.countryOut.isHidden = false
        self.statelbl.isHidden = false
  
        self.address1lbl.text = self.userInfo["address_one"] as? String ?? ""
        self.address2lbl.text =  self.userInfo["address_two"] as? String ?? ""
        self.landmarkLblOut.text = self.userInfo["pin_code"] as? String ?? ""
        self.countryOut.text =  self.userInfo["country"] as? String ?? ""
        self.statelbl.text =  self.userInfo["state"] as? String ?? ""
                
               
                    if self.address1lbl.text?.count != 0
                    {
                        self.addNavigationBtn.setImage(UIImage(named: "editProfile_Icon"), for: .normal)
                    }
                    
                let alert = UIAlertController(title: "Cellable", message: data["message"] as? String, preferredStyle: UIAlertController.Style.alert)
                                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                                    self.present(alert, animated: true, completion: nil)
                      }) { (error) in
                          print(error)
                          loader.hide()
                            self.view.dismissProgress()
                          if error.code  == 204
                          {
                              let alert = UIAlertController(title: "Cellable", message: error.localizedDescription as? String, preferredStyle: UIAlertController.Style.alert)
                      alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                  self.present(alert, animated: true, completion: nil)
                          }
                      }
                }

    func onShow(view: UIView) {
        print("onShow")
    }
    
    func onDismiss(view: UIView) {
        print("onDismiss")
    }
    
    func onTouch(view: UIView) {
        print("onTouch")
    }

    @objc func addCardBtnAction()
     {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        nextVC.userInfo = userInfo
        self.navigationController?.pushViewController(nextVC, animated: true)
        
    }
}


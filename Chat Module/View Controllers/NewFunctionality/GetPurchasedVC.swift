//
//  GetPurchasedVC.swift
//  Cellable
//
//  Created by Anish Mac Mini on 29/06/20.
//  Copyright © 2020 Rahul Sharma. All rights reserved.
//

import UIKit
import iProgressHUD
class GetPurchasedVC: UIViewController,iProgressHUDDelegete
{
    let api = API()
    var  userInfo : NSDictionary = [:]
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.title = "Purchas Order"
         
    }
    override func viewWillAppear(_ animated: Bool)
    {
    self.title = "Purchas Order"
    super.viewWillAppear(true)

        let iprogress: iProgressHUD = iProgressHUD()
        iprogress.delegete = self
        iprogress.iprogressStyle = .horizontal
        iprogress.indicatorStyle = .orbit
        iprogress.isShowModal = false
        iprogress.boxSize = 50
        iprogress.attachProgress(toViews: view)
        view.showProgress()
        
        
    self.fetchAllCard()
 

    self.navigationController!.navigationBar.tintColor = UIColor.white
    self.navigationController?.navigationBar.topItem?.title = ""
    }
    func onShow(view: UIView) {
        print("onShow")
    }
    
    func onDismiss(view: UIView) {
        print("onDismiss")
    }
    
    func onTouch(view: UIView) {
        print("onTouch")
    }
    func fetchAllCard()
          {
          var parameters = [String : Any]()
          parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
          print(parameters)
          self.api.GetPurchase(withdata: parameters, success: { (data) in
          loader.hide()
                self.view.dismissProgress()
                      
         // self.userInfo = data["data"] as! NSDictionary
         // print(self.userInfo)
          
            let alert = UIAlertController(title: "Cellable", message: data["message"] as? String, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                                      self.present(alert, animated: true, completion: nil)
                        }) { (error) in
                            print(error)
                            loader.hide()
                                self.view.dismissProgress()
                            if error.code  == 204
                            {
                                let alert = UIAlertController(title: "Cellable", message: error.localizedDescription as? String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                            }
                        }
                  }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

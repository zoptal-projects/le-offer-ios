//
//  PaymentMethodViewController.swift
//  Invoicing
//
//  Created by ZoptalMacMini on 24/10/19.
//  Copyright © 2019 ZoptalMacMini. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftUI
import iProgressHUD
class PaymentMethodViewController: UIViewController,iProgressHUDDelegete
{
    @IBOutlet weak var tblVwPayment: UITableView!
    var selectedIndex = 0
    var arrayBankAccount = [String]()
    let api = API()
    var cardDetails = [SavedCardModal]()
    var userInfo: [String] = []
    var arrayList = [Any]()
    var arraySelSpeciality = [String]()
    
    var roughtchecking = NSString()
    var selectedCardId = NSString()
    var productID = NSString()
    var postType = NSString()
    var currency = NSString()
    var pincodeString = NSString()
    var address1String = NSString()
    var address2String = NSString()
     var landmarString = NSString()
     var countryString = NSString()
     var stateString = NSString()
     var cityString = NSString()
    

    
    @IBOutlet weak var continueBtnOut: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tblVwPayment.tableFooterView = UIView()
        
        if roughtchecking == "FromFinalPaymentPart"
       {
       
        continueBtnOut.isHidden = false
       }
    }

        func fetchAllCard()
        {
            var parameters = [String : Any]()
            parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
            
            self.api.GetCard(withdata: parameters, success: { (data) in
            loader.hide()
                self.view.dismissProgress()
                print(data)
                 self.tblVwPayment.isHidden = false
                self.arrayList.removeAll()
                self.arrayList = data["data"] as! [Any]
                print(self.arrayList)
                
                if self.arrayList.count != 0
                {
                let dict = self.arrayList[0] as! [String:Any]
                    self.selectedCardId = dict["_id"] as! NSString
                }
                self.tblVwPayment.reloadData()
                let alert = UIAlertController(title: "Cellable", message: data["message"] as? String, preferredStyle: UIAlertController.Style.alert)
                                              alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                          self.present(alert, animated: true, completion: nil)
            }) { (error) in
                print(error)
                loader.hide()
                    self.view.dismissProgress()
                if error.code  == 204
                {
                     self.arrayList.removeAll()
                    print(self.arrayList)
                    self.tblVwPayment.isHidden = true
                          let alertController = UIAlertController(title: "Cellable", message: error.localizedDescription as? String, preferredStyle: UIAlertController.Style.alert)
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                  UIAlertAction in
//                                   let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                                                  let nextVC = storyBoard.instantiateViewController(withIdentifier: "SetupPaymentRiderViewController") as! SetupPaymentRiderViewController
//                                                  nextVC.isComingFromPaymentScreen = true
//                                       self.navigationController?.pushViewController(nextVC, animated: true)
                                  }
                                  alertController.addAction(okAction)
                                  self.present(alertController, animated: true, completion: nil)
                }
            }
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                   let nextVC = storyBoard.instantiateViewController(withIdentifier: "SetupCardAdd") as! SetupCardAdd
                   nextVC.isComingFromPaymentScreen = true
        self.navigationController?.pushViewController(nextVC, animated: true)
        
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.title = "Saved Cards"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
//        currentUser = AppManager.getLoggedInUser()
//        var parameters = [String: Any]()
//        parameters["accessToken"] = currentUser.userAccessToken
//
//        print()
//
//            APIManager.APIRiderCards(params: parameters, viewController: self) { (JSON) in
//                print(JSON)
//                self.cardDetails.removeAll()
//                for details in JSON["result"].arrayValue
//                {
//                    let card = SavedCardModal(cardData: details)
//                    self.cardDetails.append(card)
//                }
//                self.tblVwPayment.reloadData()
//            }
        
       
        let iprogress: iProgressHUD = iProgressHUD()
                      iprogress.delegete = self
                      iprogress.iprogressStyle = .horizontal
                      iprogress.indicatorStyle = .orbit
                      iprogress.isShowModal = false
                      iprogress.boxSize = 50
                      iprogress.attachProgress(toViews: view)
                      view.showProgress()
        
        
          self.fetchAllCard()
    }
    func onShow(view: UIView) {
           print("onShow")
       }
       
       func onDismiss(view: UIView) {
           print("onDismiss")
       }
       
       func onTouch(view: UIView) {
           print("onTouch")
       }
    @IBAction func addAction(_ sender: Any) {
        
//        if kUserType == "Driver"{
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SetupPaymentDriverViewController ") as! SetupPaymentDriverViewController
//            nextVC.isComingFromPaymentScreen = true
//            self.navigationController?.pushViewController(nextVC, animated: true)
//        } else {
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SetupPaymentRiderViewController") as! SetupPaymentRiderViewController
//            nextVC.isComingFromPaymentScreen = true
//            self.navigationController?.pushViewController(nextVC, animated: true)
//        }
    }
    @objc func markApiCalling(_ sender : UIButton)
    {
        
        let alert = UIAlertController(title: kAlertTitle, message: "Are you sure you want to mark this card as a default?", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
//             iProgressHUD.sharedInstance().attachProgress(toView: self.view)
//                self.view.showProgress()
//                self.view.updateIndicator(style: .ballBeat)
                
                let iprogress: iProgressHUD = iProgressHUD()
                iprogress.delegete = self
                iprogress.iprogressStyle = .horizontal
                iprogress.indicatorStyle = .orbit
                iprogress.isShowModal = false
                iprogress.boxSize = 50
                iprogress.attachProgress(toViews: self.view)
                self.view.showProgress()
             
                var parameters = [String : Any]()
                parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
                parameters["card_id"] = sender.titleLabel?.text ?? ""
             
                print(parameters)
                self.api.MarkDefaultCard(withdata: parameters, success: { (data) in
                                 loader.hide()
                        self.view.dismissProgress()
                    
                    
                    
                               print(data)
                
                               self.fetchAllCard()
                               let alert = UIAlertController(title: "Cellable", message: data["message"] as? String, preferredStyle: UIAlertController.Style.alert)
                                                             alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                                         self.present(alert, animated: true, completion: nil)
                           }) { (error) in
                               print(error)
                               loader.hide()
                                self.view.dismissProgress()
                               if error.code  == 204
                               {
                                   let alert = UIAlertController(title: "Cellable", message: error.localizedDescription as? String, preferredStyle: UIAlertController.Style.alert)
                           alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                               }
                           }

            })
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in})
            alert.addAction(cancel)
            alert.addAction(ok)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)})
            
           
        }
    
    
    
    @IBAction func continueToPayAction(_ sender: Any)
    {

        let iprogress: iProgressHUD = iProgressHUD()
                         iprogress.delegete = self
                         iprogress.iprogressStyle = .horizontal
                         iprogress.indicatorStyle = .orbit
                         iprogress.isShowModal = false
                         iprogress.boxSize = 50
                         iprogress.attachProgress(toViews: view)
                         view.showProgress()
        
        
        
              var parameters = [String : Any]()
              parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
              parameters["postId"] = productID
              parameters["card_id"] = selectedCardId
              parameters["type"] = postType
              parameters["pin_code"] = pincodeString
              parameters["address_one"] = address1String
              parameters["address_two"] = address2String
              parameters["landmark"] = landmarString
              parameters["country"] = countryString
              parameters["state"] = stateString
              parameters["town"] = cityString
              parameters["currency"] = "USD"
              print(parameters)
              self.api.MakePayment(withdata: parameters, success: { (data) in
              loader.hide()
                    self.view.dismissProgress()
                          
                let alert = UIAlertController(title: "Cellable", message: data["message"] as? String, preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (actionSheetController) -> Void in
                    
                     for controller in self.navigationController!.viewControllers as Array {
                                          if controller.isKind(of: BuyerSellerChatViewController.self) {
                                              self.navigationController!.popToViewController(controller, animated: true)
                                              break
                                          }
                                      }
                }))
        
                self.present(alert, animated: true, completion: nil)
                            }) { (error) in
                                print(error)
                                loader.hide()
                                    self.view.dismissProgress()
                                if error.code  == 204
                                {
                    let alert = UIAlertController(title: "Cellable", message: error.localizedDescription as? String, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                                }
                            }
                      }
        
        
        
    }
    
    func refreshCard()
    {
//        currentUser = AppManager.getLoggedInUser()
//        var parameters = [String: Any]()
//        parameters["accessToken"] = currentUser.userAccessToken
//        APIManager.APIRiderCards(params: parameters, viewController: self) { (JSON) in
//            print(JSON)
//            self.cardDetails.removeAll()
//            for details in JSON["result"].arrayValue
//            {
//                let card = SavedCardModal(cardData: details)
//                self.cardDetails.append(card)
//            }
//            self.tblVwPayment.reloadData()
//        }
    }

extension PaymentMethodViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! PaymentMethodTableViewCell
    
        let dict = arrayList[indexPath.row] as! [String:Any]
     
        cell.lblPaymentName.text = dict["name"] as? String ?? ""
        cell.markBtnOut.titleLabel?.text = dict["_id"] as? String
        cell.markBtnOut.addTarget(self, action: #selector(markApiCalling(_:)), for: .touchUpInside)
        print(dict["is_default"]!)
        if dict["is_default"] as? Int  == 1 || dict["is_default"] as? String  == "1"
        {
              cell.markBtnOut.setBackgroundImage(UIImage(named:"bookmark"), for: .normal)
        }
        else
        {
            cell.markBtnOut.setBackgroundImage(UIImage(named:"unbookmark"), for: .normal)
            
        }
        if roughtchecking == "FromFinalPaymentPart"
        {
            if selectedIndex == indexPath.row
            {
             cell.accessoryType = .checkmark
            }
            else
            {
            cell.accessoryType = .none
            }
        }
        
        
        
        
//        if arraySelSpeciality.contains(idString!)
//        {
//        cell.accessoryType = .checkmark
//        }else
//        {
//        cell.accessoryType = .none
//        }
        
        
         //cell.lblPaymentName.text = dict["name"] as? String ?? ""
          //cell.imgBookMarkSelected.tag = indexPath.row
            
//            let firstDigits = cardDetails[indexPath.row].savedCardCardNumber.prefix(4)
//            let lastDigits = cardDetails[indexPath.row].savedCardCardNumber.suffix(4)
//            cell.lblPaymentName.text = firstDigits + " **** **** " + lastDigits
//           if cardDetails[indexPath.row].savedCardIsDefault == "1"
//           {
//               cell.imgBookMarkSelected.isHidden = false
//           }
        
//        if cell.imgBookMarkSelected.tag == self.selectedIndex{
//                cell.imgBookMarkSelected.isHidden = false
//            }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
          self.selectedIndex = indexPath.row
//        tblVwPayment.reloadData()
//        var parameter = [String: Any]()
//        parameter["accessToken"] = currentUser.userAccessToken
//        parameter["id"] = cardDetails[indexPath.row].savedCardId
//        parameter["isDefault"] = "1"
//        APIManager.APIRiderMakeDefault(params: parameter, viewController: self) { (JSON) in
//            print(JSON)
//            self.navigationController?.popViewController(animated: true)
//        }
        
        let dict = arrayList[indexPath.row] as! [String:Any]
        selectedCardId = dict["_id"] as! NSString
//
//        if arraySelSpeciality.contains(idString!)
//        {
//        let array = self.arraySelSpeciality as NSArray
//        let index = array.index(of: idString as Any)
//        self.arraySelSpeciality.remove(at: index)
//
//        }
//        else
//        {
//        self.arraySelSpeciality.append(idString!)
//        //self.arrayTextSpeciality.append(self.serachListArray[indexPath.row].skill_name)
//        }
        
        
        self.tblVwPayment.reloadData()
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete)
        {
            let alert = UIAlertController(title: kAlertTitle, message: "Are you sure you want to delete this card?", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
               
                let dict = self.arrayList[indexPath.row] as! [String:Any]
                var parameters = [String : Any]()
                parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
                parameters["card_id"] = dict["_id"]
                           
                self.api.DeleteCard(withdata: parameters, success: { (data) in
            loader.hide()
                        self.view.dismissProgress()
                
                               self.fetchAllCard()
                               let alert = UIAlertController(title: "Cellable", message: data["message"] as? String, preferredStyle: UIAlertController.Style.alert)
                                                             alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                                         self.present(alert, animated: true, completion: nil)
                           }) { (error) in
                               print(error)
                               loader.hide()
                                self.view.dismissProgress()
                               if error.code  == 204
                               {
                                   let alert = UIAlertController(title: "Cellable", message: error.localizedDescription as? String, preferredStyle: UIAlertController.Style.alert)
                           alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                               }
                           }

            })
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in})
            alert.addAction(cancel)
            alert.addAction(ok)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)})
            
           
        }
    }
    
    
}


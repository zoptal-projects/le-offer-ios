//
//  InAppCellableVC.swift
//  Cellable
//
//  Created by Anish Mac Mini on 25/08/20.
//  Copyright © 2020 Anish Mac Mini. All rights reserved.
//

import UIKit
import Foundation
import StoreKit
import Kingfisher
import iProgressHUD
class InAppCellableVC: UIViewController , SKProductsRequestDelegate, SKPaymentTransactionObserver,iProgressHUDDelegete
{
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productCurrencyLbl: UILabel!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var purchaseBtn: UIButton!
    @IBOutlet weak var productPriceLbl: UILabel!
    
    @IBOutlet weak var fouthPackBtn: UIButton!
    @IBOutlet weak var thridPackBtn: UIButton!
    @IBOutlet weak var secondPackBtn: UIButton!
    @IBOutlet weak var firstPackBtn: UIButton!
   
    var productId = Int()
    var purchaseId = NSString()
    let api = API()
    var userInfo : NSDictionary = [:]
    var productObj2 : ProductDetails?

    #if DEBUG
    let verifyReceiptURL = "https://sandbox.itunes.apple.com/verifyReceipt"
    #else
    let verifyReceiptURL = "https://buy.itunes.apple.com/verifyReceipt"
    #endif
        
    let kInAppProductPurchasedNotification = "InAppProductPurchasedNotification"
    let kInAppPurchaseFailedNotification   = "InAppPurchaseFailedNotification"
    let kInAppProductRestoredNotification  = "InAppProductRestoredNotification"
    let kInAppPurchasingErrorNotification  = "InAppPurchasingErrorNotification"
        
    let unlockTestInAppPurchase1ProductId  = "com.zop.cellable.100Clicks"
    let unlockTestInAppPurchase2ProductId  = "com.zop.cellable.200Clicks"
    let autorenewableSubscriptionProductId = "com.zop.cellable.300Clicks"
    let nonrenewingSubscriptionProductId   = "com.zop.cellable.500Clicks"
    
    override func viewDidLoad()
    {
       super.viewDidLoad()
       self.productPriceLbl.text = self.productObj2!.price
       self.productNameLbl.text = self.productObj2!.productName;
     
       let imageurl = URL(string : self.productObj2!.mainUrl)
       self.productImageView!.kf.setImage(with:imageurl, placeholder: #imageLiteral(resourceName: "Logo"), options: [.transition(ImageTransition.fade(1))])
        
        SKPaymentQueue.default().add(self)
    }
    
    func buyProduct(_ product: SKProduct)
     {
        print("Sending the Payment Request to Apple")
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    func restoreTransactions()
    {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Error %@ \(error)")
        NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppPurchasingErrorNotification), object: error.localizedDescription)
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("Got the request from Apple")
        let count: Int = response.products.count
        if count > 0 {
            _ = response.products
            let validProduct: SKProduct = response.products[0]
            print(validProduct.localizedTitle)
            print(validProduct.localizedDescription)
            print(validProduct.price)
            buyProduct(validProduct);
        }
        else {
            print("No products")
        }
    }
    
    @IBAction func showProductAction(_ sender: Any) {
    }
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        print("Received Payment Transaction Response from Apple");
        
        for transaction: AnyObject in transactions {
            if let trans: SKPaymentTransaction = transaction as? SKPaymentTransaction {
                switch trans.transactionState {
                case .purchased:
                    print("Product Purchased")
                    savePurchasedProductIdentifier(trans.payment.productIdentifier)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppProductPurchasedNotification), object: nil)
                        
                    self.saveInAppPurcahseData()
                    receiptValidation()
                    
                    break
                    
                case .failed:
                   self.view.dismissProgress()
                    print("Purchased Failed")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppPurchaseFailedNotification), object: nil)
                    break
                    
                case .restored:
                           self.view.dismissProgress()
                    print("Product Restored")
                    savePurchasedProductIdentifier(trans.payment.productIdentifier)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppProductRestoredNotification), object: nil)
                    break
                    
                default:
                    break
                }
            }
            else {
                
            }
        }
    }
        
    @IBAction func cancelBtnAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    func saveInAppPurcahseData()
          {
          var parameters = [String : Any]()
          parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
          parameters["postId"] = self.productObj2!.postId
          parameters["postType"] = "0"
          parameters["noOfViews"] = productId
          parameters["promotionTitle"] = "Cellable Product"
          parameters["purchaseId"] = purchaseId
            
          self.api.PostInAppPurchase(withdata: parameters, success: { (data) in
          self.view.dismissProgress()
          print(data)
                      
            let alertController = UIAlertController(title: "Cellable", message: data["message"] as? String, preferredStyle: UIAlertController.Style.alert)
                                              let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                              UIAlertAction in
         self.dismiss(animated: true, completion: nil)
                                              }
                                              alertController.addAction(okAction)
                                              self.present(alertController, animated: true, completion: nil)
            
            
          
            
                                                      
                        }) { (error) in
                            print(error)
                            loader.hide()
                                
                            if error.code  == 204
                            {
                                 let alert = UIAlertController(title: "Cellable", message: error.localizedDescription as? String, preferredStyle: UIAlertController.Style.alert)
                                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                                    self.present(alert, animated: true, completion: nil)
                   
                            }
                        }
                  }
    
    
    func savePurchasedProductIdentifier(_ productIdentifier: String!) {
        UserDefaults.standard.set(productIdentifier, forKey: productIdentifier)
        UserDefaults.standard.synchronize()
    }
    
    func receiptValidation() {
        
        let receiptFileURL = Bundle.main.appStoreReceiptURL
        let receiptData = try? Data(contentsOf: receiptFileURL!)
        let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let jsonDict: [String: AnyObject] = ["receipt-data" : recieptString! as AnyObject, "password" : "12dc4277cdb44639bd017b87cf35ecae" as AnyObject]
        
        do {
            let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            let storeURL = URL(string: verifyReceiptURL)!
            var storeRequest = URLRequest(url: storeURL)
            storeRequest.httpMethod = "POST"
            storeRequest.httpBody = requestData
            
            let session = URLSession(configuration: URLSessionConfiguration.default)
            let task = session.dataTask(with: storeRequest, completionHandler: { [weak self] (data, response, error) in
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    print(jsonResponse)
                    if let date = self?.getExpirationDateFromResponse(jsonResponse as! NSDictionary) {
                        print(date)
                    }
                } catch let parseError {
                    print(parseError)
                }
            })
            task.resume()
        } catch let parseError {
            print(parseError)
        }
    }
    
    func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) -> Date? {
        
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {

            let lastReceipt = receiptInfo.lastObject as! NSDictionary
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            
            if let expiresDate = lastReceipt["expires_date"] as? String {
                return formatter.date(from: expiresDate)
            }
            
            return nil
        }
        else {
            return nil
        }
    }
    
    @IBAction func termsBtnAction(_ sender: Any) {
        guard let url = URL(string: "https://cellableapp.com/en/terms") else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func privacyBtnAction(_ sender: Any) {
        guard let url = URL(string: "https://cellableapp.com/en/privacy") else { return }
        UIApplication.shared.open(url)
    }
    func unlockProduct(_ productIdentifier: String!) {
        if SKPaymentQueue.canMakePayments() {
            let productID: NSSet = NSSet(object: productIdentifier)
            let productsRequest: SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>)
            productsRequest.delegate = self
            productsRequest.start()
            print("Fetching Products")
        }
        else {
            print("Сan't make purchases")
            NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppPurchasingErrorNotification), object: NSLocalizedString("CANT_MAKE_PURCHASES", comment: "Can't make purchases"))
        }
    }
    func loaderShow()
    {
       let iprogress: iProgressHUD = iProgressHUD()
               iprogress.delegete = self
               iprogress.iprogressStyle = .horizontal
               iprogress.indicatorStyle = .orbit
               iprogress.isShowModal = false
               iprogress.boxSize = 50
               iprogress.attachProgress(toViews: view)
               view.showProgress()
    }
    func onShow(view: UIView) {
           print("onShow")
       }
       
       func onDismiss(view: UIView) {
           print("onDismiss")
       }
       
       func onTouch(view: UIView) {
           print("onTouch")
       }
    @IBAction func purchaseBtnAction(_ sender: Any)
    {
        if productId == 100
        {
            self.loaderShow()
        unlockProduct(unlockTestInAppPurchase1ProductId)
        }
        else if productId == 200
        {
             self.loaderShow()
        unlockProduct(unlockTestInAppPurchase2ProductId)
        }
        else  if productId == 300
        {
             self.loaderShow()
        unlockProduct(autorenewableSubscriptionProductId)
        }
        else  if productId == 500
        {
            self.loaderShow()
        unlockProduct(nonrenewingSubscriptionProductId)
        }
        else
        {
        let alert = UIAlertController(title: kAlertTitle, message: "Please select package first.", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
        }
    }
   

    @IBAction func firstbtnAction(_ sender: Any) {
            productId = 100
        purchaseId = "com.zop.cellable.100Clicks"
        firstPackBtn.backgroundColor = UIColor(red: 66.0/255, green: 192.0/255, blue: 206.0/255, alpha: 0.5)
        
        secondPackBtn.backgroundColor = UIColor(red: 163.0/255, green: 200.0/255, blue: 63.0/255, alpha: 0.5)
         thridPackBtn.backgroundColor = UIColor(red: 163.0/255, green: 200.0/255, blue: 63.0/255, alpha: 0.5)
         fouthPackBtn.backgroundColor = UIColor(red: 163.0/255, green: 200.0/255, blue: 63.0/255, alpha: 0.5)
    }
    
    @IBAction func secondBtnAction(_ sender: Any) {
    productId = 200
       purchaseId = "com.zop.cellable.200Clicks"
               firstPackBtn.backgroundColor = UIColor(red: 163.0/255, green: 200.0/255, blue: 63.0/255, alpha: 0.5)
                thridPackBtn.backgroundColor = UIColor(red: 163.0/255, green: 200.0/255, blue: 63.0/255, alpha: 0.5)
                fouthPackBtn.backgroundColor = UIColor(red: 163.0/255, green: 200.0/255, blue: 63.0/255, alpha: 0.5)
         secondPackBtn.backgroundColor = UIColor(red: 66.0/255, green: 192.0/255, blue: 206.0/255, alpha: 0.5)
    }
    
    @IBAction func thirdBtnAction(_ sender: Any) {
            productId = 300
    
           purchaseId = "com.zop.cellable.300Clicks"
        firstPackBtn.backgroundColor = UIColor(red: 163.0/255, green: 200.0/255, blue: 63.0/255, alpha: 0.5)
                     secondPackBtn.backgroundColor = UIColor(red: 163.0/255, green: 200.0/255, blue: 63.0/255, alpha: 0.5)
                     fouthPackBtn.backgroundColor = UIColor(red: 163.0/255, green: 200.0/255, blue: 63.0/255, alpha: 0.5)
        
         thridPackBtn.backgroundColor = UIColor(red: 66.0/255, green: 192.0/255, blue: 206.0/255, alpha: 0.5)
    }

    
    @IBAction func fourthBtnAction(_ sender: Any) {
      productId = 500
purchaseId = "com.zop.cellable.500Clicks"
         fouthPackBtn.backgroundColor = UIColor(red: 66.0/255, green: 192.0/255, blue: 206.0/255, alpha: 0.5)
        
        firstPackBtn.backgroundColor = UIColor(red: 163.0/255, green: 200.0/255, blue: 63.0/255, alpha: 0.5)
                            secondPackBtn.backgroundColor = UIColor(red: 163.0/255, green: 200.0/255, blue: 63.0/255, alpha: 0.5)
                            thridPackBtn.backgroundColor = UIColor(red: 163.0/255, green: 200.0/255, blue: 63.0/255, alpha: 0.5)
               
        
        
    }
//    func buyNonrenewingSubscription() {
//
//    }
  

}

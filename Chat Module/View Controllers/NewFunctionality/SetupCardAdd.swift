//
//  SetupCardAdd.swift
//  Invoicing
//
//  Created by ZoptalMacMini on 21/10/19.
//  Copyright © 2019 ZoptalMacMini. All rights reserved.
//

import UIKit
import SwiftyJSON
import Stripe
import iProgressHUD
class SetupCardAdd: UIViewController, UITextFieldDelegate, STPAddCardViewControllerDelegate, STPPaymentOptionsViewControllerDelegate, STPShippingAddressViewControllerDelegate,iProgressHUDDelegete

{
    func shippingAddressViewController(_ addressViewController: STPShippingAddressViewController, didEnter address: STPAddress, completion: @escaping STPShippingMethodsCompletionBlock) {
       
    }
    @IBOutlet weak var addButtonPayOut: UIButton!
    @IBOutlet weak var cardImg: UIImageView!
    @IBOutlet weak var btnSkip: UIBarButtonItem!

    @IBOutlet weak var viewCardNumber: UIView!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewExpiry: UIView!
    @IBOutlet weak var viewCVC: UIView!
    @IBOutlet weak var viewConfirm: UIView!
    @IBOutlet weak var switchDefaultCard: UISwitch!
    @IBOutlet weak var txtFldZip: UITextField!
    @IBOutlet weak var tfConfirmCard: UITextField!
    @IBOutlet weak var tfConfirmExpiry: UITextField!
    @IBOutlet weak var tfConfirmCVC: UITextField!
    @IBOutlet weak var tfConfirmName: UITextField!
    @IBOutlet weak var tfCountry: UITextField!

    var setCardDefault = String()
    var settokendDefault = String()
    var cardtype = String()
    var maxNumberOfCharacters = 16
     
   // let themeViewController = ThemeViewController()
    //let customerContext = MockCustomerContext()
    var isComingFromPaymentScreen = Bool()
    var currency:String = ""
    let api = API()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Add Card"
        
        self.navigationController?.navigationBar.topItem?.title = ""

        tfConfirmCard.delegate = self
        tfConfirmExpiry.delegate = self
        tfConfirmCVC.delegate = self
        setCardDefault = "0"
        
        self.view.semanticContentAttribute = .forceLeftToRight

        self.tfConfirmExpiry.semanticContentAttribute = .forceLeftToRight
        self.tfConfirmExpiry.textAlignment = .left
        self.tfConfirmCVC.semanticContentAttribute = .forceLeftToRight
        self.tfConfirmCVC.textAlignment = .left
        
        
    }
    func onShow(view: UIView) {
           print("onShow")
       }
       
       func onDismiss(view: UIView) {
           print("onDismiss")
       }
       
       func onTouch(view: UIView) {
           print("onTouch")
       }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
//    {
//        if let touch = touches.first {
//            let currentPoint = touch.location(in: self)
//            // do something with your currentPoint
//        }
//    }
    
    @IBAction func switchAction(_ sender: Any)
    {
        if switchDefaultCard.isOn
        {
        setCardDefault = "1"
        }else
        {
        setCardDefault = "0"
        }
    }
    
    func shippingAddressViewController(_ addressViewController: STPShippingAddressViewController, didFinishWith address: STPAddress, shippingMethod method: PKShippingMethod?)
    {
        // Save selected address and shipping method
        //        selectedAddress = address
        //        selectedShippingMethod = method
        
        // Dismiss shipping address view controller
        dismiss(animated: true)
    }
    // MARK: STPPaymentOptionsViewControllerDelegate
    
    func paymentOptionsViewController(_ paymentOptionsViewController: STPPaymentOptionsViewController, didFailToLoadWithError error: Error) {
        // Dismiss payment options view controller
        dismiss(animated: true)
        
        // Present error to user...
    }

    
    func paymentOptionsViewController(_ paymentOptionsViewController: STPPaymentOptionsViewController, didSelect paymentOption: STPPaymentOption) {
        // Save selected payment option
        //        selectedPaymentOption = paymentOption
    }
    // MARK: STPAddCardViewControllerDelegate
   
    
    // MARK: STPAddCardViewControllerDelegate

       func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
           dismiss(animated: true, completion: nil)
       }

       func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreatePaymentMethod paymentMethod: STPPaymentMethod, completion: @escaping STPErrorBlock)
       {
          //self.getToken()
            let cardParams = STPCardParams()
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
                      guard let token = token, error == nil else {
                          // Present error to user...
                          
                          print(error?.localizedDescription)
                          
                          //self.view.makeToast(error?.localizedDescription)
                          return
                      }
                    //  settokendDefault = token
                      print(token)
        }

           dismiss(animated: true, completion: nil)
       }

       // MARK: STPPaymentOptionsViewControllerDelegate

       func paymentOptionsViewControllerDidCancel(_ paymentOptionsViewController: STPPaymentOptionsViewController) {
           dismiss(animated: true, completion: nil)
       }

       func paymentOptionsViewControllerDidFinish(_ paymentOptionsViewController: STPPaymentOptionsViewController) {
           dismiss(animated: true, completion: nil)
       }


       // MARK: STPShippingAddressViewControllerDelegate

       func shippingAddressViewControllerDidCancel(_ addressViewController: STPShippingAddressViewController) {
           dismiss(animated: true, completion: nil)
       }
    
    @IBAction func addCardAction(_ sender: Any) {
       if validCardInputs()
               {
                   self.view.endEditing(true)
                   addButtonPayOut.isUserInteractionEnabled = true;
                
                   setupToken()
               }
               else
       {
                   addButtonPayOut.isUserInteractionEnabled = true;
        let alert = UIAlertController(title: "Error", message: "Please fill all card details", preferredStyle: UIAlertController.Style.alert)
                       alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                       self.present(alert, animated: true, completion: nil)
         
               }
    }
    
    @IBAction func skipAction(_ sender: Any) {
        //AppDelegate.sharedDelegate.setUpRiderHome()
    }
    
    
    func validateCardType(testCard: String) -> String {
           
           let regVisa = "^4[0-9]{12}(?:[0-9]{3})?$"
           let regMaster = "^5[1-5][0-9]{14}$"
           let regExpress = "^3[47][0-9]{13}$"
           let regDiners = "^3(?:0[0-5]|[68][0-9])[0-9]{11}$"
           let regDiscover = "^6(?:011|5[0-9]{2})[0-9]{12}$"
           let regJCB = "^(?:2131|1800|35\\d{3})\\d{11}$"
           
           
           let regVisaTest = NSPredicate(format: "SELF MATCHES %@", regVisa)
           let regMasterTest = NSPredicate(format: "SELF MATCHES %@", regMaster)
           let regExpressTest = NSPredicate(format: "SELF MATCHES %@", regExpress)
           let regDinersTest = NSPredicate(format: "SELF MATCHES %@", regDiners)
           let regDiscoverTest = NSPredicate(format: "SELF MATCHES %@", regDiscover)
           let regJCBTest = NSPredicate(format: "SELF MATCHES %@", regJCB)
           
           
           if regVisaTest.evaluate(with: testCard){
               return "Visa"
           }
           else if regMasterTest.evaluate(with: testCard)
           {
               return "MasterCard"
           }
               
           else if regExpressTest.evaluate(with: testCard){
               return "American Express"
           }
               
           else if regDinersTest.evaluate(with: testCard){
               return "Diners Club"
           }
               
           else if regDiscoverTest.evaluate(with: testCard){
               return "Discover"
           }
               
           else if regJCBTest.evaluate(with: testCard){
               return "JCB"
           }
           
           return ""
           
       }
    
    func validCardInputs()->Bool{
            let cardNumber = tfConfirmCard.text ?? ""
            let expiryData = tfConfirmExpiry.text ?? ""
            let cvv = tfConfirmCVC.text ?? ""
            // let zipCOde = txtFldZip.text ?? ""
           // let county = tfCountry.text ?? ""
        
        if cardNumber.isEmpty || expiryData.isEmpty || cvv.isEmpty
            {
                return false
            }
            else{
                return true
            }
        }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
            let newLength = textField.text!.count + string.count - range.length
            
           if textField == tfConfirmCard{
          //  let allowedCharacters = CharacterSet(charactersIn:"+0123456789 ")//Here change this characters based on your requirement
          //  let characterSet = CharacterSet(charactersIn: string)
           // return allowedCharacters.isSuperset(of: characterSet)
                return newLength <= 19
    //            if(textField == cardTextField){
    //
    //            }
               // return true
           }  else if textField == tfConfirmExpiry{
    //        let maxLength = 5
    //        let currentString: NSString = textField.text! as NSString
    //        let newString: NSString =
    //            currentString.replacingCharacters(in: range, with: string) as NSString
    //        return newString.length <= maxLength
            if textField.text?.count == 5{
                return newLength <= 5
            }
            if string == "" {
                return true
            }
            
            let currentText = textField.text! as NSString
            let updatedText = currentText.replacingCharacters(in: range, with: string)
            
            textField.text = updatedText
            let numberOfCharacters = updatedText.count
            if numberOfCharacters == 2 {
                textField.text?.append("/")
            }
            return false
            
              //  return newLength <= 5
           } else if textField == tfConfirmCVC{
            return newLength <= 3
           } else{
             return true
            }
    
           
        }
    
        private func setupToken()
        {

            let iprogress: iProgressHUD = iProgressHUD()
                   iprogress.delegete = self
                   iprogress.iprogressStyle = .horizontal
                   iprogress.indicatorStyle = .orbit
                   iprogress.isShowModal = false
                   iprogress.boxSize = 50
                   iprogress.attachProgress(toViews: view)
                   view.showProgress()
        
            let number = tfConfirmCard.text?.replacingOccurrences(of: " ", with: "") ?? ""
    //        let number2 = cradnumber2.text ?? ""
    //        let number3 = cardnumber3.text ?? ""
    //        let number4 = cardnumber4.text ?? ""
            let date = tfConfirmExpiry.text?.split(separator: "/")
            var dateArr = [String]()
            for i in date!{
                dateArr.append(String(i))
            }
          //  let expYear = tfe.text ?? ""
            let cvv = tfConfirmCVC.text ?? ""
           
            print(number)
            print(date!)
            print(cvv)
            
            let cardParams = STPCardParams()
            cardParams.number = number
            cardParams.expMonth = UInt(dateArr[0]) ?? 0
            cardParams.expYear = UInt(dateArr[1]) ?? 0
            cardParams.cvc = cvv
            cardParams.currency = "usd"
            STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
guard let token = token, error == nil
    else
          {
            
            let alert = UIAlertController(title: "Alert", message: error?.localizedDescription, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true)
            
//  self.showAlert(title: kAlertTitle, message: error?.localizedDescription)
  {
self.navigationController?.popViewController(animated: true)
                       }
              
              //print(error?.localizedDescription)
           loader.hide()
                self.view.dismissProgress()
              //self.view.makeToast(error?.localizedDescription)
              return
          }
           print(token)
           self.verifyPaymentWthBackend(token: token)
  }
    }
    
    
    func verifyPaymentWthBackend(token:STPToken)
    {
     
        let iprogress: iProgressHUD = iProgressHUD()
                         iprogress.delegete = self
                         iprogress.iprogressStyle = .horizontal
                         iprogress.indicatorStyle = .orbit
                         iprogress.isShowModal = false
                         iprogress.boxSize = 50
                         iprogress.attachProgress(toViews: view)
                         view.showProgress()
        
        
        
              print(token.tokenId)
              var parameters = [String : Any]()
              parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
              parameters["country"] = ""
              parameters["zip_code"] = ""
              parameters["name"] = tfConfirmName.text
              parameters["is_default"] = setCardDefault
              parameters["stripetoken"] = token.tokenId
             self.api.AddCard(withdata: parameters, success: { (data) in
               loader.hide()
                    self.view.dismissProgress()
                           

                      let alertController = UIAlertController(title: "Cellable", message: data["message"] as? String, preferredStyle: UIAlertController.Style.alert)
                                          let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          self.navigationController?.popViewController(animated: true)
                                          }
                                          alertController.addAction(okAction)
                                          self.present(alertController, animated: true, completion: nil)
                             }) { (error) in
                                 print(error)
                                 loader.hide()
                                    self.view.dismissProgress()
                                 if error.code  == 204
                                 {
                                     let alert = UIAlertController(title: "Cellable", message: error.localizedDescription as? String, preferredStyle: UIAlertController.Style.alert)
                             alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                         self.present(alert, animated: true, completion: nil)
                                 }
                             }

        }
        
func didChangeText(textField:UITextField)
{
//
//           if textField == tfConfirmCard
//           {
//               switch textField.validateCreditCardFormat().type {
//               case CardType.Visa:
//                   cardImg.image = UIImage(named: "visa")
//               case  CardType.Amex:
//                    cardImg.image = UIImage(named: "american")
//               case  CardType.MasterCard:
//                   cardImg.image = UIImage(named: "card_master")
//               case  CardType.Hipercard:
//                   cardImg.image = UIImage(named: "Hipercard_logo")
//               case  CardType.Diners:
//                   cardImg.image = UIImage(named: "dinners")
//               case  CardType.UnionPay:
//                   cardImg.image = UIImage(named: "union")
//               case  CardType.Discover:
//                   cardImg.image = UIImage(named: "discover")
//               case  CardType.JCB:
//                   cardImg.image = UIImage(named: "jcb")
//               case CardType.Elo:
//                   cardImg.image = UIImage(named: "elo")
//               default:
//                   cardImg.image = UIImage(named: "credit-card")
//               }
//
//
//           }
         //  textField.text = modifyCreditCardString(creditCardString: textField.text!)
       }
    
   func modifyCreditCardString(creditCardString : String) -> String{
      
           let trimmedString = creditCardString.components(separatedBy: .whitespaces).joined()
           
           let arrOfCharacters = Array(trimmedString)
           
           var modifiedCreditCardString = ""
           
           
           if(arrOfCharacters.count > 0)
           {
               for i in 0...arrOfCharacters.count-1
               {
                   modifiedCreditCardString.append(arrOfCharacters[i])
                   
                   if((i+1) % 4 == 0 && i+1 != arrOfCharacters.count)
                   {
                       
                       modifiedCreditCardString.append(" ")
                   }
               }
           }
           return modifiedCreditCardString
       
       
   }

                                       
      enum CardType: String {
                case Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay
                
                static let allCards = [Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay]
                
                var regex : String {
                    switch self {
                    case .Amex:
                        return "^3[47][0-9]{5,}$"
                    case .Visa:
                        return "^4[0-9]{6,}([0-9]{3})?$"
                    case .MasterCard:
                        return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
                    case .Diners:
                        return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
                    case .Discover:
                        return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
                    case .JCB:
                        return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
                    case .UnionPay:
                        return "^(62|88)[0-9]{5,}$"
                    case .Hipercard:
                        return "^(606282|3841)[0-9]{5,}$"
                    case .Elo:
                        return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
                    default:
                        return ""
                    }
                }
            }
         
//
//extension UITextField
//{
//
//        func validateCreditCardFormat()-> (type: CardType, valid: Bool) {
//            // Get only numbers from the input string
//            let input = self.text!
//            let numberOnly = input.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
//
//            var type: CardType = .Unknown
//            var formatted = ""
//            var valid = false
//
//            // detect card type
//            for card in CardType.allCards {
//                if (matchesRegex(regex: card.regex, text: numberOnly)) {
//                    type = card
//                    break
//                }
//            }
//
//            // check validity
//            valid = luhnCheck(number: numberOnly)
//
//            // format
//            var formatted4 = ""
//            for character in numberOnly
//            {
//                if formatted4.count == 4 {
//                    formatted += formatted4 + " "
//                    formatted4 = ""
//                }
//                formatted4.append(character)
//            }
//
//            formatted += formatted4 // the rest
//
//            // return the tuple
//            return (type, valid)
//        }
//
//        func matchesRegex(regex: String!, text: String!) -> Bool {
//            do {
//                let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
//                let nsString = text as NSString
//                let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
//                return (match != nil)
//            } catch {
//                return false
//            }
//        }
//
//        func luhnCheck(number: String) -> Bool {
//            var sum = 0
//            let digitStrings = number.reversed().map { String($0) }
//
//            for tuple in digitStrings.enumerated() {
//                guard let digit = Int(tuple.element) else { return false }
//                let odd = tuple.offset % 2 == 1
//                switch (odd, digit) {
//                case (true, 9):
//                    sum += 9
//                case (true, 0...8):
//                    sum += (digit * 2) % 9
//                default:
//                    sum += digit
//                }
//            }
//
//            return sum % 10 == 0
//        }
//}
}

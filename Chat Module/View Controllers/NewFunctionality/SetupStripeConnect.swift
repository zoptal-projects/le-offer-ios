//
//  SetupPaymentDriverViewController.swift
//  Invoicing
//
//  Created by ZoptalMacMini on 21/10/19.
//  Copyright © 2019 ZoptalMacMini. All rights reserved.
//
//SetupStripeConnect.swift
import UIKit
import Stripe
import iProgressHUD
class SetupStripeConnect: UIViewController, UITextFieldDelegate,iProgressHUDDelegete
{

    var isStripe = false
    var isComingFromPaymentScreen = Bool()
     let api = API()
    var stripURL = String()
    var UserId = String()
    var AccountId = String()
    @IBOutlet weak var stripConnectOut: UIButton!
     var  userInfo : NSDictionary = [:]
    
        var checkstripe = String()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        stripConnectOut.isHidden = true
        stripConnectOut.layer.cornerRadius = stripConnectOut.frame.height/2
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController!.navigationBar.tintColor = UIColor.white
        
        //navigationController?.navigationBar.barTintColor = .white
        //navigationController?.navigationBar.tintColor = .white
        //SetupPaymentDriverViewController
    }
    func onShow(view: UIView) {
                 print("onShow")
             }
             
             func onDismiss(view: UIView) {
                 print("onDismiss")
             }
             
             func onTouch(view: UIView) {
                 print("onTouch")
             }
    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.navigationBar.barTintColor = UIColor(red: 17.0/255.0, green: 42.0/255.0, blue: 70.0/255.0, alpha: 1.0)
        self.navigationController?.isNavigationBarHidden = false
        UINavigationBar.appearance().isTranslucent = true
        
        checkstripe = UserDefaults.standard.string(forKey: mAccountId) ?? ""
        if checkstripe.count == 0
        {
       let iprogress: iProgressHUD = iProgressHUD()
                            iprogress.delegete = self
                            iprogress.iprogressStyle = .horizontal
                            iprogress.indicatorStyle = .orbit
                            iprogress.isShowModal = false
                            iprogress.boxSize = 50
                            iprogress.attachProgress(toViews: view)
                            view.showProgress()
              var parameters = [String : Any]()
              parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
              self.api.ConnectStripe(withdata: parameters, success: { (data) in
              loader.hide()
                    self.view.dismissProgress()
        
                self.userInfo = data["data"] as! NSDictionary
                print(data)
                
                if data["status"] as! Int == 1 || data["message"] as? String == "User has already stripe account id."
                {
                     self.stripConnectOut.setTitle("Stripe Disconnect", for: .normal)
                }
                else
                {
                    self.stripConnectOut.setTitle("Connect Stripe", for: .normal)

                }
                self.AccountId = self.userInfo["accountId"] as? String ?? ""
                self.stripURL =  self.userInfo["weburl"] as? String ?? ""
                //self.UserId =  JSON["data"][0]["userId"].stringValue
                // self.AccountId =  JSON["data"][0]["accountId"].stringValue
            let alert = UIAlertController(title: "Cellable", message: data["message"] as? String, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
                            }) { (error) in
                                print(error)
                                loader.hide()
                                    self.view.dismissProgress()
                                if error.code  == 204
                                {
        let alert = UIAlertController(title: "Cellable", message: error.localizedDescription as? String, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
                                }
            }
        self.stripConnectOut.isHidden = false
        }
        else
        {
            self.AccountId = checkstripe
              self.stripConnectOut.isHidden = false
             self.stripConnectOut.setTitle("Stripe Disconnect", for: .normal)
        }
    }
    
    func stripeConnectAction(_ sender: UIButton)
    {
        if sender.titleLabel?.text == "Connect Stripe"
        {
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "StripeConnectVC") as! StripeConnectVC
            nextVC.strURl = "\(stripURL)"
            self.navigationController?.pushViewController(nextVC, animated: true)

        }
        else
        {

            let alert = UIAlertController(title: kAlertTitle, message: "Are you sure you want to Disconnect?", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: { action in

        iProgressHUD.sharedInstance().attachProgress(toView: self.view)
                    self.view.showProgress()
                    self.view.updateIndicator(style: .ballBeat)
            var parameters = [String : Any]()
            parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
            parameters["accountId"] = self.AccountId
            print(parameters)
            self.api.DisconnectStripe(withdata: parameters, success: { (data) in
            loader.hide()
                    self.view.dismissProgress()
            print(data)
                                            
                        if data["status"] as! Int == 1 || data["message"] as? String == "User has already stripe account id."
                    {
                            self.stripConnectOut.setTitle("Stripe Disconnect", for: .normal)
                                                                }
                                                                else
                                                                {
                                                                    self.stripConnectOut.setTitle("Connect Stripe", for: .normal)

                                                                }
                                               
        let alert = UIAlertController(title: "Cellable", message: data["message"] as? String, preferredStyle: UIAlertController.Style.alert)
                                                                               alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                                                           self.present(alert, animated: true, completion: nil)
                                             }) { (error) in
                                                 print(error)
                                                 loader.hide()
                                                    self.view.dismissProgress()
                                                 if error.code  == 204
                                                 {
                                                     let alert = UIAlertController(title: "Cellable", message: error.localizedDescription as? String, preferredStyle: UIAlertController.Style.alert)
                                             alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                         self.present(alert, animated: true, completion: nil)
                                                 }
                                             }
                })
                let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in})
                alert.addAction( cancel)
                alert.addAction(ok)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true)})






            }
        }
    
    }
    

    
 

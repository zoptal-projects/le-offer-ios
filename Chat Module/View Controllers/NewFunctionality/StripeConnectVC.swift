//
//  StripeConnectVC.swift
//  Cellable
//
//  Created by Anish Mac Mini on 10/06/20.
//  Copyright © 2020 Rahul Sharma. All rights reserved.
//

import UIKit
import WebKit
import iProgressHUD
class StripeConnectVC: UIViewController, WKNavigationDelegate,iProgressHUDDelegete
{

    @IBOutlet weak var stripeconnectLbl: UILabel!
    @IBOutlet weak var webviewnew: WKWebView!
    
    var strURl = String()
    var UserIdSave = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
      //  stripeconnectLbl.text = "Stripe Connect Setup"
        self.title = "Stripe Connect Setup"

    
        let myRequest = URLRequest(url:URL(string:strURl)!)
        webviewnew.navigationDelegate = self
        webviewnew.load(myRequest)
    }
    @IBAction func dismissbtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
  func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
     let iprogress: iProgressHUD = iProgressHUD()
                        iprogress.delegete = self
                        iprogress.iprogressStyle = .horizontal
                        iprogress.indicatorStyle = .orbit
                        iprogress.isShowModal = false
                        iprogress.boxSize = 50
                        iprogress.attachProgress(toViews: view)
                        view.showProgress()
  }
    func onShow(view: UIView) {
              print("onShow")
          }
          
          func onDismiss(view: UIView) {
              print("onDismiss")
          }
          
          func onTouch(view: UIView) {
              print("onTouch")
          }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
 
        loader.hide()
            self.view.dismissProgress()
         //print(webView.url)
        // print(webView.url?.absoluteString)
         if (webView.url?.absoluteString.lowercased().contains("https://dev.cellableapp.com/api/success"))!
         {
         self.navigationController?.popViewController(animated: true)
         }
         else
         if (webView.url?.absoluteString.lowercased().contains("https://dev.cellableapp.com/api/failure"))!
         {
             let alert = UIAlertController(title: kAlertTitle, message: "Stripe connection failed. Please try again.", preferredStyle: .alert)
             let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
             alert.addAction(alertAction)
             self.present(alert, animated: true, completion: nil)
         }
    }
    
        
        //MARK:- WKNavigationDelegate
        func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error)
        {
    //        ServerManager.shared.hidHud()
            //print(error.localizedDescription)
        }
      
}

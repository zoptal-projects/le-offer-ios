//
//  AddAddressVC.swift
//  Cellable
//
//  Created by Anish Mac Mini on 06/06/20.
//  Copyright © 2020 Rahul Sharma. All rights reserved.
//

import UIKit
import iProgressHUD


class AddAddressVC: UIViewController,iProgressHUDDelegete
{
    @IBOutlet weak var addBtnOut: UIButton!
    @IBOutlet weak var citylbl: UITextField!
    @IBOutlet weak var stateLbl: UITextField!
    @IBOutlet weak var countryLbl: UITextField!
    @IBOutlet weak var landmarkLbl: UITextField!
    @IBOutlet weak var address2lbl: UITextField!
    @IBOutlet weak var address1lbl: UITextField!
    @IBOutlet weak var pincodelbl: UITextField!
    
    var roughtchecking = NSString()
    var productID = NSString()
    var currency = NSString()
    var postType = NSString()
    var  userInfo : NSDictionary = [:]
    let api = API()
   
    override func viewDidLoad()
    {
    super.viewDidLoad()
    print(self.userInfo)
    }
    override func viewWillAppear(_ animated: Bool)
    {
    self.title = "Add shipping address"
    self.navigationController?.navigationBar.topItem?.title = ""
    
    if roughtchecking == "FromFinalPayment"
    {
    self.fetchAllAddress()
    addBtnOut.setTitle("Deliver to this address", for: .normal)
    addBtnOut.addTarget(self, action: #selector(delivertothisAddressBtnAction), for: .touchUpInside)
    }
    else
    {
    address1lbl.text = self.userInfo["address_one"] as? String
    address2lbl.text = self.userInfo["address_two"] as? String
    pincodelbl.text = self.userInfo["pin_code"] as? String
    countryLbl.text = self.userInfo["country"] as? String
    stateLbl.text = self.userInfo["state"] as? String
    citylbl.text = self.userInfo["town"] as? String
               
               if self.address1lbl.text?.count != 0
               {
               self.title = "Update shipping address"
               addBtnOut.setTitle("Update Address", for: .normal)
                addBtnOut.addTarget(self, action: #selector(editAddressBtnAction), for: .touchUpInside)
               }
               else
               {
               self.title = "Add shipping address"
               addBtnOut.setTitle("Submit", for: .normal)
               addBtnOut.addTarget(self, action: #selector(addAddressBtnAction), for: .touchUpInside)
               }
           }
            self.navigationController!.navigationBar.tintColor = UIColor.white
    }
    
    @objc func addAddressBtnAction()
    {
        let pincodeString = pincodelbl.text?.trimmingCharacters(in: .whitespaces)
        let address1String = address1lbl.text?.trimmingCharacters(in: .whitespaces)
        let address2String =  address2lbl.text?.trimmingCharacters(in: .whitespaces)
        let landmarString = landmarkLbl.text?.trimmingCharacters(in: .whitespaces)
        let countryString = countryLbl.text?.trimmingCharacters(in: .whitespaces)
        let stateString = stateLbl.text?.trimmingCharacters(in: .whitespaces)
        let cityString = citylbl.text?.trimmingCharacters(in: .whitespaces)
        
        if pincodeString?.count == 0 || address1String?.count == 0 || countryString?.count == 0 || stateString?.count == 0 || cityString?.count == 0
        {
            let alert = UIAlertController(title: "Error", message: "Please enter all required fields.", preferredStyle: UIAlertController.Style.alert)
                                   alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                   self.present(alert, animated: true, completion: nil)
        }
        else
        {
        
     let iprogress: iProgressHUD = iProgressHUD()
                                   iprogress.delegete = self
                                   iprogress.iprogressStyle = .horizontal
                                   iprogress.indicatorStyle = .orbit
                                   iprogress.isShowModal = false
                                   iprogress.boxSize = 50
                                   iprogress.attachProgress(toViews: view)
                                   view.showProgress()
       
        var parameters = [String : Any]()
        parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
        parameters["full_name"] = ""
        parameters["mobile_num"] = ""
        parameters["pin_code"] = pincodeString
        parameters["address_one"] = address1String
        parameters["address_two"] = address2String
        parameters["landmark"] = landmarString
        parameters["country"] = countryString
        parameters["state"] = stateString
        parameters["town"] = cityString
                    
        api.AddAddress(withdata: parameters) { (JSON) in
            loader.hide()
                self.view.dismissProgress()
            let alertController = UIAlertController(title: "Cellable", message: JSON!["message"] as? String, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
            {
            UIAlertAction in
            self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
        
}
    func onShow(view: UIView) {
                    print("onShow")
                }
                
                func onDismiss(view: UIView) {
                    print("onDismiss")
                }
                
                func onTouch(view: UIView) {
                    print("onTouch")
                }
    @objc func delivertothisAddressBtnAction()
    {
        let pincodeString = pincodelbl.text?.trimmingCharacters(in: .whitespaces)
        let address1String = address1lbl.text?.trimmingCharacters(in: .whitespaces)
        let address2String =  address2lbl.text?.trimmingCharacters(in: .whitespaces)
        let landmarString = landmarkLbl.text?.trimmingCharacters(in: .whitespaces)
        let countryString = countryLbl.text?.trimmingCharacters(in: .whitespaces)
        let stateString = stateLbl.text?.trimmingCharacters(in: .whitespaces)
        let cityString = citylbl.text?.trimmingCharacters(in: .whitespaces)
                     
                     if pincodeString?.count == 0 || address1String?.count == 0 || countryString?.count == 0 || stateString?.count == 0 || cityString?.count == 0
                     {
                         let alert = UIAlertController(title: "Error", message: "Please enter all required fields.", preferredStyle: UIAlertController.Style.alert)
                                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                                self.present(alert, animated: true, completion: nil)
                     }
                     else
                     {
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                              let nextVC = storyBoard.instantiateViewController(withIdentifier: "PaymentMethodViewController") as! PaymentMethodViewController
                              nextVC.productID = productID
                              nextVC.postType = postType
                              nextVC.pincodeString = pincodelbl.text as! NSString
                              nextVC.address1String = address1lbl.text as! NSString
                              nextVC.address2String = address2lbl.text as! NSString
                              nextVC.landmarString = landmarkLbl.text as! NSString
                               nextVC.countryString = countryLbl.text as! NSString
                               nextVC.stateString = stateLbl.text as! NSString
                               nextVC.cityString = citylbl.text as! NSString
                              nextVC.roughtchecking = "FromFinalPaymentPart"
                              nextVC.currency = currency
                              self.navigationController?.pushViewController(nextVC, animated: true)
                        
        }
        
        
      
    }
    
    @objc func editAddressBtnAction()
            {
                let pincodeString = pincodelbl.text?.trimmingCharacters(in: .whitespaces)
                let address1String = address1lbl.text?.trimmingCharacters(in: .whitespaces)
                let address2String =  address2lbl.text?.trimmingCharacters(in: .whitespaces)
                let landmarString = landmarkLbl.text?.trimmingCharacters(in: .whitespaces)
                let countryString = countryLbl.text?.trimmingCharacters(in: .whitespaces)
                let stateString = stateLbl.text?.trimmingCharacters(in: .whitespaces)
                let cityString = citylbl.text?.trimmingCharacters(in: .whitespaces)
                
                if pincodeString?.count == 0 || address1String?.count == 0 || countryString?.count == 0 || stateString?.count == 0 || cityString?.count == 0
                {
                    let alert = UIAlertController(title: "Error", message: "Please enter all required fields.", preferredStyle: UIAlertController.Style.alert)
                                           alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                           self.present(alert, animated: true, completion: nil)
                }
                else
                {

                    let iprogress: iProgressHUD = iProgressHUD()
                                     iprogress.delegete = self
                                     iprogress.iprogressStyle = .horizontal
                                     iprogress.indicatorStyle = .orbit
                                     iprogress.isShowModal = false
                                     iprogress.boxSize = 50
                                     iprogress.attachProgress(toViews: view)
                                     view.showProgress()
                    
                    
                    
                    var parameters = [String : Any]()
                    parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
                    parameters["full_name"] = ""
                    parameters["mobile_num"] = ""
                    parameters["pin_code"] = pincodeString
                    parameters["address_one"] = address1String
                    parameters["address_two"] = address2String
                    parameters["landmark"] = landmarString
                    parameters["country"] = countryString
                    parameters["state"] = stateString
                    parameters["town"] = cityString
                    parameters["address_id"] = self.userInfo["_id"] as? String
                    print(parameters)
                    self.api.UpdateAddress(withdata: parameters, success: { (data) in
                        loader.hide()
                            self.view.dismissProgress()
                   let alertController = UIAlertController(title: "Cellable", message: data["message"] as? String, preferredStyle: UIAlertController.Style.alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                    }) { (error) in
                    print(error)
                    loader.hide()
                            self.view.dismissProgress()
                    if error.code  == 204
                    {
                    let alert = UIAlertController(title: "Cellable", message: error.localizedDescription as? String, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    }
                    }
            }
    }
    
    func fetchAllAddress()
           {
           var parameters = [String : Any]()
           parameters["token"] = UserDefaults.standard.string(forKey: mauthToken)
           self.api.GetAddress(withdata: parameters, success: { (data) in
           loader.hide()
                self.view.dismissProgress()
            print(data)
           self.userInfo = data["data"] as! NSDictionary
           print(self.userInfo)
           self.address1lbl.isHidden = false
           self.address2lbl.isHidden = false
           self.landmarkLbl.isHidden = false
           self.countryLbl.isHidden = false
           self.stateLbl.isHidden = false
     
           self.address1lbl.text = self.userInfo["address_one"] as? String ?? ""
           self.address2lbl.text =  self.userInfo["address_two"] as? String ?? ""
           self.pincodelbl.text = self.userInfo["pin_code"] as? String ?? ""
           self.countryLbl.text =  self.userInfo["country"] as? String ?? ""
          self.landmarkLbl.text =  self.userInfo["landmark"] as? String ?? ""
          self.stateLbl.text =  self.userInfo["state"] as? String ?? ""
         self.citylbl.text =  self.userInfo["town"] as? String ?? ""
                  
                       
                   let alert = UIAlertController(title: "Cellable", message: data["message"] as? String, preferredStyle: UIAlertController.Style.alert)
                                                           alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                                       self.present(alert, animated: true, completion: nil)
                         }) { (error) in
                             print(error)
                             loader.hide()
                                self.view.dismissProgress()
                             if error.code  == 204
                             {
                                 let alert = UIAlertController(title: "Cellable", message: error.localizedDescription as? String, preferredStyle: UIAlertController.Style.alert)
                         alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                     self.present(alert, animated: true, completion: nil)
                             }
                         }
                   }
    
    
}

//
//  ChatListTableViewCell.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 22/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

class ChatListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImageOutlet: UIImageView!
    @IBOutlet weak var userNameOutlet: UILabel!
    @IBOutlet weak var lastMessageOutlet: UILabel!
    @IBOutlet weak var lastimeMessageOutlet: UILabel!
    @IBOutlet weak var unreadMessageCountOutlet: UILabel!
    @IBOutlet weak var unreadMsgbackgroundViewOutlet: CircleBackgroundView!
    @IBOutlet weak var lastTimeForMsgOutlet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var chatVMObj : ChatViewModel! {
        didSet {
            self.userImageOutlet.image = #imageLiteral(resourceName: "itemUserDefault")
            if let name = chatVMObj.name, let lastMsgsCount = chatVMObj.newMessageCount, let lastmessageTime = chatVMObj.newMessageTime, let docID = chatVMObj.docID {
                
                if let imageURL = chatVMObj.imageURL {
                    if imageURL.absoluteString == "defaultUrl" {
                    self.userImageOutlet.image = #imageLiteral(resourceName: "itemUserDefault")
                    } else {
                   self.userImageOutlet.kf.setImage(with: imageURL, placeholder: #imageLiteral(resourceName: "itemUserDefault"), options: [.transition(ImageTransition.fade(1))])
                    }
                }
                self.userNameOutlet.text = name
                self.unreadMessageCountOutlet.text = lastMsgsCount
                self.lastimeMessageOutlet.text = lastmessageTime
                self.unreadMsgbackgroundViewOutlet.layoutIfNeeded()
                
                if !chatVMObj.hasNewMessage {
                    self.unreadMsgbackgroundViewOutlet.isHidden = true
                    self.lastimeMessageOutlet.textColor = UIColor.black
                } else {
                    self.unreadMsgbackgroundViewOutlet.isHidden = false
                    self.lastimeMessageOutlet.textColor = UIColor(red: 0/255, green: 90/255, blue: 255/255, alpha: 1)
                }
                if let newMessage = chatVMObj.newMessage {
                    if let message = newMessage.fromBase64() {
                        self.lastMessageOutlet.text = message
                    }else{
                        self.lastMessageOutlet.text = newMessage
                    }
                }
                if self.lastMessageOutlet.text!.count == 0 {
                    self.lastMessageOutlet.text = "Product is no longer available"
                }
                self.getLastMessage(withDocID: docID, chatVMObject: chatVMObj)
            }
            self.setProductNoLongerExist()
            DispatchQueue.main.async {
                self.layoutIfNeeded()
            }
        }
    }
    
    func setProductNoLongerExist() {
        if self.chatVMObj.isProductSold {
            let attachment = NSTextAttachment()
            attachment.image = #imageLiteral(resourceName: "warring")
            attachment.bounds = CGRect(x: 0, y: 0, width: 10, height: 10)
            let attachmentStr = NSAttributedString(attachment: attachment)
            let myString = NSMutableAttributedString(string: "")
            myString.append(attachmentStr)
            let myString1 = NSMutableAttributedString(string: "  Product no longer exist")
            myString.append(myString1)
            self.lastMessageOutlet.attributedText = myString
        }
    }
    
    func getLastMessage(withDocID docID : String, chatVMObject  : ChatViewModel) {
        guard let messageObj = chatVMObject.getlastMessage(fromChatDocID: docID) else { return }
        guard let isSelfMsg = messageObj.isSelfMessage else { return }
        var msg = ""
        if let message = messageObj.messagePayload {
            msg = message
        }
        guard let timestamp = messageObj.timeStamp else { return }
        let lastmsgDate = DateExtension().getDateObj(fromTimeStamp: timestamp)
        self.lastTimeForMsgOutlet.text = DateExtension().lastMessageInHours(date: lastmsgDate)
        
        if let deliveryStatus = messageObj.messageStatus {
            guard let str = chatVMObject.getAtributeString(withMessageStatus: deliveryStatus) else { return }
            if isSelfMsg == true {
                if let decodeStr = msg.fromBase64() {
                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: decodeStr, attributes: nil)
                } else {
                    self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: msg, attributes: nil)
                }
            } else {
                if let decodeStr = msg.fromBase64() {
                    str.append(NSAttributedString(string: decodeStr, attributes: nil))
                    self.lastMessageOutlet.attributedText = str
                } else {
                    let str = NSAttributedString(string: msg, attributes: nil)
                    self.lastMessageOutlet.attributedText = str
                }
            }
        }
        
        if (chatVMObject.initiated) == true  {
            if let productimage = chatVMObject.productImage, let productName = chatVMObject.productName{
                self.userImageOutlet.kf.setImage(with: productimage, placeholder: #imageLiteral(resourceName: "itemProdDefault"), options: [.transition(ImageTransition.fade(1))])
                self.userNameOutlet.text = productName
            }
        }
        
        if messageObj.isMediaMessage {
            if let messageMediaType:MessageTypes = messageObj.messageType {
                switch messageMediaType {
                    
                case .text:
                    break;
                    
                case .image:
                    if isSelfMsg == false {
                        self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: "You have got an image", attributes: nil)
                    } else {
                        self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: "You sent an image", attributes: nil)
                    }
                case .video:
                    break;
                    
                case .location:
                    if isSelfMsg == false {
                        self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: "You have got a location", attributes: nil)
                    } else {
                        self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: "You sent a location", attributes: nil)
                    }
                case .contact:
                    break;
                    
                case .audio:
                    break;
                    
                case .sticker:
                    break;
                    
                case .doodle:
                    break;
                    
                case .gif:
                    break;
                    
                case .header:
                    break;
                    
                case .order:
                    if isSelfMsg == false {
                        self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: "You have got an Offer", attributes: nil)
                    } else {
                        self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: "You sent an Offer", attributes: nil)
                    }
                    break;
                case .swap:
                    if isSelfMsg == false {
                        self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: "You have got an swap Offer", attributes: nil)
                    } else {
                        self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: "You sent an swap Offer", attributes: nil)
                    }
                case .payment:
                    if isSelfMsg == false {
                        self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: "You have got a Paypal Link", attributes: nil)
                    } else {
                        self.lastMessageOutlet.attributedText = NSMutableAttributedString(string: "You sent a Paypal Link", attributes: nil)
                    }
                    break;
                }
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

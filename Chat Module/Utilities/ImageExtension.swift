//
//  ImageExtension.swift
//  Tac Traderz
//
//  Created by Rahul Sharma on 05/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation
class ImageExtension : NSObject {
    
    class func convertImageToBase64(image: UIImage) -> String {
        
        let imageData = image.pngData()
        let base64String = imageData?.base64EncodedString(options: [])
        return base64String!
        
    }// end convertImageToBase64
    
    
    // prgm mark ----
    
    // convert images into base64 and keep them into string
    
    class func convertBase64ToImage(base64String: String) -> UIImage {
        
        let decodedData = NSData(base64Encoded: base64String, options: NSData.Base64DecodingOptions(rawValue: 0))
        let decodedimage = UIImage(data: decodedData! as Data)
        return decodedimage!
        
    }
}

//
//  MQTTModule.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 11/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import Foundation

/// This class is a model for interacting with the MQTTClient library.
class MQTT : NSObject {
    
    struct Constants {
        ///Ask server guy for host and port.
        fileprivate static let host =  mMQTTHost
        fileprivate static let port:Int = Int(mPort)!
        
        /// your current userID
    }
    
    /// Shared instance object for gettting the singleton object
    static let sharedInstance = MQTT()
    
    /**
     tells about current connection, true if it is conected or false if it is disconnected.
     
     
     let sharedInstance = MQTT.sharedInstance.manager.state
     
     
     */
    var isConnected = false
    
    ///current session object will going to store in this.
    var manager : MQTTSessionManager!
    
    ///Used for running the task in the background.
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    
    /// MQTT delegate object, its going to store the objects of the delegate receiver class.
    let mqttMessageDelegate = MQTTDelegate()
    
    
    /// Used for creating the initial connection.
    func createConnection() {
        
        /// Observer for app coming in foreground.
        NotificationCenter.default.addObserver(self, selector: #selector(MQTT.reinstateBackgroundTask), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        registerBackgroundTask()
        //userId
        
        guard let userID = Helper.getMQTTID() else { return }
        if userID != "mqttId" {
            if userID.count>1 {
                ///creating connection with the proper client ID.
                self.connect(withClientId: userID)
            }
        }
    }
    
    func disconnectMQTTConnection() {
        if self.isConnected {
            self.isConnected = false
            manager.disconnect(disconnectHandler: nil)
        }
    }
    
    /// Used for subscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to subscribe.
    func subscribeTopic(withTopicName topicName : String,withDelivering delivering:MQTTQosLevel ) {
        if topicName.hasSuffix("mqttId") {
            return
        }
        let topicToSubscribe = topicName
        self.subscribe(topic: "\(topicToSubscribe)", withDelivering: delivering)
    }
    
    /// Used for Unsubscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to Unsubscribing.
    func unsubscribeTopic(topic : String) {
        var unsubscribeDict = manager.subscriptions
        if unsubscribeDict?[topic] != nil {
            unsubscribeDict?.removeValue(forKey:topic)
        }
        self.manager.subscriptions = unsubscribeDict
        self.manager.connect(toLast: nil)
    }
    
    /// Used for subscribing the channel
    ///
    /// - Parameters:
    ///   - topic: name of the current topic (It should contain the name of the topic with saperators)
    ///
    /// eg- Message/UserName
    ///   - Delivering: Type of QOS // can be 0,1 or 2.
    fileprivate func subscribe(topic : String, withDelivering Delivering : MQTTQosLevel) {
        if (manager != nil) {
            if (manager.subscriptions != nil) {
                var subscribeDict = manager.subscriptions!
                if (subscribeDict[topic] == nil) {
                    subscribeDict[topic] = Delivering.rawValue as NSNumber
                }
                self.manager.subscriptions = subscribeDict
            }else {
                let subcription = [topic : Delivering.rawValue as NSNumber]
                self.manager.subscriptions = subcription
            }
            self.manager.connect(toLast: nil)
        }
    }
    
    
    /// Used for reinstate the background task
    @objc func reinstateBackgroundTask() {
        if (backgroundTask == UIBackgroundTaskIdentifier.invalid) {
            registerBackgroundTask()
        }
    }
    
    ///Here I am registering for the background task.
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskIdentifier.invalid)
    }
    
    ///Before background task ending this method is going to be called.
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(convertToUIBackgroundTaskIdentifier(backgroundTask.rawValue))
        backgroundTask = UIBackgroundTaskIdentifier.invalid
    }
    
    /// Used for pubishing the data in between channels.
    ///
    /// - Parameters:=
    ///   - jsonData: Data in JSON format.
    ///   - channel: current channel name to publish the data to.
    ///   - messageID: current message ID (this ID should be unique)
    ///   - Delivering: Type of QOS // can be 0,1 or 2.
    ///   - retain: true if you wanted to retain the messages or False if you don't
    ///   - completion: This will going to return MQTTSessionCompletionBlock.
    
    func publishData(wthData jsonData: Data, onTopic topic : String, retain : Bool, withDelivering delivering : MQTTQosLevel) {
        if (self.manager != nil) {
            manager.send(jsonData, topic: topic, qos: delivering, retain: retain)
        }
        else {
            guard let userID = Helper.getMQTTID() else { return }
            if (userID.count>1) {
                connect(withClientId: userID)
            }
        }
    }
    
    /// Used for connecting with the server.
    ///
    /// - Parameter clientId: current Client ID.
    func connect(withClientId clientId :String) {
        if (self.manager == nil) {
            self.manager = MQTTSessionManager()
            let host = Constants.host
            let port: Int = Constants.port
            //            let newSession = MQTTSessionManager(persistence: true, maxWindowSize: 64, maxMessages: 1024*1024, maxSize: 256, maxConnectionRetryInterval: 60, connectInForeground: true)
            //            manager = newSession
            manager.delegate = mqttMessageDelegate
            //            manager.connect(to: host, port: port, tls: false, keepalive: 60, clean: false, auth: true, user: mMqttUsername, pass: mMqttPassword, will: false, willTopic: nil, willMsg: nil, willQos: .atMostOnce, willRetainFlag: false, withClientId: "yelo_\(clientId)")
            manager.connect(to: host, port: port, tls: false, keepalive: 60, clean: false, auth: true, user: mMqttUsername, pass: mMqttPassword, will: false, willTopic: nil, willMsg: nil, willQos: .atMostOnce, willRetainFlag: false, withClientId: "yelo_\(clientId)", securityPolicy: .none, certificates: nil, protocolLevel: .version311, connectHandler: nil)
        } else {
            self.manager.connect(toLast: nil)
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIBackgroundTaskIdentifier(_ input: Int) -> UIBackgroundTaskIdentifier {
	return UIBackgroundTaskIdentifier(rawValue: input)
}

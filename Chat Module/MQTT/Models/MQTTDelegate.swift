//
//  MessageNotificationDelegate.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 31/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import Firebase

class MQTTDelegate: NSObject, MQTTSessionManagerDelegate {
    
    
    func handleMessage(_ data: Data!, onTopic topic: String!, retained: Bool) {
        do {
            guard let dataObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
            if topic.range(of:AppConstants.MQTT.messagesTopicName) != nil {
                MQTTChatManager.sharedInstance.getNewMessage(withData: dataObj, in: topic)
            }
            else if topic.range(of:AppConstants.MQTT.acknowledgementTopicName) != nil {
                MQTTChatManager.sharedInstance.getNewAcknowladgment(withData: dataObj, in: topic)
            }
            else if topic.range(of:AppConstants.MQTT.onlineStatus) != nil {
                MQTTChatManager.sharedInstance.gotOnlineStatus(withData: dataObj, withTopic:topic)
            }
            else if topic.range(of:AppConstants.MQTT.typing) != nil {
                MQTTChatManager.sharedInstance.gotTypingStatus(withData: dataObj, withTopic:topic)
            }
            else if topic.range(of: AppConstants.MQTT.getChats) != nil {
                MQTTChatManager.sharedInstance.gotChats(withData: dataObj, inTopic: topic)
            }
            else if topic.range(of: AppConstants.MQTT.getMessages) != nil {
                MQTTChatManager.sharedInstance.gotMessages(withData: dataObj, inTopic: topic)
            }
            else if topic.range(of: AppConstants.MQTT.messageOffer) != nil {
                MQTTChatManager.sharedInstance.gotInitiatedChat(withData: dataObj, inTopic: topic)
            }
        } catch let jsonError {
            print("Error !!!",jsonError)
        }
    }
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didDeliverMessage msgID: UInt16) {
        print("Message delivered")
    }
    
    fileprivate func getUserID() -> String? {
        guard let mqttID = Helper.getMQTTID() else { return nil }
        if mqttID == "mqttId" {
            return nil
        }
        return mqttID
    }
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didChange newState: MQTTSessionManagerState) {
        
        switch newState {
        case .connected:
            print("connected now")
            guard let userID = Helper.getMQTTID() else { return }
            let mqtt = MQTT.sharedInstance
            mqtt.isConnected = true
            let msgTopic = AppConstants.MQTT.messagesTopicName+userID
            mqtt.subscribeTopic(withTopicName: msgTopic, withDelivering: .atLeastOnce)
            
            //Firebase connection establishment
          //   FIRMessaging.messaging().subscribe(toTopic: userID)
            
            ///Subscribing Acknowledgment Channel
            let ackTopic = AppConstants.MQTT.acknowledgementTopicName+userID
            mqtt.subscribeTopic(withTopicName: ackTopic, withDelivering: .exactlyOnce)
            
            // Subscribing Message Offer Channel
            let messageOfferTopic = AppConstants.MQTT.messageOffer+userID
            mqtt.subscribeTopic(withTopicName: messageOfferTopic, withDelivering: .atLeastOnce)
            
            
            let state = UIApplication.shared.applicationState
            if state == .active{
                MQTTChatManager.sharedInstance.sendOnlineStatus(withOfflineStatus: false)
            } else if state == .background {
                MQTTChatManager.sharedInstance.sendOnlineStatus(withOfflineStatus: true)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                guard let userID = self.getUserID() else { return }
                sessionManager.session.persistence.deleteAllFlows(forClientId: userID)
            }
        case .closed:
            print("connection is closed")
            let mqtt = MQTT.sharedInstance
            mqtt.isConnected = false
        case .closing:
            print("connection is closing")
        case.connecting:
            print("trying to connect")
        case .error:
            print("got error on connection")
        case .starting:
            print("Starting the connection")
        }
    }
}

//
//  CouchbaseManager.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 01/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import UserNotifications

class CouchbaseManager: NSObject {
    
    static let sharedInstance = CouchbaseManager()
    static var selfID :String? {
        return self.getUserID()
    }
    
    fileprivate let mqttChatManager = MQTTChatManager.sharedInstance
    
    class func createInitialCouchBase(withUserSignedIn isUserSignedIn  : Bool ) -> String? {
        var userImage = ""
        var userName = ""
        let couchbase = Couchbase.sharedInstance
        let userDocVMObject = UsersDocumentViewModel(couchbase: couchbase)
        let indexDocVMObject = IndexDocumentViewModel(couchbase: couchbase)
        let individualChatDocVMObject = IndividualChatViewModel(couchbase: couchbase)
        if let usrName = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userName) as? String {
            userName = usrName
        }
        if let usrImage = UserDefaults.standard.value(forKey: AppConstants.UserDefaults.userImage) as? String {
            userImage = usrImage
        }
        guard let chatsDocID = individualChatDocVMObject.createIndividualChatsDocument(withRecieverUIDArray: [], recieverDocIDArray: [], secretIDArray: [] ) else { return nil }
        let unsentMessageDocVMObject = UnsentMessageViewModel(couchbase: couchbase)
        guard let unsentMessagesDocumentID = unsentMessageDocVMObject.createUnsentMessagesDocumentID() else { return nil }
        guard let userID = self.selfID else { return nil }
        guard let userDocID = userDocVMObject.createUserDocument(withUserID: userID, recieverIdentifier: "", userName: userName, userImage: userImage, apiToken: "", userLoginType: 0, chatDocument: chatsDocID, unsentMessagesDocument: unsentMessagesDocumentID, mqttTokenDocument: "", callsDocument: "", contactsDocument: "" ) else { return nil }
        
        guard let indexDocID = indexDocVMObject.createIndexDocument(withUserIDs: [userID], userDocIDs: [userDocID], isUserSignedIn: isUserSignedIn, userID: userID) else { return nil }
        return indexDocID
    }
    
    fileprivate class func getUserID() -> String? {
        guard let mqttID = Helper.getMQTTID() else { return nil }
        if mqttID == "mqttId" {
            return nil
        }
        return mqttID
    }
    
    /// Used for showing local push notification
    ///
    /// - Parameter data: current message data
    func showNotificationAlert(withMsg data: Any) {
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            let center = UNUserNotificationCenter.current()
            center.removeAllPendingNotificationRequests()
            guard let msgData = data as? [String:Any] else { return }
            guard let msg = msgData["payload"] as? String else { return }
            if let name = msgData["name"] {
                content.title = "\(name)"
                
            } else if let name = msgData["receiverIdentifier"] {
                content.title = "\(name)"
            }
            if let type = msgData["type"] as? String {
                switch type {
                case "0": // for text messages.
                    content.body = "\(msg.fromBase64()!)"
                case "1" : //
                    content.body = "You Got an image"
                case "2" :
                    content.body = "You Got an image"
                case "3" :
                    content.body = "You Got an image"
                case "4" :
                    content.body = "You Got an image"
                case "5" :
                    content.body = "You Got an image"
                case "6" :
                    content.body = "You Got an image"
                case "7" :
                    content.body = "You Got an image"
                case "8" :
                    content.body = "You Got an image"
                case "9" :
                    content.body = "You Got an image"
                default:
                    content.body = "You Got an image"
                }
            }
            content.sound = UNNotificationSound.default
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
            let identifier = "MQTTLocalNotification"
            let request = UNNotificationRequest(identifier: identifier,
                                                content: content, trigger: trigger)
            center.add(request, withCompletionHandler: { (error) in
                if let error = error {
                    // Something went wrong
                    print(error.localizedDescription)
                }
            })
        } else {
            // Fallback on earlier versions
        }
    }
}

//
//  AFWrapper.swift
//  Sales Paddock
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

@objc class AFWrapper: NSObject {
    
    
    class func requestPOSTURL(serviceName : String,withHeader header : [String : String], params : [String : Any]?, success:@escaping (JSON) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            return
        }
        
        Alamofire.request(serviceName, method:.post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: {
            response in
            print(response)
            if response.result.isSuccess
            {
                let resJson = JSON(response.result.value!)
                var statusCode = 0
                if let statsCode = resJson["code"].int
                {
                    statusCode = statsCode
                } else if let statsCode = resJson["statusCode"].int
                {
                    statusCode = statsCode
                }
                switch statusCode
                {
                case 137 :
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: "You have entered same phone number 3 or more times. Please try a new number", code: statusCode)
                    failure(error)
                case 138 :
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                case 139 :
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: "You have tried three or more failed attempts.", code: statusCode)
                    failure(error)
                case 140 :
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: "Abuse of device", code: statusCode)
                    failure(error)
                case 200..<204:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                case 204:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 409 :
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: "conflict, product sold already", code: statusCode)
                    failure(error)
                    
                case 205..<300:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                    
                case 300..<408:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    case 410..<440:
                                       let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                                       failure(error)
                    
                case 440:
                    print("session Expired")
                    
                default:
                    print("default code \(statusCode)")
                }
            }
            if response.result.isFailure {
                guard let error = response.result.error else { return }
                let customError = CustomErrorStruct(localizedTitle: "Error", localizedDescription: error.localizedDescription, code: 500)
                failure(customError)
            }
        })
    }
    
    class func requestGETURL(serviceName : String,withHeader header : [String:String], success:@escaping (JSON) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            return
        }
        let strURL = AppConstants.constantURL + serviceName
        Alamofire.request(strURL, method:.get, parameters:nil, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: {
            response in
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                var statusCode = 0
                if let statsCode = resJson["code"].int{
                    statusCode = statsCode
                } else if let statsCode = resJson["statusCode"].int{
                    statusCode = statsCode
                }
                switch statusCode
                {
                case 200..<204:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                case 204:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 205..<300:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                    
                case 300..<440:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 440:
                    print("session Expired")
                    
                default:
                    print("default code \(statusCode)")
                }
            }
            if response.result.isFailure {
                guard let error = response.result.error else { return }
                let customError = CustomErrorStruct(localizedTitle: "Error", localizedDescription: error.localizedDescription, code: 500)
                failure(customError)
            }
        })
    }
    
    class func requestDeleteURL(serviceName : String, withHeader header : [String:String], success:@escaping (JSON) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            return
        }
        let strURL = AppConstants.constantURL + serviceName
        Alamofire.request(strURL, method:.delete, parameters:nil, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: {
            response in
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                var statusCode = 0
                if let statsCode = resJson["code"].int{
                    statusCode = statsCode
                } else if let statsCode = resJson["statusCode"].int{
                    statusCode = statsCode
                }
                switch statusCode
                {
                case 200..<204:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                case 204:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 205..<300:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                    
                case 300..<440:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 440:
                    print("session Expired")
                    
                default:
                    print("default code \(statusCode)")
                }
            }
            if response.result.isFailure {
                guard let error = response.result.error else { return }
                let customError = CustomErrorStruct(localizedTitle: "Error", localizedDescription: error.localizedDescription, code: 500)
                failure(customError)
            }
        })
    }
    
    class func updloadPhoto(withPhoto photo: UIImage, andName name : String,success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        let strURL = AppConstants.uploadProfilePic
        AFWrapper.updatePhotoByUsingMultipart(serviceName: strURL, image: photo, andName:name, success: { (response) in
            success(response)
        }, failure: { (error) in
            failure(error)
        })
    }
    
    /// Uploads photo passed with the path where you wanted to store the image, with the image and its name. The image is going to be uploaded by multiparts. Encodes multipartFormData using encodingMemoryThreshold with the default SessionManager and calls encodingCompletion with new UploadRequest using the url, method and headers.
    ///
    /// - Parameters:
    ///   - serviceName: Path where the image is going to be uploaded.
    ///   - image: image which is going to be uploaded.
    ///   - name: photo name
    ///   - success: return here if it got succeed.
    ///   - failure: return here if request got failed.
    class func updatePhotoByUsingMultipart(serviceName : String, image:UIImage, andName name :String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            return
        }
        let strURL = AppConstants.uploadMultimediaURL + serviceName
        print(strURL)
        let imageData = image.jpegData(compressionQuality: 0.4)
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(imageData!, withName: "photo", fileName: "\(name).jpeg", mimeType: "image/*|video/*|audio/*")
        },
                         usingThreshold:UInt64.init(),
                         to:strURL,
                         method:.post,
                         headers:nil,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseString(completionHandler: { response in
                                    if let error = response.error {
                                        failure(error)
                                    }
                                    print(JSON(response))
                                    let resJson = JSON(response.result.value!)
                                    success(resJson)
                                })
                            case .failure(let encodingError):
                                print("an error :", encodingError)
                            }
        })
    }

    class func updateVideoByUsingMultipart(serviceName : String, videoURL:NSURL, success:@escaping (JSON) -> Void, failure:@escaping (NSError) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            return
        }
        let strURL = AppConstants.constantURL + serviceName
        let mediaURL = videoURL as URL
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(mediaURL, withName: "photo", fileName: "video123.mp4", mimeType: "image/*|video/*|audio/*")
        },
                         usingThreshold:UInt64.init(),
                         to:strURL,
                         method:.post,
                         headers:nil,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseString(completionHandler: { response in
                                    print(response)
                                    //                                    print("success", response.result.value!)
                                })
                            case .failure(let encodingError):
                                print("en error :", encodingError)
                            }
        })
    }
    
    class func requestPutURL(serviceName : String,withParameters params : [String:Any], header : [String : String], success:@escaping (JSON) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        if !Rechability.isConnectedToNetwork() {
            return
        }
        Alamofire.request(serviceName, method:.put, parameters:params, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: {
            response in
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                var statusCode = 0
                if let statsCode = resJson["code"].int{
                    statusCode = statsCode
                } else if let statsCode = resJson["statusCode"].int{
                    statusCode = statsCode
                }
                switch statusCode
                {
                case 200..<204:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                case 204:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 205..<300:
                    let resJson = JSON(response.result.value!)
                    success(resJson)
                    
                case 300..<440:
                    let error = CustomErrorStruct(localizedTitle: "Error", localizedDescription: resJson["message"].stringValue, code: statusCode)
                    failure(error)
                    
                case 440:
                    print("session Expired")
                    
                default:
                    print("default code \(statusCode)")
                }
            }
            if response.result.isFailure {
                guard let error = response.result.error else { return }
                let customError = CustomErrorStruct(localizedTitle: "Error", localizedDescription: error.localizedDescription, code: 500)
                failure(customError)
            }
        })
    }
}

//
//  API.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 25/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

public class API : NSObject{
    
    //    typedef void (^completionBlockType) ([String : Any]?)
    
    
    typealias completionBlockType = ([String : Any]?) ->Void
    
    func initiatedChat(withdata data : [String : Any], completionBlock: @escaping completionBlockType) {
        let strURL = iPhoneBaseURL+"makeOffer"
        let header = (authUsername + ":" + authPassword).toBase64()
        let headerParams = ["authorization":"Basic \(header)"]
        AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
            guard let responseData = response.dictionaryObject else { return }
            completionBlock(responseData)
        },
                                 failure: { (customErrorStruct) in
                                    print("Error",customErrorStruct)
                                    completionBlock(nil)
        })
    }
    
    
    typealias completionBlock = (_ code :NSInteger,[String : Any]?) ->Void
    func initiatedSwapOffer(withdata data : [String : Any], completionBlock: @escaping completionBlock) {
        let strURL = iPhoneBaseURL+"swapOffer"
        let header = (authUsername + ":" + authPassword).toBase64()
        let headerParams = ["authorization":"Basic \(header)"]
        AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
            guard let responseData = response.dictionaryObject else { return }
            completionBlock(200,responseData)
        },
                                 failure: { (customErrorStruct) in
                                    print("Error",customErrorStruct)
                                    completionBlock(customErrorStruct.code,nil)
        })
    }
    
    
    func AcceptSwapOffer(withdata data : [String : Any], completionBlock: @escaping completionBlockType) {
        let strURL = iPhoneBaseURL+"makeExchangePost"
        let header = (authUsername + ":" + authPassword).toBase64()
        let headerParams = ["authorization":"Basic \(header)"]
        AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
            guard let responseData = response.dictionaryObject else { return }
            completionBlock(responseData)
        },
                                 failure: { (customErrorStruct) in
                                    print("Error",customErrorStruct)
                                    completionBlock(nil)
        })
    }
    
    
    func AddCard(withdata data : [String : Any], success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)
    {
        let strURL = iPhoneBaseURL+"add_card"
        let header = (authUsername + ":" + authPassword).toBase64()
        let headerParams = ["authorization":"Basic \(header)"]
        AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
            guard let responseData = response.dictionaryObject else { return }
            success(responseData)
        },
                                 failure: { (customErrorStruct) in
                                    print("Error",customErrorStruct)
                                  failure(customErrorStruct)
        })
    }
    func GetCard(withdata data : [String : Any], success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)
    {
           let strURL = iPhoneBaseURL+"get_added_card"
           let header = (authUsername + ":" + authPassword).toBase64()
           let headerParams = ["authorization":"Basic \(header)"]
           AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
               guard let responseData = response.dictionaryObject else { return }
               success(responseData)
           },
                                    failure: { (customErrorStruct) in
                                       print("Error",customErrorStruct)
                                      failure(customErrorStruct)
           })
       }
    
    
    func AddAddress(withdata data : [String : Any], completionBlock: @escaping completionBlockType)
    {
           let strURL = iPhoneBaseURL+"add_shipping_address"
           let header = (authUsername + ":" + authPassword).toBase64()
           let headerParams = ["authorization":"Basic \(header)"]
           AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
               guard let responseData = response.dictionaryObject else { return }
               completionBlock(responseData)
           },
                                    failure: { (customErrorStruct) in
                                       print("Error",customErrorStruct)
                                       completionBlock(nil)
           })
       }
    func MakePayment(withdata data : [String : Any], success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)
    {
           let strURL = iPhoneBaseURL+"make_payment"
           let header = (authUsername + ":" + authPassword).toBase64()
           let headerParams = ["authorization":"Basic \(header)"]
           AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
               guard let responseData = response.dictionaryObject else { return }
                    success(responseData)
           },
                                    failure: { (customErrorStruct) in
                                       print("Error",customErrorStruct)
                                      failure(customErrorStruct)
           })
       }
    
    func UpdateAddress(withdata data : [String : Any], success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)
       {
              let strURL = iPhoneBaseURL+"update_shipping_address"
              let header = (authUsername + ":" + authPassword).toBase64()
              let headerParams = ["authorization":"Basic \(header)"]
              AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
                  guard let responseData = response.dictionaryObject else { return }
                  success(responseData)
              },
                                       failure: { (customErrorStruct) in
                                          print("Error",customErrorStruct)
                                           failure(customErrorStruct)
              })
          }
    

      func GetAddress(withdata data : [String : Any], success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)
       {
              let strURL = iPhoneBaseURL+"get_shipping_address"
              let header = (authUsername + ":" + authPassword).toBase64()
              let headerParams = ["authorization":"Basic \(header)"]
              AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
                  guard let responseData = response.dictionaryObject else { return }
                  success(responseData)
              },
                                       failure: { (customErrorStruct) in
                                          print("Error",customErrorStruct)
                                           failure(customErrorStruct)
              })
          }
    
    func AppleLogin(withdata data : [String : Any], success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)
          {
                 let strURL = iPhoneBaseURL+"login"
                 let header = (authUsername + ":" + authPassword).toBase64()
                 let headerParams = ["authorization":"Basic \(header)"]
                 AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
                     guard let responseData = response.dictionaryObject else { return }
                     success(responseData)
                 },
                                          failure: { (customErrorStruct) in
                                             print("Error",customErrorStruct)
                                              failure(customErrorStruct)
                 })
             }
    
    func PostInAppPurchase(withdata data : [String : Any], success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)
    {
           let strURL = iPhoneBaseURL+"inAppPurchase"
           let header = (authUsername + ":" + authPassword).toBase64()
           let headerParams = ["authorization":"Basic \(header)"]
           AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
               guard let responseData = response.dictionaryObject else { return }
               success(responseData)
           },
                                    failure: { (customErrorStruct) in
                                       print("Error",customErrorStruct)
                                        failure(customErrorStruct)
           })
       }
    
    
    func GetPurchase(withdata data : [String : Any], success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)
          {
                 let strURL = iPhoneBaseURL+"purchased_product_list"
                 let header = (authUsername + ":" + authPassword).toBase64()
                 let headerParams = ["authorization":"Basic \(header)"]
                 AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
                     guard let responseData = response.dictionaryObject else { return }
                     success(responseData)
                 },
                                          failure: { (customErrorStruct) in
                                             print("Error",customErrorStruct)
                                              failure(customErrorStruct)
                 })
             }
    
      func DeleteCard(withdata data : [String : Any], success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)

       {
              let strURL = iPhoneBaseURL+"delete_card"
              let header = (authUsername + ":" + authPassword).toBase64()
              let headerParams = ["authorization":"Basic \(header)"]
              AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
                  guard let responseData = response.dictionaryObject else { return }
                  success(responseData)
              },
                                       failure: { (customErrorStruct) in
                                          print("Error",customErrorStruct)
                                           failure(customErrorStruct)
              })
          }
    
    func MarkDefaultCard(withdata data : [String : Any], success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)

         {
                let strURL = iPhoneBaseURL+"set_card_as_default"
                let header = (authUsername + ":" + authPassword).toBase64()
                let headerParams = ["authorization":"Basic \(header)"]
                AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
                    guard let responseData = response.dictionaryObject else { return }
                    success(responseData)
                },
                                         failure: { (customErrorStruct) in
                                            print("Error",customErrorStruct)
                                             failure(customErrorStruct)
                })
            }
    
    
    func ConnectStripe(withdata data : [String : Any], success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)
          {
                 let strURL = iPhoneBaseURL+"stripe_connect"
                 let header = (authUsername + ":" + authPassword).toBase64()
                 let headerParams = ["authorization":"Basic \(header)"]
                 AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
                     guard let responseData = response.dictionaryObject else { return }
                     success(responseData)
                 },
                                          failure: { (customErrorStruct) in
                                             print("Error",customErrorStruct)
                                              failure(customErrorStruct)
                 })
             }
    
    func DisconnectStripe(withdata data : [String : Any], success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)
             {
                    let strURL = iPhoneBaseURL+"stripe_disconnect"
                    let header = (authUsername + ":" + authPassword).toBase64()
                    let headerParams = ["authorization":"Basic \(header)"]
                    AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: data, success: { (response) in
                        guard let responseData = response.dictionaryObject else { return }
                        success(responseData)
                    },
                                             failure: { (customErrorStruct) in
                                                print("Error",customErrorStruct)
                                                 failure(customErrorStruct)
                    })
                }
    
    func getPostDetailsByID(withID productID: String?, success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void) {
        let strURL = iPhoneBaseURL+"getPostsById/users"
        let header = (authUsername + ":" + authPassword).toBase64()
        let headerParams = ["authorization":"Basic \(header)"]
        guard let token = Helper.userToken() else { return }
        if token.count>1 {
            
            let params = ["token":token as Any,
                          "postId":productID as Any] as [String : Any]
            
            AFWrapper.requestPOSTURL(serviceName: strURL, withHeader: headerParams, params: params, success: { (response) in
                guard let responseData = response.dictionaryObject else { return }
                guard let dataArray = responseData["data"] as? [[String : Any]] else { return }
                success(dataArray[0])
            },
                                     failure: { (customErrorStruct) in
                                        print("Error",customErrorStruct)
                                        failure(customErrorStruct)
            })
        }
    }
    
    func getChats(withPageNo pageNo : String) {
        let strURL = AppConstants.getChats+"/0"
        guard let token = Helper.userToken() else { return }
        if token.count>1 {
            let headerParams = ["authorization":mAuthorization,
                                "token":token]
            
            AFWrapper.requestGETURL(serviceName: strURL, withHeader: headerParams,
                                    success: { (response) in
                                        guard let responseData = response.dictionaryObject else { return }
                                        print("Response",responseData)
            },
                                    failure: { (customErrorStruct) in
                                        print("Error",customErrorStruct)
            })
        }
    }
    
    func deleteChats(withSecretID secretID: String?, andRecipientID recipientID : String?, success:@escaping ([String : Any]) -> Void, failure:@escaping (CustomErrorStruct) -> Void)  {
        guard let secretID = secretID, let recipientID = recipientID else { return }
        let strURL = AppConstants.getChats+"/"+recipientID+"/"+secretID
        guard let token = Helper.userToken() else { return }
        if token.count>1 {
            let headerParams = ["authorization":mAuthorization,
                                "token":token]
            AFWrapper.requestDeleteURL(serviceName: strURL, withHeader: headerParams,
                                       success: { (response) in
                                        guard let responseData = response.dictionaryObject else { return }
                                        print("Response",responseData)
                                        success(responseData)
            },
                                       failure: { (customErrorStruct) in
                                        print("Error",customErrorStruct)
                                        failure(customErrorStruct)
            })
        }
    }
    
    func getMessages(withUrl messageUrl : String) {
        guard let token = Helper.userToken() else { return }
        if token.count>1 {
            let headerParams = ["authorization":mAuthorization,
                                "token":token]
            AFWrapper.requestGETURL(serviceName: messageUrl, withHeader: headerParams,
                                    success: { (response) in
                                        guard let responseData = response.dictionaryObject else { return }
                                        
                                        print("Response",responseData)
            },
                                    failure: { (customErrorStruct) in
                                        print("Error",customErrorStruct)
            })
        }
    }
    
    func sendNotification(withText text: String, andTitle title: String, toTopic topic: String, andData data:[String : String]) {
        let url = "https://fcm.googleapis.com/fcm/send"
        let header = ["Authorization":mFCMLegacyKey]
        
        guard let receiverID = Helper.getMQTTID(), let secretID = data["secretID"] else { return }
        
        let userID = ["receiverID":receiverID,
                      "secretID":secretID] as [String : String]
        
        let notificationParams = [ "body" : "\(text)",
            "title" : "\(title)",
            "sound": "default"]
        let notificationData = ["body":userID]
        let params : [String : Any] = ["condition": "'\(topic)' in topics",
            "priority" : "high",
            "notification" : notificationParams,
            "data":notificationData
            ] as [String : Any]
        
        AFWrapper.requestPOSTURL(serviceName: url, withHeader: header, params: params, success:{ (response) in
            guard let responseData = response.dictionaryObject else { return }
            print("Response",responseData)
        },
                                 failure: { (customErrorStruct) in
                                    print("Error",customErrorStruct)
        })
    }
}


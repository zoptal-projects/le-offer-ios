//
//  SavedAddressModel.swift
//  Cellable
//
//  Created by Anish Mac Mini on 06/06/20.
//  Copyright © 2020 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class SavedAddressModel: NSObject , NSCoding
{
var savedCardCardBrand               = String()
var savedCardCardId                  = String()
var savedCardCardNumber              = String()
var savedCardCardType                = String()
var savedCardCountry                 = String()
var savedCardCreatedAt               = String()
var savedCardCustomerId              = String()
var savedCardCvv                     = String()
var savedCardExpiry                  = String()
var savedCardIsDefault               = String()
var savedCardName                    = String()
var savedCardRole                    = String()
var savedCardStatus                  = String()
var savedCardToken                   = String()
var savedCardUpdatedAt               = String()
var savedCardUserId                  = String()
var savedCardId                      = String()


override init() {}

init(cardData:JSON) {
    self.savedCardCardBrand     = cardData[ "cardBrand"     ].stringValue
    self.savedCardCardId        = cardData[ "cardId"        ].stringValue
    self.savedCardCardNumber    = cardData[ "cardNumber"    ].stringValue
    self.savedCardCardType      = cardData[ "cardType"      ].stringValue
    self.savedCardCountry       = cardData[ "country"       ].stringValue
    self.savedCardCreatedAt     = cardData[ "createdAt"     ].stringValue
    self.savedCardCustomerId    = cardData[ "customerId"    ].stringValue
    self.savedCardCvv           = cardData[ "cvv"           ].stringValue
    self.savedCardExpiry        = cardData[ "expiry"        ].stringValue
    self.savedCardIsDefault     = cardData[ "isDefault"     ].stringValue
    self.savedCardName          = cardData[ "name"          ].stringValue
    self.savedCardRole          = cardData[ "role"          ].stringValue
    self.savedCardStatus        = cardData[ "status"        ].stringValue
    self.savedCardToken         = cardData[ "token"         ].stringValue
    self.savedCardUpdatedAt     = cardData[ "updatedAt"     ].stringValue
    self.savedCardUserId        = cardData[ "userId"        ].stringValue
    self.savedCardId            = cardData[ "_id"           ].stringValue
    
}

// MARK: NSCoding Protocol
required public init(coder aDecoder: NSCoder) {
    
    self.savedCardCardBrand      = aDecoder.decodeObject(forKey: "cardBrand" ) as! String
    self.savedCardCardId         = aDecoder.decodeObject(forKey: "cardId"    ) as! String
    self.savedCardCardNumber     = aDecoder.decodeObject(forKey: "cardNumber") as! String
    self.savedCardCardType       = aDecoder.decodeObject(forKey: "cardType"  ) as! String
    self.savedCardCountry        = aDecoder.decodeObject(forKey: "country"   ) as! String
    self.savedCardCreatedAt      = aDecoder.decodeObject(forKey: "createdAt" ) as! String
    self.savedCardCustomerId     = aDecoder.decodeObject(forKey: "customerId") as! String
    self.savedCardCvv            = aDecoder.decodeObject(forKey: "cvv"       ) as! String
    self.savedCardExpiry         = aDecoder.decodeObject(forKey: "expiry"    ) as! String
    self.savedCardIsDefault      = aDecoder.decodeObject(forKey: "isDefault" ) as! String
    self.savedCardName           = aDecoder.decodeObject(forKey: "name"      ) as! String
    self.savedCardRole           = aDecoder.decodeObject(forKey: "role"      ) as! String
    self.savedCardStatus         = aDecoder.decodeObject(forKey: "status"    ) as! String
    self.savedCardToken          = aDecoder.decodeObject(forKey: "token"     ) as! String
    self.savedCardUpdatedAt      = aDecoder.decodeObject(forKey: "updatedAt" ) as! String
    self.savedCardUserId         = aDecoder.decodeObject(forKey: "userId"    ) as! String
    self.savedCardId             = aDecoder.decodeObject(forKey: "_id"       ) as! String
}

public func encode(with aCoder: NSCoder) {
    aCoder.encode(self.savedCardCardBrand             ,  forKey: "cardBrand"  )
    aCoder.encode(self.savedCardCardId                ,  forKey: "cardId"     )
    aCoder.encode(self.savedCardCardNumber            ,  forKey: "cardNumber" )
    aCoder.encode(self.savedCardCardType              ,  forKey: "cardType"   )
    aCoder.encode(self.savedCardCountry               ,  forKey: "country"    )
    aCoder.encode(self.savedCardCreatedAt             ,  forKey: "createdAt"  )
    aCoder.encode(self.savedCardCustomerId            ,  forKey: "customerId" )
    aCoder.encode(self.savedCardCvv                   ,  forKey: "cvv"        )
    aCoder.encode(self.savedCardExpiry                ,  forKey: "expiry"     )
    aCoder.encode(self.savedCardIsDefault             ,  forKey: "isDefault"  )
    aCoder.encode(self.savedCardName                  ,  forKey: "name"       )
    aCoder.encode(self.savedCardRole                  ,  forKey: "role"       )
    aCoder.encode(self.savedCardStatus                ,  forKey: "status"     )
    aCoder.encode(self.savedCardToken                 ,  forKey: "token"      )
    aCoder.encode(self.savedCardUpdatedAt             ,  forKey: "updatedAt"  )
    aCoder.encode(self.savedCardUserId                ,  forKey: "userId"     )
    aCoder.encode(self.savedCardId                    ,  forKey: "_id"        )
}
}

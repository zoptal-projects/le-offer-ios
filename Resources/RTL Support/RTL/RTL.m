//
//  RTL.m
//  Picogram
//
//  Created by Rahul Sharma on 06/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "RTL.h"

@implementation RTL

static RTL *instance = nil;

+(id)sharedInstance
{
        if(instance == nil)
        {
            instance= [RTL new];
     
        }
    return instance;
}


-(void)initWithData :(BOOL) value
{
    self.isRTL = value ;
}
@end

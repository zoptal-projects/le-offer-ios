//
//  UIButton+FollowButtonCustomization.m

//
//  Created by Rahul_Sharma on 18/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "UIButton+FollowButtonCustomization.h"
#import "Helper.h"


@implementation UIButton (FollowButtonCustomization)
-(void)makeButtonAsFollow {
    self .layer.cornerRadius = 3;
    self .layer.borderWidth = 1;
    [self  setTitle:NSLocalizedString(followButtonTitle, followButtonTitle) forState:UIControlStateNormal];
    [self setTitleColor:followButtonBackGroundColor forState:UIControlStateNormal];
    self.backgroundColor = mOurThemeBaseColour;
    [self setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    [self setImage:[UIImage imageNamed:imageNameForFollowButton] forState:UIControlStateNormal];
   // self .layer.borderColor = [UIColor colorWithRed:0.1786 green:0.5036 blue:0.925 alpha:1.0].CGColor;
    self .layer.borderColor = followButtonBackGroundColor.CGColor;
}

-(void)makeButtonAsFollowing {
    self .layer.cornerRadius = 3;
    self .layer.borderWidth = 1;
    [self  setTitle:NSLocalizedString(followingButtonTitle, followingButtonTitle) forState:UIControlStateNormal];
    [self setTitleColor:followButtonBackGroundColor forState:UIControlStateNormal];
//    [self setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    self.backgroundColor = mOurThemeBaseColour;
    self.layer.borderColor = followButtonBackGroundColor.CGColor;
}

-(void)makeButtonAsRequested {
    self .layer.cornerRadius = 3;
    self .layer.borderWidth = 1;
    [self setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self  setTitle:textForRequestedButton  forState:UIControlStateNormal];
    [self setTitleColor:requstedButtonTextColor forState:UIControlStateNormal];
  //  [self setImage:[UIImage imageNamed:imageNameForRequestedButton] forState:UIControlStateNormal];
    self.backgroundColor = requstedButtonBackGroundColor;
    self .layer.borderColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0].CGColor;
}


-(void)makeimageFollowingButton {
    [self setImage:[UIImage imageNamed:imageForFollowingButton] forState:UIControlStateNormal];
}

-(void)makeimageFollowButton {
    [self setImage:[UIImage imageNamed:imageForFollowButton] forState:UIControlStateNormal];
}

-(void)makeimageRequestedButton {
    [self setImage:[UIImage imageNamed:imageForRequestedButton] forState:UIControlStateNormal];
}

@end

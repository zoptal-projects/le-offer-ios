//
//  FilterFieldsViewController.m
//  CollegeStax
//
//  Created by 3Embed on 18/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "FilterFieldsViewController.h"
#import "SubCategoryTableViewCell.h"
#import "ValueViewController.h"
#import "PostListingsViewController.h"

@interface FilterFieldsViewController ()


@end

@implementation FilterFieldsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *navBackTitle = [NSString stringWithFormat:@"%@ -> %@",self.appendNavTitle, self.subCategory];
    [self.backButtonOutlet setTitle:navBackTitle forState:UIControlStateNormal];
    
    if(self.filterKeyValueDict.count)
    {
     self.doneButtonOutlet.enabled = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.filter.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SubCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SubcategoryFilterCell" forIndexPath:indexPath];
    
    [cell setFieldsForFilter:self.filter[indexPath.row] previousSelection:self.filterKeyValueDict];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubCategoryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    ValueViewController  *valueVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoryValueStoryboard"];
        Filter *subFilter = self.filter[indexPath.row];
        valueVC.filter = subFilter;
    valueVC.previousValue = cell.valueLabel.text;
    valueVC.valueCallBack = ^(NSString *value,NSUInteger intValue) {
        
        if(self.filterKeyValueDict[cell.titleLabel.text]){
        [self.filterKeyValueDict removeObjectForKey:cell.titleLabel.text];
        }
        
        if(intValue){
        [self.filterKeyValueDict setValue:[NSNumber numberWithInteger:intValue] forKey:cell.titleLabel.text];
            cell.valueLabel.text = [NSString stringWithFormat:@"%ld",intValue];
        }
        else
        {
        [self.filterKeyValueDict setValue:value forKey:cell.titleLabel.text];
            cell.valueLabel.text = value;
       
        }
   
        
        self.doneButtonOutlet.enabled = YES;
        self.doneButtonOutlet.backgroundColor = mOurThemeBaseColour;
        //[self.tableViewOutlet reloadData];
    };
    
        [self.navigationController pushViewController:valueVC animated:YES];
}


- (IBAction)donebuttonAction:(id)sender {
    
    for (UIViewController *viewController in [self.navigationController viewControllers]) {
        if([viewController isKindOfClass:[PostListingsViewController class]]){
            PostListingsViewController *newVC = (PostListingsViewController *)viewController;
            newVC.listings.filter = self.filterKeyValueDict ;
            newVC.listings.category = self.appendNavTitle ;
            newVC.listings.subCategory = self.subCategory;
            [newVC.tableViewForListings reloadData];
            [self.navigationController popToViewController:viewController animated:YES];
        }
    }
}

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end

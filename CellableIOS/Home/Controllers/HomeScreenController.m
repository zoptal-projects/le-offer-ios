
//  HomeScreenController.h
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//
#import "HomeScreenController.h"
#import "ListingCollectionViewCell.h"
#import "SVPullToRefresh.h"
#import "ProductDetailsViewController.h"
#import "CellForFilteredItem.h"
#import "FilterViewController.h"
#import "RFQuiltLayout.h"
#import "ActivityViewController.h"
#import "CameraViewController.h"
#import "SearchPostsViewController.h"
#import "ZoomInteractiveTransition.h"
#import "ZoomTransitionProtocol.h"
#import "StartBrowsingViewController.h"
#import "AskPermissionViewController.h"
#import <SDWebImage/SDWebImagePrefetcher.h>
#import "ProductDetails.h"
#import "CategoryCollectionViewCell.h"
#import "Category.h"
#import "SelectedFilters.h"
#import "NativeAdsCellCollectionViewCell.h"


@import Firebase;

@interface HomeScreenController ()<WebServiceHandlerDelegate,RFQuiltLayoutDelegate,GetCurrentLocationDelegate,ZoomTransitionProtocol , SDWebImageManagerDelegate , AskPermissionDelegate, CLLocationManagerDelegate ,ProductDetailsDelegate,UICollectionViewDataSourcePrefetching >
{
    NSArray *allCategories;
}


@end

@implementation HomeScreenController

/*--------------------------------------*/
#pragma mark
#pragma mark - ViewController LifCycle
/*--------------------------------------*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.transition = [[ZoomInteractiveTransition alloc] initWithNavigationController:self.navigationController];
    [CommonMethods setNegativeSpacingforNavigationItem:self.navigationItem andExtraBarItem:self.searchButtonOutlet];
    
    minPrice = @""; maxPrice = @""; postedWithin = @""; sortBy = @"";
    dist = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    self.leadingConstraint = 40;
    
  //  [self.collectionViewOutlet registerNib:[UINib nibWithNibName:@"NativeAdsCell" bundle: nil] forCellWithReuseIdentifier:@"nativeAdsCell"];
    
    // Create a native ad request with a unique placement ID (generate your own on the Facebook app settings).
    // Use different ID for each ad placement in your app.

    
    self.categoryFiltersViewHeight.constant = 0;
    self.filterListings = [[FilterListings alloc]init];
    arrayOfcategory = [[NSMutableArray alloc]init];
    productsArray = [[NSMutableArray alloc]init];
    selectedFilters = [[NSMutableArray alloc] init];
    self.categoryObj = [[Category alloc]init];
    [self addingActivityIndicatorToCollectionViewBackGround];
    [self RFQuiltLayoutIntialization];
    [self addingRefreshControl];
    [self notficationObservers];
    [self getCategoriesListFromServer];
    [self checkPermissionForAllowLocation];
    
    if([[Helper userToken] isEqualToString:mGuestToken]){
        NSDictionary *param = [CommonMethods updateDeviceDetailsForAdmin];
        [WebServiceHandler logGuestUserDevice:param andDelegate:self];
    }
    
    self.selectedFiltersView.hidden = YES ;
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.isScreenDidAppear = NO;
    self.sellStuffButtonView.hidden = NO;
    self.sellStuffButtonView.alpha = 0 ;
    self.bottomConstraintOfSellButton.constant = 60;
    NSDictionary *checkAdsCampaign  = [[NSUserDefaults standardUserDefaults] objectForKey:mAdsCampaignKey];
    if(checkAdsCampaign)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:mAdsCampaignKey];
        [[NSNotificationCenter defaultCenter] postNotificationName:mTriggerCampaign object:checkAdsCampaign];
    }
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"openActivity"]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"openActivity"];
        [self.tabBarController setSelectedIndex:0];
        ActivityViewController *activityVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    [self getnotificationCount];
    [self showWelcomeMessageForBrowsing];
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"recent_login"])
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"recent_login"];
        [self requesForProductListings];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"isInstagramSharing"])
    {
        [_dic presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isInstagramSharing"];
    }
    [UIView transitionWithView:self.sellStuffButtonView
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.sellStuffButtonView.alpha = 1.0 ;
                    }
                    completion:NULL];
    
    self.isScreenDidAppear = YES ;
    
}


/**
 Clear cache memory on warning.
 */
-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
}

/**
 Deallocate memory again.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:mDeletePostNotifiName];
    [[NSNotificationCenter defaultCenter] removeObserver:mSellingAgainNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:mUpdatePostDataNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:@"openActivityScreen"];
    [[NSNotificationCenter defaultCenter] removeObserver:mSellingPostNotifiName];
    [[NSNotificationCenter defaultCenter] removeObserver:mShowAdsCampaign];
}


-(void)openActivityScreen{
    [self openActivityScreenForpush];
    
}


#pragma mark -
#pragma mark - Notification Observer

-(void)notficationObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePostFromNotification:) name:mDeletePostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(sellingPostAgain:) name:mSellingAgainNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(UpdatePostOnEditing:) name:mUpdatePostDataNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshData:) name:mUpdatePromotedPost object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addNewPost:)name:mSellingPostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openActivityScreen)name:@"openActivityScreen" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postNotificationToAppdelegate:)name:mShowAdsCampaign object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ShareOnInstagram:)name:@"instagramShare" object:nil];
}



#pragma mark
#pragma mark - Location Permission & Delegate -

-(void)checkPermissionForAllowLocation
{
    BOOL locationServiceEnable ;
    if ([CLLocationManager locationServicesEnabled]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            locationServiceEnable = YES;
            AskPermissionViewController *askVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AskPermissionStoryboardId"];
            askVC.permissionDelegate = self;
            askVC.locationPermission = YES ;
            askVC.locationEnable = locationServiceEnable ;
            [self.navigationController presentViewController:askVC animated:NO completion:nil];
        }
        else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            locationServiceEnable = NO;
            AskPermissionViewController *askVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AskPermissionStoryboardId"];
            askVC.permissionDelegate = self;
            askVC.locationPermission = YES ;
            askVC.locationEnable = locationServiceEnable ;
            [self.navigationController presentViewController:askVC animated:NO completion:nil];
        }
        else {
            [self allowPermission:YES];
        }
    }
    else {
        AskPermissionViewController *askVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AskPermissionStoryboardId"];
        askVC.permissionDelegate = self;
        askVC.locationPermission = YES ;
        askVC.LocationPrivacy = YES ;
        [self.navigationController presentViewController:askVC animated:NO completion:nil];
    }
}


- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self allowPermission:YES];
        
    }
    else
    {
        [self allowPermission:NO];
    }
}

- (void)updatedLocation:(double)latitude and:(double)longitude
{
    self.currentLat = latitude ;
    self.currentLong = longitude ;
    self.filterListings.latitude = [NSString stringWithFormat:@"%lf",latitude];
    self.filterListings.longitude = [NSString stringWithFormat:@"%lf",longitude];
    if(self.flagForLocation)
    {
        self.flagForLocation  = NO;
        [self requesForProductListings];
    }
}

- (void)updatedAddress:(NSString *)currentAddress
{
    
    
}

#pragma mark -
#pragma mark - RFQuiltLayout Intialization -

/**
 This method will create an object for RFQuiltLayout.
 */
-(void)RFQuiltLayoutIntialization
{
    RFQuiltLayout* layout = [[RFQuiltLayout alloc]init];
    layout.direction = UICollectionViewScrollDirectionVertical;
    if(self.view.frame.size.width == 375)
    {
        layout.blockPixels = CGSizeMake( 37,31);
    }
    
    else if(self.view.frame.size.width == 414)
    {
        layout.blockPixels = CGSizeMake( 102,31);
    }
    else
    {
        layout.blockPixels = CGSizeMake( 79,31);
    }
    
    _collectionViewOutlet.collectionViewLayout = layout;
    layout.delegate=self;
    
}



#pragma mark
#pragma mark - Product Filter -

/**
 This is Delegate method of Filter VC.
 
 */
-(void)applyfilterForSelectedLists
{
    selectedFilters = [self.filterListings getFilterArray];
    
    [productsArray removeAllObjects];
    [self reloadCollectionView] ;
    [avForCollectionView startAnimating];
    
    if(selectedFilters.count>0){
        self.categoryObj = [[Category alloc]init];
        [self.collectionViewForCategory reloadData];
        self.collectionViewForCategory.hidden = YES ;
        self.selectedFiltersView.hidden = NO;
        self.categoryFiltersViewHeight.constant = 60;
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
    else{
        [self requesForProductListings];
        self.collectionViewForCategory.hidden = NO ;
        self.selectedFiltersView.hidden = YES ;
        self.categoryFiltersViewHeight.constant = 80;
    }
    [_collectionviewFilterItem reloadData];
    
    
}




#pragma mark -
#pragma mark - Apply Filters
/**
 Apply search on Products by applying selected Filters.
 */
-(void)applyFiltersOnProductsWithIndex :(NSUInteger)checkIndex
{
    if(self.locationName.length < 1)
    {
        self.filterListings.defaultLocation = getLocation.currentCity;
        self.filterListings.defaultLatitude = [NSString stringWithFormat:@"%lf",self.self.currentLat];
        self.filterListings.defaultLongitude = [NSString stringWithFormat:@"%lf",self.currentLong];
    }
    self.filterListings.pagingIndex = checkIndex;
    NSDictionary *requestDic = [self.filterListings getParamDictionary];
    [WebServiceHandler searchProductsByFilters:requestDic andDelegate:self];
}

#pragma mark -
#pragma mark - Remove Filters
/**
 Remove filter For Product.
 
 @param sender delete Filter button.
 */
- (IBAction)removeProductFilterButtonAcion:(id)sender {
    UIButton *btn=(UIButton *)sender;
    NSInteger tag= [btn tag]%100;
    
    if([selectedFilters[tag][@"typeOfFilter"] isEqualToString:@"category"])
    {
        [selectedFilters removeAllObjects];
        [self.filterListings resetFilterListings];
    }
    else if([selectedFilters[tag][@"typeOfFilter"] isEqualToString:@"subcategory"])
    {
        [self.filterListings.filtersDictionary removeObjectForKey:@"Subcategory"];
        [self.filterListings.subFilterKeyValue removeAllObjects];
        self.filterListings.subcategoryName = @"";
        self.filterListings.subcategory = nil;
        selectedFilters = [self.filterListings getFilterArray];
    }
    
    else if([selectedFilters[tag][@"typeOfFilter"] isEqualToString:@"subFilters"])
    {
        for (NSDictionary *dict in self.filterListings.subFilterKeyValue) {
            if([dict[@"value"] isEqualToString:selectedFilters[tag][@"value"]]){
                [self.filterListings.subFilterKeyValue removeObject:dict];
                break;
            }
        }
        [selectedFilters removeObjectAtIndex:tag];
    }
    
    else if([selectedFilters[tag][@"typeOfFilter"] isEqualToString:@"sortBy"])
    {
        [selectedFilters removeObjectAtIndex:tag];
        self.filterListings.sortBy = @"";
        
    }
    else if([selectedFilters[tag][@"typeOfFilter"] isEqualToString:@"postedWithin"])
    {
        [selectedFilters removeObjectAtIndex:tag];
        self.filterListings.postedWithin = @"";
    }
    
    else if([selectedFilters[tag][@"typeOfFilter"] isEqualToString:@"price"])
    {
        [selectedFilters removeObjectAtIndex:tag];
        self.filterListings.price = @"";
        self.filterListings.from = @"";
        self.filterListings.to = @"";
    }
    else if([selectedFilters[tag][@"typeOfFilter"] isEqualToString:@"distance"])
    {
        [selectedFilters removeObjectAtIndex:tag];
        self.filterListings.distanceFilter = @"";
        self.filterListings.distanceMax = @"";
        self.filterListings.leadingConstraint = 40;
    }
    else if([selectedFilters[tag][@"typeOfFilter"] isEqualToString:@"location"])
    {
        [selectedFilters removeObjectAtIndex:tag];
        self.filterListings.location = @"";
    }
    
    else{
        [selectedFilters removeObjectAtIndex:tag];
        [productsArray removeAllObjects];
        [self reloadCollectionView] ;
        [avForCollectionView startAnimating];
    }
    
    if (selectedFilters.count ==0) {
        [UIView animateWithDuration:0.4 animations:^{
            self.collectionViewForCategory.hidden = NO ;
            self.selectedFiltersView.hidden = YES ;
            self.categoryFiltersViewHeight.constant = 80;
            [self.view layoutIfNeeded];
        }];
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self requestForExplorePosts:self.currentIndex];
    }
    else{
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
    [self.collectionviewFilterItem reloadData];
    
}


#pragma mrk -
#pragma mark - Pull To refresh -

-(void)addingRefreshControl {
    refreshControl = [[UIRefreshControl alloc]init];
    [self.collectionViewOutlet addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
}
-(void)refreshData:(id)sender {
    newPostAdded = NO;
    [self getnotificationCount];
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    if(selectedFilters.count==0){
        [self allowPermission:YES];
    }
    else{
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
    
    if(!arrayOfcategory.count)
    {
        [self getCategoriesListFromServer];
    }
    
}
- (void)stopAnimation {
    __weak HomeScreenController *weakSelf = self;
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.collectionViewOutlet.pullToRefreshView stopAnimating];
        [weakSelf.collectionViewOutlet.infiniteScrollingView stopAnimating];
    });
}


#pragma mark -
#pragma mark - collectionview delegates -


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if(collectionView==_collectionviewFilterItem)
    {
        return selectedFilters.count;
    }
    else if(collectionView == self.collectionViewForCategory)
    {
        return allCategories.count;
    }
    return  productsArray.count;
    
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row +12 == productsArray.count && self.paging!= self.currentIndex && self.cellDisplayedIndex != indexPath.row) {
        self.cellDisplayedIndex = indexPath.row ;
        if(selectedFilters.count)
        {
            [self applyFiltersOnProductsWithIndex:self.currentIndex];
        }
        else
        {
            [self requestForExplorePosts:self.currentIndex];
        }
    }
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView==_collectionviewFilterItem)
    {
        CellForFilteredItem *cell=[collectionView dequeueReusableCellWithReuseIdentifier:mProductFiltersCellId forIndexPath:indexPath];
        NSArray *array = [SelectedFilters arrayOfSeletedFilters:selectedFilters];
        SelectedFilters *selectedFilter = array[indexPath.row];
        [cell setValueForSelectedFilters:selectedFilter];
        cell.removeFilterButton.tag = 100 + indexPath.row;
        
        return cell;
    }
    
    else if(collectionView == self.collectionViewForCategory)
    {
        CategoryCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"homeCategoryCell" forIndexPath:indexPath];
        cell.category = self.categoryObj;
        cell.homeVC = self;
        [cell setCategories:allCategories[indexPath.row] forIndexPath:indexPath];
        return cell;
    }
    else
    {
//     // Native AdCell
//     if(indexPath.row == (indexPath.row/10 *10) && indexPath.row!=0 )
//     {
//            NativeAdsCellCollectionViewCell *nativeCell = (NativeAdsCellCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"nativeAdsCell" forIndexPath:indexPath];
//            if(self.nativeAd)
//            {
//               //[nativeCell setNativeAdFor:self.nativeAd];
//            }
//            return nativeCell;
//        }
//        
    ListingCollectionViewCell  *collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:mSearchCollectioncellIdentifier forIndexPath:indexPath];
    if (collectionViewCell == nil) {
        collectionViewCell.postedImageOutlet = nil ;
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(self->productsArray.count){
                [collectionViewCell setProducts:self->productsArray[indexPath.row]];
            }
        });
    }
    return collectionViewCell;
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView.tag == 1)
    {
        return CGSizeMake(75,70 );
    }
    CellForFilteredItem *cell=(CellForFilteredItem*)[collectionView cellForItemAtIndexPath:indexPath];
    if(selectedFilters.count)
    {
        cell.filterName.text=flStrForObj(selectedFilters[indexPath.row][@"value"]);
    }
    NSArray *array = [SelectedFilters arrayOfSeletedFilters:selectedFilters];
    SelectedFilters *selectedFilter = array[indexPath.row];
    UILabel *label = [[UILabel alloc]init];
    [label setFont:[UIFont fontWithName:RobotoRegular size:14]];
    label.text = selectedFilter.name;
    CGFloat labelWidth = [CommonMethods measureWidthLabel:label];
    CGFloat cellWidth= labelWidth + 80;
    if(cellWidth >200){
        cellWidth = 200;
        return CGSizeMake(cellWidth,40 );
    }
    return CGSizeMake(cellWidth,40 );
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(collectionView.tag == 0)
    {
     
        ListingCollectionViewCell *cell = (ListingCollectionViewCell *)[self.collectionViewOutlet cellForItemAtIndexPath:indexPath];
        if([collectionView isEqual:self.collectionViewOutlet])
        {
            ProductDetails *product = productsArray[indexPath.row];
            NSLog(@"product=%@",product);
            ProductDetailsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
            newView.indexPath = indexPath ;
            newView.productDelegate = self ;
            newView.hidesBottomBarWhenPushed = YES;
            newView.postId = product.postId;
            newView.dataFromHomeScreen = YES;
            newView.currentCity = getLocation.currentCity;
            newView.countryShortName = getLocation.countryShortCode ;
            newView.currentLattitude = [NSString stringWithFormat:@"%lf",self.currentLat];
            newView.currentLongitude = [NSString stringWithFormat:@"%lf",self.currentLong];
            newView.movetoRowNumber = indexPath.item;
            newView.imageFromHome = cell.postedImageOutlet.image;
            newView.product = product;
            [self.navigationController pushViewController:newView animated:YES];
        }
    }
    else if (collectionView.tag == 1)
    {
        
        Category *categoryObj = allCategories[indexPath.row];
        [arrayOfcategory addObject:categoryObj.name];
        
        self.filterListings.category = categoryObj.name;
        self.filterListings.categoryImage = categoryObj.activeimage;
        [self.filterListings.filtersDictionary setValue:categoryObj.name forKey:@"Category"];
        [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:LoadingIndicatorTitle];
        NSDictionary *param = @{@"categoryName":categoryObj.name
                                };
        [WebServiceHandler getSubCategory:param andDelegate:self];
        
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"category" forKey:@"typeOfFilter"];
        [cbmc setValue:categoryObj.name forKey:@"value"];
        [cbmc setValue:categoryObj.activeimage forKey:@"image"];
        [selectedFilters addObject:cbmc];
        [self.collectionviewFilterItem reloadData];
        self.selectedFiltersView.hidden = NO;
        self.collectionViewForCategory.hidden = YES;
        self.categoryFiltersViewHeight.constant = 60;
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [productsArray removeAllObjects];
        [self reloadCollectionView] ;
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
    
}



-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if(collectionView == self.collectionviewFilterItem){
        return UIEdgeInsetsMake(5,5,5,5);
    }
    else
    {
        return UIEdgeInsetsZero;
    }
}


#pragma mark -
#pragma mark – RFQuiltLayoutDelegate -

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == (indexPath.row/10 *10) && indexPath.row!=0)
    {
        if(self.view.frame.size.width == 414){
            return CGSizeMake(4, 8);
        }
        else if (self.view.frame.size.width == 375){
            return CGSizeMake(10, 8);
        }
        else if (self.view.frame.size.width == 320)
        {
            return CGSizeMake(4, 8);
            
        }
        else
        {
            return CGSizeZero;
        }
    }
    return [Helper blockSizeOfProduct:productsArray[indexPath.row] view:self.view forHome:YES];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetsForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return UIEdgeInsetsMake (5,5,5,0);
}


#pragma mark -
#pragma mark - Request For Listings -

-(void)requesForProductListings
{
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    [self requestForExplorePosts:0];
}

-(void)requestForExplorePosts:(NSInteger )receivedindex {
    
    NSString *currentLattitudeParam,*currentLongitudeParam ;
    currentLattitudeParam = [NSString stringWithFormat:@"%lf",self.currentLat];
    currentLongitudeParam = [NSString stringWithFormat:@"%lf",self.currentLong];
    if(self.currentLat == 0)
    {
        currentLattitudeParam = @"";
        currentLongitudeParam = @"";
    }
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:mdeviceToken];
    if (token.length>0) {
        if ([[Helper userToken] isEqualToString:mGuestToken]) {
            NSDictionary *requestDict = @{
                                          mauthToken :flStrForObj([Helper userToken]),
                                          moffset:flStrForObj([NSNumber numberWithInteger:receivedindex * mPagingValue]),
                                          mlimit:flStrForObj([NSNumber numberWithInteger:mPagingValue]),
                                          mPushTokenKey : token,
                                          mlatitude : currentLattitudeParam,
                                          mlongitude : currentLongitudeParam
                                          };
            [WebServiceHandler getExplorePostsForGuest:requestDict andDelegate:self];
        }
        else{
            NSDictionary *requestDict = @{
                                          mauthToken :flStrForObj([Helper userToken]),
                                          moffset:flStrForObj([NSNumber numberWithInteger:receivedindex* mPagingValue]),
                                          mlimit:flStrForObj([NSNumber numberWithInteger: mPagingValue]),
                                          mlatitude : currentLattitudeParam,
                                          mlongitude: currentLongitudeParam,
                                          mPushTokenKey : token
                                          };
            [WebServiceHandler getExplorePosts:requestDict andDelegate:self];
        }
        
    } else{
        
        if ([[Helper userToken] isEqualToString:mGuestToken]) {
            NSDictionary *requestDict = @{
                                          mauthToken :flStrForObj([Helper userToken]),
                                          moffset:flStrForObj([NSNumber numberWithInteger:receivedindex * mPagingValue]),
                                          mlimit:flStrForObj([NSNumber numberWithInteger:mPagingValue]),
                                          mlatitude : currentLattitudeParam,
                                          mlongitude : currentLongitudeParam
                                          };
            [WebServiceHandler getExplorePostsForGuest:requestDict andDelegate:self];
        }
        else{
            NSDictionary *requestDict = @{
                                          mauthToken :flStrForObj([Helper userToken]),
                                          moffset:flStrForObj([NSNumber numberWithInteger:receivedindex* mPagingValue]),
                                          mlimit:flStrForObj([NSNumber numberWithInteger: mPagingValue]),
                                          mlatitude : currentLattitudeParam,
                                          mlongitude: currentLongitudeParam
                                          };
            [WebServiceHandler getExplorePosts:requestDict andDelegate:self];
        }
    }
}


#pragma mark -
#pragma mark - WebServiceDelegate -



-(void)internetIsNotAvailable:(RequestType)requsetType {
    [avForCollectionView stopAnimating];
    [refreshControl endRefreshing];
    if(!productsArray.count)
    {
        self.collectionViewOutlet.backgroundView = [Helper showMessageForNoInternet:YES forView:self.view];
    }
    
}

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    
    [Helper showMessageForNoInternet:NO forView:self.view];
    
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage , mCommonServerErrorMessage) viewController:self];
        [avForCollectionView stopAnimating];
        [refreshControl endRefreshing];
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        return;
    }
    else
    {
        [avForCollectionView stopAnimating];
        [refreshControl endRefreshing];
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    NSLog(@"responseDict=%@",responseDict);
    
    switch (requestType) {
        case RequestTypeGetExploreposts:
        {
            [avForCollectionView stopAnimating];
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    
                    [self handlingResponseOfExplorePosts:responseDict];
                }
                    break;
                case 204: {
                    if(!productsArray.count)
                    {
                        [self showingMessageForCollectionViewBackgroundForType:0];
                    }
                }
                    break;
            }
        }
            break;
        case RequestTypeunseenNotificationCount:
        {
            [avForCollectionView stopAnimating];
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    NSString *unseenCount = flStrForObj(response[@"data"]);
                    if ([unseenCount isEqualToString:@"0"]) {
                        self.labelNotificationsCount.hidden = YES;
                    }
                    else
                    {
                        self.labelNotificationsCount.hidden = NO;
                        self.labelNotificationsCount.text = unseenCount ;
                    }
                }
                    break;
                    
            }
        }
            break ;
            
        case RequestTypeSearchProductsByFilters:
        {
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [self handlingResponseOfExplorePosts:response];
                }
                    break;
                case 204:{
                    if(self.cellDisplayedIndex == -1)
                    {
                        [productsArray removeAllObjects];
                        [self reloadCollectionView] ;
                        [self showingMessageForCollectionViewBackgroundForType:1];
                    }
                }
                    break;
            }
            
        }
            break ;
            
        case RequestTypeGetCategories:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    allCategories = [Category arrayOfCategory:response[@"data"]];
                    [self.collectionViewForCategory reloadData];
                    self.categoryFiltersViewHeight.constant = 80;
                }
                    break;
                default:
                    break;
            }
        }
            break ;
            
        case RequestTypeSubCategory:
        {
            switch ([response[@"code"]integerValue]) {
                case 200:
                {
                    self.filterListings.subcategoryArray = [SubCategory arrayOfSubCategory:response[@"data"]];
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            
        default:
            break;
    }
}



#pragma mark -
#pragma mark - Handling Response -



-(void)handlingResponseOfExplorePosts :(NSDictionary *)response {
    self.collectionViewOutlet.backgroundView = nil;
    [refreshControl endRefreshing];
    if(self.currentIndex == 0 ) {
        [productsArray removeAllObjects];
        productsArray = [ProductDetails arrayOfProducts:response[@"data"]];
    }
    else {
        NSArray *productsWithPaging = [ProductDetails arrayOfProducts:response[@"data"]];
        [productsArray addObjectsFromArray:productsWithPaging];
    }
    self.currentIndex ++;
    self.paging ++;
    [self stopAnimation];
    [self reloadCollectionView] ;
}


-(void)addingActivityIndicatorToCollectionViewBackGround
{
    avForCollectionView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
    avForCollectionView.frame =CGRectMake(self.view.frame.size.width/2 -12.5, self.view.frame.size.height/2 - 100, 25,25);
    avForCollectionView.tag  = 1;
    [self.collectionViewOutlet addSubview:avForCollectionView];
    [avForCollectionView startAnimating];
}


#pragma mark -
#pragma mark - Button Actions -


- (IBAction)sellStuffButton:(id)sender{
    if([[Helper userToken]isEqualToString:mGuestToken])
    {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
        
    }
    else{
        
        [self openCameraScreen];
    }
}

- (IBAction)searchButtonAction:(id)sender {
    
    
    SearchPostsViewController *searchVC = [self.storyboard instantiateViewControllerWithIdentifier:mSearchPostsID];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchVC];
    [self.navigationController presentViewController:navigationController  animated:YES completion:nil];
}

- (IBAction)notificationButtonACtion:(id)sender {
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        UINavigationController *nav = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
    else{
        ActivityViewController *activityVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        activityVC.callBackForStartSelling = ^(BOOL isStartSelling) {
            if(isStartSelling)
            {
                [self openCameraScreen];
            }
        };
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
}

-(void)openActivityScreenForpush{
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        
    }
    else{
        ActivityViewController *activityVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        activityVC.showOwnActivity = YES ;
        
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
}

- (IBAction)filterButtonAction:(id)sender {
    
    FilterViewController *Fvc = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterItemsStoryboardID"];
    Fvc.hidesBottomBarWhenPushed = YES;
    Fvc.delegate=self;
    Fvc.checkArrayOfFilters = YES;
    Fvc.filterListings = self.filterListings;
    Fvc.categoryArray = allCategories;
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:Fvc];
    [self.navigationController presentViewController:navigation animated:YES completion:nil];
}


#pragma mark -
#pragma mark - No Posts View

-(void)showingMessageForCollectionViewBackgroundForType :(NSInteger )type{
    
    if(type == 0)
    {
        self.emptyFilterTextLabel.text = NSLocalizedString(noPostsNearYouText, noPostsNearYouText) ;
    }
    else if(type == 1)
    {
        self.emptyFilterTextLabel.text = NSLocalizedString(noPostsForAppliedFilter, noPostsForAppliedFilter) ;
    }
    UIView *backGroundViewForNoPost = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.collectionViewOutlet.frame.size.width,self.collectionViewOutlet.frame.size.height)];
    self.emtyFiltersView.frame = backGroundViewForNoPost.frame ;
    [backGroundViewForNoPost addSubview:self.emtyFiltersView];
    self.collectionViewOutlet.backgroundView = backGroundViewForNoPost;
}

#pragma mark -
#pragma mark - ZoomTransitionProtocol

-(UIView *)viewForZoomTransition:(BOOL)isSource
{
    NSIndexPath *selectedIndexPath = [[self.collectionViewOutlet indexPathsForSelectedItems] firstObject];
    ListingCollectionViewCell *cell = (ListingCollectionViewCell *)[self.collectionViewOutlet cellForItemAtIndexPath:selectedIndexPath];
    return cell.postedImageOutlet;
}


#pragma mark -
#pragma mark - Welcome Screen

-(void)showWelcomeMessageForBrowsing
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:mSignupFirstTime]){
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:mSignupFirstTime];
        [[NSUserDefaults standardUserDefaults]synchronize];
        self.tabBarController.tabBar.userInteractionEnabled = NO;
        StartBrowsingViewController *browsingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StartBrowsingStoryboardId"];
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        browsingVC.callbackToActivateTab = ^(BOOL activate){
            self.tabBarController.tabBar.userInteractionEnabled = activate;
        };
        [self presentViewController:browsingVC animated:YES completion:nil];
    }
    else {
        self.tabBarController.tabBar.userInteractionEnabled = YES;
        
    }
    
}

#pragma mark -
#pragma mark - Tab Bar animation -

/* pass a param to describe the state change, an animated flag and a completion block matching UIView animations completion*/
- (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated completion:(void (^)(BOOL))completion {
    
    // bail if the current state matches the desired state
    if ([self tabBarIsVisible] == visible) return (completion)? completion(YES) : nil;
    
    // get a frame calculation ready
    CGRect frame = self.tabBarController.tabBar.frame;
    CGFloat height = frame.size.height;
    CGFloat offsetY = (visible)? -height : height;
    
    // zero duration means no animation
    CGFloat duration = (animated)? 0.3 : 0.0;
    
    [UIView animateWithDuration:duration animations:^{
        self.tabBarController.tabBar.frame = CGRectOffset(frame, 0, offsetY);
    } completion:completion];
}

//Getter to know the current state
- (BOOL)tabBarIsVisible {
    return self.tabBarController.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame);
}

#pragma mark -
#pragma mark - ScrollView Delegate -

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    scrollView = self.collectionViewOutlet;
    _currentOffset = self.collectionViewOutlet.contentOffset.y;
    lastScrollContentOffset = scrollView.contentOffset ;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.isScreenDidAppear)
    {
        dispatch_async(dispatch_get_main_queue(),^{
            if (self->lastScrollContentOffset.y > scrollView.contentOffset.y) {
                // scroll up
                self.bottomConstraintOfSellButton.constant = 60;
                [self setTabBarVisible:YES animated:YES completion:nil];
                [[self navigationController] setNavigationBarHidden:NO animated:YES];
            }
            
            else if (self->lastScrollContentOffset.y < scrollView.contentOffset.y && self->lastScrollContentOffset.y >= 0) {
                // sroll down
                self.bottomConstraintOfSellButton.constant = 15;
                [self setTabBarVisible:NO animated:YES completion:nil];
                [[self navigationController] setNavigationBarHidden:YES animated:YES];
            }
        });
    }
}


#pragma mark -
#pragma mark - Notification Methods -

-(void)sellingPostAgain:(NSNotification *)noti
{
    
    
}

#pragma mark -
#pragma mark - Product Edited Notification -

-(void)UpdatePostOnEditing:(NSNotification *)noti
{
    [self requesForProductListings];
}

#pragma mark -
#pragma mark - Product Removed Notification -

-(void)deletePostFromNotification:(NSNotification *)noti {
    [self refreshData:self];
//    NSString *updatepostId = flStrForObj(noti.object[@"data"][@"postId"]);
//    for (int i=0; i <productsArray.count;i++) {
//
//        ProductDetails *product = productsArray[i];
//        if ([product.postId isEqualToString:updatepostId])
//        {
//            [productsArray removeObjectAtIndex:i];
//            [self reloadCollectionView] ;
//            break;
//        }
//    }
}


#pragma mark -
#pragma mark - Product Added Notification -

-(void)addNewPost :(NSNotification *)noti
{
    newPostAdded = YES;
    ProductDetails *newProduct = [[ProductDetails alloc]initWithDictionary:noti.object[0]];
    [productsArray insertObject:newProduct atIndex:0];
    
    self.collectionViewOutlet.backgroundView = nil ;
    [self reloadCollectionView] ;
    
}

#pragma mark -
#pragma mark - Ads Campaign Notification


/**
 Notify the method in App delegate to open the ads campaign
 
 @param noti details of campaign in NSdictionary format.
 */
-(void)postNotificationToAppdelegate:(NSNotification *)noti
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:mTriggerCampaign object:noti.object];
}

#pragma mark -
#pragma mark - Check Permission For Location -


/**
 This method is invoked after checking the permission for location services.
 On the basis of location services further server is requested for Listings.
 @param value Bool Value.
 */
-(void)allowPermission:(BOOL)value
{
    if(value)
    {
        //To get the current location
        getLocation = [GetCurrentLocation sharedInstance];
        [getLocation getLocation];
        getLocation.delegate = self;
        self.flagForLocation = YES;
    }
    else
    {
        [self requesForProductListings];
    }
    
}

#pragma mark-
#pragma mark - ProductDetails Delegate -


/**
 If product is removed.This delegate method get invoked.
 
 @param indexpath indexpath row.
 */
-(void)productIsRemovedForIndex:(NSIndexPath *)indexpath
{
//    [productsArray removeObjectAtIndex:indexpath.row];
//    [self reloadCollectionView] ;
}

-(void)updateProductForClickCount:(ProductDetails *)updatedProduct forIndex:(NSIndexPath *)indexpath
{
    productsArray[indexpath.row] = updatedProduct ;
}
#pragma mark-
#pragma mark -  Share On Instagram -


/**
 Once Product is successfully posted.This method get invoked to ask the permission to share on instagram.
 @param noti url to share.
 */

-(void)ShareOnInstagram:(NSNotification *)noti
{
    NSURL *instagramURL = [NSURL URLWithString:@"instagram:app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        NSString *path = [Helper instagramSharing:noti.object];
        _dic = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:path]];
        _dic.UTI = @"com.instagram.exclusivegram";
        _dic.delegate = nil;
        NSString *sharedMessage = [NSString stringWithFormat:NSLocalizedString(sharedVia, sharedVia),@" %@",APP_NAME];
        _dic.annotation = [NSDictionary dictionaryWithObject:sharedMessage forKey:@"InstagramCaption"];
    }
    
}


#pragma mark -
#pragma mark - Pending Notifications -

-(void)getnotificationCount
{
    if(![[Helper userToken] isEqualToString:mGuestToken]) {
        NSDictionary *requestDict = @{
                                      @"token":[Helper userToken]
                                      };
        [WebServiceHandler getUsernNotificationCount:requestDict andDelegate:self];
    }
}


#pragma mark -
#pragma mark - Get Categories -

/**
 Prefectch The Category List From  Server for Product Filters.
 */
-(void)getCategoriesListFromServer
{
    NSDictionary *requestDict = @{
                                  mLimit : @"50",
                                  moffset :@"0"
                                  };
    [WebServiceHandler getCategories:requestDict andDelegate:self];
}

#pragma mark -
#pragma mark - reload CollectionView -


/**
 Reload collectionView without Animation.
 */
-(void)reloadCollectionView{
    [UIView animateWithDuration:0 animations:^{
        [self->_collectionViewOutlet reloadData];
    }];
}

#pragma mark -
#pragma mark - Camera Screen -

-(void)openCameraScreen
{
    CameraViewController *cameraVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CameraStoryBoardID"];
    cameraVC.sellProduct = TRUE;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraVC];
    [self presentViewController:nav animated:NO completion:nil];
}
- (IBAction)clearFiltersButtonAction:(id)sender {
    
    [selectedFilters removeAllObjects];
    [self.filterListings resetFilterListings];
    [self requesForProductListings];
    self.collectionViewForCategory.hidden = NO ;
    self.selectedFiltersView.hidden = YES ;
    self.categoryFiltersViewHeight.constant = 80;
}


@end


//
//  NativeAdsCellCollectionViewCell.m
//  Tac Traderz
//
//  Created by 3Embed on 11/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "NativeAdsCellCollectionViewCell.h"

@implementation NativeAdsCellCollectionViewCell


-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.layer.masksToBounds = NO ;
    self.layer.shadowColor = mDividerColor.CGColor;
    self.layer.shadowOffset = CGSizeZero ;
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 2.0 ;
    self.contentView.backgroundColor =[UIColor whiteColor];
    self.layer.borderColor = mDividerColor.CGColor ;
    [self layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
//    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.postedImageOutlet.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight ) cornerRadii:CGSizeMake(3.0,3.0)];
//    
//    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//    maskLayer.frame = self.postedImageOutlet.bounds;
//    maskLayer.path  = maskPath.CGPath;
//    self.postedImageOutlet.layer.mask = maskLayer;
}



@end

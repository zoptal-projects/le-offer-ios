//
//  TWPhotoPickerController.m
//  InstagramPhotoPicker
//
//  Created by Emar on 12/4/14.
//  Copyright (c) 2014 wenzhaot. All rights reserved.
//

#import "TWPhotoPickerController.h"
#import "TWPhotoCollectionViewCell.h"
#import "TWImageScrollView.h"
#import "TWPhotoPickerController.h"
#import "ProgressIndicator.h"
#import  <Photos/Photos.h>
//#import "AlbumSelectionViewController.h"

@interface TWPhotoPickerController ()<UICollectionViewDataSource, UICollectionViewDelegate,UIScrollViewDelegate>/*albumsSelected*/
{
    CGFloat beginOriginY;
    PHAsset* choosen;
}
@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UIImageView *maskView;
@property (strong, nonatomic) TWImageScrollView *imageScrollView;
@property (strong, nonatomic) UICollectionView *collectionView;

@property UIButton *navRightButton;

@property UIButton *navTitleButton;



///custom picker

@property (strong) NSArray *collectionsFetchResults;
@property (strong) NSArray *collectionsFetchResultsAssets;
@property (strong) NSArray *collectionsFetchResultsTitles;
@property (strong) PHCachingImageManager *imageManager;
@property (strong) PHFetchResult *assetsFetchResults;

@end

@implementation TWPhotoPickerController

- (void)loadView {
    [super loadView];
    self.view.backgroundColor = [UIColor grayColor];
    [self.view addSubview:self.topView];
    [self.view insertSubview:self.collectionView belowSubview:self.topView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIWindow *window1 = [[UIWindow alloc] initWithFrame:CGRectMake(0, 100/*self.view.bounds.size.height-44*/,44, self.view.bounds.size.width)];
    window1.backgroundColor = [UIColor whiteColor];
    window1.windowLevel = UIWindowLevelNormal;
    [self.collectionView bringSubviewToFront:window1];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    [self checkLibraryPermissionsStatus];
    self.navigationController.navigationBarHidden =YES;
    [[NSUserDefaults standardUserDefaults]setValue:@"Lib" forKey:kController];
    [[NSUserDefaults standardUserDefaults]synchronize];
}


-(void)itemSelected:(PHFetchResult *)selctedPhotosAssests andHeaderTitle:(NSString *)headerTitle {
    [self.navTitleButton setTitle:headerTitle forState:UIControlStateNormal];
    self.assetsFetchResults = selctedPhotosAssests;
    choosen=[self.assetsFetchResults objectAtIndex:0];
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.networkAccessAllowed = YES ;
    if (choosen.mediaType == PHAssetMediaTypeImage)  {
        [self.imageManager requestImageForAsset:choosen
                                     targetSize:PHImageManagerMaximumSize
                                    contentMode:PHImageContentModeAspectFill
                                        options:options
                                  resultHandler:^(UIImage *result, NSDictionary *info) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [self.imageScrollView displayImage:result];
                                      });
                                  }];
    }
    [self.collectionView reloadData];
}


-(void)fetchPhotosFromLibrary {
    //All album: Sorted by descending creation date.
    NSMutableArray *allFetchResultArray = [[NSMutableArray alloc] init];
    NSMutableArray *allFetchResultLabel = [[NSMutableArray alloc] init];
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    PHFetchResult *assetsFetchResult;
    
    if([_viewFromProfileSelector isEqualToString:@"itisForProfilePhoto"]) {
        assetsFetchResult = [PHAsset  fetchAssetsWithMediaType:PHAssetMediaTypeImage options:options];
    }
    else {
        //        assetsFetchResult = [PHAsset  fetchAssetsWithMediaType:PHAssetMediaTypeImage options:options];
        assetsFetchResult = [PHAsset  fetchAssetsWithOptions:options];
    }
    
    [allFetchResultArray addObject:assetsFetchResult];
    [allFetchResultLabel addObject:@"All photos"];
    
    self.assetsFetchResults = assetsFetchResult;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // This block will be executed asynchronously on the main thread.
        [self.collectionView reloadData];
        [self.navTitleButton setTitle:@"Select any Picture" forState:UIControlStateNormal];
        
        choosen=[self.assetsFetchResults objectAtIndex:0];
        if (choosen.mediaType == PHAssetMediaTypeImage)  {
            
            [self.imageManager requestImageForAsset:choosen
                                         targetSize:PHImageManagerMaximumSize
                                        contentMode:PHImageContentModeAspectFill
                                            options:nil
                                      resultHandler:^(UIImage *result, NSDictionary *info) {
                                          
                                          // Only update the thumbnail if the cell tag hasn't changed. Otherwise, the cell has been re-used.
                                          dispatch_async(dispatch_get_main_queue(),^{
                                              [self.imageScrollView displayImage:result];
                                          });
                                      }];
        }
    });
}

- (void)loadPhotos {
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if (status ==  PHAuthorizationStatusAuthorized) {
        [self fetchPhotosFromLibrary];
    }
    if (status ==PHAuthorizationStatusNotDetermined) {
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
                [self fetchPhotosFromLibrary];
            }
        }];
    }
    if (status == PHAuthorizationStatusRestricted) {
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
                dispatch_async(dispatch_get_main_queue(), ^{
                    // This block will be executed asynchronously on the main thread.
                    [self fetchPhotosFromLibrary];
                });
            }
        }];
    }
}

-(void)enableButtonAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}



- (UIView *)topView {
    if (_topView == nil) {
        CGFloat handleHeight = 44.0f;
        CGRect rect = CGRectMake(0, 20, CGRectGetWidth(self.view.bounds), CGRectGetWidth(self.view.bounds)+handleHeight);
        self.topView = [[UIView alloc] initWithFrame:rect];
        self.topView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        self.topView.backgroundColor = [UIColor whiteColor];
        self.topView.clipsToBounds = YES;
        
        rect = CGRectMake(0, 0, CGRectGetWidth(self.topView.bounds), handleHeight);
        UIView *navView = [[UIView alloc] initWithFrame:rect];//26 29 33
        navView.backgroundColor = [UIColor colorWithRed:17.0/255.0f green:42.0/255.0f blue:70.0/255.0f alpha:1.0];
        [self.topView addSubview:navView];
        rect = CGRectMake(0, 0, 60, CGRectGetHeight(navView.bounds));
        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        backBtn.frame = rect;
        [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        [navView addSubview:backBtn];
        
        rect = CGRectMake((CGRectGetWidth(navView.bounds)-200)/2, 0,200, CGRectGetHeight(navView.bounds));
        self.navTitleButton = [[UIButton alloc] initWithFrame:rect];
        [self.navTitleButton setBackgroundColor:[UIColor clearColor]];
        [self.navTitleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.navTitleButton.titleLabel setFont:[UIFont fontWithName:RobotoRegular size:16]];
        self.navTitleButton.titleLabel.minimumScaleFactor = 0.5f;
        self.navTitleButton.titleLabel.numberOfLines = 1;
        self.navTitleButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        [self.navTitleButton addTarget:self action:@selector(selectAlbumAction:) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *dividerView = [[UIView alloc]initWithFrame:CGRectMake(0, 43.5,CGRectGetWidth(self.topView.bounds),0.5)];
        dividerView.backgroundColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f];
        [self.topView addSubview:dividerView];
        
        [navView addSubview:self.navTitleButton];
        
        
        rect = CGRectMake(CGRectGetWidth(navView.bounds)-80, 0, 80, CGRectGetHeight(navView.bounds));
        self.navRightButton = [[UIButton alloc] initWithFrame:rect];
        [self.navRightButton setTitle:@"Ok" forState:UIControlStateNormal];
        [self.navRightButton.titleLabel setFont:[UIFont fontWithName:RobotoRegular size:16]];
        [self.navRightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.navRightButton addTarget:self action:@selector(cropAction) forControlEvents:UIControlEventTouchUpInside];
        [navView addSubview:self.navRightButton];
        
        rect = CGRectMake(0, 0, 100, CGRectGetHeight(navView.bounds));
        UIButton *cancelButton = [[UIButton alloc] initWithFrame:rect];
          [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [cancelButton.titleLabel setFont:[UIFont fontWithName:RobotoRegular size:16]];
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [cancelButton addTarget:self action:@selector(cancelbuttonaction) forControlEvents:UIControlEventTouchUpInside];
        [navView addSubview:cancelButton];
        
        
        rect = CGRectMake(0, CGRectGetHeight(self.topView.bounds)-handleHeight, CGRectGetWidth(self.topView.bounds), handleHeight);
        UIView *dragView = [[UIView alloc] initWithFrame:rect];
        dragView.backgroundColor = [UIColor clearColor];
        dragView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [self.topView addSubview:dragView];
        
        UIImage *img = [UIImage imageNamed:@"cameraroll-picker-grip"];
        rect = CGRectMake((CGRectGetWidth(dragView.bounds)-img.size.width)/2, (CGRectGetHeight(dragView.bounds)-img.size.height)/2, img.size.width, img.size.height);
        UIImageView *gripView = [[UIImageView alloc] initWithFrame:rect];
        gripView.image = img;
        [dragView addSubview:gripView];
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
        [dragView addGestureRecognizer:panGesture];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        [dragView addGestureRecognizer:tapGesture];
        
        [tapGesture requireGestureRecognizerToFail:panGesture];
        
        rect = CGRectMake(0, handleHeight, CGRectGetWidth(self.topView.bounds), CGRectGetHeight(self.topView.bounds)-handleHeight-1);
        self.imageScrollView = [[TWImageScrollView alloc] initWithFrame:rect];
        [self.topView addSubview:self.imageScrollView];
        [self.topView sendSubviewToBack:self.imageScrollView];
        
        self.maskView = [[UIImageView alloc] initWithFrame:rect];
        self.maskView.image = [UIImage imageNamed:@"straighten-grid"];
        self.maskView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.topView insertSubview:self.maskView aboveSubview:self.imageScrollView];
    }
    return _topView;
}
-(void)cancelbuttonaction
{
    NSString *controllerType = [[NSUserDefaults standardUserDefaults]valueForKey:@"CameraControllerType"];
    
    if ([controllerType isEqualToString:@"directChaT"]) {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"CameraControllerType"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self.navigationController popViewControllerAnimated:NO];
    }
    else {
        if(_profilePhotoForSignUP) {
            [[UIApplication sharedApplication] setStatusBarHidden:YES             withAnimation:UIStatusBarAnimationNone];
        }
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    
    [self.tabBarController setSelectedIndex:0];
}

-(void)selectAlbumAction:(id)sender {
    
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    //
    //    AlbumSelectionViewController *newView = [storyboard instantiateViewControllerWithIdentifier:@"albumSelectionVc"];
    //    newView.delegate =self;
    //    newView.titleText = [self.navTitleButton titleForState:UIControlStateNormal];
    //    newView.selectedAlbumFor = self.viewFromProfileSelector;
    //    UINavigationController *navBar =[[UINavigationController alloc]initWithRootViewController:newView];
    //    [self presentViewController:navBar animated:YES completion:nil];
    //    [navBar.navigationBar setTranslucent:NO];
    //    [self.navigationController presentViewController:navBar animated:YES completion:nil];
}



- (UICollectionView *)collectionView {
    if (_collectionView == nil) {
        CGFloat colum = 4.0, spacing = 2.0;
        CGFloat value = floorf((CGRectGetWidth(self.view.bounds) - (colum - 1) * spacing) / colum);
        
        UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize                     = CGSizeMake(value, value);
        layout.sectionInset                 = UIEdgeInsetsMake(0, 0, 0, 0);
        layout.minimumInteritemSpacing      = spacing;
        layout.minimumLineSpacing           = spacing;
        
        CGRect rect = CGRectMake(0, CGRectGetMaxY(self.topView.frame), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-CGRectGetHeight(self.topView.bounds));
        _collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:layout];
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        
        [_collectionView registerClass:[TWPhotoCollectionViewCell class] forCellWithReuseIdentifier:@"TWPhotoCollectionViewCell"];
    }
    return _collectionView;
}

- (void)cancelButtonSelected {
    if([_viewFromProfileSelector isEqualToString:@"itisForProfilePhoto"]) {
        [self dismissViewControllerAnimated:NO completion:NULL];
    }
}

-(NSString*)getCurrentTime {
    NSDate *currentDateTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // Set the dateFormatter format
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    [dateFormatter setDateFormat:@"EEEMMddyyyyHHmmss"];
    // Get the date time in NSString
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    
    return dateInStringFormated;
    // Release the dateFormatter
    //[dateFormatter release];
}
- (void)cropAction {
    // if ([[choosen valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
    if (choosen.mediaType == PHAssetMediaTypeImage) {
        if (self.cropBlock) {
            self.cropBlock(self.imageScrollView.capture);
        }
        [self cancelButtonSelected];
    }
}

- (void)panGestureAction:(UIPanGestureRecognizer *)panGesture {
    switch (panGesture.state)
    {
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
        {
            CGRect topFrame = self.topView.frame;
            CGFloat endOriginY = self.topView.frame.origin.y;
            if (endOriginY > beginOriginY) {
                topFrame.origin.y = (endOriginY - beginOriginY) >= 20 ? 0 : -(CGRectGetHeight(self.topView.bounds)-20-44);
            } else if (endOriginY < beginOriginY) {
                topFrame.origin.y = (beginOriginY - endOriginY) >= 20 ? -(CGRectGetHeight(self.topView.bounds)-20-44) : 0;
            }
            
            CGRect collectionFrame = self.collectionView.frame;
            collectionFrame.origin.y = CGRectGetMaxY(topFrame);
            collectionFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(topFrame);
            [UIView animateWithDuration:.3f animations:^{
                self.topView.frame = topFrame;
                self.collectionView.frame = collectionFrame;
            }];
            break;
        }
        case UIGestureRecognizerStateBegan:
        {
            beginOriginY = self.topView.frame.origin.y;
            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            CGPoint translation = [panGesture translationInView:self.view];
            CGRect topFrame = self.topView.frame;
            topFrame.origin.y = translation.y + beginOriginY;
            
            CGRect collectionFrame = self.collectionView.frame;
            collectionFrame.origin.y = CGRectGetMaxY(topFrame);
            collectionFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(topFrame);
            
            if (topFrame.origin.y <= 0 && (topFrame.origin.y >= -(CGRectGetHeight(self.topView.bounds)-20-44))) {
                self.topView.frame = topFrame;
                self.collectionView.frame = collectionFrame;
            }
            
            break;
        }
        default:
            break;
    }
}


#pragma mark - Collection View Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger count = self.assetsFetchResults.count;
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"TWPhotoCollectionViewCell";
    
    TWPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    PHAsset *asset = self.assetsFetchResults[indexPath.item];
    
    NSInteger retinaScale = [UIScreen mainScreen].scale;
    CGSize retinaSquare = CGSizeMake(self.view.frame.size.width/3*retinaScale, self.view.frame.size.width/3*retinaScale);
    
    PHImageRequestOptions *cropToSquare = [[PHImageRequestOptions alloc] init];
    cropToSquare.networkAccessAllowed = YES ;
    cropToSquare.resizeMode = PHImageRequestOptionsResizeModeExact;
    
    CGFloat cropSideLength = MIN(asset.pixelWidth, asset.pixelHeight);
    CGRect square = CGRectMake(0, 0, cropSideLength, cropSideLength);
    CGRect cropRect = CGRectApplyAffineTransform(square,
                                                 CGAffineTransformMakeScale(1.0 / asset.pixelWidth,
                                                                            1.0 / asset.pixelHeight));
    
    cropToSquare.normalizedCropRect = cropRect;
    
    //NSLog(@"Image manager: Requesting FILL image for iPhone");
    [self.imageManager requestImageForAsset:asset
                                 targetSize:retinaSquare
                                contentMode:PHImageContentModeAspectFill
                                    options:cropToSquare
                              resultHandler:^(UIImage *result, NSDictionary *info) {
                                  
                                  // Only update the thumbnail if the cell tag hasn't changed. Otherwise, the cell has been re-used.
                                  //[cell.imageView setContentMode:UIViewContentModeCenter];
                                  [cell.imageView setImage:result];
                              }];
    
    if (asset.mediaType == PHAssetMediaTypeImage) {
        cell.time.hidden=YES;
    }
    return cell;
}

- (NSString *)timeFormatted:(double)totalSeconds

{
    NSTimeInterval timeInterval = totalSeconds;
    long seconds = lroundf(timeInterval); // Modulo (%) operator below needs int or long
    int hour = 0;
    int minute = seconds/60.0f;
    int second = seconds % 60;
    if (minute > 59) {
        hour = minute/60;
        minute = minute%60;
        return [NSString stringWithFormat:@"%02d:%02d:%02d", hour, minute, second];
    }
    else{
        return [NSString stringWithFormat:@"%02d:%02d", minute, second];
    }
}
#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{       PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.networkAccessAllowed = YES ;
    choosen=[self.assetsFetchResults objectAtIndex:indexPath.row];
    if (choosen.mediaType == PHAssetMediaTypeImage)  {
        
        [self.imageManager requestImageForAsset:choosen
                                     targetSize:PHImageManagerMaximumSize
                                    contentMode:PHImageContentModeAspectFill
                                        options:options
                                  resultHandler:^(UIImage *result, NSDictionary *info) {
                                      
                                      // Only update the thumbnail if the cell tag hasn't changed. Otherwise, the cell has been re-used.
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [self.imageScrollView displayImage:result];
                                      });
                                      
                                  }];
    }
    
    if (self.topView.frame.origin.y != 0) {
        [self tapGestureAction:nil];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (velocity.y >= 2.0 && self.topView.frame.origin.y == 0)
    {
        [self tapGestureAction:nil];
    }
}




- (void)tapGestureAction:(UITapGestureRecognizer *)tapGesture {
    //    CGRect topFrame = self.topView.frame;
    //    topFrame.origin.y = topFrame.origin.y == 0 ? -(CGRectGetHeight(self.topView.bounds)-20-44) : 0;
    
    CGRect topFrame = self.topView.frame;
    topFrame.origin.y = topFrame.origin.y == 0 ? -(CGRectGetHeight(self.topView.bounds)-20-44) : 20;
    
    CGRect collectionFrame = self.collectionView.frame;
    collectionFrame.origin.y = CGRectGetMaxY(topFrame);
    collectionFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(topFrame);
    [UIView animateWithDuration:.3f animations:^{
        self.topView.frame = topFrame;
        self.collectionView.frame = collectionFrame;
    }];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate){
        if (scrollView==_collectionView) {
            CGRect topFrame = self.topView.frame;
            CGFloat endOriginY = self.topView.frame.origin.y;
            if (endOriginY > beginOriginY) {
                topFrame.origin.y = (endOriginY - beginOriginY) >= 20 ? 0 : -(CGRectGetHeight(self.topView.bounds)-20-44);
            } else if (endOriginY < beginOriginY) {
                topFrame.origin.y = (beginOriginY - endOriginY) >= 20 ? -(CGRectGetHeight(self.topView.bounds)-20-44) : 0;
            }
            
            CGRect collectionFrame = self.collectionView.frame;
            collectionFrame.origin.y = CGRectGetMaxY(topFrame);
            collectionFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(topFrame);
            [UIView animateWithDuration:.3f animations:^{
                self.topView.frame = topFrame;
                self.collectionView.frame = collectionFrame;
            }];
        }
    }
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView==_collectionView) {
        if (scrollView.contentOffset.y<=0) {
            CGRect topFrame = self.topView.frame;
            CGFloat endOriginY = self.topView.frame.origin.y;
            if (endOriginY > beginOriginY) {
                topFrame.origin.y = (endOriginY - beginOriginY) >= 20 ? 0 : -(CGRectGetHeight(self.topView.bounds)-20-44);
            } else if (endOriginY < beginOriginY) {
                topFrame.origin.y = (beginOriginY - endOriginY) >= 20 ? -(CGRectGetHeight(self.topView.bounds)-20-44) : 0;
            }
            
            CGRect collectionFrame = self.collectionView.frame;
            collectionFrame.origin.y = CGRectGetMaxY(topFrame);
            collectionFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(topFrame);
            [UIView animateWithDuration:.3f animations:^{
                self.topView.frame = topFrame;
                self.collectionView.frame = collectionFrame;
            }];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==_collectionView) {
        if (scrollView.contentOffset.y<=0 && self.topView.frame.origin.y<0) {
            CGRect topFrame = self.topView.frame;
            topFrame.origin.y = -(scrollView.contentOffset.y) + topFrame.origin.y;
            CGRect collectionFrame = self.collectionView.frame;
            collectionFrame.origin.y = CGRectGetMaxY(topFrame);
            collectionFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(topFrame);
            self.topView.frame = topFrame;
            self.collectionView.frame = collectionFrame;
            //            }
        }
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    beginOriginY=self.topView.frame.origin.y;
}

/**
 Check permission to check the access for Photos library.
 handle the permission view and load photos on the basis of status.
 */
-(void)checkLibraryPermissionsStatus {
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusAuthorized) {
        self.enableLibraryAccessPermissionView.hidden = YES ;
        self.imageManager  = [[PHCachingImageManager alloc]init];
        [self loadPhotos];
        // Access has been granted.
    }
    
    else if (status == PHAuthorizationStatusDenied) {
        self.enableLibraryAccessPermissionView.hidden = NO ;
    }
    
    else if (status == PHAuthorizationStatusNotDetermined) {
        
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
                self.enableLibraryAccessPermissionView.hidden = YES ;
                self.imageManager  = [[PHCachingImageManager alloc]init];
                [self loadPhotos];
            }
            
            else {
                // Access has been denied.
                self.enableLibraryAccessPermissionView.hidden = NO ;
            }
        }];
    }
    
    else if (status == PHAuthorizationStatusRestricted) {
        // Restricted access - normally won't happen.
        self.enableLibraryAccessPermissionView.hidden = NO ;
    }
    
}



/**
 Enable Photos lib access Button Action.
 Redirect user to settings.
 
 @param sender enableLibrary access button.
 */
- (IBAction)enableLibraryAccessButtonAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

@end



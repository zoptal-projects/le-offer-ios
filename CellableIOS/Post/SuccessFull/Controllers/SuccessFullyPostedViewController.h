//
//  SuccessFullyPostedViewController.h

//
//  Created by Rahul Sharma on 10/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@import Firebase ;
@protocol successfullyPostedDelegate <NSObject>

-(void)postingListedSuccessfully ;

@end

@interface SuccessFullyPostedViewController : UIViewController

@property (strong, nonatomic) ProductDetails *product;
@property BOOL isTwitterSharing,isFacebookSharing ,isTwitterSharingPending;
@property (strong, nonatomic) NSString *postId ;
@property (strong,nonatomic)id<successfullyPostedDelegate> postingDelegate;
@property (strong, nonatomic) IBOutlet UIButton *okButtonOutlet;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityLoaderOutlet;


- (IBAction)okaButtonAction:(id)sender;


@end



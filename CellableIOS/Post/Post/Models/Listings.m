//
//  Listings.m
//  Vendu
//
//  Created by Rahul Sharma on 20/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "Listings.h"

@implementation Listings


/**
 Initialization Method to return the object of ProductDetails Model.
 
 @return self object of model.
 */

-(instancetype)init
{
    self = [super init];
    if (!self) { return nil; }
    
    self.titleOfPost = @"" ;
    self.category = @"" ;
    self.subCategory = @"";
    self.condition = @"" ;
    self.currency = @"";
    self.price = @"";
    self.swapDescription = @"";
    
    self.negotiable = TRUE;
    self.address = NSLocalizedString(addLocationTitle,addLocationTitle);
    self.swapPostArray = [NSMutableArray new];
    self.filter = @{};
    return self;
}


-(BOOL)isListingDone
{
    if(self.arrayOfImagePaths.count == 0)
    {
        [Helper showAlertWithTitle:@"Cellable" Message:NSLocalizedString(addAtleastSingleImage, addAtleastSingleImage) viewController:self.refrenceVC];
        return NO;
    }
     else if ([self.titleOfPost isEqualToString:@""])
     {
      [Helper showAlertWithTitle:@"Cellable" Message:NSLocalizedString(mTitleMissingAlert, mTitleMissingAlert) viewController:self.refrenceVC];
         return NO;
     }
    else if ([self.category isEqualToString:@""])
    {
     [Helper showAlertWithTitle:@"Cellable" Message:NSLocalizedString(mCategoryMissingAlert, mCategoryMissingAlert) viewController:self.refrenceVC];
        return NO;
    }
    else if([self.condition isEqualToString:@""])
    {
    [Helper showAlertWithTitle:@"Cellable" Message:NSLocalizedString(mConditionMissingAlert, mConditionMissingAlert) viewController:self.refrenceVC];
        return NO;
    }
    else if([self.currency isEqualToString:@""])
    {
      [Helper showAlertWithTitle:@"Cellable" Message:NSLocalizedString(mCurrencyMissingAlert, mCurrencyMissingAlert) viewController:self.refrenceVC];
        return NO;
    }
    else if ([self.price isEqualToString:@""])
    {
       [Helper showAlertWithTitle:@"Cellable" Message:NSLocalizedString(mPriceMissingAlert, mPriceMissingAlert) viewController:self.refrenceVC];
        return NO;
    }
    else if([self.address isEqualToString:NSLocalizedString(addLocationTitle, addLocationTitle)])
    {
         [Helper showAlertWithTitle:@"Cellable" Message:NSLocalizedString(mAddLocationAlert,mAddLocationAlert) viewController:self.refrenceVC];
        return NO;
    }
    
    

    return YES ;
}

@end

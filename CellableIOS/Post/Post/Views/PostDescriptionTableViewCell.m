//
//  PostDescriptionTableViewCell.m
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "PostDescriptionTableViewCell.h"

@implementation PostDescriptionTableViewCell 

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*-----------------------------------------------*/
#pragma mark -
#pragma mark - textView Delegate
/*----------------------------------------------*/

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    self.refrenceVC.tapGestureOutlet.enabled = YES ;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    self.listing.descriptionForPost = textView.text;
    self.refrenceVC.tapGestureOutlet.enabled = NO ;
}

@end

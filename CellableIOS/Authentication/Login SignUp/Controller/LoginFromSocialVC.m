
//  LoginFromSocialVC.m
//  Created by Ajay Thakur on 16/09/17.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//
#import "LoginFromSocialVC.h"
#import "Photos/Photos.h"
#import "UserDetails.h"
#import "WebViewForDetailsVc.h"
#import "ForgotPasswordViewController.h"
#import "FBLoginHandler.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "AskingPermissonViewController.h"
#import "CountryListViewController.h"
#import "HomeScreenController.h"
#import "EMCCountryPickerController.h"
#import "EMCCountryDelegate.h"
#import "VNHDialCode.h"
#import "OtpScreenViewController.h"
#import "TWPhotoPickerController.h"
#import "Cellable-Swift.h"
#import "LoginSignUpViewController.h"
#define mDefualtPP   @"add_photo"

@import Firebase;
@class MQTT;

@interface LoginFromSocialVC ()<FBLoginHandlerDelegate,UIGestureRecognizerDelegate,WebServiceHandlerDelegate,EMCCountryDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    EMCCountryPickerController *emc ;
  
    BOOL userNotCheck,numberNotCheck,emailNotCheck ,invalidUsername,invalidNumber,invalidEmail ;
}
@property (strong,nonatomic) UIImagePickerController *imgpicker;
@property (weak, nonatomic) IBOutlet UIImageView *iconEmail;
@property (weak, nonatomic) IBOutlet UIView *lineDifferWayOut;

@end
@implementation LoginFromSocialVC

/*--------------------------------------*/
#pragma mark
#pragma mark - viewcontroller LifeCycle.
/*--------------------------------------*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.backButtonOutLet rotateButton];
    emc = [[EMCCountryPickerController alloc]init];
     self.viewForLoginSignup.layer.borderColor = [UIColor colorWithRed:119/255.0f green:119/255.0f blue:119/255.0f alpha:1.0f].CGColor;
    if(!self.signupFlag)
    {
    [self loginOptionButtonAction:self];
    }
    else
    {
    [self signupOptionButtonAction:self];
    }
    getLocation = [GetCurrentLocation sharedInstance];
    [getLocation getLocation];
    getLocation.delegate = self;
    
    self.loginbutton.backgroundColor = mLoginButtonDisableBackgroundColor;
    self.createButton.backgroundColor = mSignupButtonDisableBackgroundColor ;
    self.loginbutton.titleLabel.textColor = mLoginTextColor ;
    self.createButton.titleLabel.textColor = mSignupTextColor;
    
    
   UIColor *color = [UIColor whiteColor];
    _usernameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username/Email" attributes:@{NSForegroundColorAttributeName: color}];
    _loginPasswordTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    _userNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    _nameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes:@{NSForegroundColorAttributeName: color}];
    _signupPasswordTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    _numberTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Number(Optional)" attributes:@{NSForegroundColorAttributeName: color}];
    _emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  //  [[self navigationController] setNavigationBarHidden:YES animated:YES];
    self.loginbutton.titleLabel.hidden = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveAccKeybaordHeight:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveToOriginalPostion) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowInSignup:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideInSignup:) name:UIKeyboardWillHideNotification object:nil];
    self.navigationController.navigationBar.hidden = YES;
    
    
    
   }
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.view endEditing:YES];
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillHideNotification ];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillHideNotification ];

}


/*-----------------------------------*/
#pragma mark
#pragma mark -  Hide StatusBar.
/*-----------------------------------*/

-(BOOL)prefersStatusBarHidden
{
    return YES;
}



/*-----------------------------------*/
#pragma mark
#pragma mark - Location Delegate -
/*-----------------------------------*/
- (void)updatedLocation:(double)latitude and:(double)longitude
{
    self.currentLat = latitude;
    self.currentLong = longitude;
  
}

- (void)updatedAddress:(NSString *)currentAddress
{
    self.currentAddress = currentAddress ;
    
}

/*-----------------------------------*/
#pragma mark
#pragma mark - textfield Delegates for LogIn View.
/*-----------------------------------*/

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    switch (textField.tag) {
        case 9:
            [self.loginPasswordTextfield becomeFirstResponder];
            break;
        case 10:
            [self LoginButtonSelected];
        default:
            [self textFieldBecomeFirstResponder:textField.tag];
            break;
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}
/**
 This method will check Log in Button state.
 @return Yes for enable and No for disable.
 */
- (BOOL)checkForMandatoryField
{
    if (self.loginPasswordTextfield.text.length < 2 || self.usernameTextField.text.length < 2){
        self.loginbutton.enabled = NO;
        _loginbutton.backgroundColor =  mLoginButtonDisableBackgroundColor;
        self.loginbutton.titleLabel.textColor = mLoginTextColor;
        return NO;
        }
    else {
        self.loginbutton.enabled = YES;
        _loginbutton.backgroundColor = mLoginButtonDisableBackgroundColor2;
        self.loginbutton.titleLabel.textColor = [UIColor whiteColor];
        return YES;
    }
}

/*--------------------------------------*/
#pragma mark
#pragma mark - textfield Delegates For SignUp view.
/*--------------------------------------*/

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    switch (textField.tag) {
        case 1:{
            _userNameCheckImage.hidden=YES;
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            if ([string isEqualToString:filtered])
                textField.tintColor = [UIColor blueColor];
            else
                textField.tintColor = [UIColor redColor];
            return [string isEqualToString:filtered];
        }
            break;
        case 2:
        case 3:{
            if(range.length + range.location > textField.text.length)
                return NO;
            NSUInteger newLength = [textField.text length] + [string length] - range.length;  // to limit the characters entry
            return newLength <= 12;
        }
            break;
        case 4:
            break;
            
        default:
            break;
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag)
    {
        case 1:if(_userNameTextField.text.length >2)
        {
            invalidUsername = NO;
            NSDictionary *requestDict = @{mUserName    : _userNameTextField.text};
            [WebServiceHandler userNameCheck:requestDict andDelegate:self];
        }
        else if(_userNameTextField.text.length)
        {   invalidUsername = YES;
            [self showAlertFromTop:usernameIsInvalid];
        }
            break;
        case 3:if(_numberTextField.text.length >7)
        {
            invalidNumber = NO;
            NSString *phoneNumberWithCode = [self.countryCodeLabelOutlet.text stringByAppendingString:_numberTextField.text];
            NSDictionary *requestDict = @{mphoneNumber :phoneNumberWithCode};
            [WebServiceHandler phoneNumberCheck:requestDict andDelegate:self];
        }
        else if(_numberTextField.text.length)
        {
            invalidNumber = YES;
            [self showAlertFromTop:phoneNumberIsInvalid];
        }
            break;
        case 4:if([self NSStringIsValidEmail:self.emailTextField.text]){
            invalidEmail = NO;

            if (self.signUpWithFacebook)
            {
                if (!self.fbResponseDetails[@"email"])
                {
               NSDictionary *requestDict = @{mEmail  : _emailTextField.text};
                [WebServiceHandler emailCheck:requestDict andDelegate:self];
                }
               
            
            }
            
            
            
           
        }
        else if(_emailTextField.text.length)
        {   invalidEmail = YES ;
            [self showAlertFromTop:emailIsInvalid];
        }
            break;
    }
    
}

/**
 *   this user defined method called by textField shouldChangeCharactersInRange delegate method and it is bool vale
 *  @return  yes if user enter any details in textfields and NO if the textfields nameTextField,passWordTextField are empty.
 */
- (BOOL)checkForMandatoryFieldForSignup
{
//      if (_userNameTextField.text.length !=0 && _nameTextField.text.length !=0 &&_emailTextField.text.length && _numberTextField.text.length ) {
    NSLog(@"nameTextField.text.length=%@",_nameTextField.text);

    NSLog(@"_emailTextField.text.length=%@",_emailTextField.text);
   if (_nameTextField.text.length !=0 &&_emailTextField.text.length)
   {
        self.createButton.enabled = YES;
        _createButton.backgroundColor = mLoginButtonDisableBackgroundColor2;
        return YES;
    }
    else{
        self.createButton.enabled = NO;
        _createButton.backgroundColor = mSignupButtonDisableBackgroundColor;
        return NO;
    }
}

/**
 This method will recognize the textfield to make that first Responder
 @param tag int value
 */
-(void)textFieldBecomeFirstResponder :(NSInteger ) tag
{
    switch (tag) {
        case 0:
            [_userNameTextField becomeFirstResponder];
            break;
        case 1:
            [self.signupPasswordTextfield becomeFirstResponder];
            break;
        case 2:
            [_numberTextField becomeFirstResponder];
            break;
        case 3:
            [_emailTextField becomeFirstResponder];
            break;
        case 4:[_emailTextField resignFirstResponder];
            break;
            
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField ;
    switch (textField.tag)
    {
        case 1:_userNameCheckImage.hidden = YES;
            break;
        case 3:_imageForNumberCheck.hidden = YES;
            break;
        case 4:_imageForEmailCheck.hidden = YES;
            break;
    }
}



/**
 Tap gesture action.
 @param sender tapGesture.
 */
-(IBAction)dismissKeyboard:(id)sender{
   
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        [self.usernameTextField resignFirstResponder];
        [self.loginPasswordTextfield resignFirstResponder];
        CGRect frameOfView = self.view.frame;
        frameOfView.origin.y = 0;
        frameOfView.origin.x=0;
        self.view.frame = frameOfView;
    } completion:^(BOOL finished) {
        
    }];
}
//
#pragma mark - Move View

/**
 This method will move view by evaluating keyboard height.

 @param notification post by Notificationcentre.
 */
-(void)veiwMoveAccKeybaordHeight :(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    //Given size may not account for screen rotation
    NSInteger HeightOfkeyboard = MIN(keyboardSize.height,keyboardSize.width);
    float maxY = CGRectGetMaxY(_loginbutton.frame) + HeightOfkeyboard +3;
    float reminder = CGRectGetHeight(self.view.frame) - maxY;
    if (reminder < 0) {
        [UIView animateWithDuration:0.4 animations: ^ {
            CGRect frameOfView = self.view.frame;
            frameOfView.origin.y = reminder;
            self.view.frame = frameOfView;}];
    }
}




/**
 This method will move view back to original frame.
 */
-(void)veiwMoveToOriginalPostion
{
    [UIView animateWithDuration:0.4 animations: ^ {
    CGRect frameOfView = self.view.frame;
    frameOfView.origin.y = 0;
    self.view.frame = frameOfView;}];
}


/*-----------------------------*/
#pragma mark
#pragma mark - Button Actions
/*-----------------------------*/

/**
 Login button action call webService for login.

 @param sender loginButton.
 */
- (IBAction)logInButtonAction:(id)sender {
    [self LoginButtonSelected];
}

/**
 Method will request web service for login.
 */
-(void)LoginButtonSelected{
    //removing button title beacuse we need to show activity view instead of button title.
    self.signupSelectOutlet.enabled = NO;
    [self.loginbutton setTitle:@"" forState:UIControlStateNormal];
    [self.loginbutton.titleLabel setHidden:YES];
    //animating activity view(progress bar).
    [self.activityViewOutlet startAnimating];
    self.activityViewOutlet.hidden = NO;
    // login api requesting.
    NSDictionary *requestDict = @{
                                  mLoginType   : mUserNormalLogin,
                                  mUserName    : self.usernameTextField.text,
                                  mPswd :self.loginPasswordTextfield.text ,
                                  mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                                  mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                                  mCity : flStrForObj(getLocation.currentCity),
                                  mCountryShortName : flStrForObj(getLocation.countryShortCode),
                                  mpushToken   :flStrForObj([Helper deviceToken]),
                                  };
    [WebServiceHandler logId:requestDict andDelegate:self];
}


/**
 loginwithFacebook button set facebook handler.

 @param sender facebook button.
 */
- (IBAction)LogInWithFbButtonAction:(id)sender {
    //request for fb.
    FBLoginHandler *handler = [FBLoginHandler sharedInstance];
    [handler loginWithFacebook:self];
    [handler setDelegate:self];
}

/**
 Get help for signin if password is forgotten.

 @param sender getHelp button.
 */
- (IBAction)getHelpSignInButtonAction:(id)sender {
    ForgotPasswordViewController *forgotVC = [self.storyboard instantiateViewControllerWithIdentifier:mForgotPasswordStoryboardID];
    [self.navigationController pushViewController:forgotVC animated:YES];
}


/*--------------------------------------------*/
#pragma mark
#pragma mark - facebook handler
/*--------------------------------------------*/

/**
 Facebook handler get call on success of facebook service.

 @param userInfo user information in dictionary.
 */
- (void)didFacebookUserLoginWithDetails:(NSDictionary*)userInfo {
    if(self.signupFlag){
        NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1",userInfo[@"id"]]];
        
        profilePicUrl = pictureURL.absoluteString;
        if(profilePicUrl.length > 0){
            [self.profileImageView setImageWithURL:[NSURL URLWithString:profilePicUrl] placeholderImage: [UIImage imageNamed:mDefualtPP]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            ThumbnailimagePath = nil;
            self.editImageOnProfileView.hidden = NO;
        }
        else{
            self.editImageOnProfileView.hidden = YES ;
        }
    }
}



/**
 If facebook handler get unsuccessful rsponse.

 @param error errorMessage.
 */
- (void)didFailWithError:(NSError *)error {
      [Helper showAlertWithTitle:NSLocalizedString(alertMessage, alertMessage) Message:[error localizedDescription] viewController:self];
}
/*-----------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*-----------------------------*/

/**
 Webservice delegate method will get call on success of service call.

 @param requestType requestType sent.
 @param response    response comes from webservice.
 @param error       error if any.
 */
- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    self.activityViewOutlet.hidden = YES;
    [self.activityViewOutlet stopAnimating];
    self.loginbutton.titleLabel.hidden = NO;
    [self.loginbutton setTitle:NSLocalizedString(LoginButtonTitle, LoginButtonTitle) forState:UIControlStateNormal];
    [self.createButton setTitle:NSLocalizedString(signupButtonTitle, signupButtonTitle) forState:UIControlStateNormal];
    self.signupSelectOutlet.enabled = YES;
    if (error) {
        
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
        return;
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    switch (requestType) {
        case RequestTypeLogin:
        {
            UserDetails *user = response ;
            switch (user.code) {
                case 200: {
                    
                    [Helper storeUserLoginDetails:user];
                    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",user.userId] forKey:@"userId"];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"recent_login"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    self.loginbutton.titleLabel.hidden = YES;
                    // Log User Device Details
                    NSDictionary *param = [CommonMethods updateDeviceDetailsForAdmin];
                    [WebServiceHandler logUserDevice:param andDelegate:self];
                    //Check Campaign
                    NSDictionary *requestDic = @{mauthToken : [Helper userToken] };
                    [WebServiceHandler runCampaign:requestDic andDelegate:self];
                    
                    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
                    
                    // MQTT Connection Created
                    MQTT *mqttModel = [MQTT sharedInstance];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [mqttModel createConnection];
                    });
                    
                }
                    break;
                case 204: {
                    [Helper showAlertWithTitle:NSLocalizedString(muserNotFoundAlertTitle, muserNotFoundAlertTitle) Message:NSLocalizedString(userNotFound, userNotFound) viewController:self];
                }
                    break;
                case 401: {
                    [Helper showAlertWithTitle:NSLocalizedString(mincorrectPasswordAlertTitle, mincorrectPasswordAlertTitle) Message:NSLocalizedString(incorrectPasswordMessage, incorrectPasswordMessage) viewController:self];
                }
                    break;
            }
        }
            break;
        case RequestTypeUserNameCheck:
        {
            switch ([responseDict[@"code"] integerValue]) {
                    //success response.
                case 200: {
                    userNotCheck = NO ;
                   // _userNameCheckImage.hidden=NO;
                   // _userNameCheckImage.image=[UIImage imageNamed:mImageForCheckAvailability];
                }
                    break;
                case 409: {//username already registered.
                    [self movedownTheView:mUsernameRegisteredAlert];
                  //  _userNameCheckImage.hidden=NO;
                    userNotCheck = YES ;
                   // _userNameCheckImage.image=[UIImage imageNamed:mImageForNotAvail];
                }
                    break;
            }
        }
            break ;
            
        case RequestTypePhoneNumberCheck:
        {
            switch ([responseDict[@"code"] integerValue]) {
                    //success response.
                case 200: {
                    numberNotCheck = NO ;
                  //  _imageForNumberCheck.hidden=NO;
                  //  _imageForNumberCheck.image=[UIImage imageNamed:mImageForCheckAvailability];
                }
                    break;
                case 409: {//phonemuber already registered
                    [self  movedownTheView:mNumberAlreadyRegistered];
                  //  _imageForNumberCheck.hidden=NO;
                    numberNotCheck = YES ;
                  //  _imageForNumberCheck.image=[UIImage imageNamed:mImageForNotAvail];
                }
                    break;
            }
        }
            break ;
            
            case RequestTypenewRegister:
            {
                UserDetails *user = response ;
                switch (user.code)
                {
                    case 200:
                    {
                        //save username
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:mSignupFirstTime];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [[NSUserDefaults standardUserDefaults]setObject:user.username forKey:@"newRegisterUsername"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"recent_login"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [Helper storeUserLoginDetails:user];
                        NSDictionary *param = [CommonMethods updateDeviceDetailsForAdmin];
                        [WebServiceHandler logUserDevice:param andDelegate:self];
                        [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
                        [self.view endEditing:YES];
                        
                        // creating connection
                        MQTT *mqttModel = [MQTT sharedInstance];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [mqttModel createConnection];
                        });
                    }
                        break;
                    default:
                   //   [self  showingErrorAlertfromTop:user.message];
                        
                        break;
                }
            }
                break;
            
            
        case RequestTypeEmailCheck:
        {
            
            switch ([responseDict[@"code"] integerValue]) {
                    //success response.
                case 200: {
                    emailNotCheck = NO ;
                    _imageForEmailCheck.hidden=NO;
                    _imageForEmailCheck.image=[UIImage imageNamed:mImageForCheckAvailability];
                }
                    break;
                case 409: {
                    [self  movedownTheView:mEmailAlreadyRegistered];
                    emailNotCheck = YES ;
                    _imageForEmailCheck.hidden=NO;
                    _imageForEmailCheck.image=[UIImage imageNamed:mImageForNotAvail];
                }
                    break;
            }
            
        }
            break ;
        default:
            break;
    }
    
    
}


-(void)movedownTheView :(NSString *)message {
    [self showAlertFromTop:message];
}

/**
 
 If text get changed in password field this method will check the mandatory Fields.
 @param sender textfield.
 */
- (IBAction)textFieldDidChange:(id)sender {
    [self checkForMandatoryField];
}


/*------------------------------------------------------*/
#pragma mark -
#pragma mark -

#pragma mark - Sign up
/*-------------------------------------------------------*/


 #pragma mark
 #pragma mark - Move View
 
 /**
 This method will move view by evaluating keyboard height.
 
  @param n post by Notificationcentre.
 */

- (void)keyboardWillShowInSignup:(NSNotification *)n
{
    NSDictionary* info = [n userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }

}

- (void)keyboardWillHideInSignup:(NSNotification *)n
{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
   self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;

}

#pragma mark - Error Label
/**
 create label for error.
 */
-(void)createLabelToSHowErrorMessage
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    labelForErrorMessg = [[UILabel alloc] init];
    [labelForErrorMessg setTextColor:[UIColor whiteColor]];
    labelForErrorMessg.frame = CGRectMake(0, 64, self.view.frame.size.width,30);
    labelForErrorMessg.backgroundColor = mBaseOTPButton;
    labelForErrorMessg.textAlignment = NSTextAlignmentCenter ;
    [window addSubview:labelForErrorMessg];
    labelForErrorMessg.hidden = YES;
}

/*--------------------------------------------*/
#pragma mark
#pragma mark - Get DeviceID
/*--------------------------------------------*/

-(void)gettingDeviceid {
    //getting userdeviceid
    
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        deviceId = [oNSUUID UUIDString];
    else
        deviceId = [oNSUUID UUIDString];
    [[NSUserDefaults standardUserDefaults] setObject:deviceId forKey:mDeviceId];
}

/*-----------------------------------------*/
#pragma mark
#pragma mark - image picker(camera photo)
/*-----------------------------------------*/

//image picker
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary*)info {
    
    NSData *dataimage = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerOriginalImage"], 1);
    UIImage *selectedProfileImage = [Helper resizeImage:[[UIImage alloc] initWithData:dataimage]] ;
    
    NSString *imageName = [NSString stringWithFormat:@"%@%@.png",@"Image",[self getCurrentTime]];
    NSData *imgData1 = UIImageJPEGRepresentation(selectedProfileImage,1);
    //to get the image path.
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* imagePath = [documentsDirectory stringByAppendingPathComponent:imageName];
    [imgData1 writeToFile:imagePath atomically:NO];
    
    ThumbnailimagePath = imagePath ;
    
    // storing the captured image to gallery.(optional)
    UIImage *storingImage = [[UIImage alloc] initWithData:dataimage]; // if u want to store original captured image then keep dataimage instead of data.
    UIImageWriteToSavedPhotosAlbum(storingImage, nil, nil, nil);
    [self.profileImageView setImage:selectedProfileImage];
    [self.imgpicker dismissViewControllerAnimated:YES completion:nil];
    self.editImageOnProfileView.hidden = NO;
    
}

-(void)uploadingImageToCloudinary:(NSString *)imagePath {
    CLCloudinary *mobileCloudinary = [[CLCloudinary alloc] init];
    [mobileCloudinary.config setValue:cloundinaryCreditinals[@"response"][@"cloudName"] forKey:@"cloud_name"];
    CLUploader* mobileUploader = [[CLUploader alloc] init:mobileCloudinary delegate:self];
    if(cloundinaryCreditinals){
    [mobileUploader upload:imagePath options:@{
                                               @"signature":cloundinaryCreditinals[@"response"][@"signature"],
                                               @"timestamp": cloundinaryCreditinals[@"response"][@"timestamp"],
                                               @"api_key": cloundinaryCreditinals[@"response"][@"apiKey"],
                                               }];
    }
}

/*--------------------------------------------*/
#pragma mark
#pragma mark - imageResizing.
/*--------------------------------------------*/

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(NSString*)getCurrentTime {
    NSDate *currentDateTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // Set the dateFormatter format
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    [dateFormatter setDateFormat:@"EEEMMddyyyyHHmmss"];
    // Get the date time in NSString
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    //  //NSLog(@"%@", dateInStringFormated);
    return dateInStringFormated;
    // Release the dateFormatter
}

/*--------------------------------------------*/
#pragma mark
#pragma mark - cloudinaryImageUploadingDelegates.
/*--------------------------------------------*/

- (void) uploaderSuccess:(NSDictionary*)result context:(id)context {
    NSString* publicId = [result valueForKey:@"public_id"];
    NSLog(@"Upload success. Public ID=%@, Full result=%@", publicId, result);
    profilePicUrl = result[@"secure_url"];
    [self requestForSignUp];
}


-(void)uploaderError:(NSString*)result code:(NSInteger )code context:(id)context {
    NSLog(@"Upload error: %@, %ld", result, (long)code);
}

- (void) uploaderProgress:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite context:(id)context {
    NSLog(@"Upload progress: %ld/%ld (+%ld)", (long)totalBytesWritten, (long)totalBytesExpectedToWrite, (long)bytesWritten);
}

#pragma mark
#pragma mark - tapGesture
/*--------------------------------------*/

- (IBAction)tapGestureAction:(id)sender {
    [self.view endEditing:YES];
}


/**
 *  User cancelled
 */

- (void)didUserCancelLogin {
    NSLog(@"USER CANCELED THE LOGIN");
}


/**
 Check username is valid or not.
 
 @param checkingUserName text from username textfield.
 
 @return YES for illegal andNO for legal.
 */
-(BOOL)checkValidUserNameOrNot:(NSString *)checkingUserName{
    NSCharacterSet *s = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_."];
    s = [s invertedSet];
    
    NSRange r = [checkingUserName rangeOfCharacterFromSet:s];
    if (r.location != NSNotFound) {
       // NSLog(@"the string contains illegal characters");
        return YES;
    }
    else {
       // NSLog(@"the string contains legal characters");
        return NO;
    }
}

#pragma mark- WebService

-(void)requestForSignUp {
    NSString *numberWithCode =[self.countryCodeLabelOutlet.text stringByAppendingString:self.numberTextField.text];
    if([profilePicUrl length] ==0 )
    {
        profilePicUrl = @"defaultUrl";
    }
    if([[Helper deviceToken] containsString:@"failed To fetch Push Token"])
    {
//        NSString *token = [[FIRInstanceID instanceID] token];
//        [[NSUserDefaults standardUserDefaults] setObject:token forKey:mdeviceToken];
//        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    NSDictionary *requestDict;
    
    if ([self.SignUpType integerValue] == 3)
    {
        if  ([_SignUpType2 isEqualToString:@"AppleLogin"])
        {
            requestDict = @{
                          mUserName    : _userNameTextField.text,
                          mfullName    :_nameTextField.text,
                          mPswd        :@"123123123",
                          mDeviceType  :@"1",
                          mDeviceId    :deviceId,
                          mProfileUrl  :flStrForObj(profilePicUrl),
                          mSignUpType  :mUserApplePlusSignup,
                          mpushToken   :flStrForObj([Helper deviceToken]),
                          mGooglePlusId  : _appleIDString,
                          mEmail       : self.emailTextField.text,
                          mphoneNumber :numberWithCode,
                          @"accessToken":_appleIDString,
                          mGooglePlusAccessToken :_appleIDString,
                          mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                          mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                          mCity : flStrForObj(getLocation.currentCity),
                          mCountryShortName : flStrForObj(getLocation.countryShortCode),
                          mlocation : flStrForObj(self.currentAddress)

                          };
        }
        else
        {
        
        requestDict = @{
                                         mEmail       :self.emailTextField.text,
                                         mUserName    :_userNameTextField.text,
                                         mfullName    :_nameTextField.text,
                                         mPswd        :@"123123123",
                                         mDeviceType  :@"1",
                                         mDeviceId    :deviceId,
                                         mProfileUrl  :profilePicUrl,
                                         mSignUpType  :mUserGooglePlusSignup,
                                         mpushToken   :flStrForObj([Helper deviceToken]),
                                         mphoneNumber :numberWithCode,
                                         mGooglePlusId :self.googlePlusId,
                                         mGooglePlusAccessToken :self.googlePlusAccessToken,
                                         mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                                         mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                                         mCity : flStrForObj(getLocation.currentCity),
                                         mCountryShortName : flStrForObj(getLocation.countryShortCode),
                                         mlocation : flStrForObj(self.currentAddress)
                                         };
        }
    
    }
    else if ([self.SignUpType integerValue] == 2 )
        
    {
          requestDict = @{
              mUserName    : _userNameTextField.text,
              mfullName    :_nameTextField.text,
              mPswd        :@"123123123",
              mDeviceType  :@"1",
              mDeviceId    :deviceId,
              mProfileUrl  :flStrForObj(profilePicUrl),
              mSignUpType  :mUserFacebookSignup,
              mpushToken   :flStrForObj([Helper deviceToken]),
              mfbuniqueid  : _fbID,
              mEmail       : self.emailTextField.text,
              mphoneNumber :numberWithCode,
              @"accessToken":self.fbloggedUserAccessToken,
              mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
              mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
              mCity : flStrForObj(getLocation.currentCity),
              mCountryShortName : flStrForObj(getLocation.countryShortCode),
              mlocation : flStrForObj(self.currentAddress)

              };
    }
    
     NSLog(@"requestDict=%@",requestDict);
    if (_numberTextField.text.length == 0)
    {
    [WebServiceHandler newRegistration:requestDict andDelegate:self];
    }
    else if (numberNotCheck == NO)
    {
        NSLog(@"requestDict=%@",requestDict);
        OtpScreenViewController *otpVC = [self.storyboard instantiateViewControllerWithIdentifier:mOtpStoryboardID];
        otpVC.numb = [self.countryCodeLabelOutlet.text stringByAppendingString:self.numberTextField.text];
        otpVC.paramsDict = requestDict;
        [self.numberTextField resignFirstResponder];
        [self.createActivityIndicatorOutlet stopAnimating];
        [self.navigationController pushViewController:otpVC animated:YES];
    }
    else
    {   [self.createActivityIndicatorOutlet stopAnimating];
        [self showAlertFromTop:phoneNumberIsInvalid];
    }
}

//-(void)refreshApiCheck
//{
//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:mSignupFirstTime];
//                      [[NSUserDefaults standardUserDefaults] synchronize];
//                      [[NSUserDefaults standardUserDefaults]setObject:user.username forKey:@"newRegisterUsername"];
//                      [[NSUserDefaults standardUserDefaults] synchronize];
//                      [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"recent_login"];
//                      [[NSUserDefaults standardUserDefaults] synchronize];
//                      [Helper storeUserLoginDetails:user];
//                      NSDictionary *param = [CommonMethods updateDeviceDetailsForAdmin];
//                      [WebServiceHandler logUserDevice:param andDelegate:self];
//                      [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
//                      [self.view endEditing:YES];
//
//                      // creating connection
//                      MQTT *mqttModel = [MQTT sharedInstance];
//                      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//                          [mqttModel createConnection];
//                      });
//}

/**
 To show error Message according to error.
 
 @param message message associated to error.
 */
-(void)showAlertFromTop:(NSString *)message {
    [self.createActivityIndicatorOutlet stopAnimating];
    [self.activityViewOutlet stopAnimating];
    [self.createButton setTitle:NSLocalizedString(signupButtonTitle, signupButtonTitle) forState:UIControlStateNormal];
    labelForErrorMessg.text = NSLocalizedString(message, message);
    labelForErrorMessg.hidden = NO;
    [self performSelector:@selector(hideLabel) withObject:nil afterDelay:2];//1sec
}

-(void)hideLabel{
    labelForErrorMessg.hidden= YES;
}


#pragma mark
#pragma mark- Button Actions

/**
 Create button will call web service if all mandatory fields are done.
 
 @param sender createButton.
 */
- (IBAction)createButtonAction:(id)sender {
    //checking username is valid or not.
    
    if (invalidUsername || invalidNumber || invalidEmail)
    {
        if(invalidEmail)
        {
            [self showAlertFromTop:emailIsInvalid];
        }
        if(invalidUsername)
        {
            [self showAlertFromTop:usernameIsInvalid];
        }
        
        if(invalidNumber)
        {
            [self showAlertFromTop:phoneNumberIsInvalid];
        }

    }
    else if(userNotCheck || numberNotCheck || emailNotCheck)
    {
        if(emailNotCheck)
        {
            [self showAlertFromTop:mEmailAlreadyRegistered];
        }
        if(userNotCheck)
        {
            [self showAlertFromTop:mUsernameRegisteredAlert];
        }
        
        if(numberNotCheck)
        {
            [self showAlertFromTop:mNumberAlreadyRegistered];
        }
    }
    else if (!self.agreeButtonOutlet.selected)
    {
        [self showAlertFromTop:mAgreeWithTermsAndPrivacy];
    }
    else{
    if (ThumbnailimagePath) {
            self.createActivityIndicatorOutlet.hidden = NO;
           [self.createActivityIndicatorOutlet startAnimating];
            self.createButton.enabled = NO;
            [self.activityViewOutlet startAnimating];
            [self uploadingImageToCloudinary:ThumbnailimagePath];
        }
        else {
            self.createActivityIndicatorOutlet.hidden = NO;
            [self.activityViewOutlet startAnimating];
            self.createButton.enabled = NO;
            [self requestForSignUp];
        }
    }
}

/**
 This Button action will open privacy page.
 
 @param sender privacyButton.
 */
- (IBAction)privacyPolicyButtonAction:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://cellableapp.com/en/privacy"]];
    
//    WebViewForDetailsVc *newView = [self.storyboard instantiateViewControllerWithIdentifier:mDetailWebViewStoryBoardId];
//    newView.showTermsAndPolicy = NO;
//    [self.navigationController pushViewController:newView animated:YES];

}

- (IBAction)termsConditionsButtonAction:(id)sender {
    
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://cellableapp.com/en/terms"]];
    
//    WebViewForDetailsVc *newView = [self.storyboard instantiateViewControllerWithIdentifier:mDetailWebViewStoryBoardId];
//    newView.showTermsAndPolicy = YES;
//    [self.navigationController pushViewController:newView animated:YES];
}

/**
 This button is to set the profile image.
 
 @param sender set profilePicButton.
 */
- (IBAction)editProfileImageButton:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(alertCancel, alertCancel) style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *importFromFacebook = [UIAlertAction actionWithTitle:NSLocalizedString(importProfilePicFromFacebook, importProfilePicFromFacebook) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        FBLoginHandler *handler = [FBLoginHandler sharedInstance];
        [handler getProfileDetailsFromFaceBook:self];
        [handler setDelegate:self];}];
    UIAlertAction *takePhoto = [UIAlertAction actionWithTitle:NSLocalizedString(takePhotoFromCamera, takePhotoFromCamera) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self checkCameraPermissionsStatus];}];
    UIAlertAction *chooseLibrary = [UIAlertAction actionWithTitle:NSLocalizedString(chooseFromLibrary, chooseFromLibrary) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
        [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
            [self openCustomLibrary];
        } failureBlock:^(NSError *error) {
            if (error.code == ALAssetsLibraryAccessUserDeniedError) {
                NSLog(@"user denied access, code: %zd", error.code);
                [self galleryPermissionDenied];
            } else {
                NSLog(@"Other error code: %zd", error.code);
            }
        }];
    }];
    
    UIAlertAction *removeCurrentPhoto = [UIAlertAction actionWithTitle:NSLocalizedString(removeCurrentProfilePic, removeCurrentProfilePic) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        self.profileImageView.image = [UIImage imageNamed:mDefualtPP];
        self.editImageOnProfileView.hidden = YES ;
        self->ThumbnailimagePath = nil ;
        }];
    [alertController addAction:cancel];
   // [alertController addAction:importFromFacebook];
    [alertController addAction:takePhoto];
    [alertController addAction:chooseLibrary];
    if(!self.editImageOnProfileView.hidden){
    [alertController addAction:removeCurrentPhoto];
    }
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;

     self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:17.0/255.0f green:42.0/255.0f blue:70.0/255.0f alpha:1.0];

    [[UINavigationBar appearance] setTranslucent:NO];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
   [self presentViewController:alertController animated:YES completion:nil];
}

/**
 This method will open library to choose profile picture.
 */
-(void)openCustomLibrary {
    TWPhotoPickerController *photoPickr = [[TWPhotoPickerController alloc] init];
    photoPickr.viewFromProfileSelector = @"itisForProfilePhoto";
    photoPickr.cropBlock = ^(UIImage *image) {
        self->updateFbImageOnlyFirstTime = NO;
        
        if(image)
        {
            self.editImageOnProfileView.hidden = NO;
        }
        [self.profileImageView setImage:image];
        
        NSString *imageName = [NSString stringWithFormat:@"%@%@.png",@"Image",[self getCurrentTime]];
        NSData *imgData1 = UIImageJPEGRepresentation(image, 0.5f);
        //to get the image path.
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths objectAtIndex:0];
        NSString* imagePath = [documentsDirectory stringByAppendingPathComponent:imageName];
        [imgData1 writeToFile:imagePath atomically:NO];
        
        self->ThumbnailimagePath = imagePath ;
    };
    [self presentViewController:photoPickr animated:YES completion:NULL];
}


/**
 This method will check camera permission Status.
 */
-(void)checkCameraPermissionsStatus {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusAuthorized) { // authorized
        [self openCamera];
    }
    else if(status == AVAuthorizationStatusDenied){ // denied
        [self cameraPermissionDenied];
    }
    else if(status == AVAuthorizationStatusRestricted){ // restricted
        
        
    }
    else if(status == AVAuthorizationStatusNotDetermined){ // not determined
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(granted){ // Access has been granted ..do something
                    [self openCamera];
                } else { // Access denied ..do something
                    [self cameraPermissionDenied];
                }
            });
        }];
    }
}
-(void)cameraPermissionDenied {
    //askingPermissionVcStoryBoardId
    AskingPermissonViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mPermissionStoryboardID];
    newView.title = NSLocalizedString(mcameraPermissionAlertTitle, mcameraPermissionAlertTitle);
    newView.message = NSLocalizedString(mcameraPermissionAlertMessage, mcameraPermissionAlertMessage);
    newView.buttonTitle = NSLocalizedString(mcameraPermissionActionTitle, mcameraPermissionActionTitle);
    newView.navBarTitle = NSLocalizedString(cameraPermissionNavTitle, cameraPermissionNavTitle);
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:newView];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

-(void)galleryPermissionDenied {
    AskingPermissonViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mPermissionStoryboardID];
    newView.title = NSLocalizedString(mgalleryPermissionActionTitle, mgalleryPermissionActionTitle);
    newView.message = NSLocalizedString(mgalleryPermissionAlertMessage, mgalleryPermissionAlertMessage);
    newView.navBarTitle = @"";
    newView.buttonTitle = NSLocalizedString(mgalleryPermissionActionTitle, mgalleryPermissionActionTitle);
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:newView];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

-(void)openCamera {
#if TARGET_IPHONE_SIMULATOR
    [Helper showAlertWithTitle:NSLocalizedString(alertMessage, alertMessage) Message:@"IN SIMULATOR CAMERA IS NOT AVAILABLE" viewController:self];
#else
    self.imgpicker = [[UIImagePickerController alloc] init];
    self.imgpicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.imgpicker.delegate =self;
    [self presentViewController:self.imgpicker animated:YES completion:nil];
#endif
}

- (IBAction)agreeButtonAction:(id)sender {
    if(self.agreeButtonOutlet.selected)
        self.agreeButtonOutlet.selected=NO;
    else
    {
        self.agreeButtonOutlet.selected=YES;
        self.createButton.enabled = [self checkForMandatoryFieldForSignup];
    }
}

- (IBAction)navBarLeftButtonAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}



/**
 Text Field Value Changed check.
 
 @param sender textFileds.
 */
- (IBAction)textFieldValueIsChanged:(id)sender {
    [self checkForMandatoryFieldForSignup];
}

/**
 *   The button action is performed when user taps countrySelector Button.
 */
- (IBAction)countryCodeButton:(id)sender {
    
  EMCCountryPickerController *EMCview = [self.storyboard instantiateViewControllerWithIdentifier:mCountyListStoryboardID];
       //EMCview.previousSelection = previousSelection;
       EMCview.countryDelegate = self;
       UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:EMCview];
     //  [navigation setModalPresentationStyle:UIModalPresentationFullScreen];
      [self.navigationController presentViewController:navigation animated:YES completion:nil];
}
#pragma mark
#pragma mark - CountryCode Picker delegate


-(void)countryController:(id)sender didSelectCountry:(EMCCountry *)chosenCountry
{
    previousSelection = chosenCountry.countryCode;
    NSString *countryDialCode = [VNHDialCode getDialCodeForCountryCode:chosenCountry.countryCode];
    @try {
        
        
        self.countryCodeLabelOutlet.text = [@"+" stringByAppendingString:countryDialCode];
         NSString *imagePath = [NSString stringWithFormat:@"EMCCountryPickerController.bundle/%@", chosenCountry.countryCode];
        UIImage *countryFlag = [UIImage imageNamed:imagePath inBundle:[NSBundle bundleForClass:emc.class] compatibleWithTraitCollection:nil];
        self.imageViewForFlag.image = countryFlag;    }
    @catch (NSException *exception) {
    }
    
    
}

#pragma mark
#pragma mark - Validation Methods

- (BOOL)validatePhone:(NSString *)enteredphoneNumber {
    NSString *phoneNumber = enteredphoneNumber;
    NSString *phoneRegex = @"[23456789][0-9]{6}([0-9]{3})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:phoneNumber];
    return matches;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark-
#pragma mark - Login/Signup Options

- (IBAction)loginOptionButtonAction:(id)sender {
    
    [self hideLabel];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.activityViewOutlet.hidden = YES;
    self.loginViewOutlet.hidden = NO;
    self.signupViewOutlet.hidden = YES;
    self.loginSelectOutlet.backgroundColor =  [UIColor colorWithRed:110.0/255.0f green:128.0/255.0f blue:46.0/255.0f alpha:0.6f];
    self.self.signupSelectOutlet.backgroundColor =[UIColor clearColor];
    self.loginSelectOutlet.selected = YES;
    self.signupSelectOutlet.selected = NO;
}

- (IBAction)signupOptionButtonAction:(id)sender {
    
    [self hideLabel];
    self.loginViewOutlet.hidden = YES;
    self.signupViewOutlet.hidden = NO;
    self.loginSelectOutlet.backgroundColor = [UIColor clearColor] ;
    self.self.signupSelectOutlet.backgroundColor =[UIColor colorWithRed:110.0/255.0f green:128.0/255.0f blue:46.0/255.0f alpha:0.6f];
    self.loginSelectOutlet.selected = NO;
    self.signupSelectOutlet.selected = YES;
    
    [self gettingDeviceid];
    updateFbImageOnlyFirstTime = YES;
    cloundinaryCreditinals =[[NSUserDefaults standardUserDefaults]objectForKey:cloudinartyDetails];
    [self createLabelToSHowErrorMessage];
    self.SignUpType = mUserNormalSignup ;
    
    if (self.signUpWithFacebook)
    {
        self.SignUpType = mUserFacebookSignup ;
        if (self.fbResponseDetails[@"email"])
        {
        self.emailTextField.text = flStrForObj(self.fbResponseDetails[@"email"]);
        }
        else
        {
            _emailTextField.hidden = false;
            _iconEmail.hidden = false;
            _lineDifferWayOut.hidden = false;
            _imageForEmailCheck.hidden = false;
        }
        
        self.nameTextField.text = _fbName;
        self.userNameTextField.text = [NSString stringWithFormat:@"%@%@cellable",self.nameTextField.text.lowercaseString,_fbLastName.lowercaseString];
        
        NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=400&heigth=400",self.fbResponseDetails[@"id"]]];
        
        profilePicUrl = pictureURL.absoluteString;
        if(profilePicUrl.length > 0)
        {
            [self.profileImageView setImageWithURL:[NSURL URLWithString:profilePicUrl] placeholderImage: [UIImage imageNamed:mDefualtPP]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
            self.editImageOnProfileView.hidden = NO;
        }
        else
        {
            self.editImageOnProfileView.hidden = YES ;
        }
    }
    else if (self.signUpWithGooglePlus)
        
    {
        self.SignUpType = mUserGooglePlusSignup ;
        self.nameTextField.text = self.googlePlusName;
         self.userNameTextField.text = [NSString stringWithFormat:@"%@cellable",self.nameTextField.text.lowercaseString];
        self.emailTextField.text = self.googlePlusEmail;
        profilePicUrl = self.googlePlusProfileUrl ;
        if(profilePicUrl.length>0)
        {
            [self.profileImageView setImageWithURL:[NSURL URLWithString:profilePicUrl] placeholderImage: [UIImage imageNamed:mDefualtPP]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
            self.editImageOnProfileView.hidden = NO;
        }
        else
        {
            self.editImageOnProfileView.hidden = YES ;
        }
        
    }
    else if (self.signUpWithApple)
       {
           self.SignUpType = mUserApplePlusSignup ;
           self.nameTextField.text = self.appleName;
           self.emailTextField.text = self.appleEmail;
           self.userNameTextField.text = [NSString stringWithFormat:@"%@%@cellable",self.userAppleName1.lowercaseString,self.appleName.lowercaseString];

           if(profilePicUrl.length>0)
           {
               [self.profileImageView setImageWithURL:[NSURL URLWithString:profilePicUrl] placeholderImage: [UIImage imageNamed:mDefualtPP]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
               self.editImageOnProfileView.hidden = NO;
           }
           else
           {
               self.editImageOnProfileView.hidden = YES ;
           }
           
       }

    NSString *countryCode = [NSString stringWithFormat:@"%@",flStrForObj([[NSLocale currentLocale] objectForKey:NSLocaleCountryCode])];
    previousSelection = countryCode ;
    
    NSString *countryDialCode = flStrForObj([VNHDialCode getDialCodeForCountryCode:countryCode]);
    
        if(countryDialCode.length)
        {
            self.countryCodeLabelOutlet.text = [@"+" stringByAppendingString:countryDialCode];
        }
        else
        {
            self.countryCodeLabelOutlet.text = @"";
        }
    
        NSString *imagePath = [NSString stringWithFormat:@"EMCCountryPickerController.bundle/%@",countryCode];
        UIImage *countryFlag = [UIImage imageNamed:imagePath inBundle:[NSBundle bundleForClass:emc.class] compatibleWithTraitCollection:nil];
        self.imageViewForFlag.image = countryFlag;
    self.navigationController.navigationBar.shadowImage = nil;
    
    if([[Helper deviceToken] containsString:@"failed To fetch Push Token"])
    {
//    NSString *token = [[FIRInstanceID instanceID] token];
//    [[NSUserDefaults standardUserDefaults] setObject:token forKey:mdeviceToken];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    }

}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end

//
//  UserDetails.h
//
//  Created by Rahul Sharma on 19/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDetails : NSObject

- (instancetype)initWithDictionary:(NSDictionary *)response;


@property (nonatomic,copy) NSString *fullName;
@property (nonatomic,copy) NSString *username;
@property (nonatomic,copy) NSString *userId;
@property (nonatomic,copy) NSString *deviceId;
@property (nonatomic,copy) NSString *facebookId;
@property (nonatomic,copy) NSString *facebookVerified;
@property (nonatomic,copy) NSString *googleVerified;
@property (nonatomic,copy) NSString *is_strip_connected;
@property (nonatomic,copy) NSString *googleId;
@property (nonatomic,copy) NSString *profilePicUrl;
@property (nonatomic,copy) NSString *accountId;

@property (nonatomic,copy) NSString *city;
@property (nonatomic,copy) NSString *countrySname;
@property (nonatomic,copy) NSString *phoneNumber;
@property (nonatomic,copy) NSString *website;
@property (nonatomic,copy) NSString *bio ;
@property (nonatomic,copy) NSString *token;
@property (nonatomic,copy) NSString *pushToken;
@property (nonatomic,copy) NSString *mqttId;
@property (nonatomic,copy) NSString *message;
@property (nonatomic,copy) NSString *email;
@property NSInteger code;


@end

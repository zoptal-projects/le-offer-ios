//
//  ProfileHeadersTableViewCell.h
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileViewController.h"

@interface ProfileHeadersTableViewCell : UITableViewCell <WebServiceHandlerDelegate>
{
   NSInteger sellingCurrentIndex,soldCurrentIndex,favouritesCurrentIndex,sellingPaging,soldPaging,favouritePaging,sellingDisplayedIndex,soldDisplayedIndex,favouriteDisplayedIndex ;
    BOOL soldPostServiceCall;
}

@property (nonatomic,strong)ProfileViewController *profileVC;
@property (nonatomic,strong)Profile *profileObj;

@property (weak, nonatomic) IBOutlet UIButton *sellingButton;

@property (weak, nonatomic) IBOutlet UIButton *exchangesButton;

@property (weak, nonatomic) IBOutlet UIButton *soldButton;

@property (weak, nonatomic) IBOutlet UIButton *likesButton;
@property (weak, nonatomic) IBOutlet UIView *sellingUnderView;
@property (weak, nonatomic) IBOutlet UIView *exchangesUnderView;

@property (weak, nonatomic) IBOutlet UIView *soldUnderView;

@property (weak, nonatomic) IBOutlet UIView *likesUnderView;
- (IBAction)sellingButtonAction:(id)sender;

- (IBAction)exchangesButtonAction:(id)sender;

- (IBAction)soldButtonAction:(id)sender;

- (IBAction)likesButtonAction:(id)sender;

-(void)showingBackgroundMessageFor:(UICollectionView *)collectionView type:(NSInteger)type;

- (void)calculateCollectionViewHeightForTag :(NSInteger )tag;


-(void)selectedButtonIndex:(NSUInteger)index;
@property (weak, nonatomic) IBOutlet UIView *userProfileHeader;

@property (weak, nonatomic) IBOutlet UIView *memberProfileHeader;

@property (weak, nonatomic) IBOutlet UIButton *memberSellingButton;

@property (weak, nonatomic) IBOutlet UIButton *memberSoldButton;
@property (weak, nonatomic) IBOutlet UIView *memberSellingUnderView;

@property (weak, nonatomic) IBOutlet UIView *memberSoldUnderView;

@end


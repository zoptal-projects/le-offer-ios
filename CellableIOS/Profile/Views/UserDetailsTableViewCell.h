//
//  UserDetailsTableViewCell.h
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileViewController.h"
#import "KILabel.h"
#import "FBLoginHandler.h"
//#import "PaypalViewController.h"
#import <MessageUI/MessageUI.h>

@interface UserDetailsTableViewCell : UITableViewCell <WebServiceHandlerDelegate, FBLoginHandlerDelegate,MFMailComposeViewControllerDelegate>

@property (nonatomic,strong)ProfileViewController *profileVC;
@property (nonatomic,strong)Profile *profileObj;
@property BOOL isAPIcall;
@property (weak, nonatomic) IBOutlet UIView *profileView;

@property (weak, nonatomic) IBOutlet UILabel *profileName;
@property (weak, nonatomic) IBOutlet UIView *contactView;
@property (weak, nonatomic) IBOutlet UIImageView *verifyFacebookImage;

@property (weak, nonatomic) IBOutlet UIImageView *googleVerifyImage;
@property (weak, nonatomic) IBOutlet UIImageView *verifyEmailImage;
@property (weak, nonatomic) IBOutlet UIImageView *verifyMobileImage;
@property (weak, nonatomic) IBOutlet UIImageView *payapalImage;
@property (weak, nonatomic) IBOutlet UIButton *stripeConnectBtnOut;

- (IBAction)verifyFacebookButtonAction:(id)sender;
- (IBAction)verifyGoogleButtonAction:(id)sender;

- (IBAction)verifyEmailButtonAction:(id)sender;
- (IBAction)verifyMobileButtonAction:(id)sender;

- (IBAction)paypalButtonAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *verifyFacebookButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *verifyGoogleButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *verifyEmailButtonOutlet;

@property (weak, nonatomic) IBOutlet UIButton *verifyMobileButtonOutlet;

@property (weak, nonatomic) IBOutlet UIButton *paypalButtonOutlet;

@property (weak, nonatomic) IBOutlet UILabel *postsCountLabel;

@property (weak, nonatomic) IBOutlet UILabel *followersCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelForWebsite;

@property (weak, nonatomic) IBOutlet KILabel *bioLabel;
@property (weak, nonatomic) IBOutlet UIButton *starButton1;
@property (weak, nonatomic) IBOutlet UIButton *starButton2;

@property (weak, nonatomic) IBOutlet UIButton *starButton3;
@property (weak, nonatomic) IBOutlet UIButton *starButton4;
@property (weak, nonatomic) IBOutlet UIButton *starButton5;
@property (weak, nonatomic) IBOutlet UILabel *averageRatingCount;
@property (weak, nonatomic) IBOutlet UIView *verifyView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintRatingView;
- (IBAction)followersButtonAction:(id)sender;

-(void)setProfileDetails :(ProfileDetails *)profile;

- (IBAction)followingButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *contactButtonOutlet;

@property (weak, nonatomic) IBOutlet UIButton *followButtonOutlet;

- (IBAction)contactButtonAction:(id)sender;
- (IBAction)followButtonAction:(id)sender;



-(void)getProfileDetails;

@end

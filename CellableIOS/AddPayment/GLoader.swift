//
//  GLoader.swift
//  Happego
//
//  Created by eshan Cheema on 29/04/19.
//  Copyright © 2019 eshan Cheema. All rights reserved.
//

import UIKit
class GLoader: UIView
{
    var superView = UIView()
    
    @IBOutlet weak var vwBackground: UIView!
    @IBOutlet var vwLoader: UIView!
    @IBOutlet weak var vwDots: DotsLoader!
    //MARK: - Initializer -
    override init(frame: CGRect) { // For using custom view in code
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) { // For using custom view in IB
        super.init(coder:aDecoder)
        commonInit()
    }
    //MARK: - Methods -
    private func commonInit(){
        Bundle.main.loadNibNamed("Gloader", owner: self, options: nil)
        addSubview(vwLoader)
        self.vwBackground.layer.cornerRadius = 5.0
        self.vwBackground.clipsToBounds = true
    }
    func show(){
       // self.addSubview(superView)
        self.superView.addSubview(self)
        self.superView.isUserInteractionEnabled = false
        vwLoader.frame = self.superView.bounds
        vwLoader.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.vwDots.startAnimating()
    }
    func hide() {
        self.vwDots.stopAnimating()
        self.superView.isUserInteractionEnabled = true
        self.removeFromSuperview()
    }

}

//
//  APIManager.swift
//  Liquid
//
//  Created by Zoptal on 12/10/19.
//  Copyright © 2019 eshan Cheema. All rights reserved.
//
import UIKit
import Foundation
import SwiftyJSON
import Alamofire

class APIManager {
    // MARK:- Rider Signup -
    class func APIRiderSignup(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_SIGNUP_RIDER, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            print(JSON)
            
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                }
            }
            
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- Driver Signup -
    class func APIDriverSignup(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        
        print(params)
        ServerManager.shared.Post(request: API_SIGNUP_DRIVER, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                 
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- Send OTP -
    class func APISendOtp(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        print(params)
        ServerManager.shared.Post(request: API_SEND_OTP, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            print(JSON)
            
            if JSON["code"].intValue == 200
            {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                   
                }
            }
            
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    // MARK:- Login -
    
    class func APILogin(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_SIGNIN, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                   
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- Forgot Password -
    
    class func APIForgotPassword(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_FORGOT_PASSWORD, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                   
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    
    // MARK:- Forgot APILogout -
      
      class func APILogout(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
          loader.show()
          ServerManager.shared.Put(request: API_LOGOUT, params: params, successHandler: { (JSON, Status) in
              loader.hide()
            print(JSON)
              if JSON["code"].intValue == 200 {
                  completion(JSON)
              }
              else if JSON["code"].intValue == 203{
                  AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                      UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                      UserDefaults.standard.synchronize()
                     
                  }
              }
              else {
                  AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
              }
          }) { (error) in
              loader.hide()
              AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
          }
      }
    
    
    // MARK:- Change Password -
    
    class func APIChangePassword(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        
        ServerManager.shared.Put(request: API_CHANGE_PASSWORD, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            print(JSON)
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                  
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    //ApiUpdateStripeConnect
    
    // MARK:- Edit Profile -
    
    class func APIEditProfile(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        
        ServerManager.shared.Put(request: API_EDIT_PROFILE, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                   
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- New Trip -
    
    class func APINewTrip(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_NEW_TRIP, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            print(JSON)
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                  
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- Driver  Trip History -
    
    class func APIDriverTripHis(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_DRIVER_TRIP_HISTORY, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                 
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    
    // MARK:- Driver  Trip History -
       
       class func ApiConnectStatus(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
           loader.show()
           ServerManager.shared.Post(request: ApiUpdateStripeConnectApi, params: params, successHandler: { (JSON, Status) in
               loader.hide()
               if JSON["code"].intValue == 200 {
                   completion(JSON)
               }
               else if JSON["code"].intValue == 203{
                   AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                       UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                       UserDefaults.standard.synchronize()
                    
                   }
               }
               else {
                   AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
               }
           }) { (error) in
               loader.hide()
               AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
           }
       }
       
    
    
    // MARK:- Driver  Trip Details -
    
    class func APIDriverTripDetails(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_DRIVER_TRIP_DETAIL, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    
    // MARK:- Driver  StripConnect -
       
       class func APIDriverConnect(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
           loader.show()
           ServerManager.shared.Post(request: API_STRIPECONNECT, params: params, successHandler: { (JSON, Status) in
               loader.hide()
               if JSON["code"].intValue == 200 {
                   completion(JSON)
               }
               else if JSON["code"].intValue == 203{
                   AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                       UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                       UserDefaults.standard.synchronize()
                  
                   }
               }
               else {
                   AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
               }
           }) { (error) in
               loader.hide()
               AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
           }
       }
    
    // MARK:- Driver  StripDisConnect -
    
    class func APIDriverDisConnect(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: DISCONNECT_STRIPE, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
              
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    
    // MARK:- Driver Payment Setup -

    class func APIDriverPaymentSetup(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_DRIVER_PAYMENT_SETUP, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
               
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- DRIVER Payment Methods -
    
    class func APIDriverPaymentMethods(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_DRIVER_PAYMENT_DETAILS, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- Driver Search RiderName -
    
    class func APIDriverSearchRider(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
//        loader.show()
        ServerManager.shared.Post(request: API_USER_SEARCH, params: params, successHandler: { (JSON, Status) in
//            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                
                }
            }
            
            
            else {
//                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
//            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- Riders Trip List -
    
    class func APIRiderTrip(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_RIDER_TRIPS, params: params, successHandler: { (JSON, Status) in
            print(JSON)
            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
        
                }
            }
            else
            {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- Riders DeleteCard -
       
       class func APIRiderDeleteCard(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
           loader.show()
           ServerManager.shared.Post(request: API_RIDER_DELETECARD, params: params, successHandler: { (JSON, Status) in
               print(JSON)
               loader.hide()
               if JSON["code"].intValue == 200 {
                   completion(JSON)
               }
               else if JSON["code"].intValue == 203{
                   AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                       UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                       UserDefaults.standard.synchronize()
               
                   }
               }
               else
               {
                   AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
               }
           }) { (error) in
               loader.hide()
               AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
           }
       }
    
    
    // MARK:- Riders Payment Status -
    
    class func APIRiderPayment(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_RIDER_PAYMENT_STATUS, params: params, successHandler: { (JSON, Status) in
            loader.hide()
                print(JSON)
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
              
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- Riders Payment Setup -
    
    class func APIRiderPaymentSetup(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_RIDER_ADD_CARD, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            print(JSON)
            if JSON["code"].intValue == 200
            {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203
            {
            AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
                
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- Riders Cards -
    
    class func APIRiderCards(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_RIDER_CARDS, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
              
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- Riders Make Card Default -
    
    class func APIRiderMakeDefault(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
        loader.show()
        ServerManager.shared.Post(request: API_RIDER_MAKE_CARD_DEFAULT, params: params, successHandler: { (JSON, Status) in
            loader.hide()
            if JSON["code"].intValue == 200 {
                completion(JSON)
            }
            else if JSON["code"].intValue == 203{
                AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                    UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                    UserDefaults.standard.synchronize()
              
                }
            }
            else {
                AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
            }
        }) { (error) in
            loader.hide()
            AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
        }
    }
    
    // MARK:- Riders Resend Receipt -
       
       class func APIRiderResendReceipt(params:[String:Any], viewController:UIViewController, completion:@escaping(JSON) -> Void){
           loader.show()
           ServerManager.shared.Post(request: API_RIDER_RESEND_INVOICE, params: params, successHandler: { (JSON, Status) in
               loader.hide()
               if JSON["code"].intValue == 200 {
                   completion(JSON)
               }
               else if JSON["code"].intValue == 203{
                   AppManager.showAlertLogout(viewControler: viewController, title: kAlertTitle, message: JSON["message"].stringValue) {
                       UserDefaults.SFSDefault(setBool: false, forKey: kIsLogin)
                       UserDefaults.standard.synchronize()
                 
                   }
               }
               else {
                   AppManager.showAlert(viewControler: viewController, message: JSON["message"].stringValue)
               }
           }) { (error) in
               loader.hide()
               AppManager.showAlert(viewControler: viewController, message: error!.localizedDescription)
           }
       }
}

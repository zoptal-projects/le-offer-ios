//
//  Extensions.swift
//  Liquid
//
//  Created by Gurwinder Singh on 12/10/19.
//  Copyright © 2019 Zoptal Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import Dispatch
import Foundation

//--------------MARK:- NSUserDefaults Extension -
public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPhone11,2":                              return "iPhone XS"
        case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
        case "iPhone11,8":                              return "iPhone XR"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}
extension UserDefaults
{
    class func SFSDefault(setIntegerValue integer: Int , forKey key : String){
        UserDefaults.standard.set(integer, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func SFSDefault(setObject object: Any , forKey key : String){
        UserDefaults.standard.set(object, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func SFSDefault(setValue object: Any , forKey key : String){
        UserDefaults.standard.setValue(object, forKey: key)
        UserDefaults.standard.synchronize()
    }
    class func SFSDefault(setBool boolObject:Bool  , forKey key : String){
        UserDefaults.standard.set(boolObject, forKey : key)
        UserDefaults.standard.synchronize()
    }
    
    class func SFSDefault(integerForKey  key: String) -> Int{
        let integerValue : Int = UserDefaults.standard.integer(forKey: key) as Int
        UserDefaults.standard.synchronize()
        return integerValue
    }
    
    class func SFSDefault(objectForKey key: String) -> Any{
        let object : Any = UserDefaults.standard.object(forKey: key)! as Any
        UserDefaults.standard.synchronize()
        return object
    }
    
    class func SFSDefault(valueForKey  key: String) -> Any
    {
        let value : Any = UserDefaults.standard.value(forKey: key)! as Any
        UserDefaults.standard.synchronize()
        return value
    }
    class func SFSDefault(boolForKey  key : String) -> Bool{
        let booleanValue : Bool = UserDefaults.standard.bool(forKey: key) as Bool
        UserDefaults.standard.synchronize()
        return booleanValue
    }
    
    class func SFSDefault(removeObjectForKey key: String)
    {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    //Save no-premitive data
    class func SFSDefault(setArchivedDataObject object: Any , forKey key : String) {
        if let data  = NSKeyedArchiver.archivedData(withRootObject: object) as? Data {
            UserDefaults.standard.set(data, forKey: key)
            UserDefaults.standard.synchronize()
        }
    }
    class func SFSDefault(getUnArchiveObjectforKey key: String) -> Any? {
        var objectValue : Any?
        if  let storedData  = UserDefaults.standard.object(forKey: key) as? Data {
            objectValue   =  NSKeyedUnarchiver.unarchiveObject(with: storedData) as Any?
            UserDefaults.standard.synchronize()
            return objectValue!;
        } else {
            objectValue = "" as Any?
            return objectValue!
        }
    }
}

extension UIColor
{
    convenience public init(hex: String, alpha: CGFloat = 1.0) {
        let number = UInt(hex, radix: 16)!
        let red = CGFloat((number & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((number & 0xFF00) >> 8) / 255.0
        let blue = CGFloat((number & 0xFF)) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    class  func smNavigationBarColor()->UIColor {
        return UIColor(red: 48/255, green: 178/255, blue: 236/255, alpha: 1.0)
        
    }
    class  func smGreenColor()->UIColor {
        return UIColor(red: 101/255, green: 195/255, blue: 149/255, alpha: 1.0)
    }
    
    class  func smGreyBorderColor()->UIColor {
        return UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1.0)
    }
    
    class  func sepratorHeaderColor()->UIColor {
        return UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)
    }
    
    class  func sepratorDarkHeaderColor()->UIColor {
        return UIColor(red: 128.0/255.0, green: 151.0/255.0, blue: 167.0/255.0, alpha: 1.0)
    }
    
    class  func sepratorColor()->UIColor {
        return UIColor(red: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
    }
    
    class func smPlaceHolderColor()->UIColor {
        return UIColor(red: 161.0/255.0, green: 161.0/255.0, blue: 162.0/255.0, alpha: 1.0)
    }
    
    class func smRGB(smRed r:CGFloat , smGrean g: CGFloat , smBlue b: CGFloat)->UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
    }
    
    class  func getRandomColor() -> UIColor{
        return UIColor(red: CGFloat(drand48()), green: CGFloat(drand48()), blue: CGFloat(drand48()), alpha: 1.0)
    }
    
    class func colorFromHex(hexString:String) -> UIColor {
        func clean(hexString: String) -> String {
            var cleanedHexString = String()
            // Remove the leading "#"
            if(hexString[hexString.startIndex] == "#") {
                let index = hexString.index(hexString.startIndex, offsetBy: 1)
                cleanedHexString = String(hexString[index...])
            }
            // TODO: Other cleanup. Allow for a "short" hex string, i.e., "#fff"
            return cleanedHexString
        }
        let cleanedHexString = clean(hexString: hexString)
        // If we can get a cached version of the colour, get out early.
        if let cachedColor = UIColor.getColorFromCache(hexString: cleanedHexString) {
            return cachedColor
        }
        // Else create the color, store it in the cache and return.
        let scanner = Scanner(string: cleanedHexString)
        var value:UInt32 = 0
        // We have the hex value, grab the red, green, blue and alpha values.
        // Have to pass value by reference, scanner modifies this directly as the result of scanning the hex string. The return value is the success or fail.
        if(scanner.scanHexInt32(&value)){
            // intValue = 01010101 11110111 11101010    // binary
            // intValue = 55       F7       EA          // hexadecimal
            //                     r
            //   00000000 00000000 01010101 intValue >> 16
            // & 00000000 00000000 11111111 mask
            //   ==========================
            // = 00000000 00000000 01010101 red
            //            r        g
            //   00000000 01010101 11110111 intValue >> 8
            // & 00000000 00000000 11111111 mask
            //   ==========================
            // = 00000000 00000000 11110111 green
            //   r        g        b
            //   01010101 11110111 11101010 intValue
            // & 00000000 00000000 11111111 mask
            //   ==========================
            // = 00000000 00000000 11101010 blue
            
            let intValue = UInt32(value)
            let mask:UInt32 = 0xFF
            
            let red = intValue >> 16 & mask
            let green = intValue >> 8 & mask
            let blue = intValue & mask
            
            // red, green, blue and alpha are currently between 0 and 255
            // We want to normalise these values between 0 and 1 to use with UIColor.
            let colors:[UInt32] = [red, green, blue]
            let normalised = normalise(colors: colors)
            
            let newColor = UIColor(red: normalised[0], green: normalised[1], blue: normalised[2], alpha: 1)
            UIColor.storeColorInCache(hexString: cleanedHexString, color: newColor)
            
            return newColor
        }
            // We couldn't get a value from a valid hex string.
        else {
            // print("Error: Couldn't convert the hex string to a number, returning UIColor.whiteColor() instead.")
            return UIColor.white
        }
    }
    
    // Takes an array of colours in the range of 0-255 and returns a value between 0 and 1.
    private class func normalise(colors: [UInt32]) -> [CGFloat]{
        var normalisedVersions = [CGFloat]()
        
        for color in colors{
            normalisedVersions.append(CGFloat(color % 256) / 255)
        }
        return normalisedVersions
    }
    
    // Caching
    // Store any colours we've gotten before. Colours don't change.
    private static var hexColorCache = [String : UIColor]()
    
    private class func getColorFromCache(hexString: String) -> UIColor? {
        guard let color = UIColor.hexColorCache[hexString] else {
            return nil
        }
        return color
    }
    
    private class func storeColorInCache(hexString: String, color: UIColor) {
        if UIColor.hexColorCache.keys.contains(hexString) {
            return // No work to do if it is already there.
        }
        UIColor.hexColorCache[hexString] = color
    }
    
    private class func clearColorCache() {
        UIColor.hexColorCache.removeAll()
    }
}
extension UITextView: UITextViewDelegate {
    // Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    // The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            return placeholderText
        } set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    // When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    //
    // - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
        }
    }
    // Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding + 10
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
          placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
}


extension UITextField {
    
    @IBInspectable var placeholderColor: UIColor {
        get {
            guard let currentAttributedPlaceholderColor = attributedPlaceholder?.attribute(NSAttributedString.Key.foregroundColor, at: 0, effectiveRange: nil) as? UIColor else { return UIColor.clear }
            return currentAttributedPlaceholderColor
        }
        set {
            guard let currentAttributedString = attributedPlaceholder else { return }
            let attributes = [NSAttributedString.Key.foregroundColor : newValue]
            
            attributedPlaceholder = NSAttributedString(string: currentAttributedString.string, attributes: attributes)
        }
    }
    
    func textFieldPlaceholderColor(_ color:UIColor){
        let attibutedStr:NSAttributedString=NSAttributedString(string: self.placeholder!, attributes: [NSAttributedString.Key.foregroundColor: color]);
        self.attributedPlaceholder=attibutedStr
    }
    
    func setLeftPaddingImageIcon(_ imageicon:UIImage){
        let image1: UIImage? = imageicon
        if image1 != nil {
            let view: UIView? = UIView()
            view!.frame = CGRect(x: 0, y: 0, width:40, height: self.frame.size.height)
            let imageView = UIButton()
            imageView.frame = CGRect(x: 0, y: 0, width: 20, height: self.frame.size.height/2)
            imageView.setImage(imageicon, for: .normal)
            imageView.center = (view?.center)!
            imageView.contentMode = .scaleAspectFit
            view?.addSubview(imageView)
            self.leftView=view
            self.leftViewMode=UITextField.ViewMode.always
        }
    }
    func setLeftCountryImageIcon(_ imageicon:UIImage){
        let image1: UIImage? = imageicon
        if image1 != nil {
            let view: UIView? = UIView()
            view!.frame = CGRect(x: 0, y: 0, width:50, height: self.frame.size.height)
            let imageView = UIButton()
            imageView.frame = CGRect(x: 0, y: 0, width: 25, height: self.frame.size.height/2)
            imageView.setImage(imageicon, for: .normal)
            imageView.center = (view?.center)!
            imageView.contentMode = .scaleAspectFit
            view?.addSubview(imageView)
            self.leftView=view
            self.leftViewMode=UITextField.ViewMode.always
        }
    }
    
    func setLeftPaddingView(){
        let paddingView=UIView()
        paddingView.frame=CGRect(x: 0, y: 0, width: 10, height: 30)
        self.leftView=paddingView
        self.layer.borderColor = UIColor.init(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0).cgColor
        self.autocorrectionType = .no
        self.leftViewMode=UITextField.ViewMode.always
    }
    
    func setRightPaddingImageIcon(_ imageicon:UIImage){
        let image1: UIImage? = imageicon
        if image1 != nil {
            let view: UIView? = UIView()
            view!.frame = CGRect(x: 0, y: 0, width:30 , height: self.frame.size.height)
            let imageView =  UIButton()
            imageView.frame=CGRect(x: 0, y: 0, width:30 , height: self.frame.size.height)
            imageView.setImage(imageicon, for: .normal)
            imageView.contentMode = .scaleAspectFit
            view?.addSubview(imageView)
            self.rightView = view
            self.rightViewMode = UITextField.ViewMode.always
            
        }
    }
    
    func setRightPaddingView(){
        let paddingView=UIView()
        paddingView.frame=CGRect(x: 0, y: 0, width: 10, height: 30)
        self.rightView=paddingView
        self.rightViewMode=UITextField.ViewMode.always
    }
    
    func setLeftAndRightPadding() {
        self.setLeftPaddingView()
        self.setRightPaddingView()
        // self.setTextAlginmentWithLanguage()
    }
    
    func setTextFieldRadiusCorner(_ cornerRadius:CGFloat){
        self.layer.cornerRadius=cornerRadius;
        self.layer.masksToBounds=true;
    }
    
    func setTextFieldBoader(_ borderW:CGFloat,borderC:UIColor){
        self.layer.borderWidth=borderW;
        self.layer.borderColor=borderC.cgColor;
    }
    
    func roundCornersView(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

//MARK
//MARK :- UIView Extension

extension UIView {
    func makeCircular() {
        self.layer.cornerRadius = self.bounds.width/2
        self.layer.masksToBounds = true
        // self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func dropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        
    }
    /**
     Rounds the given set of corners to the specified radius
     
     - parameter corners: Corners to round
     - parameter radius:  Radius to round to
     */
    func round(corners: UIRectCorner, radius: CGFloat) {
        let mask =   _round(corners: corners, radius: radius)
        self.layer.mask = mask
        
    }
    
    /**
     Rounds the given set of corners to the specified radius with a border
     
     - parameter corners:     Corners to round
     - parameter radius:      Radius to round to
     - parameter borderColor: The border color
     - parameter borderWidth: The border width
     */
    func round(corners: UIRectCorner, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        let mask = _round(corners: corners, radius: radius)
        addBorder(mask: mask, borderColor: borderColor, borderWidth: borderWidth)
    }
    
    /**
     Fully rounds an autolayout view (e.g. one with no known frame) with the given diameter and border
     
     - parameter diameter:    The view's diameter
     - parameter borderColor: The border color
     - parameter borderWidth: The border width
     */
    func fullyRound(diameter: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        layer.masksToBounds = true
        layer.cornerRadius = diameter / 2
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor;
    }
    
    func addBorder(edges: UIRectEdge, color: UIColor = UIColor.white, thickness: CGFloat = 1.0) -> [UIView] {
        var borders = [UIView]()
        func border() -> UIView {
            let border = UIView(frame: CGRect.zero)
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            return border
        }
        
        if edges.contains(.top) || edges.contains(.all) {
            let top = border()
            addSubview(top)
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[top(==thickness)]",
                                               options: [],
                                               metrics: ["thickness": thickness],
                                               views: ["top": top]))
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[top]-(0)-|",
                                               options: [],
                                               metrics: nil,
                                               views: ["top": top]))
            borders.append(top)
        }
        
        if edges.contains(.left) || edges.contains(.all) {
            let left = border()
            addSubview(left)
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[left(==thickness)]",
                                               options: [],
                                               metrics: ["thickness": thickness],
                                               views: ["left": left]))
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[left]-(0)-|",
                                               options: [],
                                               metrics: nil,
                                               views: ["left": left]))
            borders.append(left)
        }
        
        if edges.contains(.right) || edges.contains(.all) {
            let right = border()
            addSubview(right)
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "H:[right(==thickness)]-(0)-|",
                                               options: [],
                                               metrics: ["thickness": thickness],
                                               views: ["right": right]))
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[right]-(0)-|",
                                               options: [],
                                               metrics: nil,
                                               views: ["right": right]))
            borders.append(right)
        }
        
        if edges.contains(.bottom) || edges.contains(.all) {
            let bottom = border()
            addSubview(bottom)
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "V:[bottom(==thickness)]-(0)-|",
                                               options: [],
                                               metrics: ["thickness": thickness],
                                               views: ["bottom": bottom]))
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[bottom]-(0)-|",
                                               options: [],
                                               metrics: nil,
                                               views: ["bottom": bottom]))
            borders.append(bottom)
        }
        
        return borders
    }
    
    func roundViewCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}

private extension UIView {
    
    func _round(corners: UIRectCorner, radius: CGFloat) -> CAShapeLayer {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        
        return mask
    }
    
    func addBorder(mask: CAShapeLayer, borderColor: UIColor, borderWidth: CGFloat) {
        let borderLayer = CAShapeLayer()
        borderLayer.path = mask.path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.lineWidth = borderWidth
        borderLayer.frame = bounds
        layer.addSublayer(borderLayer)
    }
    
}

//MARK
//MARK :- UIButton Extension
extension UIButton{
    func btnRoundCorner(){
        self.layer.cornerRadius = 10
    }
    
    func btnCornerRadious(height : CGFloat){
        self.layer.cornerRadius = height / 2
    }
}


//--------------MARK:- UIAlertController Extension -
let UIAlertControllerBlocksCancelButtonIndex : Int = 0;
let UIAlertControllerBlocksDestructiveButtonIndex : Int = 1;
let UIAlertControllerBlocksFirstOtherButtonIndex : Int = 2;
typealias UIAlertControllerPopoverPresentationControllerBlock = (_ popover:UIPopoverPresentationController?)->Void
typealias UIAlertControllerCompletionBlock = (_ alertController:UIAlertController?,_ action:UIAlertAction?,_ buttonIndex:Int?)->Void
extension UIAlertController{
    //MARK:- showInViewController -
    class func showInViewController(viewController:UIViewController!,withTitle title:String?,withMessage message:String?,withpreferredStyle preferredStyle:UIAlertController.Style?,cancelButtonTitle cancelTitle:String?,destructiveButtonTitle destructiveTitle:String?,otherButtonTitles otherTitles:[String?]?,popoverPresentationControllerBlock:UIAlertControllerPopoverPresentationControllerBlock?,tapBlock:UIAlertControllerCompletionBlock?) -> UIAlertController!{
        
        let strongController : UIAlertController! = UIAlertController(title: title, message: message, preferredStyle: preferredStyle!)
        strongController.view.tintColor = UIColor.black
        if (cancelTitle != nil)
        {
            let cancelAction : UIAlertAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: { (action:UIAlertAction) in
                if (tapBlock != nil){
                    tapBlock!(strongController,action,UIAlertControllerBlocksCancelButtonIndex)
                }
            })
            strongController.addAction(cancelAction)
        }
        if (destructiveTitle != nil)
        {
            let destructiveAction : UIAlertAction = UIAlertAction(title: destructiveTitle, style:.destructive, handler: { (action:UIAlertAction) in
                if (tapBlock != nil){
                    tapBlock!(strongController,action,UIAlertControllerBlocksDestructiveButtonIndex)
                }
            })
            strongController.addAction(destructiveAction)
        }
        if (otherTitles != nil)
        {
            for btnx in 0..<otherTitles!.count
            {
                let otherButtonTitle:String = otherTitles![btnx]!
                
                let otherAction : UIAlertAction = UIAlertAction(title: otherButtonTitle, style: .default, handler: { (action:UIAlertAction) in
                    if (tapBlock != nil){
                        tapBlock!(strongController,action,UIAlertControllerBlocksFirstOtherButtonIndex+btnx)
                    }
                })
                strongController.addAction(otherAction)
                
            }
        }
        
        if (popoverPresentationControllerBlock != nil)
        {
            popoverPresentationControllerBlock!(strongController.popoverPresentationController!)
        }
        
        DispatchQueue.main.async {
            viewController.present(strongController, animated: true, completion:{})
        }
        
        return strongController
    }
    //MARK:- showAlertInViewController -
    class func showAlertInViewController(viewController:UIViewController!,withTitle title:String?,message:String?,cancelButtonTitle cancelTitle:String?,destructiveButtonTitle destructiveTitle:String?,otherButtonTitles otherTitles:[String?]?,tapBlock:UIAlertControllerCompletionBlock?) -> UIAlertController!{
        
        return self.showInViewController(viewController: viewController, withTitle: title, withMessage: message, withpreferredStyle:.alert, cancelButtonTitle: cancelTitle, destructiveButtonTitle: destructiveTitle, otherButtonTitles: otherTitles, popoverPresentationControllerBlock: nil, tapBlock: tapBlock)
    }
    //MARK:- showActionSheetInViewController -
    class func showActionSheetInViewController(viewController:UIViewController!,withTitle title:String?,message:String?,cancelButtonTitle cancelTitle:String?,destructiveButtonTitle destructiveTitle:String?,otherButtonTitles otherTitles:[String?]?,tapBlock:UIAlertControllerCompletionBlock?) -> UIAlertController!{
        
        return self.showInViewController(viewController: viewController, withTitle: title, withMessage: message, withpreferredStyle:.actionSheet, cancelButtonTitle: cancelTitle, destructiveButtonTitle: destructiveTitle, otherButtonTitles: otherTitles, popoverPresentationControllerBlock: nil, tapBlock: tapBlock)
    }
    class func showActionSheetInViewController(viewController:UIViewController!,withTitle title:String?,message:String?,cancelButtonTitle cancelTitle:String?,destructiveButtonTitle destructiveTitle:String?,otherButtonTitles otherTitles:[String?]?,popoverPresentationControllerBlock:UIAlertControllerPopoverPresentationControllerBlock?,tapBlock:UIAlertControllerCompletionBlock?) -> UIAlertController!{
        
        return self.showInViewController(viewController: viewController, withTitle: title, withMessage: message, withpreferredStyle:.actionSheet, cancelButtonTitle: cancelTitle, destructiveButtonTitle: destructiveTitle, otherButtonTitles: otherTitles, popoverPresentationControllerBlock: popoverPresentationControllerBlock, tapBlock: tapBlock)
    }
    
}

extension String{
    func insert(seperator: String, afterEveryXChars: Int, intoString: String) -> String
    {
        var output = ""
        intoString.enumerated().forEach { index, c in
            if index % afterEveryXChars == 0 && index > 0
            {
                output += seperator
            }
            output.append(c)
        }
        //        insert(":", afterEveryXChars: 2, intoString: "11231245")
        //print(output)
        return output
    }
    func capitalizingFirstLetter() -> String {
        let first = String(prefix(1)).capitalized
        let other = String(dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    // EZSE: Converts String to Double
    public func toDouble() -> Double?
    {
        if let num = NumberFormatter().number(from: self) {
            return num.doubleValue
        } else {
            return nil
        }
    }
}
extension UILabel {
    func m_SetGradiantColor() -> UILabel {
        // PART 1
        // create a view with size 400 x 400
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 400, height: 400))
        
        // Create a gradient layer
        let gradient = CAGradientLayer()
        
        // gradient colors in order which they will visually appear
        gradient.colors = [UIColor.red.cgColor, UIColor.blue.cgColor]
        
        // Gradient from left to right
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        // set the gradient layer to the same size as the view
        gradient.frame = view.bounds
        // add the gradient layer to the views layer for rendering
        view.layer.addSublayer(gradient)
        
        
        // PART 2
        // Create a label and add it as a subview
        let label = UILabel(frame: view.bounds)
        label.text = "Hello World"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.textAlignment = .center
        view.addSubview(label)
        
        // Tha magic! Set the label as the views mask
        view.mask = label
        
        return label
    }
}


extension UIImage {
    class func imageWithView(view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
        //RESOLVED CRASH HERE
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds))
    }
}

extension UIView {
    
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    func addCenterConstraintsOf(item:UIView ,itemSize size:CGSize!){
        item.translatesAutoresizingMaskIntoConstraints = false
        let width : NSLayoutConstraint = NSLayoutConstraint(item: item, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: size.width)
        let height : NSLayoutConstraint = NSLayoutConstraint(item: item, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: size.height)
        let xConstraint : NSLayoutConstraint = NSLayoutConstraint(item: item, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let yConstraint : NSLayoutConstraint = NSLayoutConstraint(item: item, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([width,height,xConstraint,yConstraint])
        item.setNeedsDisplay()
    }
}

extension DispatchQueue {
    static var userInteractive: DispatchQueue { return DispatchQueue.global(qos: .userInteractive) }
    static var userInitiated: DispatchQueue { return DispatchQueue.global(qos: .userInitiated) }
    static var utility: DispatchQueue { return DispatchQueue.global(qos: .utility) }
    static var background: DispatchQueue { return DispatchQueue.global(qos: .background) }
    
    func after(_ delay: TimeInterval, execute closure: @escaping () -> Void) {
        asyncAfter(deadline: .now() + delay, execute: closure)
    }
    
    func syncResult<T>(_ closure: () -> T) -> T {
        var result: T!
        sync { result = closure() }
        return result
    }
}
extension CALayer {
    var borderColorFromUIColor: UIColor {
        get {
            return UIColor.init(cgColor: self.borderColor!)
        } set {
            self.borderColor = newValue.cgColor
        }
    }
}

extension UIViewController{
    
    // MARK: - Alert functions-
    func showAlert(title:String? = kAlertTitle , message:String?, completion: (() -> Swift.Void)? = nil){
        _ =  UIAlertController.showAlertInViewController(viewController: self, withTitle: title, message: message, cancelButtonTitle: "OK", destructiveButtonTitle: nil, otherButtonTitles: nil) { (alert, acrtion, btnIndex) in
            if (completion != nil){
                completion!()
            }
        }
    }
    
    func showAlertAction(title:String? = kAlertTitle , message:String?,cancel:String = "OK", Other:String, completion: ((Int) -> Swift.Void)? = nil){
        _ =  UIAlertController.showAlertInViewController(viewController: self, withTitle: title, message: message, cancelButtonTitle: cancel, destructiveButtonTitle: nil, otherButtonTitles: [Other]) { (alert, action, btnIndex) in
            
            if (completion != nil){
                completion!(btnIndex!)
            }
        }
    }
    
    
    //MARK:- Push Method -
    func sendTo(identifier: String) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let passVC = storyBoard.instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(passVC, animated: true)
    }
}

extension UINavigationController {
    func popToViewController(ofClass: AnyClass, animated: Bool = true) {
        if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
            popToViewController(vc, animated: animated)
        }
    }
}

extension UIApplication {
    @discardableResult
    static func openAppSettings() -> Bool {
        guard
            let settingsURL = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settingsURL)
            else {
                return false
        }
        UIApplication.shared.open(settingsURL)
        return true
    }
}

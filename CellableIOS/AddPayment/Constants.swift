//
//  Constants.swift
//  Liquid
//
//  Created by Gurwinder Singh on 12/10/19.
//  Copyright © 2019 Zoptal Solutions Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
//old: http://3.232.3.175:3007/
//new: http://3.219.220.147:5003/
#if DEBUG
//MARK:- DEBUG MODE -
let BASE_URL                    = "https://dev.cellableapp.com/api/"
let BASE_URL_DRIVER             = "https://dev.cellableapp.com/api/"
let BASE_URL_RIDER              = "https://dev.cellableapp.com/api/"
let PAGE_URL                    = "https://dev.cellableapp.com/api/"
//let API_GOOGLE_MAPS_BASE_URL    = "https://maps.googleapis.com/maps/api/"
#else

//MARK:- RELEASE MODE -
let BASE_URL                    = "https://dev.cellableapp.com/api/"
let BASE_URL_DRIVER             = "https://dev.cellableapp.com/api/"
let BASE_URL_RIDER              = "https://dev.cellableapp.com/api/"
let PAGE_URL                    = "https://dev.cellableapp.com/api/"
//let API_GOOGLE_MAPS_BASE_URL    = "https://maps.googleapis.com/maps/api/"
#endif

//MARK:- COMMON USER API's -
let API_SIGNUP_RIDER                  = BASE_URL + "signupRider"
let API_SIGNUP_DRIVER                 = BASE_URL + "signupDriver"
let API_SIGNIN                        = BASE_URL + "signIn"
let API_SEND_OTP                      = BASE_URL + "sendOtp"
let API_FORGOT_PASSWORD               = BASE_URL + "forgotpassword"
let API_CHANGE_PASSWORD               = BASE_URL + "change_password"
let API_EDIT_PROFILE                  = BASE_URL + "editProfile"
let API_USER_SEARCH                   = BASE_URL + "search"

let API_LOGOUT                        = BASE_URL + "logout"

//MARK:- DRIVER API's -
let API_NEW_TRIP                      = BASE_URL_DRIVER + "newTrip"
let API_DRIVER_TRIP_HISTORY           = BASE_URL_DRIVER + "tripHistory"
let API_DRIVER_TRIP_DETAIL            = BASE_URL_DRIVER + "tripDetail"
let API_DRIVER_PAYMENT_SETUP          = BASE_URL_DRIVER + "paymentSetup"
let API_DRIVER_PAYMENT_DETAILS        = BASE_URL_DRIVER + "bank_info"
let API_DRIVER_PAYMENT_DEFAULT        = BASE_URL_DRIVER + "payment_method_default"
let API_STRIPECONNECT                 = BASE_URL_DRIVER + "stripe/connect"
let DISCONNECT_STRIPE                 = BASE_URL_DRIVER +  "stripe/diconnected"
let ApiUpdateStripeConnect            = BASE_URL_DRIVER + "connect/success"
let ApiUpdateStripeConnectApi         = BASE_URL_DRIVER + "connect/successapi"

//MARK:- RIDER API's -
let API_RIDER_TRIPS                   = BASE_URL_RIDER + "invoiceReceipt"
let API_RIDER_PAYMENT_STATUS          = BASE_URL_RIDER + "payment_request"
let API_RIDER_ADD_CARD                = BASE_URL_RIDER + "add_card"
let API_RIDER_CARDS                   = BASE_URL_RIDER + "paymentMethod"
let API_RIDER_MAKE_CARD_DEFAULT       = BASE_URL_RIDER + "makecarddefault"
let API_RIDER_RESEND_INVOICE          = BASE_URL_RIDER + "invoiceResend"

let API_RIDER_DELETECARD              = BASE_URL_RIDER + "removeCard"

//MARK: - GOOGLE API'S -
//let API_GOOGLE_MAPS_DIRECTION               = API_GOOGLE_MAPS_BASE_URL + "directions/json?"
//let API_GOOGLE_MAPS_DISTANCE_MATRIX         = API_GOOGLE_MAPS_BASE_URL + "distancematrix/json?"
//let API_GOOGLE_MAPS_PLACE_AUTOCOMPLETE      = API_GOOGLE_MAPS_BASE_URL + "place/autocomplete/json?"
//let API_GOOGLE_MAPS_PLACE_DETAIL            = API_GOOGLE_MAPS_BASE_URL + "place/details/json?"
//let API_GOOGLE_MAPS_GEOCODE                 = API_GOOGLE_MAPS_BASE_URL + "geocode/json?"

//MARK: - OTHER URL's -
//let URL_PRIVACY     = "https://api.happego.app/hapego/privacy"
//let URL_DISCLAIMER  = "https://api.happego.app/hapego/disclaimer"
//let URL_TERMS       = "https://api.happego.app/hapego/terms"
//let URL_SUPPORT     = "https://api.happego.app/hapego/support"
//let URL_YOUTUBE     = "https://www.youtube.com/channel/UC74X9WZYiuARDTd8Ms4k-8w"

//MARK: - CONSTANT VARIABLES -

let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
var loader = GLoader()
var kUserType = String()

let KLoginType              = ""

let kAlertTitle             = "Cellable"
let kDeviceType             = "Ios"
let kApiVersion             = "1.0"
let kDeviceToken            = "device_token"
let kUserID                 = "user_id"
let kIsLogin                = "is_login"
let kAuthToken              = "Basic YmFzaWNBdXRoOiZqbm8tQDhhej13U28qTkhZVkdwRl5BUT80eW4zNlp2VzVUb1VDVU4rWEdPdUM/c3ojU0Ukb3hYVmJ3UUdQfDNXRnlqY1RBajJTSVJRbkxFfHZvXi18LUFUVjVGWlVmMio1QTNPaXV8X0VPTW1HPT0maUFwelFMM1I3SEhRaj9qdGIwbWMybVQkWSVJc3Jncnh2ZWxkI1peZzMtdWxefDB4QUlUZ2FuSXVGMjNKMHdhU2E2ejZhUF8rJURlNUxxdHVZJnB0eD9xaFp5U0VDZHlFXio0Ul5iKmhGalEtOT9jQ1NKTmZST3p6dEVZYlJ5Tj1TcUR5aGhwelNtbVB8RWI="
let kAccessToken            = "access_token"
let kAccountSetup           = "is_account_setup"
let kCurrentUser            = "currnetUser"
let kZipCode                = "zipCode"
let kGooglePlaceApiKey      = "AIzaSyCmsqY3_JkIUzMjeBBu7cGTOTnTUbEsioA"
let kGoogleClientId         = "1059138018982-d6hkkh0sbfcso6hgda3rcbmplsqcguvi.apps.googleusercontent.com"
let kUsernameValidation     = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ "
let kZipCodeValidation      = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"
let kEmailCodeValidation    = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.@-"
let kPhoneNumberValidation  = "+1234567890"
let kNumberValidation       = "1234567890"
let kPasswordMaxLength      = 20
let kUserNameMaxLength      = 40
let kAgeMaxLength           = 3
let kZipCodeMaxLenght       = 10
let kCityMaxLength          = 30
let kPhoneNumberMaxLenght   = 12
let kOTPMaxLenght           = 1
let kSubjectMaxLength       = 80
let kFeedbackMaxLenght      = 400
let kPriceMaxLenght         = 5
let kOffsetDefaultValue     = 10

// MARK: Declaration for string constants to be used to notification.
public struct NotificationKeys
{
    static let Location_Updated         = "locationUpdated"
    static let Update_User_In_Side_Menu = "updateUserInSideMenu"
}

//
//  PaymentMethodTableViewCell.swift
//  Invoicing
//
//  Created by ZoptalMacMini on 24/10/19.
//  Copyright © 2019 ZoptalMacMini. All rights reserved.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {

    @IBOutlet weak var markBtnOut: UIButton!
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var lblPaymentName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

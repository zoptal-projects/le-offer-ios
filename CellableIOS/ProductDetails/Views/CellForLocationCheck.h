//
//  CellForLocationCheck.h

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface CellForLocationCheck : UITableViewCell
@property (weak, nonatomic) IBOutlet GMSMapView *viewForMap;
@property (weak, nonatomic) IBOutlet UITextField *shippingTextField;
@property (weak, nonatomic) IBOutlet UIView *shippingView;
@property (weak, nonatomic) IBOutlet UILabel *receiveLbl;
@property (weak, nonatomic) IBOutlet UIButton *gotitBtnAction;
@property (weak, nonatomic) IBOutlet UIButton *noBtnAction;
@property (weak, nonatomic) IBOutlet UILabel *labelForLoc;
@property (weak, nonatomic) IBOutlet UIButton *updateBtnOut;
-(void)displayLocationName:(ProductDetails *)product ;
-(void)setUpMapView:(double)latitude andLongitude:(double )longi;
@end

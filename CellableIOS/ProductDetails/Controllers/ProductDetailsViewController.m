
//  ProductDetailsViewController.m
//  Created by Ajay Thakur on 16/09/17.
//  Copyright © 2016 Rahul Sharma. All rights reserved.

#import "ProductDetailsViewController.h"
#import "LikeCommentTableViewCell.h"
#import "SVPullToRefresh.h"
#import "CellForProfile.h"
#import "CellForDescNCond.h"
#import "CellForProdSummry.h"
#import "CellForLocationCheck.h"
#import "PinAddressController.h"
#import "LikersCollectionViewCell.h"
#import "ProfileViewController.h"
#import "CommentsViewController.h"
#import "LikeViewController.h"
#import "MakeOfferViewController.h"
#import "ShowFullImageViewController.h"
#import "ReportPostViewController.h"
#import "ZoomTransitionProtocol.h"
#import "EditProductViewController.h"
#import "ProductDetails.h"
#import "BannerAdsTableViewCell.h"
#import "InformationTableViewCell.h"
#import "ExchangeListTableViewCell.h"
#import "MyListingsViewController.h"
#import "PostListingsViewController.h"
#import  <UIKit/UIKit.h>
#import "Cellable-Swift.h"

#define mShowFullImageStoryboardID          @"showFullImageStoryboard"
@import Firebase;
@interface ProductDetailsViewController ()<WebServiceHandlerDelegate,UIGestureRecognizerDelegate,GMSMapViewDelegate,UITableViewDelegate,ZoomTransitionProtocol,PageControlDelegate,updateCallBack,UITextFieldDelegate>

{   UIView *navBackView, *dividerView, *headerView , *bannerAdsView;
    UIButton *navLeft, *navRight;
    UILabel *errorMessageLabelOutlet;
    NSMutableArray  *arrayOfProfilePics, *arrayOfImageUrls;
    UIImageView *headerImageView;
    CGFloat xOrigin;
    UIScrollView *scrollView;
    float viewWidth;
    int sold;
    BOOL classIsAppearing ,addBannerSubview;
    NSInteger imageCount , imageTag;
    NSString *currency,*userName;
    CGFloat tableHeaderHeight;
    CGRect headerRect;
    NSString *shippingval;
}
@end

@implementation ProductDetailsViewController


#pragma mark -
#pragma mark - ViewController LifeCycle -

-(void)viewDidLoad {
    [super viewDidLoad];
     [[self navigationController] setNavigationBarHidden:NO animated:YES];
    imageCount = 0;
    self.floatingButtonTopConstraint.constant = self.view.frame.size.width - 94 ;
    arrayOfProfilePics = [NSMutableArray new];
    viewWidth = self.view.frame.size.width;
    
    tableHeaderHeight = self.view.frame.size.width;
    headerView = self.tableView.tableHeaderView;
    self.tableView.tableHeaderView  = nil;
    self.tableView.contentInset = UIEdgeInsetsMake(tableHeaderHeight, 0, 0, 0);
    self.tableView.contentOffset = CGPointMake(0,-tableHeaderHeight);
    
    [self showLoading:self.noAnimation];
    [self setValuesFromHomeScreenData];
    [self createNavLeftButton];
    [self addPanGestureToChatButton];
    [self setNotificationObserver];
    userName = [Helper userName];

    [self webServiceForGettingPostsByID];
 
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
    self.makeOfferOutlet.hidden = YES;
    
    shippingval = @"";
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    headerRect = CGRectMake(0, -tableHeaderHeight, self.tableView.bounds.size.width,tableHeaderHeight);
    
    headerView.frame = headerRect;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    CGFloat movedOffset = self.headerScrollView.contentOffset.y;
    if (movedOffset > -self.view.frame.size.width) {
        CGFloat ratio= (self.view.frame.size.width + movedOffset)/self.view.frame.size.width;
        if (ratio<0.9) {
            self.title = @"";
        }
    }
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"recent_login"])
    {
       self.dataFromHomeScreen = YES ;
       self.makeOfferOutlet.enabled = NO ;
       self.floatingButtonOutlet.enabled = NO;
        userName = [Helper userName];
       [self webServiceForGettingPostsByID];
    }
    
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:17.0/255.0f green:42.0/255.0f blue:70.0/255.0f alpha:1.0]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    classIsAppearing = YES;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    classIsAppearing = NO;
}

/*---------------------------------------------------*/
#pragma
#pragma mark - Status Bar.
/*---------------------------------------------------*/

-(BOOL)prefersStatusBarHidden
{
    return NO  ;
}


#pragma mark -
#pragma mark - SetValues For Data -

/**
 This method is to set the data we get from product in HomeScreen.
 Show the data instantly with respective fields untill webService call get
 finished.
 */
-(void)setValuesFromHomeScreenData
{
     CGFloat navHeight = [UIApplication sharedApplication].statusBarFrame.size.height +  self.navigationController.navigationBar.frame.size.height ;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    navBackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,navHeight)];
    navBackView.backgroundColor=[UIColor clearColor];
    self.title = @"";
    dividerView = [[UIView alloc]initWithFrame:CGRectMake(0, navHeight -0.5, self.view.frame.size.width,0.5)];
    dividerView.backgroundColor = [UIColor clearColor] ;
    [navBackView addSubview:dividerView];
    [self.view addSubview:navBackView];
    
    if([[Helper userToken] isEqualToString:mGuestToken])
    {
        [self.makeOfferOutlet setTitle:NSLocalizedString(MakeofferButtonTitle, MakeofferButtonTitle) forState:UIControlStateNormal];
        self.makeOfferOutlet.enabled = YES;
        self.floatingButtonOutlet.enabled = YES;
    }
    else
    {
        self.makeOfferOutlet.enabled = NO;
        self.makeOfferOutlet.backgroundColor = [UIColor colorWithRed:66.0/255.0f green:192.0/255.0f blue:206.0/255.0f alpha:1.0f];
        self.floatingButtonOutlet.enabled = NO;
        
    }
    
    [self setSwapOfferView];
    [self headerForTableView];
    [self updatePricedetails];
    NSString *curncy = [self returnCurrency:[NSString stringWithFormat:@"%@",self.product.currency]];
    _offercurrencyLabel.text = curncy ;
    
}


/**
 This method will get invoked on successfull response of webService.
 update the fields with response data.
 */
-(void)showOutletsOnTheBasisOfData{
    
    [self setSwapOfferView];
    if([self.product.membername isEqualToString:userName])
    {
        if(self.product.sold == 2)
        {
        sold = 1 ;
        self.floatingButtonView.hidden = YES;
       // [_makeOfferOutlet setTitle:NSLocalizedString(MarkAsSoldButtonTitle, MarkAsSoldButtonTitle) forState:UIControlStateNormal];
        self.makeOfferOutlet.hidden = YES;
        }
        else
        {
     
        self.floatingButtonView.hidden = NO ;
        self.floatingButtonOutlet.hidden = YES;
        self.editButtonOutlet.hidden = NO ;
        sold = 1 ;
        self.makeOfferOutlet.hidden = YES;
            
       // [_makeOfferOutlet setTitle:NSLocalizedString(MarkAsSoldButtonTitle, MarkAsSoldButtonTitle) forState:UIControlStateNormal];
        }
    }
    else
    {   self.floatingButtonView.hidden = NO ;
        self.floatingButtonOutlet.hidden = NO;
        self.editButtonOutlet.hidden = YES ;
        
        [self createNavRightButton];
        if(self.product.sold || sold == 2)
        {   sold = 2;
               self.makeOfferOutlet.hidden = YES;
            //self.makeOfferOutlet.enabled = NO;
                   //self.makeOfferOutlet.hidden = NO;
           // [_makeOfferOutlet setTitle:NSLocalizedString(productSoldOut, productSoldOut) forState:UIControlStateNormal];
        }
        else
        {
                self.makeOfferOutlet.hidden = NO;
            sold  = 0 ;
            [_makeOfferOutlet setTitle:NSLocalizedString(MakeofferButtonTitle, MakeofferButtonTitle) forState:UIControlStateNormal];
        }
        
    }
}



#pragma mark -
#pragma  mark - Notification Observer

/**
 Set Notification observer.
 1.) If product goes Sold update same for product.
 2.) Update follow status.
 */
-(void)setNotificationObserver
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeSellingToSold:) name:mSellingToSoldNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollwoStatus:) name:@"updatedFollowStatus" object:nil];
}


#pragma mark -
#pragma mark - Notification Methods

#pragma mark -
#pragma mark - Follow Status Notification

-(void)updateFollwoStatus:(NSNotification *)noti {
    
    if (!classIsAppearing) {
        
        NSIndexPath *index = [NSIndexPath indexPathForRow:2 inSection:0];
        CellForProfile *cell=[_tableView cellForRowAtIndexPath:index];
        
        //check the postId and Its Index In array.
        NSInteger followStatusRespectToUser = [noti.object[@"newUpdatedFollowData"][@"newFollowStatus"] integerValue];
        
        switch (followStatusRespectToUser) {
            case 0:{
                
            }
                break;
            case 1:{
                [cell.buttonFollowOutlet makeButtonAsFollowing];
            }
                break;
            case 2:
            {
                [cell.buttonFollowOutlet makeButtonAsFollow];
            }
                break;
                
        }
    }
}

#pragma mark -
#pragma mark - MarkAsSold Notification

/**
 This notification method is invoked on post notification for selling to sold.
 To show the correct status of product for user and members.
 @param noti notificationObject.
 */
-(void)changeSellingToSold:(NSNotification *)noti
{
    self.makeOfferOutlet.enabled = NO;
    [_makeOfferOutlet setTitle:NSLocalizedString(productSoldOut, productSoldOut) forState:UIControlStateNormal];
    
}

-(void)dealloc
{
    // Remove Noification Observers
    [[NSNotificationCenter defaultCenter] removeObserver:mSellingToSoldNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:@"updatedFollowStatus"];
}

#pragma mark -
#pragma mark - Navigation Bar Buttons

//   navigation bar back button

- (void)createNavLeftButton {
    navLeft = [CommonMethods createNavButtonsWithselectedState:@"item backButton" normalState:@"item backButton"];
    [navLeft rotateButton];
    navLeft.layer.masksToBounds = NO;
    navLeft.layer.shadowOffset = CGSizeMake(0, -2);
    navLeft.layer.shadowRadius = 4;
    navLeft.layer.shadowOpacity = 0.4;
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,30,30)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}





// navigation Report Button
- (void)createNavRightButton {
    
    navRight = [CommonMethods createNavButtonsWithselectedState:@"item_Report" normalState:@"item_Report"];
    navRight.layer.masksToBounds = NO;
    navRight.layer.shadowOffset = CGSizeMake(0, -2);
    navRight.layer.shadowRadius = 2;
    navRight.layer.shadowOpacity = 0.2;
    UIBarButtonItem *navRightButton = [[UIBarButtonItem alloc]initWithCustomView:navRight];
    [navRight setFrame:CGRectMake(0,0,30,30)];
    self.navigationItem.rightBarButtonItem = navRightButton;
    [navRight addTarget:self action:@selector(reportItem) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setRightBarButtonItems:@[[CommonMethods getNegativeSpacer],navRightButton]];
}

-(void)backButtonClicked {
    
    headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake( self.view.frame.size.width
                                                                    , self.view.frame.size.width, self.view.frame.size.width,  self.view.frame.size.width)];
    headerImageView.layer.cornerRadius = 5 ;
    headerImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)reportItem {
    
    if(![[Helper userToken] isEqualToString:mGuestToken])
    {
        ReportPostViewController *reportVc = [self.storyboard instantiateViewControllerWithIdentifier:@"reprtPostVc"];
        reportVc.postdetails = self.product;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:reportVc];
        [self presentViewController:nav animated:YES completion:nil];
    }
    else
    {
        if([[Helper userToken] isEqualToString:mGuestToken])
        {
            UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
            [self presentViewController:navigationController animated:YES completion:nil];
        }
    }
}

#pragma mark -
#pragma mark - No Animations -


/**
 If redirection is from HomeScreen no need to show loader as there is data available to show on fields.
 In case redirection from other screens where data is not available untill webservice call get finished need to show Loading Indicator..

 @param noAnimation Falg for animation.
 */
-(void)showLoading :(BOOL)noAnimation
{
    if(self.noAnimation)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showPIOnView:self.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
        self.viewForMakeOffer.hidden = YES;
        self.tableView.separatorColor = [UIColor clearColor];
    }
    else
    {
        self.viewForMakeOffer.hidden = NO;
        self.tableView.separatorColor = mTableViewCellSepratorColor;
        
    }
}


#pragma
#pragma mark -  TableView data source and delegates -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(! self.product)
        return 0;
    else
        return 9;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
        {
            CellForProdSummry *cell=[tableView dequeueReusableCellWithIdentifier:@"cellForProdSummry" forIndexPath:indexPath];
            [cell updateFieldsForProductWithDataArray:self.product];
            return cell;
        }
            break;
        case 1:
        {
            LikeCommentTableViewCell *cell = (LikeCommentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"LikeCommentTableViewCell"];
            
            cell.arrayOfProfilePics = arrayOfProfilePics;
            cell.product = self.product;
            cell.index = indexPath;
            cell.collectionViewCellID = mCollectionViewCellIDInProductDetails;
            cell.navController = self.navigationController;
            [cell setPropertiesOfOutletscheckFlag:self.dataFromHomeScreen];
            
            if(self.product.likeStatus){
                [cell.likeButtonOutlet setTitle:self.product.likes forState:UIControlStateSelected];
                cell.likeButtonOutlet.layer.borderColor = mBaseColor2.CGColor;
                cell.likeButtonOutlet.selected = YES;
            }
            else{
                [cell.likeButtonOutlet setTitle:self.product.likes forState:UIControlStateNormal];
                cell.likeButtonOutlet.selected = NO;
                cell.likeButtonOutlet.layer.borderColor = [UIColor colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:.7].CGColor;
            }
            return cell;
        }
            break;
        case 2:
        {   CellForProfile *cell=[tableView dequeueReusableCellWithIdentifier:@"cellForProfile" forIndexPath:indexPath];
            [cell setObjectForData:self.product andDataFrom:self.dataFromHomeScreen];
            return cell;
        }
            break;
        case 3:
        {
            CellForDescNCond *cell=[tableView dequeueReusableCellWithIdentifier:@"cellForDescNCond" forIndexPath:indexPath];
            cell.productVC = self ;
            cell.labelTitle.text = NSLocalizedString(productDescriptionTitle, productDescriptionTitle);
            cell.labelForText.text = self.product.productDescription;
            return cell;
        }
            break;
        case 4:
        {
            InformationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InformationCell"];
            cell.product = self.product;
            return cell;
        }
            break;
        case 5:
        {
            ExchangeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"exchangeCell"];
            cell.isProductDeatils = YES;
            cell.swapPostsArray = self.product.swapPosts;
            [cell setPriceForProduct:self.product];
            return cell;
            
        }
            break;
            
        case 6:
        {
            CellForDescNCond *cell=[tableView dequeueReusableCellWithIdentifier:@"cellForDescNCond" forIndexPath:indexPath];
            cell.labelTitle.text = NSLocalizedString(productConditionTitle, productConditionTitle);
            cell.labelForText.text =  self.product.condition ;
            
            
            
            return cell;
        }
            
            break;
            
        case 7:
        {
            
            BannerAdsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellForBannerAds" forIndexPath:indexPath];
            
            [cell.bannerAdsView addSubview:bannerAdsView] ;
            
            return cell ;
        }
            break ;
            
        case 8:
        {
            CellForLocationCheck *cell=[tableView dequeueReusableCellWithIdentifier:@"cellForLocationCheck" forIndexPath:indexPath];
            self.lat = self.product.latitude;
            self.log = self.product.longitude;
            
            [cell setUpMapView:self.lat andLongitude:self.log];
            [cell displayLocationName:self.product];
    
            if([self.product.showQuestionString isEqualToString: @"0"] && [self.product.is_recieved isEqualToString: @"0"])
            {
                if(self.product.sold == 1)
                {
                cell.shippingView.hidden = false;
                cell.receiveLbl.hidden = true;
                cell.gotitBtnAction.hidden = true;
                cell.noBtnAction.hidden = true;
                cell.shippingTextField.delegate = self;
                shippingval = cell.shippingTextField.text;
                    
                }
                
            }
//            else
//            {
//                cell.receiveLbl.hidden = true;
//                cell.gotitBtnAction.hidden = true;
//                cell.noBtnAction.hidden = true;
//            }
            NSLog(@"showQuestionString=%@",self.product.showQuestionString);
            NSLog(@"is_recieved=%@",self.product.is_recieved);
            if([self.product.showQuestionString isEqualToString: @"1"] && [self.product.is_recieved isEqualToString: @"0"])
            {
                cell.shippingTextField.hidden = true;
                cell.updateBtnOut.hidden = true;
                
            cell.shippingView.hidden = false;
            cell.receiveLbl.hidden = false;
            cell.gotitBtnAction.hidden = false;
            cell.noBtnAction.hidden = false;
            }
            
            
            return cell;
        }
            
            
        default:
        {
            UITableViewCell *cell = [[UITableViewCell alloc]init];
            return cell;
        }
            
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
        case 0:
            return UITableViewAutomaticDimension;
            break;
        case 1:  return 97;
            break;
        case 2:
            return 60;
            break;
        case 3:
            if (self.product.productDescription.length == 0)
            {
                return 0;
            }
            return UITableViewAutomaticDimension;
        case 4:
            if(self.product.propertyArray.count)
            {
                CGFloat height =  (self.product.propertyArray.count/2 + self.product.propertyArray.count % 2)*50 + 40;
                return height ;
            }
            else
            {
                return 0;
            }
            break;
        case 5:
        {
            if(self.product.isSwap)
            {
                return 90;
            }
            else
            {
                return 0;
            }
        }
        case 7 :
        {
            if(addBannerSubview)
            {
                return 50 ;
            }
            else
            {
                return 0 ;
            }
        }
            break ;
        case 8:
            return 250;
            break;
        default:
        {

             return  UITableViewAutomaticDimension;
        }
            
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if(indexPath.row == 8)
    {
        PinAddressController *pinAddrVC = [self.storyboard instantiateViewControllerWithIdentifier:mPinAddressStoryboardID];
        pinAddrVC.lat = self.lat;
        pinAddrVC.longittude = self.log;
        pinAddrVC.isProductLocation = YES;
        pinAddrVC.navigationItem.title = NSLocalizedString(mNavTitleForProductLocation, mNavTitleForProductLocation) ;
        [self.navigationController pushViewController:pinAddrVC animated:YES];
    }
}

#pragma mark -
#pragma mark - Table HeaderView -


/**
 Custom Header for tableview.
 This method will add scrollView, ImageViews, Page control and set as a header
 for tableview.
 */
-(void) headerForTableView
{
    if (!headerImageView) {
        headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin, 0, self.view.frame.size.width, self.view.frame.size.width)];
        headerImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.headerScrollView addSubview:headerImageView];
    }
    
    
    for (int i = 0; i < imageCount; i++)
    {
        xOrigin = i * self.headerScrollView.frame.size.width;
        headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin, 0, self.view.frame.size.width, self.view.frame.size.width)];
        headerImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        
        if(i == 0)
        {
            [headerImageView setImageWithURL:[NSURL URLWithString:[arrayOfImageUrls objectAtIndex:i]] placeholderImage:self.imageFromHome usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        else
        {
            [headerImageView setImageWithURL:[NSURL URLWithString:[arrayOfImageUrls objectAtIndex:i]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        
        headerImageView.clipsToBounds = YES ;
        [self.headerScrollView addSubview:headerImageView];
        
        headerImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [_imgScrollBackView addConstraint:[NSLayoutConstraint constraintWithItem:headerImageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_imgScrollBackView attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
        
        [_imgScrollBackView addConstraint:[NSLayoutConstraint constraintWithItem:headerImageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_imgScrollBackView attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        
        [_imgScrollBackView addConstraint:[NSLayoutConstraint constraintWithItem:headerImageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:_headerScrollView attribute:NSLayoutAttributeLeft multiplier:1 constant:xOrigin]];
        
        [_imgScrollBackView addConstraint:[NSLayoutConstraint constraintWithItem:headerImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:self.view.frame.size.width]];
        
        UITapGestureRecognizer *tapForImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
        tapForImage.delegate =self;
        headerImageView.tag = i;
        headerImageView.userInteractionEnabled = YES;
        tapForImage.numberOfTapsRequired = 1 ;
        [headerImageView addGestureRecognizer:tapForImage];
    }
    [self.headerScrollView setContentSize:CGSizeMake(self.headerScrollView.frame.size.width * imageCount, self.headerScrollView.frame.size.height)];
    
    self.pageControlView.numberOfPages = imageCount;
    self.pageControlView.currentPage = 0;
    self.pageControlView.delegate = self ;
    self.pageControlView.dotColorCurrentPage = mBaseColor ;
    self.pageControlView.dotColorOtherPage = mBaseColor2;
    
    if(imageCount>1)
    {
        self.pageControlView.hidden = NO;
    }
    [self.tableView addSubview:headerView];
}


#pragma mark -
#pragma mark - ScrollView Delegate -

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
    if([sender isEqual:self.headerScrollView])
    {
        CGFloat pageWidth = sender.frame.size.width;
        float fractionalPage = sender.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        self.pageControlView.currentPage = page;
    }
    else{
        CGFloat movedOffset = sender.contentOffset.y;
        
        if (movedOffset > -self.view.frame.size.width) {
            CGFloat ratio= (self.view.frame.size.width + movedOffset)/self.view.frame.size.width;
            if (ratio>0.8) {
                navBackView.backgroundColor = [UIColor colorWithRed:249/255.0f green:246/255.0f blue:255/255.0f alpha:1.0f];
                dividerView.backgroundColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f];
                [[navLeft layer] setShadowOpacity:0.0];
                [[navLeft layer] setShadowRadius:0.0];
                [[navLeft layer] setShadowColor:[UIColor clearColor].CGColor];
                [[navRight layer] setShadowOpacity:0.0];
                [[navRight layer] setShadowRadius:0.0];
                [[navRight layer] setShadowColor:[UIColor clearColor].CGColor];
                self.title= self.product.productName;
                [navLeft setImage:[UIImage imageNamed:mNavigationBackButtonImageName] forState:UIControlStateNormal];
                [navRight setImage:[UIImage imageNamed:@"more_button"] forState:UIControlStateNormal];
                
            }else{
                navBackView.backgroundColor = dividerView.backgroundColor = [UIColor clearColor];
                [[navLeft layer] setShadowColor:[UIColor blackColor].CGColor];
                navLeft.layer.shadowOffset = CGSizeMake(0, -2);
                navLeft.layer.shadowRadius = 5;
                navLeft.layer.shadowOpacity = 0.5;
                [[navRight layer] setShadowColor:[UIColor blackColor].CGColor];
                navRight.layer.shadowOffset = CGSizeMake(0, -2);
                navRight.layer.shadowRadius = 5;
                navRight.layer.shadowOpacity = 0.5;
                [navLeft setImage:[UIImage imageNamed:@"item backButton"] forState:UIControlStateNormal];
                [navRight setImage:[UIImage imageNamed:@"item_Report"] forState:UIControlStateNormal];
                self.title=@"";
            }
        }
        
        else{
            if(self.tableView.contentOffset.y < -tableHeaderHeight)
            {
                headerRect.origin.y = self.tableView.contentOffset.y;
                headerRect.size.height = -self.tableView.contentOffset.y;
                
            }
            headerView.frame = headerRect;
        }
        
    }
}



#pragma mark -
#pragma mark - Reload Row Of TableView -

/**
 Animation for like button outlet on changing status.

 @param likeButton likeButton Outlet.
 */
-(void)animateButton:(UIButton *)likeButton {
    CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    [ani setDuration:0.2];
    [ani setRepeatCount:1];
    [ani setFromValue:[NSNumber numberWithFloat:1.0]];
    [ani setToValue:[NSNumber numberWithFloat:0.5]];
    [ani setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[likeButton layer] addAnimation:ani forKey:@"zoom"];
}

/**
 Reload particualr row for updating status.

 @param reloadRowAtSection indexPath.
 */
-(void)reloadRowToShowNewNumberOfLikes:(NSInteger )reloadRowAtSection {
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:1 inSection:reloadRowAtSection];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}

/*---------------------------------------*/
#pragma
#pragma mark - Button Actions
/*---------------------------------------*/

#pragma mark - LikeButton Action -

- (IBAction)likeButtonAction:(id)sender {
    UIButton *likeButton = (UIButton *)sender;
    // adding animation for selected button
    
    if([[Helper userToken] isEqualToString:@"guestUser"])
    {
        UINavigationController *navigation = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
    }
    else{
        
        [self animateButton:likeButton];
        if(likeButton.selected) {
            likeButton.selected = NO;
            self.product.likeStatus = NO ;
            NSInteger newNumberOfLikes = [self.product.likes integerValue];
            newNumberOfLikes --;
            [likeButton setTitle:[NSString stringWithFormat:@"%ld",(long)newNumberOfLikes] forState:UIControlStateNormal];
            self.product.likes  = [@(newNumberOfLikes)stringValue];
//            for(int j=0;j<arrayOfProfilePics.count;j++)
//            {
//                if([arrayOfProfilePics[j][@"likedByUsers"] isEqualToString:userName])    // remove ProfilePic if userName has liked the post.
//                {   [arrayOfProfilePics removeObjectAtIndex:j];
//                    [self.product.likedByUsers removeObjectAtIndex:j];
//                }
//            }
            [self reloadRowToShowNewNumberOfLikes:likeButton.tag];
            [self unlikeAPost:self.product.postId postType:0];
            
            NSIndexPath *indexpathToreload = [NSIndexPath indexPathForRow:1 inSection:0];
            LikeCommentTableViewCell *cell = (LikeCommentTableViewCell *)[self.tableView cellForRowAtIndexPath:indexpathToreload];
            NSDictionary *dicForUnlike = @{
                                           @"notificationForLike":@"0",
                                           @"postdetails":self.product
                                           };
            [[NSNotificationCenter defaultCenter] postNotificationName:mfavoritePostNotifiName object:dicForUnlike];
            [cell.likersCollectionViewOutlet reloadData];
        }
        else  {
            likeButton.selected = YES;
            
            self.product.likeStatus = YES ;
            NSInteger newNumberOfLikes = [self.product.likes integerValue];
            newNumberOfLikes ++;
            [likeButton setTitle:[NSString stringWithFormat:@"%ld",(long)newNumberOfLikes] forState:UIControlStateSelected];
            NSDictionary *dic = [NSMutableDictionary new];
            [dic setValue:self.product.profilePicUrl forKey:@"profilePicUrl"]; // add Profile pic of user If liked he post.
            [dic setValue:userName forKey:@"likedByUsers"];
           // [arrayOfProfilePics insertObject:dic atIndex:0];
            [self.product.likedByUsers insertObject:dic atIndex:0];
            self.product.likes  = [@(newNumberOfLikes)stringValue];
            [self reloadRowToShowNewNumberOfLikes:likeButton.tag];
            
            [self likeAPost:self.product.postId postType:0];
            
            NSIndexPath *indexpathToreload = [NSIndexPath indexPathForRow:1 inSection:0];
            LikeCommentTableViewCell *cell = (LikeCommentTableViewCell *)[self.tableView cellForRowAtIndexPath:indexpathToreload];
            NSDictionary *dicForUnlike = @{
                                            @"notificationForLike":@"1",
                                           @"postdetails":self.product
                                           };
            [[NSNotificationCenter defaultCenter] postNotificationName:mfavoritePostNotifiName object:dicForUnlike];
            [cell.likersCollectionViewOutlet reloadData];
        }
        
        
        if (self.isNewsFeedSocial)
        {
         NSDictionary *likeEventData = @{
                                       @"productDetails": self.product,
                                       @"indexPath": self.indexPath,
                                       @"arrayOfProfilePics" : arrayOfProfilePics
                                       };
        
         [[NSNotificationCenter defaultCenter] postNotificationName:@"likesEvent" object:likeEventData];
        }
        
    }
}

#pragma mark - All LikesButton Action -

- (IBAction)numberOfLikesButtonAction:(id)sender {
    if([[Helper userToken] isEqualToString:mGuestToken]){
        UINavigationController *navigation = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
    }
    else{
        LikeViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"likeStoryBoardId"];
        newView.navigationTitle = NSLocalizedString(navTitleForLikes, navTitleForLikes);
        newView.postId =  self.product.postId ;
        newView.postType = 0;
        [self.navigationController pushViewController:newView animated:YES];
    }
}

#pragma mark - Share Button Action -

- (IBAction)shareButtonAction:(id)sender {
    
    if([[Helper userToken] isEqualToString:mGuestToken]){
        
        UINavigationController *navigation = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleActionSheet]; // 1
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:NSLocalizedString(actionSheetShare, actionSheetShare)
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                                                                  [components shortenWithCompletion:^(NSURL *_Nullable shortURL,
//                                                                                                      NSArray *_Nullable warnings,
//                                                                                                      NSError *_Nullable error) {
//                                                                      // Handle shortURL or error.
//                                                                      if (error) {
//                                                                          NSLog(@"Error generating short link: %@", error.description);
//                                                                          return;
//                                                                      }
//
//                                                                      NSArray *items = @[shortURL.absoluteString];
//                                                                      // build an activity view controller
//                                                                      UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
//                                                                      // and present it
//                                                                      [Helper presentActivityController:controller forViewController:self];
//
//                                                                  }];
                                                              }]; // 2
        UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:NSLocalizedString(alertCancel, alertCancel) style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:firstAction];
        [alert addAction:thirdAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - CommentButton Action -

- (IBAction)viewAllCommentsButtonAction:(id)sender {
    
    if([[Helper userToken] isEqualToString:@"guestUser"])
    {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
    }
    else{
        UIButton *allCommentButton = (UIButton *)sender;
        CommentsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"commentsStoryBoardId"];
        newView.postId =  self.product.postId;
        newView.postCaption =  self.product.productDescription ;
        newView.postType = 0 ;
        newView.imageUrlOfPostedUser = (NSString *)self.product.profilePicUrl ;
        newView.selectedCellIs = allCommentButton.tag;
        newView.userNameOfPostedUser = self.product.postedByUserName;
        newView.commentingOnPostFrom = @"ToHomeScreen";
        [self.navigationController pushViewController:newView animated:YES];
    }
}
#pragma mark -
#pragma mark Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField canResignFirstResponder])
    {
        [textField resignFirstResponder];
    }

    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    shippingval = textField.text;

    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    shippingval = textField.text;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    shippingval = textField.text;
    return  YES;
}
#pragma mark - Show UserProfile Action -

- (IBAction)showProfileButton:(id)sender {
    ProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
    newView.memberName = self.product.membername ;
    newView.isMemberProfile = YES;
    newView.productDetails = YES;
    newView.hidesBottomBarWhenPushed = YES ;
    [self.navigationController pushViewController:newView animated:YES];
}
- (IBAction)yesBtnActionCalling:(id)sender
{
    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
       [HomePI showPIOnView:self.view withMessage:@"Loading..."];
       NSDictionary *requestDict = @{
                                     mauthToken :[Helper userToken],
                                     @"postId" : self.product.postId,
                                     @"is_recieved" : @"1"
                                     };
       NSLog(@"requestDict=%@",requestDict);
       [WebServiceHandler RequestTypePostsIsRecevieProduct:requestDict andDelegate:self];
}
- (IBAction)NoBtnActionCalling:(id)sender
{
}
- (IBAction)updateAddressBntAction:(id)sender
{
   // CellForLocationCheck *cell = (CellForLocationCheck*)[sender superview];
   // NSIndexPath* indexPath = [_tableView indexPathForCell:cell];
    
    //NSLog(@"cell=%@",cell.shippingTextField.text);
    if ([shippingval isEqualToString:@""])
    {
        [Helper showAlertWithTitle:@"Cellable" Message:@"Please enter shipping info first." viewController:self];
    }
    else{
    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
       [HomePI showPIOnView:self.view withMessage:@"Loading..."];
       NSDictionary *requestDict = @{
                                     mauthToken :[Helper userToken],
                                     @"postId" : self.product.postId,
                                     @"type" : @"0",
                                     @"link" : shippingval,
                                     };
       NSLog(@"requestDict=%@",requestDict);
       [WebServiceHandler RequestTypePostsShippingUpdate:requestDict andDelegate:self];
    }
}

#pragma mark - Follow Button Action -
- (IBAction)followButtonAction:(id)sender {
    
    if([[Helper userToken] isEqualToString:mGuestToken])
    {
        UINavigationController *nav = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
    else
        [self followButtonClicked:sender];
}

-(void)followButtonClicked:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    //actions for when the account is public.
    if ([selectedButton.titleLabel.text containsString:NSLocalizedString(followingButtonTitle, followingButtonTitle)]) {
        UIImageView *userImageView =[[UIImageView alloc] init];
        [userImageView sd_setImageWithURL:[NSURL URLWithString: self.product.memberProfilePicUrl] placeholderImage:[UIImage imageNamed: @"defaultpp.png"]];
        [Helper showUnFollowAlert:userImageView.image and:self.product.membername viewControllerReference:self onComplition:^(BOOL isUnfollow)
         {
             if(isUnfollow){
                 [self unfollowAction:sender];
             }
         }];
    }
    else {
        [selectedButton makeButtonAsFollowing];
        [self sendNewFollowStatusThroughNotification:self.product.username andNewStatus:@"1"];
        //passing parameters.
        NSDictionary *requestDict = @{muserNameTofollow     :self.product.membername,
                                      mauthToken            :flStrForObj([Helper userToken]),
                                      };
        //requesting the service and passing parametrs.
        [WebServiceHandler follow:requestDict andDelegate:self];
    }
}

#pragma mark - DirectChat Button Action -

- (IBAction)FloatingButtonAction:(id)sender {
    if ([[Helper userToken] isEqualToString:@"guestUser"]) {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
    } else {
        MakeOfferViewController *controller = [[MakeOfferViewController alloc] init];
        [self.navigationController dismissViewControllerAnimated:true completion:nil];
        [controller createObjectForMakeOfferwithData:self.product withPrice:0 andType:@"0" withAPICall:NO];
    }
}

#pragma mark - Make Offer Action -

- (IBAction)makeOfferButton:(id)sender {
    
    if ([[Helper userToken] isEqualToString:mGuestToken]) {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
    }
    else if(sold == 0){
         MakeOfferViewController *makeOfferVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"MakeOfferViewController"];
        makeOfferVC.product = self.product;
        [self.navigationController pushViewController:makeOfferVC animated:YES];
    }
    else
    {
        EditProductViewController *editVC = [self.storyboard instantiateViewControllerWithIdentifier:mEditItemStoryBoardID];
        editVC.hidesBottomBarWhenPushed = YES;
        editVC.product = self.product ;
        [self.navigationController pushViewController:editVC animated:YES];
    }
}

#pragma mark - Exchange Flow -

- (IBAction)swapButtonAction:(id)sender {
    
    if([[Helper userToken]isEqualToString:mGuestToken]){
        
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
    }
    else
    {
        MyListingsViewController *myPosts = [self.storyboard instantiateViewControllerWithIdentifier:@"myListingsStoryboard"];
        myPosts.hidesBottomBarWhenPushed = YES;
        myPosts.product = self.product ;
        [self.navigationController pushViewController:myPosts animated:YES];
    }
    
}



#pragma mark -
#pragma mark - Show Full Image -
/**
 This Method will show full images of products on tapping.
 
 @param tap object to get the tap details like tags.
 */
-(void)imageTapped:(UITapGestureRecognizer *)tap
{
    imageTag = tap.view.tag ;
    ShowFullImageViewController *showFullVC = [self.storyboard instantiateViewControllerWithIdentifier:mShowFullImageStoryboardID];
    showFullVC.arrayOfImagesURL  = arrayOfImageUrls;
    showFullVC.imageCount = imageCount;
    showFullVC.imageTag = imageTag ;
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:showFullVC];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
}

/*-----------------------------------------------------*/
#pragma mark -
#pragma mark - Unfollow Action
/*----------------------------------------------------*/


/**
 This method will get invoked on unfollow button action to ask the permission to get
 user unfollowed with one action sheet.
 @param sender      unFollow Button .
 */

-(void)unfollowAction:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    [selectedButton makeButtonAsFollow];
    [self sendNewFollowStatusThroughNotification:self.product.postedByUserName andNewStatus:@"2"];
    //passing parameters.
    NSDictionary *requestDict = @{muserNameToUnFollow:self.product.membername,
                                  mauthToken            :flStrForObj([Helper userToken]),
                                  };
    //requesting the service and passing parametrs.
    [WebServiceHandler unFollow:requestDict andDelegate:self];
}

-(void)sendNewFollowStatusThroughNotification:(NSString *)username andNewStatus:(NSString *)newFollowStatus {
    NSDictionary *newFollowDict = @{@"newFollowStatus"     :flStrForObj(newFollowStatus),
                                    mUserName            :flStrForObj(username),
                                    };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedFollowStatus" object:[NSDictionary dictionaryWithObject:newFollowDict forKey:@"newUpdatedFollowData"]];
}


#pragma mark -
#pragma mark - Server Requests. -

#pragma mark -
#pragma mark - RequestForPostDetails

/**
 Server call for getting post details by post ID.
 */
-(void)webServiceForGettingPostsByID
{

    if([[Helper userToken] isEqualToString:mGuestToken])
    {
        NSDictionary *requestDict = @{
                                      mpostid:flStrForObj(self.postId),
                                      mCity : flStrForObj(self.currentCity),
                                      mCountryShortName : flStrForObj(self.countryShortName),
                                      mlatitude : flStrForObj(self.currentLattitude),
                                      mlongitude : flStrForObj(self.currentLongitude)
                                      };
        
        [WebServiceHandler getPostsForGuests:requestDict andDelegate:self];
    }
    else
    {
        NSDictionary *requestDict = @{
                                      mauthToken :flStrForObj([Helper userToken]),
                                      mpostid:flStrForObj(self.postId),
                                      mCity : flStrForObj(self.currentCity),
                                      mCountryShortName : flStrForObj(self.countryShortName),
                                      mlatitude : flStrForObj(self.currentLattitude),
                                      mlongitude : flStrForObj(self.currentLongitude)
                                      };
        NSLog(@"requestDict=%@",requestDict);
        [WebServiceHandler getPostsByusers:requestDict andDelegate:self];
    }
}


#pragma mark -
#pragma mark - Like Post Server Request

-(void)likeAPost:(NSString *)postId postType:(NSString *)postType {
    NSDictionary *requestDict;
    requestDict = @{
                    mauthToken:flStrForObj([Helper userToken]),
                    mpostid:postId,
                    mLabel:@"Photo"
                    };
    [WebServiceHandler likeAPost:requestDict andDelegate:self];
}

#pragma mark -
#pragma mark - Unlike Post Server Request

-(void)unlikeAPost:(NSString *)postId postType:(NSString *)postType {
    NSDictionary *requestDict;
    requestDict = @{
                    mauthToken:flStrForObj([Helper userToken]),
                    mpostid:postId,
                    mLabel:@"Photo",
                    mUserName:userName
                    };
    [WebServiceHandler unlikeAPost:requestDict andDelegate:self];
}

#pragma mark
#pragma mark - WebServiceDelegate

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    if (error)
    {
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
        return;
    }
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    NSDictionary *responseDict = (NSDictionary*)response;
    NSLog(@"response=%@",response);
    switch (requestType)
    {
        case RequestTypeGetPostsByUsers:
        {
            self.product = response ;
            NSLog(@"response=%@",self.product);
            switch (self.product.code)
            {
                case 200: {
                    if(self.productDelegate)
                    {
                        [self.productDelegate updateProductForClickCount:self.product forIndex:self.indexPath];
                    }
                    [[ProgressIndicator sharedInstance] hideProgressIndicator];
                    self.makeOfferOutlet.enabled =  YES;
                    self.floatingButtonOutlet.enabled = YES;
                    [self showOutletsOnTheBasisOfData];
                    [self handlingResponseOfExplorePosts];
                    [self updatePricedetails];
                }
                    break;
                case 204:
                {
                    [self productIsNotFound];
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case RequestTypeGetPostsForGuests:
        {  self.product = response ;
            switch (self.product.code) {
                case 200: {
                    [self updatePricedetails];
                    [self handlingResponseOfExplorePosts];
                    [[ProgressIndicator sharedInstance] hideProgressIndicator];
                }
                    break;
                case 204:
                {
                    [self productIsNotFound];
                }
                    break;
                    
                default:
                {   [[ProgressIndicator sharedInstance] hideProgressIndicator];
                    [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
                }
                    break;
            }
        }
            break ;
        case RequestTypeLikeAPost:
        {
            
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatePostDetails" object:[NSDictionary dictionaryWithObject:responseDict forKey:@"profilePicUrl"]];
                }
                    break;
                default:
                    break;
            }
        }
            break ;
        case RequestTypeUnlikeAPost:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatePostDetails" object:[NSDictionary dictionaryWithObject:responseDict forKey:@"profilePicUrl"]];
                }
                    break;
                default:
                    break;
            }
            
        }
            //
         break;
        case RequestTypeDeletePost:
        {
            switch ([response[@"code"] integerValue])
            {
                case 200:
                    [[NSNotificationCenter defaultCenter] postNotificationName:mDeletePostNotifiName object:[NSDictionary dictionaryWithObject:response forKey:@"data"]];
                    [self.navigationController popViewControllerAnimated:YES];
                    break;
                default:
                    break;
            }
        }
            break;
                case RequestTypePostsPurchasedIsUser:
                   {
                       switch ([response[@"code"] integerValue])
                       {
                        case 200:
                               
                     [Helper showAlertWithTitle2:@"Cellable" Message:response[@"message"] viewController:self];
                    
                      [self.navigationController popViewControllerAnimated:YES];
                        break;
                        case 404 :{
                        [Helper showAlertWithTitle:@"Cellable" Message:response[@"message"] viewController:self];
                        }
                        break;
                       }
                   }
                    break;
        
            case RequestTypePostsShippingUpdateS:
               {
                   switch ([response[@"code"] integerValue])
                   {
                    case 200:
                    shippingval = @"";
                    [Helper showAlertWithTitle:@"Cellable" Message:response[@"message"] viewController:self];
                    break;
                    case 404 :{
                    [Helper showAlertWithTitle:@"Cellable" Message:response[@"message"] viewController:self];
                    }
                           case 204 :{
                           [Helper showAlertWithTitle:@"Cellable" Message:response[@"message"] viewController:self];
                           }
                    break;
                   }
               }
                break;
            
            //
            
        default:
            break;
    }
    
    
}



#pragma mark -
#pragma mark - Share -


//-(void)genrateDeepLinkingComponent
//{
//    NSString *url =  [SHARE_LINK stringByAppendingString:[NSString stringWithFormat:@"%@",self.product.postId]];
//
//    NSString *originalLink = [SHARE_LINK  stringByAppendingString:self.product.postId] ;
//    NSURL *link = [NSURL URLWithString:originalLink];
//    components =
//    [FIRDynamicLinkComponents componentsWithLink:link
//                                          domain:mDomainForDeepLinking];
//
//    FIRDynamicLinkSocialMetaTagParameters *socialParams = [FIRDynamicLinkSocialMetaTagParameters parameters];
//    socialParams.title = self.product.productName ;
//    socialParams.descriptionText = self.product.productDescription ;
//    socialParams.imageURL = [NSURL URLWithString:self.product.mainUrl] ;
//    components.socialMetaTagParameters = socialParams;
//
//    NSString *bundleId = mIosBundleId;
//
//    FIRDynamicLinkIOSParameters *iosParams = [FIRDynamicLinkIOSParameters parametersWithBundleID:bundleId];
//    iosParams.fallbackURL = [NSURL URLWithString: url] ;
//    components.iOSParameters = iosParams;
//
//
//    FIRDynamicLinkAndroidParameters *androidParam = [FIRDynamicLinkAndroidParameters parametersWithPackageName:mAndroidBundleId];
//    androidParam.fallbackURL = [NSURL URLWithString: url] ;
//    components.androidParameters = androidParam;
//
//    FIRDynamicLinkNavigationInfoParameters *navigationInfoParameters = [FIRDynamicLinkNavigationInfoParameters parameters];
//    navigationInfoParameters.forcedRedirectEnabled = 0;
//    components.navigationInfoParameters = navigationInfoParameters;
//}


#pragma mark -
#pragma mark - Handle PostDetails Response -
/**
 Handling the response comes from WebService to extract the required data.
 */
-(void)handlingResponseOfExplorePosts
{
    self.dataFromHomeScreen = NO;
    
    [arrayOfProfilePics removeAllObjects];
    arrayOfImageUrls = [[NSMutableArray alloc] initWithObjects:self.product.mainUrl,self.product.imageUrl1,self.product.imageUrl2,self.product.imageUrl3,self.product.imageUrl4, nil ];
    
    NSInteger strForCount = self.product.imageCount;
    
    if (strForCount >0) {
        imageCount = strForCount;
    }
    else {
        imageCount = 1;
    }
    [self headerForTableView];
    //get array of Likers profile
    NSInteger likeCount = [self.product.likes integerValue];
    if(likeCount>6)
        likeCount = 6;
//    for(int j=0;j<likeCount;j++){
//        NSString *user = flStrForObj(self.product.likedByUsers[j][@"likedByUsers"]);
//        NSString *profilePicOfUser = flStrForObj(self.product.likedByUsers[j][@"profilePicUrl"]);
//        NSMutableDictionary  *tempDict = [[NSMutableDictionary alloc] init];
//        [tempDict setValue:user forKey:@"likedByUsers"];
//        [tempDict setValue:profilePicOfUser forKey:@"profilePicUrl"];
//        [arrayOfProfilePics addObject:tempDict];
//    }
    self.tableView.separatorColor = mTableViewCellSepratorColor;
    self.viewForMakeOffer.hidden = NO ;
    InformationTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];

    dispatch_async(dispatch_get_main_queue(), ^{
      [self.tableView reloadData];
      [cell.propertiesCollectionView reloadData];
    });
    
    //[self genrateDeepLinkingComponent];
    
}


#pragma mark -
#pragma mark - ZoomTransitionProtocol -

-(UIView *)viewForZoomTransition:(BOOL)isSource
{
    
    headerImageView.image = _imageFromHome;
    return headerImageView;
}


-(void)updatePricedetails {
    _offercurrencyLabel.text =  [self returnCurrency:self.product.currency];
    _priceOutlet.text = self.product.price;
    
    if(!self.product.negotiable)
    {
        _offerNegotLabel.text = NSLocalizedString(productNotNegotiable, productNotNegotiable);
    }else
    {
        _offerNegotLabel.text = NSLocalizedString(productNegotiable, productNegotiable);
        
    }
}


#pragma mark -
#pragma mark - Product Removed -

// Product not found. Remove product, If product is removed at Real time.
-(void)productIsNotFound
{
    if(self.productDelegate)
    {
        [self.productDelegate productIsRemovedForIndex:self.indexPath];
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(productRemovedAlertMessage, productRemovedAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(alertOk, alertOk) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                             {
                                 [self.navigationController popViewControllerAnimated:YES];
                             }];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

#pragma mark -
#pragma mark - Pan Gesture -

// Pan Gesture for chat Button to make it floating on the screen.
-(void)addPanGestureToChatButton
{
    UIPanGestureRecognizer *panRecognizer;
    panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(wasDragged:)];
    panRecognizer.cancelsTouchesInView = YES;
    [[self floatingButtonView] addGestureRecognizer:panRecognizer];
}

- (void)wasDragged:(UIPanGestureRecognizer *)recognizer {
    UIButton *button = (UIButton *)recognizer.view;
    CGPoint translation = [recognizer translationInView:button];
    
    button.center = CGPointMake(button.center.x + translation.x, button.center.y + translation.y);
    [recognizer setTranslation:CGPointZero inView:button];
}

#pragma mark -
#pragma mark - Return CurrencySym -


/**
 This method will return currency symbol for respective currency.
 
 @param curr currency.
 
 @return currency symbol for respective currency.
 */
-(NSString*)returnCurrency:(NSString *)curr
{
    if(curr.length == 0)
    {
        return @"";
    }
    NSLocale *local = [[NSLocale alloc] initWithLocaleIdentifier:curr];
    NSString *currencySym = [NSString stringWithFormat:@"%@",[local displayNameForKey:NSLocaleCurrencySymbol value:curr]];
    return currencySym;
}

#pragma mark -
#pragma mark - Alert From Top -


/**
 This method will Invoked to show message from top.
 
 @param message message to show.
 */
-(void)showingErrorAlertfromTop:(NSString *)message {
    [errorMessageLabelOutlet setHidden:NO];
    
    [errorMessageLabelOutlet setFrame:CGRectMake(0,50, [UIScreen mainScreen].bounds.size.width, 30)];
    
    [self.view layoutIfNeeded];
    errorMessageLabelOutlet.text = message;
    [UIView animateWithDuration:0.4 animations:
     ^ {
         [self.view layoutIfNeeded];
     }];
    int duration = 2;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.4 animations:
         ^ {
             [errorMessageLabelOutlet setFrame:CGRectMake(0, -100, [UIScreen mainScreen].bounds.size.width, 100)];
             [errorMessageLabelOutlet setHidden:YES];
             [self.view layoutIfNeeded];
         }];
    });
    
}


//-(void)handleDynamicLink :(FIRDynamicLink *)dynamicLink
//{
//    NSLog(@"Your Dynamic Link parameter: %@",dynamicLink);
//}


#pragma mark - Exchange -

-(void)setSwapOfferView
{
    if(self.product.isSwap && ![self.product.membername isEqualToString:[Helper userName]])
    {
        if ([_pathCheck isEqualToString:@"HiddenYes"])
        {
        self.swapOfferView.hidden = YES;
        }
        else
        {
        self.swapOfferView.hidden = NO;
        }
    }
    else
    {
        self.swapOfferView.hidden = YES;
    }
}

#pragma mark - Edit Product -

- (IBAction)editButtonAction:(id)sender {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        ProgressIndicator *loginPI = [ProgressIndicator sharedInstance];
        [loginPI showPIOnView:self.view withMessage:@"Deleting post .."];
        
        
        NSDictionary *requestDict = @{mpostid : self.product.postId ,
                                      mauthToken : [Helper userToken]};
        
        [WebServiceHandler deletePost:requestDict andDelegate:self];
    }];
    UIAlertAction *editAction = [UIAlertAction actionWithTitle:@"Edit" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        PostListingsViewController *postVC = [self.storyboard instantiateViewControllerWithIdentifier:mPostScreenStoryboardID];
        postVC.editingPost = YES;
        postVC.product = self.product ;
        postVC.popDelegate  = self ;
        UINavigationController *navBar =[[UINavigationController alloc]initWithRootViewController:postVC];
        [self.navigationController presentViewController:navBar animated:YES completion:nil];;
    }];
    
    UIAlertAction *promotePostAction = [UIAlertAction actionWithTitle:@"Promote your item" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        InAppCellableVC *promotionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InAppCellableVC"];
        promotionVC.productObj2 = self.product;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:promotionVC];
        [self.navigationController presentViewController:navigationController animated:YES completion:nil];
    }];
    [controller addAction:cancelAction];
    [controller addAction:editAction];
    [controller addAction:deleteAction];
    [controller addAction:promotePostAction];
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)popToRootViewController :(BOOL)pop
{
    [self webServiceForGettingPostsByID];
}
-(void)removeAlert:(UIAlertController *) alert
{
  //  [self dismissViewControllerAnimated:YES completion:nil];
   // [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end


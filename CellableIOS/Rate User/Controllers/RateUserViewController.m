//
//  StarScreenViewController.m
//  Picogram
//
//  Created by Rahul Sharma on 27/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "RateUserViewController.h"
#import "RateUserTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "WebServiceHandler.h"
#import "ProgressIndicator.h"

@interface RateUserViewController()<UITableViewDataSource, UITableViewDelegate, WebServiceHandlerDelegate>

@end

@implementation RateUserViewController
{
    RateUserTableViewCell *cell;
}

- (void)viewDidLoad {
    self.navigationItem.title = NSLocalizedString(navTitleForRateUser, navTitleForRateUser);
    [super viewDidLoad];
    [self createNavLeftButton];
    // Do any additional setup after loading the view.
}
-(void)createNavLeftButton
{
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,25,40)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}

-(void)navLeftButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"StarCell"
                                           forIndexPath:indexPath];
    NSString *imageURL1;
    if(self.activityResponse.count > 0)
    {
        if (self.ratiingForSeller) {
       cell.starLabel.text = [NSString stringWithFormat:@"%@ %@ ?",NSLocalizedString(wouldYouLikeToRate, wouldYouLikeToRate), self.activityResponse[@"membername"]];
            imageURL1 = flStrForObj(self.activityResponse[@"memberProfilePicUrl"]);
        }
        else {
            cell.starLabel.text = [NSString stringWithFormat:@"%@ %@ again ?",NSLocalizedString(wouldYouLikeToSell, wouldYouLikeToSell), self.activityResponse[@"buyername"]];
            imageURL1 = flStrForObj(_activityResponse[@"buyerProfilePicUrl"]);
        }
    }
    
    [cell.sellerPic sd_setImageWithURL:[NSURL URLWithString:imageURL1] placeholderImage:[UIImage imageNamed:@"defaultpp"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)SubmitButton:(id)sender {
    int rate = cell.ratingValue;
    if (self.ratiingForSeller ) {
        if (self.activityResponse) {
            NSDictionary* markSold = @{ @"token":[Helper userToken],
                                        @"rating":[NSString stringWithFormat:@"%d",rate],
                                        @"seller" : [NSString stringWithFormat:@"%@",self.activityResponse[@"membername"] ],
                                         mpostid : self.activityResponse[@"postId"]
                                        };
            [WebServiceHandler rateForSeller:markSold andDelegate:self];
        }
        
    } else{
        if(self.activityResponse.count >0) {
            NSDictionary* markSold = @{ @"token":[Helper userToken],
                                        @"postId":[NSString stringWithFormat:@"%@",self.activityResponse[@"postId"] ],
                                        @"type":@"0",
                                        @"ratings":[NSString stringWithFormat:@"%d",rate],
                                        @"membername" : [NSString stringWithFormat:@"%@",self.activityResponse[@"buyername"] ]
                                        };
            [WebServiceHandler RequestTypeMarkAsSold:markSold andDelegate:self];
        }
    }
    
    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
    [HomePI showPIOnView:self.view withMessage:@"Rating..."];
}

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error{
    NSDictionary *responseDict = (NSDictionary*)response;
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    
    if (requestType == RequestTypemarkSold) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200:
                //// notification*///
                [[NSNotificationCenter defaultCenter]postNotificationName:mSellingToSoldNotification object:[NSDictionary dictionaryWithObject:response[@"data"] forKey:@"data"]];
                [self.navigationController popToRootViewControllerAnimated:YES];
                break;
            case 409: {
                [Helper showAlertWithTitle:@"Cellable" Message:NSLocalizedString(productIsAlreadySoldOut, productIsAlreadySoldOut) viewController:self];
            }
                break;
            default:
                break;
        }
    }
    
    
    if (requestType == RequestTyperateForSeller) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(thanksForRating, thanksForRating) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(alertOk, alertOk) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                    if(self.callBackForRateUser)
                                  {
                                    self.callBackForRateUser(YES);
                                    }
                [self.navigationController popViewControllerAnimated:YES];
                                                   }];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
            }
                break;
            default:{
                [Helper showAlertWithTitle:@"" Message:responseDict[@"message"] viewController:self];
            }
                break;
        }
    }
}
@end

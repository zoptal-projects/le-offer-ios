//
//  AdsCampaignView.h

//
//  Created by Rahul Sharma on 07/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

//#import <UIKit/UIKit.h>
//
//
//@protocol AdsCampaignViewDelegate <NSObject>
//-(void)knowmoreButtonClickedForUrl :(NSString *)url;
//@end
//
//@interface AdsCampaignView : UIView <UIGestureRecognizerDelegate,WebServiceHandlerDelegate >
//@property (strong,nonatomic)NSString *url , *userId , *campaignId;
//@property (strong, nonatomic) IBOutlet UIView *mainView;
//@property (strong, nonatomic) IBOutlet UIImageView *imageForAdsCampaign;
//@property (strong, nonatomic) IBOutlet UILabel *titleForAdsCampaign;
//@property (strong, nonatomic) IBOutlet UILabel *messageForAdsCampaign;
//- (IBAction)openUrlButtonAction:(id)sender;
//
//@property (nonatomic, weak) id <AdsCampaignViewDelegate> delegate;
//
//-(void)showOnWindow:(UIWindow *)onWindow;
//
//- (IBAction)closeButtonAction:(id)sender;


//@end

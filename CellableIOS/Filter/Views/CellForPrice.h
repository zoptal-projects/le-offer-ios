//
//  CellForPrice.h

//
//  Created by Rahul Sharma on 03/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CurrencySelectVC.h"


typedef void (^fromPrice)(NSString *fromPrice);
typedef void (^toPrice)(NSString *toPrice);
typedef void (^currencyDetails)(NSString *currencyCode, NSString *currencySymbol);
@interface CellForPrice : UITableViewCell < UITextFieldDelegate,CurrencyDelegate>

@property (strong, nonatomic) IBOutlet UILabel *labelForTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelForCurrency;
@property (strong, nonatomic) IBOutlet UITextField *textField;

@property (weak, nonatomic) IBOutlet UIImageView *dropDownImage;
@property (strong, nonatomic)NSString *currencyCode, *currencySymbol ;
@property (strong,nonatomic) UINavigationController *navigationController;
@property(nonatomic,copy)fromPrice callBackForFromPrice;
@property(nonatomic,copy)toPrice callBackFortoPrice;
@property (nonatomic,copy)currencyDetails callbackForCurrency ;

-(void)setObjectsForIndex :(NSIndexPath *)index minPrice:(NSString *)minPrice maxPrice:(NSString *)maxPrice currencyCode :(NSString *)currencyCode;
@property (weak, nonatomic) IBOutlet UIButton *currencyButtonOutlet;

- (IBAction)currencyButtonAction:(id)sender;

@end

//
//  EditProductViewController.m

//
//  Created by Rahul Sharma on 01/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "EditProductViewController.h"
#import "ScreenConstants.h"
#import "PostingScreenModel.h"
#import "WebServiceHandler.h"
#import "BuyersTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "BuyersViewController.h"
#import "InsightsViewController.h"
#import "PostListingsViewController.h"
#import "ProductDetailsViewController.h"
#import "Cellable-Swift.h"
@interface EditProductViewController ()<UITableViewDataSource,UITableViewDelegate,WebServiceHandlerDelegate,updateCallBack>
{
    PostingScreenModel *post;
    NSInteger imgNumber;
    NSMutableArray *makeOfferUsers, *basicInsights, *timeInsights, *locationInsights;
   
}
@end

@implementation EditProductViewController

/*-----------------------------------------------------*/
#pragma mark - ViewController LifeCycle.
/*----------------------------------------------------*/

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = self.navTitleImageView;
    [self updateFeildsWithData];
     self.markAsSoldView.layer.borderColor = mBaseColor.CGColor ;
    self.updateView.layer.borderColor = mBaseColor.CGColor ;
    
    [self createNavLeftButton];
    [self getPostDetails];
    if(_showInsights)
    {
     [self getInsightsOfProduct];
    }
    
    basicInsights = [NSMutableArray new];
    timeInsights = [NSMutableArray new];
    locationInsights = [NSMutableArray new];
    makeOfferUsers = [NSMutableArray new];
  }

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if(self.product.isPromoted)
    {
        self.promoteThisItem.hidden = YES ;
    }
    else
    {
        self.promoteThisItem.hidden = NO ;
    }
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
}




-(void)getPostDetails
{
        [self.imageViewItemImage sd_setImageWithURL:[NSURL URLWithString:self.product.thumbnailImageUrl] placeholderImage:[UIImage imageNamed:@"itemProdDefault.png"]];
        self.labelProductName.text = self.product.productName ;
        self.labelCategory.text = self.product.category ;
        NSDictionary *dictWeb = @{mauthToken: [Helper userToken],
                                  mpostid: self.product.postId,
                                  mposttype: @"0",
                                  mLimit: @"20",
                                  moffset:@"0",
                                  };
        [WebServiceHandler acceptedOffers:dictWeb andDelegate:self];
        ProgressIndicator *loginPI = [ProgressIndicator sharedInstance];
       [loginPI showPIOnView:self.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
}




/**
 Update Fields acording to posted data.
 */
-(void)updateFeildsWithData
{
    [self.imageViewItemImage sd_setImageWithURL:[NSURL URLWithString:self.product.mainUrl] placeholderImage:[UIImage imageNamed:@"itemProdDefault.png"]];
    self.labelProductName.text = self.product.productName ;
    self.labelCategory.text = self.product.category ;
}


-(void)createNavRightButton
{
    [self.insightsButtonOutlet addTarget:self action:@selector(insightsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *navLeft = [[UIBarButtonItem alloc]initWithCustomView:self.insightsButtonOutlet];
    self.navigationItem.rightBarButtonItem = navLeft;
}
-(void)createNavLeftButton
{
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,20,20)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];

}

-(void)insightsButtonAction:(id)sender
{
    InsightsViewController *insightsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"insightsStoryboard"];
    insightsVC.postId = self.product.postId ;
    insightsVC.basicInsight = basicInsights;
    insightsVC.timeInsight = timeInsights;
    insightsVC.locationInsight = locationInsights ;
    [self.navigationController pushViewController:insightsVC animated:YES];
}

-(void)navLeftButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*-----------------------------------------------------*/
#pragma mark - Button Actions.
/*----------------------------------------------------*/

/**
 Mark button action method.This method willmark product as selling or sold.
 
 @param sender Mark button.
 */
- (IBAction)buttonMarkAsAction:(id)sender {
    if(makeOfferUsers.count != 0 )
    {
    [self performSegueWithIdentifier:@"toSellectionListView" sender:nil];
    }
    else{
        
        UIAlertController *alertController  = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(soldSomewhereElseAlertMessage, soldSomewhereElseAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *yes = [UIAlertAction actionWithTitle:NSLocalizedString(alertYes, alertYes) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
            NSDictionary *requestDic = @{ mpostid : self.product.postId,
                                          mtype : @"0",
                                          mauthToken : [Helper userToken]
                                          };
            
            [ WebServiceHandler soldSomeWhere:requestDic andDelegate:self];
        
        }];
        UIAlertAction *no = [UIAlertAction actionWithTitle:NSLocalizedString(alertNo, alertNo) style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:yes];
        [alertController addAction:no];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"toSellectionListView"]) {
        BuyersViewController *vc = [segue destinationViewController];
        vc.responseDict = makeOfferUsers;
    }
}

/**
 Edit Button action will prsent posting screen for editing.

 @param sender edit button.
 */
- (IBAction)buttonEditAction:(id)sender {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(alertCancel, alertCancel) style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(deleteThisPostActionSheetTitle, deleteThisPostActionSheetTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
     
        ProgressIndicator *loginPI = [ProgressIndicator sharedInstance];
        [loginPI showPIOnView:self.view withMessage:NSLocalizedString(DeletingIndicatorTitle, DeletingIndicatorTitle)];
        
        
        NSDictionary *requestDict = @{mpostid : self.product.postId,
                                      mauthToken : [Helper userToken]};
            
        [WebServiceHandler deletePost:requestDict andDelegate:self];
    }];
    UIAlertAction *editAction = [UIAlertAction actionWithTitle:NSLocalizedString(editThisPostActionSheetTitle, editThisPostActionSheetTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){ PostListingsViewController *shareVC = [self.storyboard instantiateViewControllerWithIdentifier:mPostScreenStoryboardID];
        shareVC.editingPost = YES;
        shareVC.product = self.product ;
        shareVC.popDelegate  = self ;
        UINavigationController *navBar =[[UINavigationController alloc]initWithRootViewController:shareVC];
        [self.navigationController presentViewController:navBar animated:YES completion:nil];;
    }];
    
    [controller addAction:cancelAction];
    [controller addAction:deleteAction];
    [controller addAction:editAction];
    
    mPresentAlertController;
   }

/*-----------------------------------------------------*/
#pragma mark - Webservice Delegate.
/*----------------------------------------------------*/


-(void)internetIsNotAvailable:(RequestType)requsetType
{
    
}

/**
 WebService delegate

 @param requestType RequestType.
 @param response    response by Web Service.
 @param error       error.
 */
-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
     [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    switch (requestType) {
        case RequestTypeacceptedOffers:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200:
                    makeOfferUsers = responseDict[@"data"];
                    [self.tableView reloadData];
                    break;
                case 204 :
                    [self showingMessageForCollectionViewBackground];
                    break;
                default:
                    break;
            }
        }
            break;
            
        case RequestTypeGetPostsByUsers:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    //                self.currentIndex = 0;
                    
                    self.product = [[ProductDetails alloc]initWithDictionary:responseDict[@"data"][0]];
                    [self updateFeildsWithData];
                    [self handlingResponseOfExplorePosts:response];
                    [[ProgressIndicator sharedInstance] hideProgressIndicator];
                }
                    break;
                    
                default:
                    break;
            }
            
            
        }
            break;
            
            case RequestTypeDeletePost:
        {
            switch ([response[@"code"] integerValue])
            {
                case 200:
                    [[NSNotificationCenter defaultCenter] postNotificationName:mDeletePostNotifiName object:[NSDictionary dictionaryWithObject:response forKey:@"data"]];
                    [self.navigationController popViewControllerAnimated:YES];
                    break;
                default:
                    break;
            }
        }
            break;
            
        case RequestTypesoldElseWhere:
        {
            
            switch ([response[@"code"] integerValue])
            {
                case 200:
                    [[NSNotificationCenter defaultCenter]postNotificationName:mSellingToSoldNotification object:[NSDictionary dictionaryWithObject:response[@"data"] forKey:@"data"]];
                    [[NSNotificationCenter defaultCenter]postNotificationName:mDeletePostNotifiName object:[NSDictionary dictionaryWithObject:response[@"data"] forKey:@"data"]];
                    [self.navigationController popViewControllerAnimated:YES];
                    break;
                default:
                    break;
            }
            
        }
        
            break;
        case RequestToGetInsights :
        {
            
            switch ([response[@"code"] integerValue])
            {
                case 200:
                {
                [self createNavRightButton];
                [self extractInsights:responseDict[@"data"]];
                    
                }
                    break;
                default:
                    break;
            }
            
        }
            break ;
        default:
            break;
    }
    
}


-(void)handlingResponseOfExplorePosts :(NSDictionary *) response
{
  
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return makeOfferUsers.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BuyersTableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"ListCell" forIndexPath:indexPath];
    NSDictionary * data = [makeOfferUsers objectAtIndex:indexPath.row];
    cell.nameLabel.text = [NSString stringWithFormat:@"%@",data[@"buyername"]];
   

    NSString *currency = flStrForObj(data[@"currency"]);
    NSString *currencySymbol = [Helper returnCurrency:currency];
    
    cell.addressLabel.text = [NSString stringWithFormat:@"%@ %@%@", NSLocalizedString(priceOfferedStatement, priceOfferedStatement),currencySymbol,flStrForObj(data[@"offerPrice"])] ;
    
    NSString *imageURL1 =[NSString stringWithFormat:@"%@",data[@"buyerProfilePicUrl"]];
        NSURL *imageUrl =[NSURL URLWithString:imageURL1];
        [cell.memberPic sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"defaultpp"]];
    NSString *timeStamp = [Helper convertEpochToNormalTimeInshort:data[@"offerCreatedOn"]];
//    NSString *time = [Helper convertEpochToNormalTime:data[@"offerCreatedOn"]];
    
    cell.timeStampLabel.text = timeStamp;

    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)showingMessageForCollectionViewBackground{
    
    self.backViewForNoOffers.frame = CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height);
    self.tableView.backgroundView = self.backViewForNoOffers;
}

#pragma mark-
#pragma mark- Pop To root viewcontroller

-(void)popToRootViewController:(BOOL)pop
{
    if(pop)
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}




-(void)getInsightsOfProduct
{
    
    NSDictionary *requestDict = @{mauthToken : [Helper userToken],
                                  mpostid :self.product.postId,
                                  mDurationType : @"week"
                                  };
    
    [WebServiceHandler getInsightsOfProduct:requestDict andDelegate:self];
    
    
}
                   
-(void)extractInsights:(NSMutableArray *)response
{
    [basicInsights addObjectsFromArray:response[0][@"basicInsight"][@"data"]];
    
    [timeInsights addObjectsFromArray:response[1][@"timeInsight"][@"data"][@"count"]];
    
    [locationInsights addObjectsFromArray:response[2][@"locationInsight"][@"data"]];
    
    
}


#pragma mark-
#pragma mark - Tap to see full image

- (IBAction)showFullProductImageButtonAction:(id)sender {
    ProductDetailsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
    newView.movetoRowNumber = 0;
    newView.postId = self.product.postId;
    newView.noAnimation = YES;
    [self.navigationController pushViewController:newView animated:YES];
}
- (IBAction)promotoButtonAction:(id)sender {
    
     InAppCellableVC *promotionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InAppCellableVC"];
     promotionVC.productObj2 = self.product;
     UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:promotionVC];
     [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}
@end

//
//  UpDatingEmailViewController.m

//
//  Created by Rahul Sharma on 05/07/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "UpDatingEmailViewController.h"
#import "TinderGenericUtility.h"
#import "WebServiceConstants.h"
#import "WebServiceHandler.h"
#import "FontDetailsClass.h"
#import "Helper.h"

@interface UpDatingEmailViewController ()<WebServiceHandlerDelegate>
{

    UIButton *navDoneButton;
    UIActivityIndicatorView *av;
}
@end

@implementation UpDatingEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavLeftButton];
    [self createNavRightButton];
    self.navigationItem.title = Localized(mNavTitleForUpdateEmail);
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.emailTextField  becomeFirstResponder];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*-------------------------------------------*/
#pragma mark
#pragma mark - navigation bar buttons
/*-------------------------------------------*/

- (void)createNavLeftButton {
    
    if(self.verifyEmail)
    {
        UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mCloseButton normalState:mCloseButton];
        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
        [navLeft setFrame:CGRectMake(0,0,25,40)];
        self.navigationItem.leftBarButtonItem = navLeftButton;
        [navLeft addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
        
    }
    else {
    UIButton *navCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton rotateButton];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]
                     forState:UIControlStateSelected];
    [navCancelButton addTarget:self
                        action:@selector(backButtonClicked)
              forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0,0,25,40)];
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    }
}
//method for creating navigation bar right button.
- (void)createNavRightButton {
    navDoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [navDoneButton setTitle:@"Done"
                   forState:UIControlStateNormal];
     [navDoneButton setTitleColor:mNavigationBarColor forState:UIControlStateNormal];
    navDoneButton.titleLabel.font = [UIFont fontWithName:RobotoRegular size:17];
    [navDoneButton setFrame:CGRectMake(0,0,50,30)];
    [navDoneButton addTarget:self action:@selector(doneButtonClicked)
            forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navDoneButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
     negativeSpacer.width = -10;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

- (void)backButtonClicked {
    
    if(_verifyEmail)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
    [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark
#pragma mark - Email validation

- (BOOL)validateEmail:(NSString *)checkString {
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
 -(void)doneButtonClicked {
     
    if ([self validateEmail:[_emailTextField text]])
    {
    navDoneButton.hidden = YES;
    [self createActivityViewInNavbar];
    // Requesting For Post Api.(passing "token" as parameter)
    NSDictionary *requestDict = @{
                                  mauthToken :flStrForObj([Helper userToken]),
                                  mEmail :flStrForObj(self.emailTextField.text)
                                  };
    [WebServiceHandler RequestTypeEmailCheckEditProfile:requestDict andDelegate:self];
    }
    else {
        [Helper showAlertWithTitle:@"Cellable" Message:@"Enter a Valid Email Id" viewController:self];
    }
}

//method for creating activityview in  navigation bar right.
- (void)createActivityViewInNavbar {
    av = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [av setFrame:CGRectMake(0,0,50,30)];
    [self.view addSubview:av];
    av.tag  = 1;
    [av startAnimating];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:av];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/*---------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*---------------------------------*/

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
  
    [self createNavRightButton];
    
    if (error) {
        [Helper showAlertWithTitle:@"Cellable" Message:[error localizedDescription] viewController:self];
        return;
    }
    //storing the response from server to dictonary.
    NSDictionary *responseDict = (NSDictionary*)response;
    //checking the request type and handling respective response code.
    if (requestType ==  RequestTypeEmailCheckInEditProfile ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
            
                NSDictionary *requestDict = @{
                                              mauthToken :flStrForObj([Helper userToken]),
                                              mEmail :flStrForObj(self.emailTextField.text)
                                              };
            [WebServiceHandler sendEmailLink:requestDict andDelegate:self];
           
            }
                break;
                //failure response.
            case 409: {
                [self errAlert:Localized(mEmailAlreadyRegistered)];
            }
                break;
            case 204: {
                [self errAlert:@"user not Found"];
            
                break;
            }
                break;

            case 500: {
                [self errAlert:@"Sorry Internal Server"];
            }
                break;
            default:
            
                 [self errAlert:response[@"message"]];
                break;
        }
    }
    
    if (requestType ==  RequestTypeupdateEmail )
    {
         [av stopAnimating];
        NSLog(@"responseDict=%@",responseDict);
        switch ([responseDict[@"code"] integerValue])
        {
            case 200: {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"passEmailData" object:[NSDictionary dictionaryWithObject:self.emailTextField.text forKey:@"updatedEmailId"]];
                if(_verifyEmail)
                {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                else
                    [self.navigationController popViewControllerAnimated:YES];
            }
                break;
            default:
                
                [self errAlert:response[@"message"]];
                break;
        }
    }
}

- (void)errAlert:(NSString *)message {
    //creating alert for error message.
    [Helper showAlertWithTitle:Localized(alertMessage) Message:message viewController:self];
}


@end

//
//  UpdatePhoneNumberVC.m

//
//  Created by Rahul Sharma on 7/22/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "UpdatePhoneNumberVC.h"
#import "TinderGenericUtility.h"
#import "WebServiceConstants.h"
#import "WebServiceHandler.h"
#import "CountryListViewController.h"
#import "FontDetailsClass.h"
#import "Helper.h"
#import "OtpScreenViewController.h"


@interface UpdatePhoneNumberVC ( )<WebServiceHandlerDelegate>
{
    UIButton *nextButton;
    UIActivityIndicatorView *av;
}
@end

@implementation UpdatePhoneNumberVC
-(void)viewDidLoad {
    [super viewDidLoad];
    [self createNavRightButton];
    [self createNavLeftButton];
    self.navigationItem.title = Localized(mNavTitleForUpdateNumber);
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    [self.phoneNumberTextField becomeFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBar.hidden = NO ;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.view endEditing:YES];
}


/*-------------------------------------------*/
#pragma mark
#pragma mark - navigation bar buttons
/*-------------------------------------------*/

- (void)createNavLeftButton {
    
    if(self.verify)
    {
        UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mCloseButton normalState:mCloseButton];
        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
        [navLeft setFrame:CGRectMake(0,0,25,40)];
        self.navigationItem.leftBarButtonItem = navLeftButton;
        [navLeft addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];

    }
    else
    {
    UIButton *navCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
     [navCancelButton rotateButton];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]
                     forState:UIControlStateSelected];
    [navCancelButton addTarget:self
                        action:@selector(backButtonClicked)
              forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0,0,25,40)];
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    }
}

//method for creating navigation bar right button.
- (void)createNavRightButton {
   nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton setTitle:Localized(nextButtonTitle)
                   forState:UIControlStateNormal];
      [nextButton setTitleColor:mBaseColor forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont fontWithName:RobotoRegular size:17];
    [nextButton setFrame:CGRectMake(0,0,40,40)];
    [nextButton addTarget:self action:@selector(nextButtonClicked)
            forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:nextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

- (void)backButtonClicked {
    if(self.verify)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
    [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)nextButtonClicked {
    if ([self validatePhone:_phoneNumberTextField.text]) {
        nextButton.hidden = YES;
        [self createActivityViewInNavbar];
        _phoneNumberWithCountryCode  = [self.countryCodeLabel.text stringByAppendingString:self.phoneNumberTextField.text ];
        NSDictionary *requestDict = @{
                                      mauthToken :flStrForObj([Helper userToken]),
                                      mphoneNumber :flStrForObj(_phoneNumberWithCountryCode)
                                      };
        [WebServiceHandler RequestTypePhoneNumberCheckEditProfile:requestDict andDelegate:self];
    }
    else {
        
        [Helper showAlertWithTitle:@"Cellable" Message:Localized(phoneNumberIsInvalid) viewController:self];
    }
}

#pragma mark
#pragma mark - phone number validation

- (BOOL)validatePhone:(NSString *)enteredphoneNumber {
    NSString *phoneNumber = enteredphoneNumber;
    NSString *phoneRegex = @"[2356789][0-9]{6}([0-9]{3})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:phoneNumber];
    return matches;
}

//method for creating activityview in  navigation bar right.
- (void)createActivityViewInNavbar {
    av = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [av setFrame:CGRectMake(0,0,50,30)];
    [self.view addSubview:av];
    av.tag  = 1;
    [av startAnimating];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:av];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/*-------------------------------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*-------------------------------------------------------*/

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    
    [av stopAnimating];
    [self createNavRightButton];
    
    if (error) {
        [Helper showAlertWithTitle:Localized(alertError) Message:[error localizedDescription] viewController:self];
        return;
    }
    //storing the response from server to dictonary.
    NSDictionary *responseDict = (NSDictionary*)response;
    //checking the request type and handling respective response code.
    if (requestType ==  RequestTypePhoneNumberCheckEditProfile ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
                OtpScreenViewController *otpVC = [self.storyboard instantiateViewControllerWithIdentifier:mOtpStoryboardID];
                otpVC.numb = self.phoneNumberWithCountryCode;
                otpVC.updateNumber = YES;
                [self.navigationController pushViewController:otpVC animated:YES];
                
            }
                break;
            default:
                 [self errAlert:responseDict[@"message"]];
                break;
        }
    }
}

- (void)errAlert:(NSString *)message {
    //creating alert for error message.
    [Helper showAlertWithTitle:Localized(alertMessage) Message:message viewController:self];
}

- (IBAction)countrySelectionButton:(id)sender {
    CountryListViewController *cv = [[CountryListViewController alloc] initWithNibName:@"CountryListViewController" delegate:self];
    [self presentViewController:cv animated:YES completion:NULL];
}

- (void)didSelectCountry:(NSDictionary *)country {
    _countryCodeLabel.text =[country objectForKey:@"dial_code"];
    NSString *Name =[country objectForKey:@"code"];
   
   // self.countryNameLabel.text= countryNameWithCode ;
    [_countryNameLabel setTitle:Name forState:UIControlStateNormal];
}





@end

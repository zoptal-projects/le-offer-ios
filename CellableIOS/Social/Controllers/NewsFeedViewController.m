
//  NewsFeedViewController.m
//  Created by Ajay Thakur on 28/09/17
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "NewsFeedViewController.h"
#import "ProductImageTableViewCell.h"
#import "LikeCommentTableViewCell.h"
#import "SVPullToRefresh.h"
#import "FontDetailsClass.h"
#import "UIImageView+WebCache.h"
#import "UIImage+GIF.h"
#import "ProfileViewController.h"
#import "CommentsViewController.h"
#import "LikeViewController.h"
#import "PGDiscoverPeopleViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ActivityViewController.h"
#import "LikersCollectionViewCell.h"
#import "ProductDetailsViewController.h"
#import "ProductDetails.h"
#import "SectionHeaderTableViewCell.h"
@import FirebaseInstanceID;

@interface NewsFeedViewController ()<WebServiceHandlerDelegate,UIActionSheetDelegate,UITableViewDelegate,UITableViewDataSource,CLUploaderDelegate,UIGestureRecognizerDelegate,CLUploaderDelegate,ZoomTransitionProtocol,GetCurrentLocationDelegate ,ProductDetailsDelegate>
{
    UIRefreshControl *refreshControl;
    NSDictionary *cloundinaryCreditinals;
    NSString *thumbNailUrl ,*userName;
    NSString *mainUrl;
    UILabel *errorMessageLabelOutlet;
    NSIndexPath *selectedCellIndexPathForActionSheet;
    UIProgressView *customprogressView;
    NSMutableArray *arrayOfProfilePicsInNewsFeed , *arrayOfProfilePicsForEvent, *productsArray;
    GetCurrentLocation *getLocation;
    ProductDetails *product ;
}
@property (nonatomic)double currentLat, currentLong;
@property int currentIndex,paging;
@property NSInteger cellDisplayedIndex;
@property NSInteger presentCellIndex;
@property bool classIsAppearing , newsFeedLikesRefresh;
@property (nonatomic, strong) ZoomInteractiveTransition * transition;
@end

@implementation NewsFeedViewController


-(void)viewDidLoad {
    [super viewDidLoad];
    self.transition = [[ZoomInteractiveTransition alloc] initWithNavigationController:self.navigationController];
    self.viewWhenNoPostsOutlet.hidden = NO;
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    arrayOfProfilePicsInNewsFeed = [NSMutableArray new];
    arrayOfProfilePicsForEvent = [NSMutableArray new];
    cloundinaryCreditinals =[[NSUserDefaults standardUserDefaults]objectForKey:cloudinartyDetails];
    productsArray = [NSMutableArray new];
    [self addingRefreshControl];
    [self customeError];
    [self creatingNotificationForUpdatingLikeDetails];
    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];  [HomePI showPIOnView:self.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
    //NSString *token = [[FIRInstanceID instanceID] token];
   // [[NSUserDefaults standardUserDefaults] setObject:token forKey:mdeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self setNavigationBar];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if(self.tabBarController.tabBar.hidden)
    {
        self.tabBarController.tabBar.hidden = NO;
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
    if(![Helper userName].length){
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        userName = [ud objectForKey:@"newRegisterUsername"];
    }
    else
        userName = [Helper userName];
    
     if (productsArray.count == 0) {
         self.currentIndex = 0;
        [self serviceRequestingForPosts:self.currentIndex];
    }
    
    _classIsAppearing = YES;
}


/**
 Clear cache memory on warning.
 */
-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
}




-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:true];
    _classIsAppearing = NO ;
}


-(void)setNavigationBar
{
    self.navigationController.navigationBar.translucent = NO ;

}

- (IBAction)bellNotificationButtonAction:(id)sender {
    
    
    
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        UINavigationController *nav = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
    else{

        ActivityViewController *activityVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
        
    }
}


/*---------------------------------------------------------*/
#pragma mark - Pull To refresh
/*---------------------------------------------------------*/
-(void)addingRefreshControl {
    refreshControl = [[UIRefreshControl alloc]init];
    [self.tableViewOutlet addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
}
-(void)refreshData:(id)sender {
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    [self serviceRequestingForPosts:self.currentIndex];
}

/*---------------------------------------------------------*/
#pragma mark - Notifications Method
/*---------------------------------------------------------*/

/**
 Delete post based on Notification
 
 @param noti notification object
 */
-(void)deletePostFromNotificationInNewsFeed:(NSNotification *)noti {
    NSString *updatepostId = flStrForObj(noti.object[@"data"][@"postId"]);
    for (int i=0; i <productsArray.count;i++) {
        
        ProductDetails *product = productsArray[i];
        if ([product.postId isEqualToString:updatepostId])
        {
            //NSUInteger atSection = [selectedCellIndexPathForActionSheet section];
            selectedCellIndexPathForActionSheet = [NSIndexPath indexPathForRow:0 inSection:i];
            [self removeRelatedDataOfDeletePost:i];
            [self.tableViewOutlet beginUpdates];
            [self.tableViewOutlet deleteSections:[NSIndexSet indexSetWithIndex:selectedCellIndexPathForActionSheet.section] withRowAnimation:UITableViewRowAnimationNone];
            [self.tableViewOutlet endUpdates];
            
            break;
            
        }
    }
}
- (void)removeRelatedDataOfDeletePost:(NSInteger )atSection {
    
    [productsArray removeObjectAtIndex:atSection];
    
    if (productsArray.count == 0) {
        self.viewWhenNoPostsOutlet.hidden = NO;
    }
}


/**
 Remove observer on memory deallocating.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:mDeletePostNotifiName];
    [[NSNotificationCenter defaultCenter]removeObserver:@"updatePostDetails"];
    [[NSNotificationCenter defaultCenter]removeObserver:@"likesEvent"];
    
}


/**
 Notifications Method.
 */
-(void)creatingNotificationForUpdatingLikeDetails {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePostFromNotificationInNewsFeed:) name:mDeletePostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDetails:) name:@"updatePostDetails" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLikesEvent:) name:@"likesEvent" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePostsForFollowUnfollow:) name:@"updatedFollowStatus" object:nil];
}



/*-----------------------------------------------*/
#pragma mark -
#pragma mark - Update Posts on Follow/Unfollow
/*-----------------------------------------------*/

-(void)updatePostsForFollowUnfollow:(NSNotification *)noti {
    [self refreshData:self];
}



/**
 Handle event for likes

 @param noti notification object is dictionary.
 */
-(void)handleLikesEvent:(NSNotification *)noti
{
    _newsFeedLikesRefresh = YES ;
    arrayOfProfilePicsForEvent = noti.object[@ "arrayOfProfilePics"];
    NSIndexPath* rowToReload = noti.object[@"indexPath"];
    NSIndexPath* secondRowToreload  = [NSIndexPath indexPathForRow:1 inSection:rowToReload.section];
    NSArray* rowsToReload = [NSArray arrayWithObjects:secondRowToreload, nil];
    productsArray[rowToReload.section] =  noti.object[@"productDetails"];
    [self.tableViewOutlet reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
//    if (product.likeStatus) {
//        product.likeStatus = YES ;
//    }
//    else {
//         product.likeStatus = NO ;
//    }
//    NSString *updatedNumberOfLikes = product.likes ;
//    [[productsArray objectAtIndex:rowToReload.section] setObject:updatedNumberOfLikes forKey:@"likes"];
    
}


/**
 Update clicks on each successfull view.

 @param noti notification object is dictionary.
 */
-(void)updateClickCount:(NSNotification *)noti
{
    NSIndexPath* rowToReload = noti.object[@"indexPath"];
    NSIndexPath* secondRowToreload  = [NSIndexPath indexPathForRow:1 inSection:rowToReload.section];
     productsArray[rowToReload.section] = noti.object[@"productDetails"];
    NSArray* rowsToReload = [NSArray arrayWithObjects:secondRowToreload, nil];
   
//    [[self.dataArray objectAtIndex:rowToReload.section] setObject:prod.clickCount forKey:@"clickCount"];
    [self.tableViewOutlet reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}


-(void)updateDetails:(NSNotification *)noti {
    //check the postId and Its Index In array.
//    if (!_classIsAppearing) {
//
//        NSString *updatepostId = flStrForObj(noti.object[@"profilePicUrl"][@"data"][0][@"postId"]);
//
//        for (int i=0; i <self.dataArray.count;i++) {
//
//            if ([flStrForObj(self.dataArray[i][@"postId"]) isEqualToString:updatepostId])
//            {
//                //  updating the new data and reloading particular section.(row is constant)
//                // row 1 is likebutton.
//
//                if ([flStrForObj(noti.object[@"profilePicUrl"][@"message"]) isEqualToString:@"unliked the post"]) {
//                    //notification for unlike a post
//                    //so update like status to zero.
//
//                    [[self.dataArray objectAtIndex:i] setObject:@"0" forKey:@"likeStatus"];
//                }
//                else {
//                    [[self.dataArray objectAtIndex:i] setObject:@"1" forKey:@"likeStatus"];
//                }
//
//                NSString *updatedNumberOfLikes = flStrForObj(noti.object[@"profilePicUrl"][@"data"][0][@"likes"]);
//                [[self.dataArray objectAtIndex:i] setObject:updatedNumberOfLikes forKey:@"likes"];
//                _dataArray[i][@"likes"] = noti.object[@"profilePicUrl"][@"data"][0][@"likes"];
//
//                NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:1 inSection:i];
//                NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
//                [self.tableViewOutlet reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
//
//                break;
//            }
//        }
//    }
}




-(void)serviceRequestingForPosts :(NSInteger )index {
    //service requesting
    NSDictionary *requestDict = @{
                                  mauthToken:flStrForObj([Helper userToken]),
                                  moffset:flStrForObj([NSNumber numberWithInteger:index* mPagingValue]),
                                  mlimit:flStrForObj([NSNumber numberWithInteger:mPagingValue])
                                  };
    [WebServiceHandler getPostsInHOmeScreen:requestDict andDelegate:self];
}

//   navigation bar back button

/*---------------------------------------------------*/
#pragma
#pragma mark -  Table view data source and delegates
/*---------------------------------------------------*/


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section +5 == productsArray.count && self.paging != self.currentIndex  && self.cellDisplayedIndex!=indexPath.section) {
        self.cellDisplayedIndex = indexPath.section ;
        [self serviceRequestingForPosts:self.currentIndex];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return productsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     ProductDetails *product = productsArray[indexPath.section];
    if(indexPath.row ==0) {
        ProductImageTableViewCell *cell = (ProductImageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ProductImageCell" forIndexPath:indexPath];
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell setProductImage:productsArray[indexPath.section]];
            cell.imageViewOutlet.tag = 5000 +indexPath.section;
        });
        return cell;
    }
    else {
//        [arrayOfProfilePicsInNewsFeed removeAllObjects];
//        NSInteger likeCount = [product.likes integerValue];
//        if(likeCount >5)
//            likeCount = 5;

//            for(int j=0;j<likeCount;j++){
//            @try
//            {
//                NSString *userNamel = flStrForObj(product.likedByUsers[j][@"likedByUsers"]);
//                NSString *profilePicOfUser = flStrForObj(product.likedByUsers[j][@"profilePicUrl"]);
//                NSMutableDictionary  *tempDict = [[NSMutableDictionary alloc] init];
//                [tempDict setValue:userNamel forKey:@"likedByUsers"];
//                [tempDict setValue:profilePicOfUser forKey:@"profilePicUrl"];
//                [arrayOfProfilePicsInNewsFeed addObject:tempDict];
//            }
//                // To handle any exception on hiting Like button continuous manner.
//                   @catch (NSException *exception)
//                   {
//                   }
//                   @finally
//                   {
//                   }
//            }

        LikeCommentTableViewCell *cell = (LikeCommentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"LikeTableViewNewsFeedCell"];

        if(_newsFeedLikesRefresh)
        {
          _newsFeedLikesRefresh = NO ;
            [arrayOfProfilePicsInNewsFeed removeAllObjects];
            [arrayOfProfilePicsInNewsFeed addObjectsFromArray:arrayOfProfilePicsForEvent];
        }
        cell.product = product;
        cell.arrayOfProfilePics = arrayOfProfilePicsInNewsFeed;
        cell.index = indexPath;
        cell.collectionViewCellID = mCollectionViewCellIDInNewsFeed;
        cell.navController = self.navigationController;
        [cell setPropertiesOfOutletscheckFlag:NO];
        [cell.viewCountButton setTitle:product.clickCount forState:UIControlStateNormal];
        [cell setPriceCurrencyNameOfProduct:product];

        if(product.likeStatus){
            [cell.likeButtonOutlet setTitle:product.likes forState:UIControlStateSelected];
            cell.likeButtonOutlet.layer.borderColor = mBaseColor2.CGColor;
            cell.likeButtonOutlet.selected = YES;
        }
        else{
            [cell.likeButtonOutlet setTitle:product.likes forState:UIControlStateNormal];
            cell.likeButtonOutlet.selected = NO;
            cell.likeButtonOutlet.layer.borderColor = [UIColor colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:.7].CGColor;
        }

        cell.moreButtonOutlet.tag = 1000 + indexPath.section;
        cell.likeButtonOutlet.tag = indexPath.section;
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row ==0)
        return self.view.frame.size.width;
    else
        return 120;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        ProductImageTableViewCell *cell = (ProductImageTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:indexPath];
        ProductDetailsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
        ProductDetails *product = productsArray[indexPath.section];
        newView.hidesBottomBarWhenPushed = YES;
        newView.productDelegate = self ;
        newView.product = product;
        newView.postId = product.postId;
        newView.movetoRowNumber =indexPath.item;
        newView.dataFromHomeScreen = YES;
        newView.isNewsFeedSocial = YES ;
        newView.indexPath = indexPath ;
        newView.imageFromHome = cell.imageViewOutlet.image;
        newView.currentCity = getLocation.currentCity;
        newView.countryShortName = getLocation.countryShortCode ;
        newView.currentLattitude = [NSString stringWithFormat:@"%lf",self.currentLat];
        newView.currentLongitude = [NSString stringWithFormat:@"%lf",self.currentLong];
        [self.navigationController pushViewController:newView animated:YES];
    }
}



- (void)stopAnimation {
    __weak NewsFeedViewController *weakSelf = self;
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.tableViewOutlet.pullToRefreshView stopAnimating];
        [weakSelf.tableViewOutlet.infiniteScrollingView stopAnimating];
    });
}

/*---------------------------------------*/
#pragma
#pragma mark - Reloading TableView
/*---------------------------------------*/

-(void)animateButton:(UIButton *)likeButton {
    CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    [ani setDuration:0.2];
    [ani setRepeatCount:1];
    [ani setFromValue:[NSNumber numberWithFloat:1.0]];
    [ani setToValue:[NSNumber numberWithFloat:0.5]];
    [ani setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[likeButton layer] addAnimation:ani forKey:@"zoom"];
}

-(void)reloadRowToShowNewNumberOfLikes:(NSInteger )reloadRowAtSection {
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:1 inSection:reloadRowAtSection];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.tableViewOutlet reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
}

/*---------------------------------------*/
#pragma
#pragma mark - Button Actions
/*---------------------------------------*/

- (IBAction)likeButtonAction:(id)sender {
    UIButton *likeButton = (UIButton *)sender;
    // adding animation for selected button
    ProductDetails *product = productsArray[likeButton.tag] ;
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        
        UINavigationController *navigation = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
    }
    else{
        [self animateButton:likeButton];
        if(likeButton.selected) {
            likeButton.selected = NO;
            product.likeStatus = NO;
            NSInteger newNumberOfLikes = [product.likes integerValue] ;
            newNumberOfLikes --;
            [likeButton setTitle:[NSString stringWithFormat:@"%ld",(long)newNumberOfLikes] forState:UIControlStateNormal];
            product.likes =  [NSString stringWithFormat:@"%ld",(long)newNumberOfLikes];
//            [arrayOfProfilePicsInNewsFeed removeAllObjects];
//            [arrayOfProfilePicsInNewsFeed addObjectsFromArray:product.likedByUsers];
//            for(int j=0;j<arrayOfProfilePicsInNewsFeed.count;j++)
//            {
//                if([flStrForObj(arrayOfProfilePicsInNewsFeed[j][@"likedByUsers"]) isEqualToString:userName])    // remove ProfilePic if userName has liked the post.
//                    [product.likedByUsers removeObjectAtIndex:j];
//            }
            // Notify to favorites
            NSDictionary *dicForUnlike = @{
                                             @"notificationForLike":@"0",
                                             @"postdetails":product
                                           };
            [[NSNotificationCenter defaultCenter] postNotificationName:mfavoritePostNotifiName object:dicForUnlike];
            [self reloadRowToShowNewNumberOfLikes:likeButton.tag];
            [self unlikeAPost:product.postId postType:@"0"];
        }
        else  {
            likeButton.selected = YES;
            product.likeStatus = YES;
            NSInteger newNumberOfLikes = [product.likes integerValue];
            newNumberOfLikes ++;
            [likeButton setTitle:[NSString stringWithFormat:@"%ld",(long)newNumberOfLikes] forState:UIControlStateSelected];
            NSDictionary *dic = [NSMutableDictionary new];
            [dic setValue:product.profilePicUrl forKey:@"profilePicUrl"]; // add Profile pic of user If liked he post.
            [dic setValue:userName forKey:@"likedByUsers"];
          //  [product.likedByUsers insertObject:dic atIndex:0];
            product.likes = [NSString stringWithFormat:@"%ld",newNumberOfLikes];
            NSDictionary *dicForlike = @{
                                         @"notificationForLike":@"1",
                                         @"postdetails":product
                                         };
            [[NSNotificationCenter defaultCenter] postNotificationName:mfavoritePostNotifiName object:dicForlike];
            [self reloadRowToShowNewNumberOfLikes:likeButton.tag];
            [self likeAPost:product.postId postType:@"0"];
        }
        
    }
    
    
}


/*----------------------------------------------------------------------------*/
#pragma
#pragma mark - tableview Header Buttons And Actions.
/*----------------------------------------------------------------------------*/


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{  return 60;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    static NSString *CellIdentifier = @"sectionHeaderCell";
    SectionHeaderTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    headerView.headerProfileButtonOulet.tag = section ;
    [headerView setSectionHeader:productsArray[section]];
       return headerView;
}


-(void)customeError
{
    errorMessageLabelOutlet = [[UILabel alloc]initWithFrame:CGRectMake(0, -80, [UIScreen mainScreen].bounds.size.width, 50)];
    errorMessageLabelOutlet.backgroundColor = [UIColor colorWithRed:108/255.0f green:187/255.0f blue:79/255.0f alpha:1.0];
    errorMessageLabelOutlet.textColor = [UIColor whiteColor];
    errorMessageLabelOutlet.textAlignment = NSTextAlignmentCenter;
    [errorMessageLabelOutlet setHidden:YES];
    [self.view addSubview:errorMessageLabelOutlet];
}

#pragma mark-Error Alert
-(void)showingErrorAlertfromTop:(NSString *)message {
    [errorMessageLabelOutlet setHidden:NO];
    
    [errorMessageLabelOutlet setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
    [self.view layoutIfNeeded];
    errorMessageLabelOutlet.text = message;
    
    /**
     *  changing the error message view position if user enter  wrong number
     */
    
    [UIView animateWithDuration:0.4 animations:
     ^ {
         
         [self.view layoutIfNeeded];
     }];
    
    int duration = 2; // duration in seconds
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.4 animations:
         ^ {
             [errorMessageLabelOutlet setFrame:CGRectMake(0, -100, [UIScreen mainScreen].bounds.size.width, 100)];
             [errorMessageLabelOutlet setHidden:YES];
             [self.view layoutIfNeeded];
         }];
    });
}

/*---------------------------------------------------*/
#pragma mark
#pragma mark - converting video to thumbnail image
/*---------------------------------------------------*/

-(UIImage *)gettingThumbnailImage :(NSString *)url {
    NSURL *videoURl = [NSURL fileURLWithPath:url];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURl options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    
    UIImage *img = [[UIImage alloc] initWithCGImage:imgRef];
    return img;
}

- (void)makeMyProgressBarMoving {
    if (customprogressView.progress < 1) {
        //it will progress of 0.016666666666 for every sec.(value for 60 secs.)(1/60)
        [customprogressView setProgress:0.1 + customprogressView.progress animated:YES];
    }
    else if (customprogressView.progress == 1){
        [customprogressView setProgress:0 animated:NO];
    }
}


- (IBAction)headerProfileButtonACtion:(id)sender {
    UIButton *selectedHeaderButton = (UIButton *)sender;
    NSInteger selectedIndex = selectedHeaderButton.tag % 10000;
    ProductDetails *product = productsArray[selectedIndex];
    [self openProfileOfUsername:product.membername];
}


/*-----------------------------------------------------------------------------*/
#pragma
#pragma mark - REQUESTING SERVICES .
/*------------------------------------------------------------------------------*/


-(void)likeAPost:(NSString *)postId postType:(NSString *)postType {
    NSDictionary *requestDict = @{
                        mauthToken:flStrForObj([Helper userToken]),
                        mpostid:postId,
                        mLabel:@"Photo"
                        };
    [WebServiceHandler likeAPost:requestDict andDelegate:self];
}

-(void)unlikeAPost:(NSString *)postId postType:(NSString *)postType {
   NSDictionary *requestDict = @{
                        mauthToken:flStrForObj([Helper userToken]),
                        mpostid:postId,
                        mLabel:@"Photo",
                        mUserName: userName
                        };
    [WebServiceHandler unlikeAPost:requestDict andDelegate:self];
}


/*----------------------------------------------------------------------*/
#pragma mark
#pragma mark - WebServiceDelegate(Response)
/*----------------------------------------------------------------------*/

//handling response.

-(void)internetIsNotAvailable:(RequestType)requsetType
{
    [refreshControl endRefreshing];
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    
    if(!productsArray.count)
    {
    self.tableViewOutlet.backgroundView = [Helper showMessageForNoInternet:YES forView:self.view];
    self.viewWhenNoPostsOutlet.hidden = YES;
    self.tableViewOutlet.hidden = NO;
    }
}

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    [refreshControl endRefreshing];
    if (error) {
        
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
        return ;
        }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    if (requestType == RequestTypegetPostsInHOmeScreen ) {
        self.tableViewOutlet.backgroundView = nil ;
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
                self.viewWhenNoPostsOutlet.hidden = YES;
                self.tableViewOutlet.hidden = NO;
                
                if(self.currentIndex == 0) {
                    [productsArray removeAllObjects];
                    productsArray = [ProductDetails arrayOfProducts:response[@"data"]];
                }
                else {
                    NSArray *productsWithPaging = [ProductDetails arrayOfProducts:response[@"data"]];
                    [productsArray addObjectsFromArray:productsWithPaging];
                }
                self.currentIndex ++;
                self.paging ++;
                [self stopAnimation];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableViewOutlet reloadData];
                 });
            }
                break;
            case 204 :
            {
                if(productsArray.count && self.currentIndex == 0)
                {
                    self.currentIndex = 0 ;
                    [productsArray removeAllObjects];
                    self.viewWhenNoPostsOutlet.hidden = NO;
                    self.tableViewOutlet.hidden = YES;
                }
            }
                break ;
            default:
                break;
        }
    }
    else if (requestType == RequestTypeLikeAPost) {
        
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
            }
                break;
                default:
                break;
        }
    }
    else if (requestType == RequestTypeUnlikeAPost) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updatePostDetails" object:[NSDictionary dictionaryWithObject:responseDict forKey:@"profilePicUrl"]];
            }
                break;
                default:
                break;
        }
    }else   if (requestType == RequestTypeunseenNotificationCount) {
        
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
             }
                break;
            
        }
    }
}


- (IBAction)showAllLikersButtonAction:(id)sender {
    
    UIButton *moreButton = (UIButton *)sender;
    NSInteger tag = moreButton.tag%1000;
    ProductDetails *product = productsArray[tag];
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        UINavigationController *navigation = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
    }
    else{
        LikeViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"likeStoryBoardId"];
        newView.navigationTitle = NSLocalizedString(navTitleForLikes,navTitleForLikes );
        newView.postId =  product.postId ;
        newView.postType = @"0";
        [self.navigationController pushViewController:newView animated:YES];
    }
}

- (IBAction)findFriendsButtonAction:(id)sender {
    PGDiscoverPeopleViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"discoverPeopleStoryBoardId"];
    [self.navigationController pushViewController:newView animated:YES];
    
}


-(void)openProfileOfUsername:(NSString *)selectedUserName {
    ProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
    newView.isMemberProfile = YES;
    newView.memberName = selectedUserName;
    newView.productDetails = YES;
    [self.navigationController pushViewController:newView animated:YES];
}


#pragma mark - ZoomTransitionProtocol

-(UIView *)viewForZoomTransition:(BOOL)isSource {
    NSIndexPath *selectedIndexPath = [self.tableViewOutlet indexPathForSelectedRow];
    ProductImageTableViewCell *cell = (ProductImageTableViewCell *)[self.tableViewOutlet cellForRowAtIndexPath:selectedIndexPath];
    return cell.imageViewOutlet;
}


#pragma mark
#pragma mark - Location Delegate -

- (void)updatedLocation:(double)latitude and:(double)longitude
{
 
    self.currentLat = latitude ;
    self.currentLong = longitude ;
    
}

#pragma mark-
#pragma mark - ProductDetails Delegate -


/**
 If product is removed.This delegate method get invoked.
 
 @param indexpath indexpath row.
 */
-(void)productIsRemovedForIndex:(NSIndexPath *)indexpath
{
    [productsArray removeObjectAtIndex:indexpath.section];
    [self.tableViewOutlet reloadData];
}

-(void)updateProductForClickCount:(ProductDetails *)updatedProduct forIndex:(NSIndexPath *)indexpath
{
    productsArray[indexpath.section] = updatedProduct ;
    NSIndexPath* rowToReload = indexpath;
    NSIndexPath* secondRowToreload  = [NSIndexPath indexPathForRow:1 inSection:rowToReload.section];
    NSArray* rowsToReload = [NSArray arrayWithObjects:secondRowToreload, nil];
    [self.tableViewOutlet reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}



@end

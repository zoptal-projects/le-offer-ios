//
//  PrivacyPostPageVC.m
//  Cellable
//
//  Created by Anish Mac Mini on 10/09/20.
//  Copyright © 2020 Anish Mac Mini. All rights reserved.
//

#import "PrivacyPostPageVC.h"

@interface PrivacyPostPageVC ()

@end

@implementation PrivacyPostPageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
 
}
-(void)setNavigationBar
{
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.title = @"Privacy Policy";
    
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,25,40)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
    
    
}
-(void)navLeftButtonAction
{
      [self.navigationController popViewControllerAnimated:YES];;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

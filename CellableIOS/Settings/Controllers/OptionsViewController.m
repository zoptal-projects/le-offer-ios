//
//  OptionsViewController.m

//  Created by Rahul Sharma on 8/4/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "OptionsViewController.h"
#import "ImageTitleTableViewCell.h"
#import "TitleSwitchButtonTableViewCell.h"
#import "FontDetailsClass.h"
#import "EditProfileViewController.h"
#import "ConnectToFaceBookViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import "FontDetailsClass.h"
#import "WebServiceConstants.h"
#import "WebServiceHandler.h"
#import "TinderGenericUtility.h"
#import "AppDelegate.h"
#import "Helper.h"
#import "WebViewForDetailsVc.h"
#import <MessageUI/MessageUI.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import "CBObjects.h"
#import "CouchbaseEvents.h"
#import "ContacDataBase.h"
//#import "PaypalViewController.h"
#import "PurchasedVCOC.h"
#import <FirebaseMessaging/FirebaseMessaging.h>
#import "Cellable-Swift.h"
@import UIKit;

@class MQTT;
@interface OptionsViewController () <UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,WebServiceHandlerDelegate,UIApplicationDelegate,MFMailComposeViewControllerDelegate>
{
    NSArray *tittleArrayForfirstSection;
    NSString *titleForContacts;
    NSString *subTitleForContacts;
    NSString *imageForContcts;
    CBLQueryEnumerator *result;
    
    TitleSwitchButtonTableViewCell *titleswitchcell;
}

@end

@implementation OptionsViewController

/* --------------------------------*/
#pragma mark-
#pragma mark- ViewController LifeCycle
/* --------------------------------*/
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createNavLeftButton];
  
    
    [self settingTitleForFirstSection];
    [self creatingNotificationForUpdatingTitleContacts];
}

-(void)viewWillAppear:(BOOL)animated
{
   self.navigationItem.title = NSLocalizedString(navTitleForSettingsScreen, navTitleForSettingsScreen);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)creatingNotificationForUpdatingTitleContacts {
    NSInteger numberOfContactsInPicogram = [[[NSUserDefaults standardUserDefaults]
                                             stringForKey:numberOfContactsFoundInPicogram] integerValue];
    
    NSString *numberofContscs = [NSString stringWithFormat:@"%ld",(long)numberOfContactsInPicogram];
    
    if (numberOfContactsInPicogram > 0) {
        titleForContacts = NSLocalizedString(connectedContacts, connectedContacts);
        subTitleForContacts = [numberofContscs stringByAppendingString:NSLocalizedString(titleForContacts, titleForContacts)];
        imageForContcts = @"discovery_people_contact_icon";
    }
    else {
        titleForContacts = NSLocalizedString(connectToContacts, connectToContacts);
        subTitleForContacts = NSLocalizedString(toFollowYourFriends, toFollowYourFriends);
        imageForContcts = @"discover_people_contacts_icon_off";
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTitleForContcts:) name:@"updateContactSectionTitle" object:nil];
}

-(void)updateTitleForContcts:(NSNotification *)noti {
    
    NSString *numberOfContcts = flStrForObj(noti.object[@"numberOfContactsSynced"][@"numberOfContacts"]);
    
    NSInteger numberOfContactsInPicogram = [numberOfContcts integerValue];
    
    
    if (numberOfContactsInPicogram > 0) {
        titleForContacts = NSLocalizedString(connectedContacts, connectedContacts);
        subTitleForContacts = [numberOfContcts stringByAppendingString:NSLocalizedString(titleForContacts, titleForContacts)];
        imageForContcts = @"discovery_people_contact_icon";
    }
    else {
        titleForContacts = NSLocalizedString(connectToContacts, connectToContacts);
        
        subTitleForContacts = NSLocalizedString(toFollowYourFriends, toFollowYourFriends);
        imageForContcts = @"discover_people_contacts_icon_off";
    }
    
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:1 inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.optionsTableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}


-(void)settingTitleForFirstSection{
    NSInteger numberOfFaceBookFriendsInPicogram = [[[NSUserDefaults standardUserDefaults]
                                                    stringForKey:numberOfFbFriendFoundInPicogram] integerValue];
    
    tittleArrayForfirstSection = [[NSArray alloc] initWithObjects:titleForFriend,NSLocalizedString(findContacts, findContacts),nil];
    
   if (numberOfFaceBookFriendsInPicogram >0) {
        NSString *titleForFbCell = [[NSString stringWithFormat:@"%lu",(unsigned long)numberOfFaceBookFriendsInPicogram]  stringByAppendingString:@" Facebook Friends"];
        tittleArrayForfirstSection = [[NSArray alloc] initWithObjects:titleForFbCell,NSLocalizedString(findContacts, findContacts),nil];
    }
    else {

    }
}

/**
 Create Navigation Bar Left Button method.
 */
- (void)createNavLeftButton {
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
    
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName  normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,25,40)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}

-(void)backButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}


/* --------------------------------*/
#pragma mark-
#pragma mark- TableView DataSource Methods
/* --------------------------------*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    switch(section){
        case 0:
        case 3:
            return 2;
            break ;
         case 1:
              return 3;
            break ;
        default:  return 1;
            break;
    }
    
}

//Custom Header
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    // creating custom header view
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    view.backgroundColor =[UIColor colorWithRed:0.9804 green:0.9804 blue:0.9804 alpha:1.0];
    UIView *TopLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width,1)];
    view.backgroundColor =[UIColor colorWithRed:0.9753 green:0.9753 blue:0.9753 alpha:1.0];
    UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(49, 0, tableView.frame.size.width,1)];
    view.backgroundColor =[UIColor colorWithRed:0.9753 green:0.9753 blue:0.9753 alpha:1.0];
    /* Create custom view to display section header... */
    AlignmentOfLabel *titileForHeader = [[AlignmentOfLabel alloc] init];
    titileForHeader.text =[mArrayForSectionHeaders objectAtIndex:section];
    [titileForHeader setFont:[UIFont fontWithName:RobotoMedium size:14]];
    titileForHeader.textColor =[UIColor colorWithRed:0.5296 green:0.5296 blue:0.5296 alpha:1.0];
    titileForHeader.frame=CGRectMake(20, 20, self.view.frame.size.width - 40, 15);
    [view addSubview:titileForHeader];
    [view addSubview:TopLine];
    [view addSubview:bottomLine];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 4) {
        return 40.0;
    }
    else {
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ImageTitleTableViewCell   *imagetitleCell;
    imagetitleCell   = [tableView dequeueReusableCellWithIdentifier:@"imageTitleCell"
                                                       forIndexPath:indexPath];
//    if (indexPath.section == 0)
//       {
//           if (indexPath.row == 0)
//           {
//               imagetitleCell.textLabel.text = @"Find Facebook Friends";
//           }
//           else
//           {
//               imagetitleCell.textLabel.text = titleForContacts;
//           }
//
//           imagetitleCell.textLabel.textColor = [UIColor colorWithRed:0.1569 green:0.1569 blue:0.1569 alpha:1.0];
//           imagetitleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//       }
//    else
        if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
        imagetitleCell.textLabel.text =  NSLocalizedString([mTitleArryForSecondSection objectAtIndex:indexPath.row], [mTitleArryForSecondSection objectAtIndex:indexPath.row]) ;
        imagetitleCell.textLabel.textColor = [UIColor colorWithRed:0.1569 green:0.1569 blue:0.1569 alpha:1.0];
        }
        else
        if (indexPath.row == 1)
        {
            imagetitleCell.textLabel.text =  NSLocalizedString([mTitleArryForSecondSection objectAtIndex:indexPath.row], [mTitleArryForSecondSection objectAtIndex:indexPath.row]) ;
            imagetitleCell.textLabel.textColor = [UIColor colorWithRed:0.1569 green:0.1569 blue:0.1569 alpha:1.0];
               }
    }
    else if (indexPath.section ==1)
    {
//        if (indexPath.row == 0)
//               {
//
//        imagetitleCell.textLabel.text =  NSLocalizedString(myPayments, myPayments);
//        imagetitleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        imagetitleCell.textLabel.textColor = [UIColor colorWithRed:0.1569 green:0.1569 blue:0.1569 alpha:1.0];
//               }
        if (indexPath.row == 0)
                     {
                         
              imagetitleCell.textLabel.text =  NSLocalizedString(myAddCard, myAddCard);
              imagetitleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
              imagetitleCell.textLabel.textColor = [UIColor colorWithRed:0.1569 green:0.1569 blue:0.1569 alpha:1.0];
                     }
        if (indexPath.row == 1)
                            {
                                
                     imagetitleCell.textLabel.text =  NSLocalizedString(myCardList, myCardList);
                     imagetitleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                     imagetitleCell.textLabel.textColor = [UIColor colorWithRed:0.1569 green:0.1569 blue:0.1569 alpha:1.0];
                            }
        
        if (indexPath.row == 2)
                                   {
                                       
                            imagetitleCell.textLabel.text =  NSLocalizedString(myPurchaseOrder, myCardList);
                            imagetitleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                            imagetitleCell.textLabel.textColor = [UIColor colorWithRed:0.1569 green:0.1569 blue:0.1569 alpha:1.0];
                                   }
        
    }
    else if (indexPath.section == 2 ) {
        imagetitleCell.textLabel.text =  NSLocalizedString(reportProblem, reportProblem);
        imagetitleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        imagetitleCell.textLabel.textColor = [UIColor colorWithRed:0.1569 green:0.1569 blue:0.1569 alpha:1.0];
    }else if (indexPath.section == 3) {
        
        imagetitleCell.textLabel.text = NSLocalizedString([mTittleArrayForFourthSection objectAtIndex:indexPath.row],[mTittleArrayForFourthSection objectAtIndex:indexPath.row]) ;
        imagetitleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        imagetitleCell.textLabel.textColor = [UIColor colorWithRed:0.1569 green:0.1569 blue:0.1569 alpha:1.0];
        
    }
    else if (indexPath.section ==4) {
        imagetitleCell.textLabel.text = NSLocalizedString(LogOutTitle, LogOutTitle);
        imagetitleCell.accessoryType = UITableViewCellAccessoryNone;
        imagetitleCell.textLabel.textColor = [UIColor colorWithRed:0.0107 green:0.1495 blue:0.3366 alpha:1.0];
    }
    
    if (indexPath.section == 0 && indexPath.section == 1) {
        imagetitleCell.imageView.image = [UIImage imageNamed:[mImageArray objectAtIndex:indexPath.row]];
    }
    else {
        imagetitleCell.imageView.image  = nil;
    }
    return imagetitleCell;
}


/* --------------------------------*/
#pragma mark-
#pragma mark- TableView Delegate Method
/* --------------------------------*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//   //section 0,row o ---> find facebook friends.
//    if (indexPath.section == 0 && indexPath.row ==0) {
//        [self actionForFindFacebookFriends];
//    }
//    //section 0,row 1 ---> find  contacts.
//    if (indexPath.section == 0 && indexPath.row ==1) {
//        [self actionForFindContacts];
//    }
    //section 1,row 0 --->Edit Profile
    if (indexPath.section == 0 && indexPath.row ==0) {
        [self actionForeditProfile];
    }
    //section 2,row 0 ---> Add A New Address
        if (indexPath.section ==0 && indexPath.row ==1)
        {
        SavedAddressVC *addNewAddressVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:maddSavedAddressStoryboardID];
        [ self.navigationController pushViewController :addNewAddressVC animated:YES];
        }
    
//    //section 2,row 0 ---> Report a Problem
//    if (indexPath.section ==2 && indexPath.row ==0) {
//
//        PaypalViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mpaypalView];
//        newView.hidesBottomBarWhenPushed = YES;
//        newView.paypalLink = [[NSUserDefaults standardUserDefaults]objectForKey:payPalLink];
//        [self.navigationController pushViewController:newView animated:YES];
//
//    }
    //section 2,row 0 ---> Add Card
       if (indexPath.section ==1 && indexPath.row ==0)
       {
           SetupCardAdd *addNewAddressVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:maddCardStoryboardID];
           [ self.navigationController pushViewController :addNewAddressVC animated:YES];
       }
    //section 2,row 0 ---> Add Card
    if (indexPath.section ==1 && indexPath.row ==1) {
        
        PaymentMethodViewController *addNewAddressVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:maSavedCardStoryboardID];
        [ self.navigationController pushViewController :addNewAddressVC animated:YES];
    }
    //section 2,row 0 ---> Add Card
       if (indexPath.section ==1 && indexPath.row ==2)
       {
           
           PurchasedVCOC *getPurchasedVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:maGetPurchasedVCdStoryboardID];
           [ self.navigationController pushViewController :getPurchasedVC animated:YES];
       }

    //section 3,row 0 ---> privacy Policy
    if (indexPath.section == 2 && indexPath.row ==0) {
        [self reportaProblem];
    }
    if (indexPath.section == 3 && indexPath.row ==0) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://cellableapp.com/en/privacy"]];
    
    }
    //section 3,row 1---> Terms
    if (indexPath.section ==3 && indexPath.row ==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://cellableapp.com/en/terms"]];
    }
    //section 4,row 0--->  Logout
    if (indexPath.section ==4 && indexPath.row ==0) {
        
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(logoutAlertMessage, logoutAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(alertCancel, alertCancel) style:UIAlertActionStyleDefault handler:nil];
        
        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(alertYes, alertYes) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self actionForLogout];
        }];
        [controller addAction:cancelAction];
        [controller addAction:yesAction];
        [self presentViewController:controller animated:YES completion:nil];
        
    }
}


/* --------------------------------*/
#pragma mark-
#pragma mark- Button Actions
/* --------------------------------*/
/**
 This method get invoked on choosing email option in contact.
 */
-(void)reportaProblem {
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;        // Required to invoke mailComposeController when send
        [mailCont setSubject:NSLocalizedString(problemOn, problemOn)];
        [mailCont setToRecipients:[NSArray arrayWithObject:mReportEmail]];
        mailCont.navigationBar.tintColor = [UIColor blackColor];
        [mailCont.navigationBar setBackgroundColor:[UIColor whiteColor]];
        mailCont.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:mNavigationBarColor};
        [self presentViewController:mailCont animated:YES completion:^{
        }];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

//-(void)actionForFindFacebookFriends {
//    // connectToFaceBookFriendsStoryBoardId
//
//    ConnectToFaceBookViewController *postsVc = [self.storyboard instantiateViewControllerWithIdentifier:mConnectToFaceBookFriendsStoryBoardId];
//    postsVc.syncingContactsOf = @"faceBook";
//    [self.navigationController pushViewController:postsVc animated:YES];
//}


//-(void)actionForFindContacts {
//    ConnectToFaceBookViewController *postsVc = [self.storyboard instantiateViewControllerWithIdentifier:mConnectToFaceBookFriendsStoryBoardId];
//    postsVc.syncingContactsOf = @"phoneBook";
//    [self.navigationController pushViewController:postsVc animated:YES];
//}


-(void)actionForLogout {
    
    
    ProgressIndicator *logOurPI = [ProgressIndicator sharedInstance];
    [logOurPI showPIOnView:self.view withMessage:NSLocalizedString(LoggingOutIndicatorTitle, LoggingOutIndicatorTitle)];
    
    
    [[NSUserDefaults standardUserDefaults] setValue:@"recntlyLoggedOut" forKey:@"showLoginScreen"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate *appDelegateTemp = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        UIViewController* rootController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"TabBarStoryboardID"];
        appDelegateTemp.window.rootViewController = rootController;
    });
    
    NSDictionary *requestDict = @{ mauthToken:[Helper userToken],
                                   mpushToken : [Helper deviceToken]
                                   };
    
    [WebServiceHandler Logout:requestDict andDelegate:self];
    [[MQTTChatManager sharedInstance] sendOnlineStatusWithOfflineStatus:YES];
     [[[MQTT sharedInstance] manager] disconnectWithDisconnectHandler:nil];
    [[MQTTChatManager sharedInstance] unsubscribeAllTopics];
    [[MQTT sharedInstance] setManager:nil];
    [[FIRMessaging messaging] unsubscribeFromTopic:[Helper getMQTTID]];
    
    ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
    status =  kABAuthorizationStatusDenied;
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userDetailWhileRegistration"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"preferenceName"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userFbDetails"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"phoneContacts"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userToken"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userFullName"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:mMqttId];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:numberOfFbFriendFoundInPicogram];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:numberOfContactsFoundInPicogram];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self deletCouchDataBase];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tabBarController setSelectedIndex:0];
        [self.view removeFromSuperview];
    });
}

-(void)deletCouchDataBase
{
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        if (![key isEqualToString:cloudinartyDetails] && ![key isEqualToString:mdeviceToken] && ![key isEqualToString:mpushToken]) {
            [defs removeObjectForKey:key];
        }
    }
    
    //    _totalRows = [NSMutableArray new];
    CBLManager *manager = [CBLManager sharedInstance];
    CBLManager* bgMgr = [manager copy];
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSError *error;
        CBLDatabase* bgDB = [bgMgr databaseNamed:@"couchbasenew" error: &error];
        CBLQuery *query = [bgDB createAllDocumentsQuery];
        query.allDocsMode = kCBLAllDocs;
        query.descending = YES;
        
        result = [query run:&error];
        for (NSInteger count = 0; count < result.count; count++) {
            
            CBLDocument* document = [bgDB documentWithID:[result rowAtIndex:count].documentID];
            NSString *groupId = [NSString stringWithFormat:@"%@",[document.properties objectForKey:@"groupID"]];
            [[FIRMessaging messaging] unsubscribeFromTopic:groupId];
            NSError* error;
            [document deleteDocument:&error];
            
            //                [bgDB deleteLocalDocumentWithID:[result rowAtIndex:count].documentID error:nil];
        }
    });
}


-(void)actionForeditProfile {
    EditProfileViewController *postsVc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileStoryboard"];
    postsVc.necessarytocallEditProfile = YES;
    [self.navigationController pushViewController:postsVc animated:YES];
}

/*-------------------------------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*-------------------------------------------------------*/

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
        return;
    }
    
}



@end

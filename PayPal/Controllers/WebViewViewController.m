//
//  WebViewViewController.m
//
//  Created by Imma Web Pvt Ltd on 10/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

//#import "WebViewViewController.h"
//
//@interface WebViewViewController () 
//@property(strong,nonatomic) WKWebView *webView;
//@property (strong,nonatomic) UIActivityIndicatorView  *activityIndicator;
//@end
//
//@implementation WebViewViewController
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    
//    self.navigationItem.hidesBackButton = YES;
//    
//    self.navigationController.navigationBar.translucent = NO;
//    
//    if(self.navTitle.length >0)
//        self.navigationItem.title = self.navTitle;
//    else
//        self.navigationItem.title = @"Paypal Login";
//    
//
//    
//    self.activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    self.activityIndicator.frame = CGRectMake(self.view.frame.size.width/2 -12.5,self.view.frame.size.height/2 - 50,25,25);
//    
//    [self.activityIndicator startAnimating];
//    
//    if(!self.webAppdress.length)
//    {
//    self.webAppdress = @"https://www.paypal.com/paypalme/grab";
//    }
//    
//    NSURL *url = [NSURL URLWithString:self.webAppdress];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    
//    self.webView.navigationDelegate = self ;
//    _webView = [[WKWebView alloc] initWithFrame:self.view.frame];
//    [_webView loadRequest:request];
//    _webView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
//    
//  
//    [self.view addSubview:_webView];
//    [self.view insertSubview:self.activityIndicator aboveSubview :self.webView];
//    
//     [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:NULL];
//    
//
//    
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//-(void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:YES];
//    [self createNavigatLeftButton];
//}
//
//- (void)dealloc {
//    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
//    
//    // if you have set either WKWebView delegate also set these to nil here
//    [self.webView setNavigationDelegate:nil];
//    [self.webView setUIDelegate:nil];
//}
//
//-(void)viewDidDisappear:(BOOL)animated
//{
//    [super viewDidDisappear:YES];
//    [self.view endEditing:YES];
//}
//
//- (void)createNavigatLeftButton
//{
//    
//    UIButton *navCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [navCancelButton rotateButton];
//    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]forState:UIControlStateNormal];
//    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]forState:UIControlStateSelected];
//    [navCancelButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//    [navCancelButton setFrame:CGRectMake(0,0,40,40)];
//    // Create a container bar button
//    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
//    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
//    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
//    negativeSpacer.width = -14;// it was -6 in iOS 6
//    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
//}
//
//// hiding navigation bar and changing to previous controloler.
//
//- (void)backButtonClicked
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}
//
//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
//{
//    if ([keyPath isEqualToString:@"estimatedProgress"] && object == self.webView) {
//        self.activityIndicator.hidden = self.webView.estimatedProgress == 1 ;
//    }
//}
//@end

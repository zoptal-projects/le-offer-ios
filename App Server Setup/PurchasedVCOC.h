//
//  PurchasedVCOC.h
//  Cellable
//
//  Created by Anish Mac Mini on 30/06/20.
//  Copyright © 2020 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileViewController.h"
#import "UserProfileCollectionViewCell.h"
#import "PostsCollectionViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface PurchasedVCOC : UIViewController
{
     UserProfileCollectionViewCell *collectionViewCell;
}
@property (weak, nonatomic) IBOutlet UICollectionView *purchasedCollectionView;
@property(nonatomic,retain)NSMutableArray *purchasePostData;
@property (nonatomic,strong)ProfileViewController *profileVC;
@property (nonatomic,strong)Profile *profileObj;
@property (strong, nonatomic)ProductDetails *product ;
@end

NS_ASSUME_NONNULL_END

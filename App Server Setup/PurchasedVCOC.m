//
//  PurchasedVCOC.m
//  Cellable
//
//  Created by Anish Mac Mini on 30/06/20.
//  Copyright © 2020 Rahul Sharma. All rights reserved.
//

#import "PurchasedVCOC.h"
#import "SectionCollectionViewCell.h"
#import "EditProductViewController.h"
#import "ProductDetailsViewController.h"
#import "ExchangePostsTableViewCell.h"
#import "FrameSize.h"


@interface PurchasedVCOC () <WebServiceHandlerDelegate ,RFQuiltLayoutDelegate ,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation PurchasedVCOC

- (void)viewDidLoad
{
    [super viewDidLoad];
     _purchasePostData = [NSMutableArray new];
}
/**
 Create Navigation Bar Left Button method.
 */
- (void)createNavLeftButton {
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
    
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName  normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,25,40)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}
-(void)backButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"Purchase Order";
      [self createNavLeftButton];
    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
    [HomePI showPIOnView:self.view withMessage:@"Loading..."];
    NSDictionary *requestDict = @{
                                  mauthToken :[Helper userToken],
                                 // moffset : [NSNumber numberWithInteger:20],
                                //  mlimit : mLimitValue,
                                  };
    NSLog(@"requestDict=%@",requestDict);
    [WebServiceHandler RequestTypePostsPurchasedByUser:requestDict andDelegate:self];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
/**
 *  declaring numberOfItemsInSection
 *  @param collectionView declaring numberOfItemsInSection in collection view.
 *  @param section    here only one section.
 *  @return number of items in collection view here it is 100.
 */

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  _purchasePostData.count;
}

/**
 *  implementation of collection view cell
 *  @param collectionView collectionView has only image view
 *  @return implemented cell will return.
 */

//-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//  UserProfileCollectionViewCell *mCell = (UserProfileCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"purchaseCellIdentifier" forIndexPath:indexPath];
//  return mCell;
//}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"purchaseCellIdentifier" forIndexPath:indexPath];
    [collectionViewCell setProducts:_purchasePostData[indexPath.row]];

    return collectionViewCell;

}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (collectionView.tag)
    {
        case 0:
        {
            if(!self.profileVC.isMemberProfile)
            {
                EditProductViewController *editVC = [self.profileVC.storyboard instantiateViewControllerWithIdentifier:mEditItemStoryBoardID];
                editVC.product = self.profileObj.sellingPostData[indexPath.row] ;
                editVC.hidesBottomBarWhenPushed = YES;
                editVC.showInsights = YES ;
                [self.navigationController pushViewController:editVC animated:YES];
                
            }
            else
            {
                    UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[self.purchasedCollectionView cellForItemAtIndexPath:indexPath];
                    ProductDetailsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
                    ProductDetails *product = self.profileObj.sellingPostData[indexPath.row];
                    newView.imageFromHome  = cell.postedImagesOutlet.image ;
                    newView.postId = product.postId ;
                    newView.movetoRowNumber =indexPath.item;
                    newView.dataFromHomeScreen = YES;
                    newView.hidesBottomBarWhenPushed = YES ;
                    newView.product = self.profileObj.sellingPostData[indexPath.row];
                    newView.indexPath = indexPath ;
                   [self.navigationController pushViewController:newView animated:YES];
                
            }
        }
            break;
        case 1:
        {
            if(!self.profileVC.isMemberProfile)
            {
                UserProfileCollectionViewCell *cell = (UserProfileCollectionViewCell *)[self.purchasedCollectionView cellForItemAtIndexPath:indexPath];
                
                ProductDetailsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
                ProductDetails *product = self.purchasePostData[indexPath.row];
                newView.imageFromHome  = cell.postedImagesOutlet.image ;
                newView.postId = product.postId ;
                newView.movetoRowNumber =indexPath.item;
                newView.dataFromHomeScreen = YES;
                newView.pathCheck = @"HiddenYes";
                newView.hidesBottomBarWhenPushed = YES ;
                newView.product = self.purchasePostData[indexPath.row];
                newView.indexPath = indexPath ;
                [self.navigationController pushViewController:newView animated:YES];
                
            }
        }
            break;
    }
}
#pragma mark –
#pragma mark – RFQuiltLayoutDelegate

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
         // return CGSizeMake(4, 8);
    return [Helper blockSizeOfProduct:_purchasePostData[indexPath.row] view:self.view forHome:YES];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetsForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return UIEdgeInsetsMake (5,5,0,0);
}

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];

    if (error)
    {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    NSLog(@"responseDict=%@",responseDict);
    switch (requestType)
    {
        case RequestTypePostsPurchaseddByUser:
        {
            switch ([responseDict[@"code"] integerValue])
            {
                case 200:
                {
                [self.purchasePostData removeAllObjects];
                self.purchasePostData = [ProductDetails arrayOfProducts:responseDict[@"data"]];
                [self handlingResponseForPurchasedPosts:response];
                }
                break;
                case 204:
                {
                [self.purchasePostData removeAllObjects];
                 [Helper showAlertWithTitle:nil Message:responseDict[@"message"] viewController:self];
                }
                break;
            
                default :
                    break;
            }
        }
       default:
            break;
    }
 }
    
-(void)handlingResponseForPurchasedPosts :(NSDictionary *)response
{
    [self reloadTableView];
    [_purchasedCollectionView setDataSource:self];
    [_purchasedCollectionView setDelegate:self];
    [_purchasedCollectionView reloadData];
    
//     UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
//    _purchasedCollectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
//      [_purchasedCollectionView setDataSource:self];
//      [_purchasedCollectionView setDelegate:self];
//
//
//    [_purchasedCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"purchaseCellIdentifier"];
    
   
           dispatch_async(dispatch_get_main_queue(), ^{
              // [self reloadTableView];
           });
}
-(void)reloadTableView
{
    RFQuiltLayout* PurchaseLayout = [[RFQuiltLayout alloc]init];
    PurchaseLayout.direction = UICollectionViewScrollDirectionVertical;
    if(self.purchasedCollectionView.frame.size.width == 375)
        PurchaseLayout.blockPixels = CGSizeMake( 37,31);
    else if(self.purchasedCollectionView.frame.size.width == 414)
    {
        PurchaseLayout.blockPixels = CGSizeMake( 102,31);
    }
    
    else
    PurchaseLayout.blockPixels = CGSizeMake( 79,31);
    self.purchasedCollectionView.collectionViewLayout = PurchaseLayout;
    PurchaseLayout.delegate=self;
    
}
    /*-----------------------------------------------*/

@end

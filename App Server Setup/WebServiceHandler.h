
//  WebServiceHandler.h
//
//  Created by 3Embed on 03/12/15.
//  Copyright (c) 2014 3Embed. All rights reserved.


#import <Foundation/Foundation.h>
#import "WebServiceConstants.h"
#import "AFNetworking/AFNetworking.h"

/*
 @protocol WebServiceHandlerDelegate
 @abstract protocol to be iplemented by the calling class to get the response
*/
@protocol WebServiceHandlerDelegate <NSObject>

/*
 @method didFinishLoadingRequest
 @abstract Method called when web service request is complete
 @param requestType - Request Type for this request
 @param response - id Response of this request. Can be nil in case of an error.
 @param error - NSError - error object in case of any error. Nil in case of success.
*/
@required
- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error;
@optional
-(void)internetIsNotAvailable:(RequestType )requsetType;
@end

/*
 @class WebServiceHandler
 @abstract Class to handle request and response of web services
*/

@interface WebServiceHandler : NSObject

/*
 @method getPhotosWithDelegate
 @abstract Method to get photos from the service
 @param delegate - calling class object to recieve the response. Response is delivered WebServiceHandlerDelegate protocol method didFinishLoadingRequest
 @result void
*/


/* ---------------------------
#pragma mark - AUTHENTICATION -
----------------------------*/

+ (void) logId:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - Sign Up Service
+ (void) newRegistration:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) newRegistration2:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - Add Address
+ (void) newAddAddress:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


#pragma mark - Log Device Service
+ (void) logUserDevice:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) logGuestUserDevice:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+(void)session:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - Email/Phone/UserNameCheck
+ (void) emailCheck:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) phoneNumberCheck:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) userNameCheck:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

#pragma mark - Genrate OTP Service
+ (void) generateOtp:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void) generateOtpForEditNumber:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void)resetPassword:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void) Logout:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


/* ---------------------------
 #pragma mark - SYNC -
 ----------------------------*/

+ (void) faceBookContactSync:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) phoneContactSync:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


/* ---------------------------
 #pragma mark - POST LISTINGS -
 ----------------------------*/
+ (void) postImageOrVideo:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - FOLLOW UNFOLLOW -
 ----------------------------*/
+ (void) follow:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) unFollow:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - FOLLOWERS/ FOLLOWINGS -
 ----------------------------*/
+ (void) getFollowingList:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getFollowersList:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getMemberFollowingList:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getMemberFollowersList:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - CLOUDINARY
 ----------------------------*/
+ (void) getCloudinaryCredintials:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - SOCIAL NEWSFEED
 ----------------------------*/
+ (void) getPostsInHOmeScreen:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - COMMENTS/ REVIEWS
 ----------------------------*/
+ (void) commentOnPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void) deleteComment:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void) getCommentsOnPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


/* ---------------------------
 #pragma mark - PROFILE
 ----------------------------*/
/**
 Get member  profile details.
 */
+ (void) getProfileDetailsOfUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/**
 Get profile Details of Member.
 */
+ (void) getProfileDetailsOfMember:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void) getMemberPosts:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;//
+ (void) getUserPosts:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getMemberPostsForGuest:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;//
+ (void) getProfileDetailsOfMemberForGuest:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;//
+ (void) getUserProfileDetails:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypePostsLikedByUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getExchangedPosts:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void) RequestTypePostsPurchasedByUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void) RequestTypePostsIsRecevieProduct:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void) RequestTypePostsShippingUpdate:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - EDIT PROFILE
 ----------------------------*/
+ (void) RequestTypeSavingProfile:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypeEditProfile:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypeEmailCheckEditProfile:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypePhoneNumberCheckEditProfile:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) RequestTypeupdatePhoneNumber:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) sendEmailLink:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


/* ---------------------------
 #pragma mark - EDIT POST
 ----------------------------*/
+ (void) getStripePost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) editPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) deletePost:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - HOME / EXPLORE
 ----------------------------*/
+ (void) getExplorePosts:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getExplorePostsForGuest:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - POST DETAILS
 ----------------------------*/
+ (void) getPostDetails:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - SEARCH POST/ PEOPLE
 ----------------------------*/
#pragma mark - search People
+ (void) getSearchPeople:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getSearchPeopleForGuestUser:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
#pragma mark - Search Posts

+ (void) getSearchForPosts:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


/* ---------------------------
 #pragma mark - RATE USER
 ----------------------------*/

+ (void)rateForSeller:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - LIKE/ UNLIKE
 ----------------------------*/

+ (void) likeAPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) unlikeAPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) getAllLikesOnPost:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - DISCOVER PEOPLE
 ----------------------------*/
+ (void) discoverPeople:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void) hideFromDiscovery:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark -ACTIVITY
 ----------------------------*/
+ (void) followingActivities:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) ownActivities:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
// get notification count  unseenNotificationCount

+ (void) getUsernNotificationCount:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - CATEGORY
 ----------------------------*/

+ (void) getCategories:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

// SUBCATEGORY
+ (void) getSubCategory:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - POST BY ID
 ----------------------------*/
+ (void)getPostsByusers:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)getPostsForGuests:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - PRODUCT FILTER
 ----------------------------*/
+ (void)searchProductsByFilters:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


/* ---------------------------
 #pragma mark - SOLD
 ----------------------------*/
+ (void) RequestTypeMarkAsSold:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)soldSomeWhere:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - SELLING AGAIN
 ----------------------------*/
+ (void) markAsSelling:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


/* ---------------------------
 #pragma mark - REPORT
 ----------------------------*/
#pragma mark - Report

+(void)getReportReasonForUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+(void)getReportReasonForPost:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)sendreportPost:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void)sendreportUser:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


/* ---------------------------
 #pragma mark - ACCEPTED OFFERS
 ----------------------------*/
+ (void)acceptedOffers:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - PAYPAL
 ----------------------------*/
#pragma mark - verifications
#pragma mark - PayPal Url

+ (void) savePayPalLink:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


/* ---------------------------
 #pragma mark - VERIFY
 ----------------------------*/
#pragma mark - verifications
+ (void) verifyWithFacebook:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) verifyWithGooglePlus:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

+ (void) stripeDisconnect:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
/* ---------------------------
 #pragma mark - ADS CAMPAIGN
 ----------------------------*/
#pragma mark - Run Campaign
+ (void) userCampaign:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+ (void) runCampaign:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


/* ---------------------------
 #pragma mark - INSIGHTS
 ----------------------------*/
#pragma mark - Insights
+(void)getInsightsOfProduct:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+(void)getMonthInsightsOfProduct:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
+(void)getInsightsCityWise:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


/* ---------------------------
 #pragma mark - In-APP Purchase
 ----------------------------*/
#pragma mark - Promotions Plans
+(void)PurchasePromoPlans:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - Exchange
 ----------------------------*/
+(void)getProductSuggesstion:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

/* ---------------------------
 #pragma mark - ChatMessage Suggestions
 ----------------------------*/

+ (void) getChatMessageSuggestion:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
@end

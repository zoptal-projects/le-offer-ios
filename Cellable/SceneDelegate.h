//
//  SceneDelegate.h
//  Cellable
//
//  Created by Anish Mac Mini on 04/08/20.
//  Copyright © 2020 Anish Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>
@property (strong, nonatomic) UIWindow * window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) UIView *internetConnectionErrorView;
@property(nonatomic,assign)Float32 lat;
@property(nonatomic,assign)Float32 log;
@end


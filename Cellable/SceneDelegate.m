#import "SceneDelegate.h"
#import "PGTabBar.h"
#import "UIImageView+WebCache.h"
#import "WebServiceHandler.h"
#import "TinderGenericUtility.h"
#import "WebServiceConstants.h"
#import "AFNetworkReachabilityManager.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "WebViewForDetailsVc.h"
#import "Helper.h"
#import "AdsCampaignView.h"
#import "iRate.h"
#import "ChatSuggestion.h"
#import "SingletonData.h"
#import "Cellable-Swift.h"

#define storyBoard [UIStoryboard storyboardWithName:@"Main" bundle:nil]

@import Stripe;
@import UserNotifications;
@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;

@class MQTT;
@class MQTTChatManager;


@interface SceneDelegate () <FIRMessagingDelegate>
@property MQTTChatManager *mqttChatManager;
@end

@implementation SceneDelegate


- (void)scene:(UIScene *)scene willConnectToSession:(UISceneSession *)session options:(UISceneConnectionOptions *)connectionOptions
{
    //[Stripe setDefaultPublishableKey:@"pk_test_jBYzjPBSS73HeuBIKzcFSXz3004Nv2Sp1W"];
    [Stripe setDefaultPublishableKey:@"pk_live_LUnXQzvwDZXEUdyaEsDteOmE00DxCm2Q3k"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openAdsCampaignWithData:) name:mTriggerCampaign object:nil];
       NSError* configureError;
    //   [[GGLContext sharedInstance] configureWithError: &configureError];
       NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
       self.mqttChatManager = [MQTTChatManager sharedInstance];
       [[AFNetworkReachabilityManager sharedManager] startMonitoring];
//       [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:connectionOptions];
       if([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
       {
           UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
           center.delegate = self;
           [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
               if( !error ){
                   dispatch_async(dispatch_get_main_queue(), ^{
                   [[UIApplication sharedApplication] registerForRemoteNotifications];
                       });
               }
           }];
       }
       else {
           [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
           [[UIApplication sharedApplication] registerForRemoteNotifications];
       }
       
       
       [GMSServices provideAPIKey:mGoogleAPIKey];
      // [FIRApp configure];
       [FIRMessaging messaging].delegate = self;
      
       
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
       
       UIViewController* rootController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"TabBarStoryboardID"];
       self.window.rootViewController = rootController;
       
       [self requestForCloundinaryDetails];
       [NSTimer scheduledTimerWithTimeInterval:30.0*60
                                        target:self
                                      selector:@selector(requestForCloundinaryDetails)
                                      userInfo:nil
                                       repeats:NO];
       
//      NSString *token = [[FIRInstanceID instanceID] token];
//      [[NSUserDefaults standardUserDefaults] setObject:token forKey:mdeviceToken];
//       [[NSUserDefaults standardUserDefaults] synchronize];
       //[self handlePushNotificationWithLaunchOption:connectionOptions];
       [self setNavigationBar];
       [self addNoInternetConnectionView];
       _internetConnectionErrorView.hidden = YES;
       
       [[AFNetworkReachabilityManager sharedManager] startMonitoring];
       
       [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
           if (status == AFNetworkReachabilityStatusNotReachable) {
               [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isNetworkAvailable"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               _internetConnectionErrorView.hidden = NO;
               [_window bringSubviewToFront:_internetConnectionErrorView];
               
               NSDictionary *dict = @{
                                      @"message":@"NO"
                                      };
               [[NSNotificationCenter defaultCenter] postNotificationName:@"observeNetworkStatus" object:nil userInfo:dict];
           }
           else {
               MQTT *mqttModel = [MQTT sharedInstance];
               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                   [mqttModel createConnection];
               });
               _internetConnectionErrorView.hidden = YES;
               [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isNetworkAvailable"];
               [[NSUserDefaults standardUserDefaults] synchronize];
               NSDictionary *dict = @{
                                      @"message":@"YES"
                                      };
               [[NSNotificationCenter defaultCenter] postNotificationName:@"observeNetworkStatus" object:nil userInfo:dict];        }
       }];
       [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
       
       /*
           adsTimer = [NSTimer scheduledTimerWithTimeInterval:240 target: self
                                                     selector: @selector(callAfterSixtySeconds:) userInfo: nil repeats: YES];
           self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:mGoogleInterstitialAdsUnitId];
           self.interstitial.delegate = self;
           GADRequest *request = [GADRequest request];
           [self.interstitial loadRequest:request];

       */
       [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
       [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
       [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    
}

-(void)requestForCloundinaryDetails{
    [WebServiceHandler getCloudinaryCredintials:@{@"":@""} andDelegate:self];
    [WebServiceHandler getChatMessageSuggestion:@{mauthToken:[Helper userToken]} andDelegate:self];
}
#pragma mark - adding internet connection error view

- (void)addNoInternetConnectionView
{
    _internetConnectionErrorView = [[UIView alloc] initWithFrame:CGRectMake(0, 64,[UIScreen mainScreen].bounds.size.width, 20)];
    UILabel *internetConectionErrorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, 20)];
    internetConectionErrorLabel.text = @"No internet connection";
    //    [Helper setToLabel:internetConectionErrorLabel Text:@"No internet connection" WithFont:LATO_BOLD FSize:14.0 Color:[UIColor whiteColor]];
    internetConectionErrorLabel.textAlignment = NSTextAlignmentCenter;
    [_internetConnectionErrorView addSubview:internetConectionErrorLabel];
    _internetConnectionErrorView.backgroundColor = [UIColor redColor];
    [self.window addSubview:_internetConnectionErrorView];
}
- (void)tokenRefreshNotification:(NSNotification *)notification
{
//    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
//    NSLog(@"InstanceID token: %@", refreshedToken);
//    [[NSUserDefaults standardUserDefaults] setObject:refreshedToken forKey:mdeviceToken];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    [self connectToFcm];
}
- (void)setNavigationBar
{
    if (@available(iOS 11.0, *)) {
        
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
        [[UITableView appearance] setContentInsetAdjustmentBehavior: UIScrollViewContentInsetAdjustmentNever] ;
    } else {
        // Fallback on earlier versions
        
    }
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setShadowImage:nil];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:17.0/255.0f green:42.0/255.0f blue:70.0/255.0f alpha:1.0]];
    
    
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           mBaseColorNavTItle, NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:RobotoMedium size:17.0], NSFontAttributeName, nil]];
    
    
    
    
    
}
-(void)handlePushNotificationWithLaunchOption:(NSDictionary*)launchOptions{
    NSDictionary *receivedDataFromPush = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    NSData *data = [receivedDataFromPush[@"body"] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonResponse = [[NSDictionary alloc]init];
    if(data) {
        jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:Nil];
    }
    if([jsonResponse[@"type"]integerValue] == 73) {
        [[NSUserDefaults standardUserDefaults] setObject:jsonResponse forKey:mAdsCampaignKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return ;
    } else if (jsonResponse[@"receiverID"]) {
        [self openChatController:jsonResponse];
    }
    else if(receivedDataFromPush) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"openActivity"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
-(void)openChatController:(NSDictionary *)chatData {
    // redirecting it to the next controller with the chat object data.
    // check for empty object.

    UIViewController *tabController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    if ([tabController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tbControler = (UITabBarController *)tabController;
        dispatch_async(dispatch_get_main_queue(), ^{
                [[[tbControler viewControllers] objectAtIndex:3] popToRootViewControllerAnimated: NO];
                [tbControler setSelectedIndex:3];
        });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OfferInitiated" object:nil userInfo:@{@"chatObj":chatData}];
        });
    }
}
- (void)sceneDidDisconnect:(UIScene *)scene {
    // Called as the scene is being released by the system.
    // This occurs shortly after the scene enters the background, or when its session is discarded.
    // Release any resources associated with this scene that can be re-created the next time the scene connects.
    // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
}


- (void)sceneDidBecomeActive:(UIScene *)scene {
    // Called when the scene has moved from an inactive state to an active state.
    // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
}


- (void)sceneWillResignActive:(UIScene *)scene {
    // Called when the scene will move from an active state to an inactive state.
    // This may occur due to temporary interruptions (ex. an incoming phone call).
}


- (void)sceneWillEnterForeground:(UIScene *)scene {
    // Called as the scene transitions from the background to the foreground.
    // Use this method to undo the changes made on entering the background.
}


- (void)sceneDidEnterBackground:(UIScene *)scene {
    // Called as the scene transitions from the foreground to the background.
    // Use this method to save data, release shared resources, and store enough scene-specific state information
    // to restore the scene back to its current state.
}


@end
